# Tools

This folder contains various tools to assist in the creation of the Accordance modules.

## Spelling dictionary tools

Custom spelling dictionaries for the various modules are included in the repository.

### Updating LibreOffice spelling dictionaries from repository

The spelling dictionaries need to be copied to your LibreOffice installation when first editing a module, or after pulling the latest changes from the remote repository in which the spelling dictionaries have been updated. In either of these scenarios, you need to copy the changed files from the local repository so they will be used the next time you edit the modules. Run the following command to update your LibreOffice spelling dictionaries with the latest copies in the local repository:

```sh
$ ./tools/copy_spelling_dictionary_to_lo
```

Restart LibreOffice (not just Writer!) for the updated spelling dictionaries to be recognized.

In addition, if this is the first time you are editing the modules, you need to enable the spelling dictionaries within LibreOffice. From LibreOffice Writer, perform the following steps:

1. From the main menu, select **Tools > Spelling...**.
1. Click **Options...**.
1. Set the check box for the entry **NIV (1984)** (only enable when checking the spelling of the text).
    1. Click **Edit...**.
    1. Change the **Language** field to **[All]**.
    1. Click **Close**.
1. Set the check box for the entry **NIV (1984) Front-Back Matter** (only enable when checking the spelling of the front/back matter).
    1. Click **Edit...**.
    1. Change the **Language** field to **[All]**.
    1. Click **Close**.
1. Set the check box for the entry **NIV (1984) Notes** (only enable when checking the spelling of the notes).
    1. Click **Edit...**.
    1. Change the **Language** field to **[All]**.
    1. Click **Close**.
1. Unset the check box for all other entries.
1. Click **OK**.
1. Click **Close**.

### Updating repository spelling dictionaries from LibreOffice

If during the course of editing the modules, you update your LibreOffice spelling dictionaries, you need to copy the changed files to the local repository so they will be included in the commit containing your module changes. Run the following command to update your local repository spelling dictionaries with the latest copies in LibreOffice:

```sh
$ ./tools/copy_spelling_dictionary_from_lo
```
