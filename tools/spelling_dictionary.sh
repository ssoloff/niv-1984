#!/usr/bin/env bash
#
# Common definitions for working with the module spelling dictionaries.
#

# Path to LibreOffice spelling dictionaries.
declare -r LO_FRONT_BACK_MATTER_DICTIONARY_FILE=~/.config/libreoffice/4/user/wordbook/NIV\ \(1984\)\ Front-Back\ Matter.dic
declare -r LO_NOTES_DICTIONARY_FILE=~/.config/libreoffice/4/user/wordbook/NIV\ \(1984\)\ Notes.dic
declare -r LO_TEXT_DICTIONARY_FILE=~/.config/libreoffice/4/user/wordbook/NIV\ \(1984\).dic

# Path to repository spelling dictionaries (relative to repository root).
declare -r REPO_FRONT_BACK_MATTER_DICTIONARY_FILE=src/front-back-matter.dic
declare -r REPO_NOTES_DICTIONARY_FILE=src/notes.dic
declare -r REPO_TEXT_DICTIONARY_FILE=src/text.dic

# Copies the spelling dictionaries from the current user's LibreOffice configuration to this repository.
function copy_spelling_dictionary_from_lo() {
  cp "${LO_FRONT_BACK_MATTER_DICTIONARY_FILE}" "${REPO_FRONT_BACK_MATTER_DICTIONARY_FILE}"
  cp "${LO_NOTES_DICTIONARY_FILE}" "${REPO_NOTES_DICTIONARY_FILE}"
  cp "${LO_TEXT_DICTIONARY_FILE}" "${REPO_TEXT_DICTIONARY_FILE}"
}

# Copies the spelling dictionaries from this repository to the current user's LibreOffice configuration.
function copy_spelling_dictionary_to_lo() {
  cp "${REPO_FRONT_BACK_MATTER_DICTIONARY_FILE}" "${LO_FRONT_BACK_MATTER_DICTIONARY_FILE}"
  cp "${REPO_NOTES_DICTIONARY_FILE}" "${LO_NOTES_DICTIONARY_FILE}"
  cp "${REPO_TEXT_DICTIONARY_FILE}" "${LO_TEXT_DICTIONARY_FILE}"
}
