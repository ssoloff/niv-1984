# Accordance modules for The Holy Bible, New International Version (1984)

## Installing the latest release

1. Download the **Distribution** asset from the [latest release](https://gitlab.com/ssoloff/niv-1984/-/releases/permalink/latest).
1. Extract the contents of the archive downloaded in the previous step to a folder of your choice.
1. Follow the installation instructions in the _README.md_ file located at the root of the folder created in the previous step.

## Building from source

Instructions for building the modules from the source files can be found [here](build/README.md#build-process).

## License

### Bible text and notes

Copyright © 1973, 1978, 1984 by International Bible Society

All rights reserved.

### Everything else

Copyright © 2022-2025 Steven Soloff

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see [here](https://www.gnu.org/licenses/).

## Disclaimer

This repository exists for the purpose of the owner's personal use of the NIV (1984) within Accordance. Please do not download a release distribution unless 1) you own a digital version of the NIV or NIVUK (1984), and 2) you own the NIV (2011) module for Accordance, per the publisher's guidelines described [here](https://forums.accordancebible.com/topic/18674-niv-1984/) ([archive](https://archive.ph/zXZcP)).
