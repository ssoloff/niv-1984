<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <!-- Title page -->
        <h1>The HOLY BIBLE</h1>
        <p align="center">NEW INTERNATIONAL VERSION</p>
        <p align="center"><em>Containing The Old Testament and The New Testament</em></p>
        <p align="center">INTERNATIONAL BIBLE SOCIETY<br>
            COLORADO SPRINGS, COLORADO</p>

        <!-- Copyright page -->
        <p>This Bible is a ministry of the International Bible Society. It has been provided at less than cost to encourage personal faith in Jesus Christ. The generous contributions of Bible-believing Christians make this ministry possible.</p>
        <p>In addition to translating and publishing New International Version Scriptures such as the one you hold in your hands, the Bible Society’s evangelistic ministry spans the globe. It includes such varied activities as translation work in over 350 languages around the world, providing Scriptures for evangelical missions and ministry organizations in dozens of countries each year, and meeting the need for Scriptures in prisons, among marine workers and in other evangelistic outreach efforts in the United States. The Bible Society is dedicated to bringing people everywhere to faith in Jesus Christ by providing God’s Holy Word to all people who truly want it, in a language they can understand. For a catalog of other available Scriptures write: International Bible Society, P.O. Box 62970, Colorado Springs, CO 80962-2970.</p>
        <p align="center">The Holy Bible, New International Version<br>
            Copyright © 1973, 1978, 1984 by International Bible Society</p>
        <p>The NIV text may be quoted and/or reprinted up to and inclusive of one thousand (1,000) verses without express written permission of the publisher, providing the verses quoted do not amount to more than 50% of a complete book of the Bible nor do the verses quoted comprise more than 50% of the total work in which they are quoted.</p>
        <p>Notice of copyright must appear on the title or copyright page of the work as follows:</p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;Scripture quotations taken from the HOLY BIBLE, NEW INTERNATIONAL VERSION. Copyright © 1973, 1978, 1984 by International Bible Society.</p>
        <p>When quotations from the NIV are used in non-saleable media, such as church bulletins, orders of service, posters, transparencies or similar media, a complete copyright notice is not required but the initials (NIV) must appear at the end of each quotation.</p>
        <p>Quotations and/or reprints in excess of one thousand (1,000) verses, or other permission requests, must be directed to and approved in writing by International Bible Society.</p>
        <p align="center">All Rights Reserved</p>
        <p align="center">INTERNATIONAL BIBLE SOCIETY<br>
            P.O. BOX 62970, COLORADO SPRINGS, CO 80962-2970</p>
        <p align="center">Printed in the United States of America</p>
        <p>RRD (H) 5-89</p>
        <p>Eng. Bible NIV63-00111/00122/00123/00129<br>
            IBS89015000/025000/025000/010000</p>

        <!-- Preface -->
        <h1>Preface</h1>
        <p>The New International Version is a completely new translation of the Holy Bible made by over a hundred scholars working directly from the best available Hebrew, Aramaic and Greek texts. It had its beginning in 1965 when, after several years of exploratory study by committees from the Christian Reformed Church and the National Association of Evangelicals, a group of scholars met at Palos Heights, Illinois, and concurred in the need for a new translation of the Bible in contemporary English. This group, though not made up of official church representatives, was transdenominational. Its conclusion was endorsed by a large number of leaders from many denominations who met in Chicago in 1966.</p>
        <p>Responsibility for the new version was delegated by the Palos Heights group to a self-governing body of fifteen, the Committee on Bible Translation, composed for the most part of biblical scholars from colleges, universities and seminaries. In 1967 the New York Bible Society (now the International Bible Society) generously undertook the financial sponsorship of the project—a sponsorship that made it possible to enlist the help of many distinguished scholars. The fact that participants from the United States, Great Britain, Canada, Australia and New Zealand worked together gave the project its international scope. That they were from many denominations—including Anglican, Assemblies of God, Baptist, Brethren, Christian Reformed, Church of Christ, Evangelical Free, Lutheran, Mennonite, Methodist, Nazarene, Presbyterian, Wesleyan and other churches—helped to safeguard the translation from sectarian bias.</p>
        <p>How it was made helps to give the New International Version its distinctiveness. The translation of each book was assigned to a team of scholars. Next, one of the Intermediate Editorial Committees revised the initial translation, with constant reference to the Hebrew, Aramaic or Greek. Their work then went to one of the General Editorial Committees, which checked it in detail and made another thorough revision. This revision in turn was carefully reviewed by the Committee on Bible Translation, which made further changes and then released the final version for publication. In this way the entire Bible underwent three revisions, during each of which the translation was examined for its faithfulness to the original languages and for its English style.</p>
        <p>All this involved many thousands of hours of research and discussion regarding the meaning of the texts and the precise way of putting them into English. It may well be that no other translation has been made by a more thorough process of review and revision from committee to committee than this one.</p>
        <p>From the beginning of the project, the Committee on Bible Translation held to certain goals for the New International Version: that it would be an accurate translation and one that would have clarity and literary quality and so prove suitable for public and private reading, teaching, preaching, memorizing and liturgical use. The Committee also sought to preserve some measure of continuity with the long tradition of translating the Scriptures into English.</p>
        <p>In working toward these goals, the translators were united in their commitment to the authority and infallibility of the Bible as God’s Word in written form. They believe that it contains the divine answer to the deepest needs of humanity, that it sheds unique light on our path in a dark world, and that it sets forth the way to our eternal well-being.</p>
        <p>The first concern of the translators has been the accuracy of the translation and its fidelity to the thought of the biblical writers. They have weighed the significance of the lexical and grammatical details of the Hebrew, Aramaic and Greek texts. At the same time, they have striven for more than a word-for-word translation. Because thought patterns and syntax differ from language to language, faithful communication of the meaning of the writers of the Bible demands frequent modifications in sentence structure and constant regard for the contextual meanings of words.</p>
        <p>A sensitive feeling for style does not always accompany scholarship. Accordingly the Committee on Bible Translation submitted the developing version to a number of stylistic consultants. Two of them read every book of both Old and New Testaments twice—once before and once after the last major revision—and made invaluable suggestions. Samples of the translation were tested for clarity and ease of reading by various kinds of people—young and old, highly educated and less well educated, ministers and laymen.</p>
        <p>Concern for clear and natural English—that the New International Version should be idiomatic but not idiosyncratic, contemporary but not dated—motivated the translators and consultants. At the same time, they tried to reflect the differing styles of the biblical writers. In view of the international use of English, the translators sought to avoid obvious Americanisms on the one hand and obvious Anglicisms on the other. A British edition reflects the comparatively few differences of significant idiom and of spelling.</p>
        <p>As for the traditional pronouns “thou,” “thee” and “thine” in reference to the Deity, the translators judged that to use these archaisms (along with the old verb forms such as “doest,” “wouldest” and “hadst”) would violate accuracy in translation. Neither Hebrew, Aramaic nor Greek uses special pronouns for the persons of the Godhead. A present-day translation is not enhanced by forms that in the time of the King James Version were used in everyday speech, whether referring to God or man.</p>
        <p>For the Old Testament the standard Hebrew text, the Masoretic Text as published in the latest editions of <em>Biblia Hebraica</em>, was used throughout. The Dead Sea Scrolls contain material bearing on an earlier stage of the Hebrew text. They were consulted, as were the Samaritan Pentateuch and the ancient scribal traditions relating to textual changes. Sometimes a variant Hebrew reading in the margin of the Masoretic Text was followed instead of the text itself. Such instances, being variants within the Masoretic tradition, are not specified by footnotes. In rare cases, words in the consonantal text were divided differently from the way they appear in the Masoretic Text. Footnotes indicate this. The translators also consulted the more important early versions—the Septuagint; Aquila, Symmachus and Theodotion; the Vulgate; the Syriac Peshitta; the Targums; and for the Psalms the <em>Juxta Hebraica</em> of Jerome. Readings from these versions were occasionally followed where the Masoretic Text seemed doubtful and where accepted principles of textual criticism showed that one or more of these textual witnesses appeared to provide the correct reading. Such instances are footnoted. Sometimes vowel letters and vowel signs did not, in the judgment of the translators, represent the correct vowels for the original consonantal text. Accordingly some words were read with a different set of vowels. These instances are usually not indicated by footnotes.</p>
        <p>The Greek text used in translating the New Testament was an eclectic one. No other piece of ancient literature has such an abundance of manuscript witnesses as does the New Testament. Where existing manuscripts differ, the translators made their choice of readings according to accepted principles of New Testament textual criticism. Footnotes call attention to places where there was uncertainty about what the original text was. The best current printed texts of the Greek New Testament were used.</p>
        <p>There is a sense in which the work of translation is never wholly finished. This applies to all great literature and uniquely so to the Bible. In 1973 the New Testament in the New International Version was published. Since then, suggestions for corrections and revisions have been received from various sources. The Committee on Bible Translation carefully considered the suggestions and adopted a number of them. These were incorporated in the first printing of the entire Bible in 1978. Additional revisions were made by the Committee on Bible Translation in 1983 and appear in printings after that date.</p>
        <p>As in other ancient documents, the precise meaning of the biblical texts is sometimes uncertain. This is more often the case with the Hebrew and Aramaic texts than with the Greek text. Although archaeological and linguistic discoveries in this century aid in understanding difficult passages, some uncertainties remain. The more significant of these have been called to the reader’s attention in the footnotes.</p>
        <p>In regard to the divine name <em>YHWH</em>, commonly referred to as the <em>Tetragrammaton</em>, the translators adopted the device used in most English versions of rendering that name as “LORD” in capital letters to distinguish it from <em>Adonai</em>, another Hebrew word rendered “Lord,” for which small letters are used. Wherever the two names stand together in the Old Testament as a compound name of God, they are rendered “Sovereign LORD.”</p>
        <p>Because for most readers today the phrases “the LORD of hosts” and “God of hosts” have little meaning, this version renders them “the LORD Almighty” and “God Almighty.” These renderings convey the sense of the Hebrew, namely, “he who is sovereign over all the ‘hosts’ (powers) in heaven and on earth, especially over the ‘hosts’ (armies) of Israel.” For readers unacquainted with Hebrew this does not make clear the distinction between <em>Sabaoth</em> (“hosts” or “Almighty”) and <em>Shaddai</em> (which can also be translated “Almighty”), but the latter occurs infrequently and is always footnoted. When <em>Adonai</em> and <em>YHWH Sabaoth</em> occur together, they are rendered “the Lord, the LORD Almighty.”</p>
        <p>As for other proper nouns, the familiar spellings of the King James Version are generally retained. Names traditionally spelled with “ch,” except where it is final, are usually spelled in this translation with “k” or “c,” since the biblical languages do not have the sound that “ch” frequently indicates in English—for example, in <em>chant</em>. For well-known names such as Zechariah, however, the traditional spelling has been retained. Variation in the spelling of names in the original languages has usually not been indicated. Where a person or place has two or more different names in the Hebrew, Aramaic or Greek texts, the more familiar one has generally been used, with footnotes where needed.</p>
        <p>To achieve clarity the translators sometimes supplied words not in the original texts but required by the context. If there was uncertainty about such material, it is enclosed in brackets. Also for the sake of clarity or style, nouns, including some proper nouns, are sometimes substituted for pronouns, and vice versa. And though the Hebrew writers often shifted back and forth between first, second and third personal pronouns without change of antecedent, this translation often makes them uniform, in accordance with English style and without the use of footnotes.</p>
        <p>Poetical passages are printed as poetry, that is, with indentation of lines and with separate stanzas. These are generally designed to reflect the structure of Hebrew poetry. This poetry is normally characterized by parallelism in balanced lines. Most of the poetry in the Bible is in the Old Testament, and scholars differ regarding the scansion of Hebrew lines. The translators determined the stanza divisions for the most part by analysis of the subject matter. The stanzas therefore serve as poetic paragraphs.</p>
        <p>As an aid to the reader, italicized sectional headings are inserted in most of the books. They are not to be regarded as part of the NIV text, are not for oral reading, and are not intended to dictate the interpretation of the sections they head.</p>
        <p>The footnotes in this version are of several kinds, most of which need no explanation. Those giving alternative translations begin with “Or” and generally introduce the alternative with the last word preceding it in the text, except when it is a single-word alternative; in poetry quoted in a footnote a slant mark indicates a line division. Footnotes introduced by “Or” do not have uniform significance. In some cases two possible translations were considered to have about equal validity. In other cases, though the translators were convinced that the translation in the text was correct, they judged that another interpretation was possible and of sufficient importance to be represented in a footnote.</p>
        <p>In the New Testament, footnotes that refer to uncertainty regarding the original text are introduced by “Some manuscripts” or similar expressions. In the Old Testament, evidence for the reading chosen is given first and evidence for the alternative is added after a semicolon (for example: Septuagint; Hebrew <em>father</em>). In such notes the term “Hebrew” refers to the Masoretic Text.</p>
        <p>It should be noted that minerals, flora and fauna, architectural details, articles of clothing and jewelry, musical instruments and other articles cannot always be identified with precision. Also measures of capacity in the biblical period are particularly uncertain (see the table of weights and measures following the text).</p>
        <p>Like all translations of the Bible, made as they are by imperfect man, this one undoubtedly falls short of its goals. Yet we are grateful to God for the extent to which he has enabled us to realize these goals and for the strength he has given us and our colleagues to complete our task. We offer this version of the Bible to him in whose name and for whose glory it has been made. We pray that it will lead many into a better understanding of the Holy Scriptures and a fuller knowledge of Jesus Christ the incarnate Word, of whom the Scriptures so faithfully testify.</p>
        <p align="right">The Committee on Bible Translation</p>
        <p>June 1978<br>
            (Revised August 1983)</p>
        <p align="right">Names of the translators and editors may be secured<br>
            from the International Bible Society,<br>
            translation sponsors of the New International Version,<br>
            P.O. Box 62970, Colorado Springs, Colorado, 80962-2970 U.S.A.</p>

        <!-- Table of weights and measures -->
        <h1>Table of Weights and Measures</h1>
        <p align="center"><strong>WEIGHTS</strong></p>
        <ul>
            <li>BIBLICAL UNIT | APPROXIMATE AMERICAN EQUIVALENT | APPROXIMATE METRIC EQUIVALENT</li>
            <li>talent <em>(60 minas)</em> | 75 pounds | 34 kilograms</li>
            <li>mina <em>(50 shekels)</em> | 1 1/4 pounds | 0.6 kilogram</li>
            <li>shekel <em>(2 bekas)</em> | 2/5 ounce | 11.5 grams</li>
            <li>pim <em>(2/3 shekel)</em> | 1/3 ounce | 7.6 grams</li>
            <li>beka <em>(10 gerahs)</em> | 1/5 ounce | 5.5 grams</li>
            <li>gerah | 1/50 ounce | 0.6 gram</li>
        </ul>
        <p align="center"><strong>LENGTH</strong></p>
        <ul>
            <li>BIBLICAL UNIT | APPROXIMATE AMERICAN EQUIVALENT | APPROXIMATE METRIC EQUIVALENT</li>
            <li>cubit | 18 inches | 0.5 meter</li>
            <li>span | 9 inches | 23 centimeters</li>
            <li>handbreadth | 3 inches | 8 centimeters</li>
        </ul>
        <p align="center"><strong>CAPACITY</strong></p>
        <p align="center"><strong>Dry Measure</strong></p>
        <ul>
            <li>BIBLICAL UNIT | APPROXIMATE AMERICAN EQUIVALENT | APPROXIMATE METRIC EQUIVALENT</li>
            <li>cor [homer] <em>(10 ephahs)</em> | 6 bushels | 220 liters</li>
            <li>lethek <em>(5 ephahs)</em> | 3 bushels | 110 liters</li>
            <li>ephah <em>(10 omers)</em> | 3/5 bushel | 22 liters</li>
            <li>seah <em>(1/3 ephah)</em> | 7 quarts | 7.3 liters</li>
            <li>omer <em>(1/10 ephah)</em> | 2 quarts | 2 liters</li>
            <li>cab <em>(1/18 ephah)</em> | 1 quart | 1 liter</li>
        </ul>
        <p align="center"><strong>Liquid Measure</strong></p>
        <ul>
            <li>BIBLICAL UNIT | APPROXIMATE AMERICAN EQUIVALENT | APPROXIMATE METRIC EQUIVALENT</li>
            <li>bath <em>(1 ephah)</em> | 6 gallons | 22 liters</li>
            <li>hin <em>(1/6 bath)</em> | 4 quarts | 4 liters</li>
            <li>log <em>(1/72 bath)</em> | 1/3 quart | 0.3 liter</li>
        </ul>
        <p>The figures of the table are calculated on the basis of a shekel equaling 11.5 grams, a cubit equaling 18 inches and an ephah equaling 22 liters. The quart referred to is either a dry quart (slightly larger than a liter) or a liquid quart (slightly smaller than a liter), whichever is applicable. The ton referred to in the footnotes is the American ton of 2,000 pounds.</p>
        <p>This table is based upon the best available information, but it is not intended to be mathematically precise; like the measurement equivalents in the footnotes, it merely gives approximate amounts and distances. Weights and measures differed somewhat at various times and places in the ancient world. There is uncertainty particularly about the ephah and the bath; further discoveries may give more light on these units of capacity.</p>
    </body>
</html>
