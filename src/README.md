# Accordance modules for The Holy Bible, New International Version (1984)

Version {{VERSION}} ({{BUILD_ID}}).

The Holy Bible, New International Version.\
Copyright © 1973, 1978, 1984 by International Bible Society.\
All rights reserved.

## Installation instructions for Accordance 13

### Importing the Bible into Accordance

1. Follow the instructions in the Accordance help system for importing a Bible.
1. When prompted for the text file to import, choose the _NIV (1984).txt_ file that is included in the distribution.
1. You will be prompted to enter the Bible name and other information:
    * For the new Bible name, Accordance should default to **NIV (1984)**, but you may change it to anything you like.
    * Click **Advanced** if the advanced controls are not already displayed.
    * For versification, select **NIV (2011) with Enhanced G/K Numbers**.
    * Click **OK**.
1. Wait for the Bible to be imported.
1. You will be prompted to enter information about the imported Bible:
    * For the full name of the text, enter the following:\
        **The Holy Bible, New International Version — NIV 1984**
    * For the other text information, enter the following:\
        **Copyright © 1973, 1978, 1984 by International Bible Society.**\
        **All rights reserved.**\
        \
        **Version {{VERSION}}**
    * Click **OK**.

### Importing the notes into Accordance

1. Follow the instructions in the Accordance help system for importing RTF notes.
1. When prompted to enter the imported user notes file name, you should enter **NIV (1984) Notes**, but you may change it to anything you like.
1. When prompted to select the folder to import, choose the _notes_ folder that is included in the distribution.
1. Wait for the user notes to be imported.

### Importing the front/back matter into Accordance

1. Follow the instructions in the Accordance help system for importing an HTML user tool.
1. You will be prompted for the user tool import method to use:
    * For import method, select **HTML**.
    * For import mode, select **Create a new User Tool**.
1. When prompted for the file to import, choose the _NIV (1984) Front-Back Matter.html_ file that is included in the distribution.
1. When prompted to enter the new user tool file name, Accordance should default to **NIV (1984) Front-Back Matter**, but you may change it to anything you like.
1. Wait for the user tool to be imported.
