# Gen 1:1
***The Beginning (1:1–2:3)***

# Gen 1:2
*ᵃ2* Or possibly *became*

# Gen 1:26
*ᵃ26* Hebrew; Syriac *all the wild animals*

# Gen 2:2
*ᵃ2* Or *ceased*; also in verse [3](vref:Gen 2:3)

# Gen 2:4
***Adam and Eve (2:4–25)***

# Gen 2:5
*ᵃ5* Or *land*; also in verse [6](vref:Gen 2:6)

# Gen 2:6
*ᵃ6* Or *mist*

# Gen 2:7
*ᵃ7* The Hebrew for *man (adam)* sounds like and may be related to the Hebrew for *ground (adamah)*; it is also the name *Adam* (see [Gen. 2:20](vref:Gen 2:20)).

# Gen 2:12
*ᵃ12* Or *good*; *pearls*

# Gen 2:13
*ᵃ13* Possibly southeast Mesopotamia

# Gen 2:20
*ᵃ20* Or *the man*

# Gen 2:21
*ᵃ21* Or *took part of the man’s side*

# Gen 2:22
*ᵃ22* Or *part*

# Gen 2:23
*ᵃ23* The Hebrew for *woman* sounds like the Hebrew for *man*.

# Gen 3:1
***The Fall of Man (3:1–24)***

# Gen 3:15
*ᵃ15* Or *seed*\
*ᵇ15* Or *strike*

# Gen 3:20
*ᵃ20* Or *The man*\
*ᵇ20* *Eve* probably means *living*.

# Gen 3:24
*ᵃ24* Or *placed in front*

# Gen 4:1
***Cain and Abel (4:1–26)***\
\
*ᵃ1* Or *The man*\
*ᵇ1* *Cain* sounds like the Hebrew for *brought forth* or *acquired*.\
*ᶜ1* Or *have acquired*

# Gen 4:8
*ᵃ8* Samaritan Pentateuch, Septuagint, Vulgate and Syriac; Masoretic Text does not have *“Let’s go out to the field.”*

# Gen 4:15
*ᵃ15* Septuagint, Vulgate and Syriac; Hebrew *Very well*

# Gen 4:16
*ᵃ16* *Nod* means *wandering* (see verses [12](vref:Gen 4:12) and [14](vref:Gen 4:14)).

# Gen 4:22
*ᵃ22* Or *who instructed all who work in*

# Gen 4:23
*ᵃ23* Or *I will kill*

# Gen 4:25
*ᵃ25* *Seth* probably means *granted*.

# Gen 4:26
*ᵃ26* Or *to proclaim*

# Gen 5:1
***From Adam to Noah (5:1–32)***

# Gen 5:2
*ᵃ2* Hebrew *adam*

# Gen 5:6
*ᵃ6* *Father* may mean *ancestor*; also in verses [7–26](vref:Gen 5:7-26).

# Gen 5:29
*ᵃ29* *Noah* sounds like the Hebrew for *comfort*.

# Gen 6:1
***The Flood (6:1–8:22)***

# Gen 6:3
*ᵃ3* Or *My spirit will not remain in*\
*ᵇ3* Or *corrupt*

# Gen 6:14
*ᵃ14* The meaning of the Hebrew for this word is uncertain.

# Gen 6:15
*ᵃ15* Hebrew *300 cubits long, 50 cubits wide and 30 cubits high* (about 140 meters long, 23 meters wide and 13.5 meters high)

# Gen 6:16
*ᵃ16* Or *Make an opening for light by finishing*\
*ᵇ16* Hebrew *a cubit* (about 0.5 meter)

# Gen 7:2
*ᵃ2* Or *seven pairs*; also in verse [3](vref:Gen 7:3)

# Gen 7:20
*ᵃ20* Hebrew *fifteen cubits* (about 6.9 meters)\
*ᵇ20* Or *rose more than twenty feet, and the mountains were covered*

# Gen 8:21
*ᵃ21* Or *man, for*

# Gen 9:1
***God’s Covenant With Noah (9:1–17)***

# Gen 9:18
***The Sons of Noah (9:18–29)***

# Gen 9:20
*ᵃ20* Or *soil, was the first*

# Gen 9:26
*ᵃ26* Or *be his slave*

# Gen 9:27
*ᵃ27* *Japheth* sounds like the Hebrew for *extend*.\
*ᵇ27* Or *their*

# Gen 10:1
***The Table of Nations (10:1)***

# Gen 10:2
***The Japhethites (10:2–5)***\
\
*ᵃ2* *Sons* may mean *descendants* or *successors* or *nations*; also in verses [3](vref:Gen 10:3), [4](vref:Gen 10:4), [6](vref:Gen 10:6), [7](vref:Gen 10:7), [20–23](vref:Gen 10:20-23), [29](vref:Gen 10:29) and [31](vref:Gen 10:31).

# Gen 10:4
*ᵃ4* Some manuscripts of the Masoretic Text and Samaritan Pentateuch (see also Septuagint and [1 Chron. 1:7](vref:1 Chr 1:7)); most manuscripts of the Masoretic Text *Dodanim*

# Gen 10:6
***The Hamites (10:6–20)***\
\
*ᵃ6* That is, Egypt; also in verse [13](vref:Gen 10:13)

# Gen 10:8
*ᵃ8* *Father* may mean *ancestor* or *predecessor* or *founder*; also in verses [13](vref:Gen 10:13), [15](vref:Gen 10:15), [24](vref:Gen 10:24) and [26](vref:Gen 10:26).

# Gen 10:10
*ᵃ10* Or *Erech and Akkad—all of them in*\
*ᵇ10* That is, Babylonia

# Gen 10:11
*ᵃ11* Or *Nineveh with its city squares*

# Gen 10:15
*ᵃ15* Or *of the Sidonians, the foremost*

# Gen 10:21
***The Semites (10:21–32)***\
\
*ᵃ21* Or *Shem, the older brother of*

# Gen 10:23
*ᵃ23* See Septuagint and [1 Chron. 1:17](vref:1 Chr 1:17); Hebrew *Mash*

# Gen 10:24
*ᵃ24* Hebrew; Septuagint *father of Cainan, and Cainan was the father of*

# Gen 10:25
*ᵃ25* *Peleg* means *division*.

# Gen 11:1
***The Tower of Babel (11:1–9)***

# Gen 11:2
*ᵃ2* Or *from the east*; or *in the east*\
*ᵇ2* That is, Babylonia

# Gen 11:9
*ᵃ9* That is, Babylon; *Babel* sounds like the Hebrew for *confused*.

# Gen 11:10
***From Shem to Abram (11:10–32)***\
\
*ᵃ10* *Father* may mean *ancestor*; also in verses [11–25](vref:Gen 11:11-25).

# Gen 11:13
*ᵃ12,13* Hebrew; Septuagint (see also [Luke 3:35](vref:Luke 3:35), [36](vref:Luke 3:36) and note at [Genesis 10:24](vref:Gen 10:24)) *35 years, he became the father of Cainan. ¹³And after he became the father of Cainan, Arphaxad lived 430 years and had other sons and daughters, and then he died. When Cainan had lived 130 years, he became the father of Shelah. And after he became the father of Shelah, Cainan lived 330 years and had other sons and daughters*

# Gen 12:1
***The Call of Abram (12:1–9)***

# Gen 12:7
*ᵃ7* Or *seed*

# Gen 12:10
***Abram in Egypt (12:10–20)***

# Gen 13:1
***Abram and Lot Separate (13:1–18)***

# Gen 13:15
*ᵃ15* Or *seed*; also in verse [16](vref:Gen 13:16)

# Gen 14:1
***Abram Rescues Lot (14:1–24)***\
\
*ᵃ1* That is, Babylonia; also in verse [9](vref:Gen 14:9)

# Gen 14:3
*ᵃ3* That is, the Dead Sea

# Gen 14:13
*ᵃ13* Or *a relative*; or *an ally*

# Gen 14:18
*ᵃ18* That is, Jerusalem

# Gen 14:19
*ᵃ19* Or *Possessor*; also in verse [22](vref:Gen 14:22)

# Gen 14:20
*ᵃ20* Or *And praise be to*

# Gen 15:1
***God’s Covenant With Abram (15:1–21)***\
\
*ᵃ1* Or *sovereign*\
*ᵇ1* Or *shield; / your reward will be very great*

# Gen 15:2
*ᵃ2* The meaning of the Hebrew for this phrase is uncertain.

# Gen 15:18
*ᵃ18* Or *Wadi*

# Gen 16:1
***Hagar and Ishmael (16:1–16)***

# Gen 16:11
*ᵃ11* *Ishmael* means *God hears*.

# Gen 16:12
*ᵃ12* Or *live to the east / of*

# Gen 16:13
*ᵃ13* Or *seen the back of*

# Gen 16:14
*ᵃ14* *Beer Lahai Roi* means *well of the Living One who sees me*.

# Gen 17:1
***The Covenant of Circumcision (17:1–27)***\
\
*ᵃ1* Hebrew *El-Shaddai*

# Gen 17:5
*ᵃ5* *Abram* means *exalted father*.\
*ᵇ5* *Abraham* means *father of many*.

# Gen 17:19
*ᵃ19* *Isaac* means *he laughs*.

# Gen 18:1
***The Three Visitors (18:1–15)***

# Gen 18:3
*ᵃ3* Or *O Lord*

# Gen 18:6
*ᵃ6* That is, probably about 20 quarts (about 22 liters)

# Gen 18:10
*ᵃ10* Hebrew *Then he*

# Gen 18:12
*ᵃ12* Or *husband*

# Gen 18:16
***Abraham Pleads for Sodom (18:16–33)***

# Gen 18:22
*ᵃ22* Masoretic Text; an ancient Hebrew scribal tradition *but the Lᴏʀᴅ remained standing before Abraham*

# Gen 18:24
*ᵃ24* Or *forgive*; also in verse [26](vref:Gen 18:26)

# Gen 18:25
*ᵃ25* Or *Ruler*

# Gen 19:1
***Sodom and Gomorrah Destroyed (19:1–29)***

# Gen 19:14
*ᵃ14* Or *were married to*

# Gen 19:18
*ᵃ18* Or *No, Lord*; or *No, my lord*

# Gen 19:19
*ᵃ19* The Hebrew is singular.

# Gen 19:22
*ᵃ22* *Zoar* means *small*.

# Gen 19:30
***Lot and His Daughters (19:30–38)***

# Gen 19:37
*ᵃ37* *Moab* sounds like the Hebrew for *from father*.

# Gen 19:38
*ᵃ38* *Ben-Ammi* means *son of my people*.

# Gen 20:1
***Abraham and Abimelech (20:1–18)***

# Gen 20:16
*ᵃ16* That is, about 25 pounds (about 11.5 kilograms)

# Gen 21:1
***The Birth of Isaac (21:1–7)***

# Gen 21:3
*ᵃ3* *Isaac* means *he laughs*.

# Gen 21:8
***Hagar and Ishmael Sent Away (21:8–21)***

# Gen 21:12
*ᵃ12* Or *seed*

# Gen 21:16
*ᵃ16* Hebrew; Septuagint *the child*

# Gen 21:22
***The Treaty at Beersheba (21:22–34)***

# Gen 21:31
*ᵃ31* *Beersheba* can mean *well of seven* or *well of the oath*.

# Gen 22:1
***Abraham Tested (22:1–19)***

# Gen 22:13
*ᵃ13* Many manuscripts of the Masoretic Text, Samaritan Pentateuch, Septuagint and Syriac; most manuscripts of the Masoretic Text *a ram behind ⌞​him​⌟*

# Gen 22:18
*ᵃ18* Or *seed*

# Gen 22:20
***Nahor’s Sons (22:20–24)***

# Gen 23:1
***The Death of Sarah (23:1–20)***

# Gen 23:3
*ᵃ3* Or *the sons of Heth*; also in verses [5](vref:Gen 23:5), [7](vref:Gen 23:7), [10](vref:Gen 23:10), [16](vref:Gen 23:16), [18](vref:Gen 23:18) and [20](vref:Gen 23:20)

# Gen 23:11
*ᵃ11* Or *sell*

# Gen 23:15
*ᵃ15* That is, about 10 pounds (about 4.5 kilograms)

# Gen 24:1
***Isaac and Rebekah (24:1–67)***

# Gen 24:2
*ᵃ2* Or *oldest*

# Gen 24:7
*ᵃ7* Or *seed*

# Gen 24:10
*ᵃ10* That is, Northwest Mesopotamia

# Gen 24:22
*ᵃ22* That is, about 1/5 ounce (about 5.5 grams)\
*ᵇ22* That is, about 4 ounces (about 110 grams)

# Gen 24:36
*ᵃ36* Or *his*

# Gen 24:55
*ᵃ55* Or *she*

# Gen 24:63
*ᵃ63* The meaning of the Hebrew for this word is uncertain.

# Gen 25:1
***The Death of Abraham (25:1–11)***\
\
*ᵃ1* Or *had taken*

# Gen 25:10
*ᵃ10* Or *the sons of Heth*

# Gen 25:12
***Ishmael’s Sons (25:12–18)***

# Gen 25:18
*ᵃ18* Or *lived to the east of*

# Gen 25:19
***Jacob and Esau (25:19–34)***

# Gen 25:20
*ᵃ20* That is, Northwest Mesopotamia

# Gen 25:25
*ᵃ25* *Esau* may mean *hairy*; he was also called Edom, which means *red*.

# Gen 25:26
*ᵃ26* *Jacob* means *he grasps the heel* (figuratively, *he deceives*).

# Gen 25:30
*ᵃ30* *Edom* means *red*.

# Gen 26:1
***Isaac and Abimelech (26:1–35)***

# Gen 26:4
*ᵃ4* Or *seed*

# Gen 26:20
*ᵃ20* *Esek* means *dispute*.

# Gen 26:21
*ᵃ21* *Sitnah* means *opposition*.

# Gen 26:22
*ᵃ22* *Rehoboth* means *room*.

# Gen 26:33
*ᵃ33* *Shibah* can mean *oath* or *seven*.\
*ᵇ33* *Beersheba* can mean *well of the oath* or *well of seven*.

# Gen 27:1
***Jacob Gets Isaac’s Blessing (27:1–40)***

# Gen 27:36
*ᵃ36* *Jacob* means *he grasps the heel* (figuratively, *he deceives*).

# Gen 27:41
***Jacob Flees to Laban (27:41–28:9)***

# Gen 28:1
*ᵃ1* Or *greeted*

# Gen 28:2
*ᵃ2* That is, Northwest Mesopotamia; also in verses [5](vref:Gen 28:5), [6](vref:Gen 28:6) and [7](vref:Gen 28:7)

# Gen 28:3
*ᵃ3* Hebrew *El-Shaddai*

# Gen 28:10
***Jacob’s Dream at Bethel (28:10–22)***

# Gen 28:12
*ᵃ12* Or *ladder*

# Gen 28:13
*ᵃ13* Or *There beside him*

# Gen 28:19
*ᵃ19* *Bethel* means *house of God*.

# Gen 28:21
*ᵃ20,21* Or *Since God . . . father’s house, the Lᴏʀᴅ*

# Gen 28:22
*ᵃ21,22* Or *house, and the Lᴏʀᴅ will be my God, ²²then*

# Gen 29:1
***Jacob Arrives in Paddan Aram (29:1–14a)***

# Gen 29:14
***Jacob Marries Leah and Rachel (29:14b–30)***

# Gen 29:17
*ᵃ17* Or *delicate*

# Gen 29:31
***Jacob’s Children (29:31–30:24)***

# Gen 29:32
*ᵃ32* *Reuben* sounds like the Hebrew for *he has seen my misery*; the name means *see, a son*.

# Gen 29:33
*ᵃ33* *Simeon* probably means *one who hears*.

# Gen 29:34
*ᵃ34* *Levi* sounds like and may be derived from the Hebrew for *attached*.

# Gen 29:35
*ᵃ35* *Judah* sounds like and may be derived from the Hebrew for *praise*.

# Gen 30:6
*ᵃ6* *Dan* here means *he has vindicated*.

# Gen 30:8
*ᵃ8* *Naphtali* means *my struggle*.

# Gen 30:11
*ᵃ11* Or *“A troop is coming!”*\
*ᵇ11* *Gad* can mean *good fortune* or *a troop*.

# Gen 30:13
*ᵃ13* *Asher* means *happy*.

# Gen 30:18
*ᵃ18* *Issachar* sounds like the Hebrew for *reward*.

# Gen 30:20
*ᵃ20* *Zebulun* probably means *honor*.

# Gen 30:24
*ᵃ24* *Joseph* means *may he add*.

# Gen 30:25
***Jacob’s Flocks Increase (30:25–43)***

# Gen 30:27
*ᵃ27* Or possibly *have become rich and*

# Gen 31:1
***Jacob Flees From Laban (31:1–21)***

# Gen 31:18
*ᵃ18* That is, Northwest Mesopotamia

# Gen 31:21
*ᵃ21* That is, the Euphrates

# Gen 31:22
***Laban Pursues Jacob (31:22–55)***

# Gen 31:47
*ᵃ47* The Aramaic *Jegar Sahadutha* means *witness heap*.\
*ᵇ47* The Hebrew *Galeed* means *witness heap*.

# Gen 31:49
*ᵃ49* *Mizpah* means *watchtower*.

# Gen 32:1
***Jacob Prepares to Meet Esau (32:1–21)***

# Gen 32:2
*ᵃ2* *Mahanaim* means *two camps*.

# Gen 32:7
*ᵃ7* Or *camps*; also in verse [10](vref:Gen 32:10)

# Gen 32:8
*ᵃ8* Or *camp*

# Gen 32:22
***Jacob Wrestles With God (32:22–32)***

# Gen 32:28
*ᵃ28* *Israel* means *he struggles with God*.

# Gen 32:30
*ᵃ30* *Peniel* means *face of God*.

# Gen 32:31
*ᵃ31* Hebrew *Penuel*, a variant of *Peniel*

# Gen 33:1
***Jacob Meets Esau (33:1–20)***

# Gen 33:17
*ᵃ17* *Succoth* means *shelters*.

# Gen 33:18
*ᵃ18* That is, Northwest Mesopotamia\
*ᵇ18* Or *arrived at Shalem, a*

# Gen 33:19
*ᵃ19* Hebrew *hundred kesitahs*; a kesitah was a unit of money of unknown weight and value.

# Gen 33:20
*ᵃ20* *El Elohe Israel* can mean *God, the God of Israel* or *mighty is the God of Israel*.

# Gen 34:1
***Dinah and the Shechemites (34:1–31)***

# Gen 34:7
*ᵃ7* Or *against*

# Gen 34:10
*ᵃ10* Or *move about freely*; also in verse [21](vref:Gen 34:21)

# Gen 34:17
*ᵃ17* Hebrew *daughter*

# Gen 34:27
*ᵃ27* Or *because*

# Gen 35:1
***Jacob Returns to Bethel (35:1–15)***

# Gen 35:7
*ᵃ7* *El Bethel* means *God of Bethel*.

# Gen 35:8
*ᵃ8* *Allon Bacuth* means *oak of weeping*.

# Gen 35:9
*ᵃ9* That is, Northwest Mesopotamia; also in verse [26](vref:Gen 35:26)

# Gen 35:10
*ᵃ10* *Jacob* means *he grasps the heel* (figuratively, *he deceives*).\
*ᵇ10* *Israel* means *he struggles with God*.

# Gen 35:11
*ᵃ11* Hebrew *El-Shaddai*

# Gen 35:15
*ᵃ15* *Bethel* means *house of God*.

# Gen 35:16
***The Deaths of Rachel and Isaac (35:16–29)***

# Gen 35:18
*ᵃ18* *Ben-Oni* means *son of my trouble*.\
*ᵇ18* *Benjamin* means *son of my right hand*.

# Gen 36:1
***Esau’s Descendants (36:1–30)***

# Gen 36:16
*ᵃ16* Masoretic Text; Samaritan Pentateuch (see also [Gen. 36:11](vref:Gen 36:11) and [1 Chron. 1:36](vref:1 Chr 1:36)) does not have *Korah*.

# Gen 36:22
*ᵃ22* Hebrew *Hemam*, a variant of *Homam* (see [1 Chron. 1:39](vref:1 Chr 1:39))

# Gen 36:24
*ᵃ24* Vulgate; Syriac *discovered water*; the meaning of the Hebrew for this word is uncertain.

# Gen 36:26
*ᵃ26* Hebrew *Dishan*, a variant of *Dishon*

# Gen 36:31
***The Rulers of Edom (36:31–43)***\
\
*ᵃ31* Or *before an Israelite king reigned over them*

# Gen 36:37
*ᵃ37* Possibly the Euphrates

# Gen 36:39
*ᵃ39* Many manuscripts of the Masoretic Text, Samaritan Pentateuch and Syriac (see also [1 Chron. 1:50](vref:1 Chr 1:50)); most manuscripts of the Masoretic Text *Hadar*

# Gen 37:1
***Joseph’s Dreams (37:1–11)***

# Gen 37:3
*ᵃ3* The meaning of the Hebrew for *richly ornamented* is uncertain; also in verses [23](vref:Gen 37:23) and [32](vref:Gen 37:32).

# Gen 37:12
***Joseph Sold by His Brothers (37:12–36)***

# Gen 37:28
*ᵃ28* That is, about 8 ounces (about 0.2 kilogram)

# Gen 37:35
*ᵃ35* Hebrew *Sheol*

# Gen 37:36
*ᵃ36* Samaritan Pentateuch, Septuagint, Vulgate and Syriac (see also verse [28](vref:Gen 37:28)); Masoretic Text *Medanites*

# Gen 38:1
***Judah and Tamar (38:1–30)***

# Gen 38:29
*ᵃ29* *Perez* means *breaking out*.

# Gen 38:30
*ᵃ30* *Zerah* can mean *scarlet* or *brightness*.

# Gen 39:1
***Joseph and Potiphar’s Wife (39:1–23)***

# Gen 40:1
***The Cupbearer and the Baker (40:1–23)***

# Gen 40:16
*ᵃ16* Or *three wicker baskets*

# Gen 40:19
*ᵃ19* Or *and impale you on a pole*

# Gen 40:22
*ᵃ22* Or *impaled*

# Gen 41:1
***Pharaoh’s Dreams (41:1–40)***

# Gen 41:13
*ᵃ13* Or *impaled*

# Gen 41:38
*ᵃ38* Or *of the gods*

# Gen 41:41
***Joseph in Charge of Egypt (41:41–57)***

# Gen 41:43
*ᵃ43* Or *in the chariot of his second-in-command*; or *in his second chariot*\
*ᵇ43* Or *Bow down*

# Gen 41:45
*ᵃ45* That is, Heliopolis; also in verse [50](vref:Gen 41:50)

# Gen 41:51
*ᵃ51* *Manasseh* sounds like and may be derived from the Hebrew for *forget*.

# Gen 41:52
*ᵃ52* *Ephraim* sounds like the Hebrew for *twice fruitful*.

# Gen 42:1
***Joseph’s Brothers Go to Egypt (42:1–38)***

# Gen 42:34
*ᵃ34* Or *move about freely*

# Gen 42:38
*ᵃ38* Hebrew *Sheol*

# Gen 43:1
***The Second Journey to Egypt (43:1–34)***

# Gen 43:14
*ᵃ14* Hebrew *El-Shaddai*

# Gen 44:1
***A Silver Cup in a Sack (44:1–34)***

# Gen 44:29
*ᵃ29* Hebrew *Sheol*; also in verse [31](vref:Gen 44:31)

# Gen 45:1
***Joseph Makes Himself Known (45:1–28)***

# Gen 45:7
*ᵃ7* Or *save you as a great band of survivors*

# Gen 45:22
*ᵃ22* That is, about 7 1/2 pounds (about 3.5 kilograms)

# Gen 46:1
***Jacob Goes to Egypt (46:1–47:12)***

# Gen 46:13
*ᵃ13* Samaritan Pentateuch and Syriac (see also [1 Chron. 7:1](vref:1 Chr 7:1)); Masoretic Text *Puvah*\
*ᵇ13* Samaritan Pentateuch and some Septuagint manuscripts (see also [Num. 26:24](vref:Num 26:24) and [1 Chron. 7:1](vref:1 Chr 7:1)); Masoretic Text *Iob*

# Gen 46:15
*ᵃ15* That is, Northwest Mesopotamia

# Gen 46:16
*ᵃ16* Samaritan Pentateuch and Septuagint (see also [Num. 26:15](vref:Num 26:15)); Masoretic Text *Ziphion*

# Gen 46:20
*ᵃ20* That is, Heliopolis

# Gen 46:27
*ᵃ27* Hebrew; Septuagint *the nine children*\
*ᵇ27* Hebrew (see also [Exodus 1:5](vref:Exod 1:5) and footnote); Septuagint (see also [Acts 7:14](vref:Acts 7:14)) *seventy-five*

# Gen 46:29
*ᵃ29* Hebrew *around him*

# Gen 47:7
*ᵃ7* Or *greeted*

# Gen 47:10
*ᵃ10* Or *said farewell to*

# Gen 47:13
***Joseph and the Famine (47:13–31)***

# Gen 47:21
*ᵃ21* Samaritan Pentateuch and Septuagint (see also Vulgate); Masoretic Text *and he moved the people into the cities*

# Gen 47:31
*ᵃ31* Or *Israel bowed down at the head of his bed*

# Gen 48:1
***Manasseh and Ephraim (48:1–22)***

# Gen 48:3
*ᵃ3* Hebrew *El-Shaddai*

# Gen 48:7
*ᵃ7* That is, Northwest Mesopotamia

# Gen 48:20
*ᵃ20* The Hebrew is singular.

# Gen 48:21
*ᵃ21* The Hebrew is plural.

# Gen 48:22
*ᵃ22* Or *And to you I give one portion more than to your brothers—the portion*

# Gen 49:1
***Jacob Blesses His Sons (49:1–28)***

# Gen 49:5
*ᵃ5* The meaning of the Hebrew for this word is uncertain.

# Gen 49:8
*ᵃ8* *Judah* sounds like and may be derived from the Hebrew for *praise*.

# Gen 49:10
*ᵃ10* Or *until Shiloh comes*; or *until he comes to whom tribute belongs*

# Gen 49:12
*ᵃ12* Or *will be dull from wine, / his teeth white from milk*

# Gen 49:14
*ᵃ14* Or *strong*\
*ᵇ14* Or *campfires*

# Gen 49:16
*ᵃ16* *Dan* here means *he provides justice*.

# Gen 49:19
*ᵃ19* *Gad* can mean *attack* and *band of raiders*.

# Gen 49:21
*ᵃ21* Or *free; / he utters beautiful words*

# Gen 49:22
*ᵃ22* Or *Joseph is a wild colt, / a wild colt near a spring, / a wild donkey on a terraced hill*

# Gen 49:24
*ᵃ23,24* Or *archers will attack . . . will shoot . . . will remain . . . will stay*

# Gen 49:25
*ᵃ25* Hebrew *Shaddai*

# Gen 49:26
*ᵃ26* Or *of my progenitors, / as great as*\
*ᵇ26* Or *the one separated from*

# Gen 49:29
***The Death of Jacob (49:29–50:14)***

# Gen 49:32
*ᵃ32* Or *the sons of Heth*

# Gen 50:9
*ᵃ9* Or *charioteers*

# Gen 50:11
*ᵃ11* *Abel Mizraim* means *mourning of the Egyptians*.

# Gen 50:15
***Joseph Reassures His Brothers (50:15–21)***

# Gen 50:22
***The Death of Joseph (50:22–26)***

# Gen 50:23
*ᵃ23* That is, were counted as his

# Exod 1:1
***The Israelites Oppressed (1:1–22)***

# Exod 1:5
*ᵃ5* Masoretic Text (see also [Gen. 46:27](vref:Gen 46:27)); Dead Sea Scrolls and Septuagint (see also [Acts 7:14](vref:Acts 7:14) and note at [Gen. 46:27](vref:Gen 46:27)) *seventy-five*

# Exod 1:22
*ᵃ22* Masoretic Text; Samaritan Pentateuch, Septuagint and Targums *born to the Hebrews*

# Exod 2:1
***The Birth of Moses (2:1–10)***

# Exod 2:10
*ᵃ10* *Moses* sounds like the Hebrew for *draw out*.

# Exod 2:11
***Moses Flees to Midian (2:11–25)***

# Exod 2:22
*ᵃ22* *Gershom* sounds like the Hebrew for *an alien there*.

# Exod 3:1
***Moses and the Burning Bush (3:1–22)***

# Exod 3:12
*ᵃ12* The Hebrew is plural.

# Exod 3:14
*ᵃ14* Or *I ᴡɪʟʟ ʙᴇ ᴡʜᴀᴛ I ᴡɪʟʟ ʙᴇ*

# Exod 3:15
*ᵃ15* The Hebrew for *Lᴏʀᴅ* sounds like and may be derived from the Hebrew for *I ᴀᴍ* in verse [14](vref:Exod 3:14).

# Exod 4:1
***Signs for Moses (4:1–17)***

# Exod 4:6
*ᵃ6* The Hebrew word was used for various diseases affecting the skin—not necessarily leprosy.

# Exod 4:18
***Moses Returns to Egypt (4:18–31)***

# Exod 4:24
*ᵃ24* Or *⌞​Moses’ son​⌟*; Hebrew *him*

# Exod 4:25
*ᵃ25* Or *and drew near ⌞​Moses’​⌟ feet*

# Exod 5:1
***Bricks Without Straw (5:1–21)***

# Exod 5:22
***God Promises Deliverance (5:22–6:12)***

# Exod 6:3
*ᵃ3* Hebrew *El-Shaddai*\
*ᵇ3* See note at [Exodus 3:15](vref:Exod 3:15).\
*ᶜ3* Or *Almighty, and by my name the Lᴏʀᴅ did I not let myself be known to them?*

# Exod 6:12
*ᵃ12* Hebrew *I am uncircumcised of lips*; also in verse [30](vref:Exod 6:30)

# Exod 6:13
***Family Record of Moses and Aaron (6:13–27)***

# Exod 6:14
*ᵃ14* The Hebrew for *families* here and in verse [25](vref:Exod 6:25) refers to units larger than clans.

# Exod 6:28
***Aaron to Speak for Moses (6:28–7:7)***

# Exod 7:8
***Aaron’s Staff Becomes a Snake (7:8–13)***

# Exod 7:14
***The Plague of Blood (7:14–24)***

# Exod 7:25
***The Plague of Frogs (8:1–15)***

# Exod 8:16
***The Plague of Gnats (8:16–19)***

# Exod 8:20
***The Plague of Flies (8:20–32)***

# Exod 8:23
*ᵃ23* Septuagint and Vulgate; Hebrew *will put a deliverance*

# Exod 9:1
***The Plague on Livestock (9:1–7)***

# Exod 9:8
***The Plague of Boils (9:8–12)***

# Exod 9:13
***The Plague of Hail (9:13–35)***

# Exod 9:16
*ᵃ16* Or *have spared you*

# Exod 10:1
***The Plague of Locusts (10:1–20)***

# Exod 10:10
*ᵃ10* Or *Be careful, trouble is in store for you!*

# Exod 10:19
*ᵃ19* Hebrew *Yam Suph*; that is, Sea of Reeds

# Exod 10:21
***The Plague of Darkness (10:21–29)***

# Exod 11:1
***The Plague on the Firstborn (11:1–10)***

# Exod 12:1
***The Passover (12:1–30)***

# Exod 12:3
*ᵃ3* The Hebrew word can mean *lamb* or *kid*; also in verse [4](vref:Exod 12:4).

# Exod 12:31
***The Exodus (12:31–42)***

# Exod 12:40
*ᵃ40* Masoretic Text; Samaritan Pentateuch and Septuagint *Egypt and Canaan*

# Exod 12:43
***Passover Restrictions (12:43–51)***

# Exod 13:1
***Consecration of the Firstborn (13:1–16)***

# Exod 13:17
***Crossing the Sea (13:17–14:31)***

# Exod 13:18
*ᵃ18* Hebrew *Yam Suph*; that is, Sea of Reeds

# Exod 13:19
*ᵃ19* See [Gen. 50:25](vref:Gen 50:25).

# Exod 14:9
*ᵃ9* Or *charioteers*; also in verses [17](vref:Exod 14:17), [18](vref:Exod 14:18), [23](vref:Exod 14:23), [26](vref:Exod 14:26) and [28](vref:Exod 14:28)

# Exod 14:25
*ᵃ25* Or *He jammed the wheels of their chariots* (see Samaritan Pentateuch, Septuagint and Syriac)

# Exod 14:27
*ᵃ27* Or *from*

# Exod 15:1
***The Song of Moses and Miriam (15:1–21)***

# Exod 15:4
*ᵃ4* Hebrew *Yam Suph*; that is, Sea of Reeds; also in verse [22](vref:Exod 15:22)

# Exod 15:15
*ᵃ15* Or *rulers*

# Exod 15:16
*ᵃ16* Or *created*

# Exod 15:19
*ᵃ19* Or *charioteers*

# Exod 15:22
***The Waters of Marah and Elim (15:22–27)***

# Exod 15:23
*ᵃ23* *Marah* means *bitter*.

# Exod 16:1
***Manna and Quail (16:1–36)***

# Exod 16:16
*ᵃ16* That is, probably about 2 quarts (about 2 liters); also in verses [18](vref:Exod 16:18), [32](vref:Exod 16:32), [33](vref:Exod 16:33) and [36](vref:Exod 16:36)

# Exod 16:22
*ᵃ22* That is, probably about 4 quarts (about 4.5 liters)

# Exod 16:28
*ᵃ28* The Hebrew is plural.

# Exod 16:31
*ᵃ31* *Manna* means *What is it?* (see verse [15](vref:Exod 16:15)).

# Exod 17:1
***Water From the Rock (17:1–7)***

# Exod 17:7
*ᵃ7* *Massah* means *testing*.\
*ᵇ7* *Meribah* means *quarreling*.

# Exod 17:8
***The Amalekites Defeated (17:8–16)***

# Exod 17:16
*ᵃ16* Or *“Because a hand was against the throne of the Lᴏʀᴅ, the*

# Exod 18:1
***Jethro Visits Moses (18:1–27)***

# Exod 18:3
*ᵃ3* *Gershom* sounds like the Hebrew for *an alien there*.

# Exod 18:4
*ᵃ4* *Eliezer* means *my God is helper*.

# Exod 19:1
***At Mount Sinai (19:1–25)***

# Exod 19:6
*ᵃ5,6* Or *possession, for the whole earth is mine. ⁶You*

# Exod 19:18
*ᵃ18* Most Hebrew manuscripts; a few Hebrew manuscripts and Septuagint *all the people*

# Exod 19:19
*ᵃ19* Or *and God answered him with thunder*

# Exod 20:1
***The Ten Commandments (20:1–21)***

# Exod 20:3
*ᵃ3* Or *besides*

# Exod 20:22
***Idols and Altars (20:22–21:1)***

# Exod 20:24
*ᵃ24* Traditionally *peace offerings*

# Exod 21:2
***Hebrew Servants (21:2–11)***

# Exod 21:6
*ᵃ6* Or *before God*

# Exod 21:8
*ᵃ8* Or *master so that he does not choose her*

# Exod 21:12
***Personal Injuries (21:12–36)***

# Exod 21:15
*ᵃ15* Or *kills*

# Exod 21:18
*ᵃ18* Or *with a tool*

# Exod 21:22
*ᵃ22* Or *she has a miscarriage*

# Exod 21:32
*ᵃ32* That is, about 12 ounces (about 0.3 kilogram)

# Exod 22:1
***Protection of Property (22:1–15)***

# Exod 22:3
*ᵃ3* Or *if he strikes him*

# Exod 22:8
*ᵃ8* Or *before God*; also in verse [9](vref:Exod 22:9)

# Exod 22:9
*ᵃ9* Or *whom God declares*

# Exod 22:16
***Social Responsibility (22:16–31)***

# Exod 22:20
*ᵃ20* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Exod 22:25
*ᵃ25* Or *excessive interest*

# Exod 22:28
*ᵃ28* Or *Do not revile the judges*

# Exod 22:29
*ᵃ29* The meaning of the Hebrew for this phrase is uncertain.

# Exod 23:1
***Laws of Justice and Mercy (23:1–9)***

# Exod 23:10
***Sabbath Laws (23:10–13)***

# Exod 23:14
***The Three Annual Festivals (23:14–19)***

# Exod 23:20
***God’s Angel to Prepare the Way (23:20–33)***

# Exod 23:31
*ᵃ31* Hebrew *Yam Suph*; that is, Sea of Reeds\
*ᵇ31* That is, the Mediterranean\
*ᶜ31* That is, the Euphrates

# Exod 24:1
***The Covenant Confirmed (24:1–18)***

# Exod 24:5
*ᵃ5* Traditionally *peace offerings*

# Exod 24:10
*ᵃ10* Or *lapis lazuli*

# Exod 25:1
***Offerings for the Tabernacle (25:1–9)***

# Exod 25:5
*ᵃ5* That is, dugongs

# Exod 25:10
***The Ark (25:10–22)***\
\
*ᵃ10* That is, about 3 3/4 feet (about 1.1 meters) long and 2 1/4 feet (about 0.7 meter) wide and high

# Exod 25:17
*ᵃ17* Traditionally *a mercy seat*\
*ᵇ17* That is, about 3 3/4 feet (about 1.1 meters) long and 2 1/4 feet (about 0.7 meter) wide

# Exod 25:23
***The Table (25:23–30)***\
\
*ᵃ23* That is, about 3 feet (about 0.9 meter) long and 1 1/2 feet (about 0.5 meter) wide and 2 1/4 feet (about 0.7 meter) high

# Exod 25:25
*ᵃ25* That is, about 3 inches (about 8 centimeters)

# Exod 25:31
***The Lampstand (25:31–40)***

# Exod 25:39
*ᵃ39* That is, about 75 pounds (about 34 kilograms)

# Exod 26:1
***The Tabernacle (26:1–37)***

# Exod 26:2
*ᵃ2* That is, about 42 feet (about 12.5 meters) long and 6 feet (about 1.8 meters) wide

# Exod 26:8
*ᵃ8* That is, about 45 feet (about 13.5 meters) long and 6 feet (about 1.8 meters) wide

# Exod 26:13
*ᵃ13* That is, about 1 1/2 feet (about 0.5 meter)

# Exod 26:14
*ᵃ14* That is, dugongs

# Exod 26:16
*ᵃ16* That is, about 15 feet (about 4.5 meters) long and 2 1/4 feet (about 0.7 meter) wide

# Exod 27:1
***The Altar of Burnt Offering (27:1–8)***\
\
*ᵃ1* That is, about 4 1/2 feet (about 1.3 meters)\
*ᵇ1* That is, about 7 1/2 feet (about 2.3 meters) long and wide

# Exod 27:9
***The Courtyard (27:9–19)***\
\
*ᵃ9* That is, about 150 feet (about 46 meters); also in verse [11](vref:Exod 27:11)

# Exod 27:12
*ᵃ12* That is, about 75 feet (about 23 meters); also in verse [13](vref:Exod 27:13)

# Exod 27:14
*ᵃ14* That is, about 22 1/2 feet (about 6.9 meters); also in verse [15](vref:Exod 27:15)

# Exod 27:16
*ᵃ16* That is, about 30 feet (about 9 meters)

# Exod 27:18
*ᵃ18* That is, about 150 feet (about 46 meters) long and 75 feet (about 23 meters) wide\
*ᵇ18* That is, about 7 1/2 feet (about 2.3 meters)

# Exod 27:20
***Oil for the Lampstand (27:20–21)***

# Exod 28:1
***The Priestly Garments (28:1–5)***

# Exod 28:6
***The Ephod (28:6–14)***

# Exod 28:15
***The Breastpiece (28:15–30)***

# Exod 28:16
*ᵃ16* That is, about 9 inches (about 22 centimeters)

# Exod 28:18
*ᵃ18* Or *lapis lazuli*

# Exod 28:20
*ᵃ20* The precise identification of some of these precious stones is uncertain.

# Exod 28:31
***Other Priestly Garments (28:31–43)***

# Exod 28:32
*ᵃ32* The meaning of the Hebrew for this word is uncertain.

# Exod 29:1
***Consecration of the Priests (29:1–46)***

# Exod 29:9
*ᵃ9* Hebrew; Septuagint *on them*

# Exod 29:28
*ᵃ28* Traditionally *peace offerings*

# Exod 29:40
*ᵃ40* That is, probably about 2 quarts (about 2 liters)\
*ᵇ40* That is, probably about 1 quart (about 1 liter)

# Exod 30:1
***The Altar of Incense (30:1–10)***

# Exod 30:2
*ᵃ2* That is, about 1 1/2 feet (about 0.5 meter) long and wide and about 3 feet (about 0.9 meter) high

# Exod 30:11
***Atonement Money (30:11–16)***

# Exod 30:13
*ᵃ13* That is, about 1/5 ounce (about 6 grams); also in verse [15](vref:Exod 30:15)

# Exod 30:17
***Basin for Washing (30:17–21)***

# Exod 30:22
***Anointing Oil (30:22–33)***

# Exod 30:23
*ᵃ23* That is, about 12 1/2 pounds (about 6 kilograms)

# Exod 30:24
*ᵃ24* That is, probably about 4 quarts (about 4 liters)

# Exod 30:34
***Incense (30:34–38)***

# Exod 31:1
***Bezalel and Oholiab (31:1–11)***

# Exod 31:12
***The Sabbath (31:12–18)***

# Exod 31:13
*ᵃ13* Or *who sanctifies you*; or *who sets you apart as holy*

# Exod 32:1
***The Golden Calf (32:1–33:6)***\
\
*ᵃ1* Or *a god*; also in verses [23](vref:Exod 32:23) and [31](vref:Exod 32:31)

# Exod 32:4
*ᵃ4* Or *This is your god*; also in verse [8](vref:Exod 32:8)

# Exod 32:6
*ᵃ6* Traditionally *peace offerings*

# Exod 33:7
***The Tent of Meeting (33:7–11)***

# Exod 33:12
***Moses and the Glory of the Lᴏʀᴅ (33:12–23)***

# Exod 34:1
***The New Stone Tablets (34:1–28)***

# Exod 34:13
*ᵃ13* That is, symbols of the goddess Asherah

# Exod 34:22
*ᵃ22* That is, in the fall

# Exod 34:29
***The Radiant Face of Moses (34:29–35)***

# Exod 35:1
***Sabbath Regulations (35:1–3)***

# Exod 35:4
***Materials for the Tabernacle (35:4–29)***

# Exod 35:7
*ᵃ7* That is, dugongs; also in verse [23](vref:Exod 35:23)

# Exod 35:30
***Bezalel and Oholiab (35:30–36:7)***

# Exod 36:8
***The Tabernacle (36:8–38)***

# Exod 36:9
*ᵃ9* That is, about 42 feet (about 12.5 meters) long and 6 feet (about 1.8 meters) wide

# Exod 36:15
*ᵃ15* That is, about 45 feet (about 13.5 meters) long and 6 feet (about 1.8 meters) wide

# Exod 36:19
*ᵃ19* That is, dugongs

# Exod 36:21
*ᵃ21* That is, about 15 feet (about 4.5 meters) long and 2 1/4 feet (about 0.7 meter) wide

# Exod 37:1
***The Ark (37:1–9)***\
\
*ᵃ1* That is, about 3 3/4 feet (about 1.1 meters) long and 2 1/4 feet (about 0.7 meter) wide and high

# Exod 37:6
*ᵃ6* That is, about 3 3/4 feet (about 1.1 meters) long and 2 1/4 feet (about 0.7 meter) wide

# Exod 37:10
***The Table (37:10–16)***\
\
*ᵃ10* Or *He*; also in verses [11–29](vref:Exod 37:11-29)\
*ᵇ10* That is, about 3 feet (about 0.9 meter) long, 1 1/2 feet (about 0.5 meter) wide, and 2 1/4 feet (about 0.7 meter) high

# Exod 37:12
*ᵃ12* That is, about 3 inches (about 8 centimeters)

# Exod 37:17
***The Lampstand (37:17–24)***

# Exod 37:24
*ᵃ24* That is, about 75 pounds (about 34 kilograms)

# Exod 37:25
***The Altar of Incense (37:25–29)***\
\
*ᵃ25* That is, about 1 1/2 feet (about 0.5 meter) long and wide, and about 3 feet (about 0.9 meter) high

# Exod 38:1
***The Altar of Burnt Offering (38:1–7)***\
\
*ᵃ1* Or *He*; also in verses [2–9](vref:Exod 38:2-9)\
*ᵇ1* That is, about 4 1/2 feet (about 1.3 meters)\
*ᶜ1* That is, about 7 1/2 feet (about 2.3 meters) long and wide

# Exod 38:8
***Basin for Washing (38:8)***

# Exod 38:9
***The Courtyard (38:9–20)***\
\
*ᵃ9* That is, about 150 feet (about 46 meters)

# Exod 38:12
*ᵃ12* That is, about 75 feet (about 23 meters)

# Exod 38:14
*ᵃ14* That is, about 22 1/2 feet (about 6.9 meters)

# Exod 38:18
*ᵃ18* That is, about 30 feet (about 9 meters)\
*ᵇ18* That is, about 7 1/2 feet (about 2.3 meters)

# Exod 38:21
***The Materials Used (38:21–31)***

# Exod 38:24
*ᵃ24* The weight of the gold was a little over one ton (about 1 metric ton).

# Exod 38:25
*ᵃ25* The weight of the silver was a little over 3 3/4 tons (about 3.4 metric tons).

# Exod 38:26
*ᵃ26* That is, about 1/5 ounce (about 5.5 grams)

# Exod 38:27
*ᵃ27* That is, about 3 3/4 tons (about 3.4 metric tons)

# Exod 38:28
*ᵃ28* That is, about 45 pounds (about 20 kilograms)

# Exod 38:29
*ᵃ29* The weight of the bronze was about 2 1/2 tons (about 2.4 metric tons).

# Exod 39:1
***The Priestly Garments (39:1)***

# Exod 39:2
***The Ephod (39:2–7)***\
\
*ᵃ2* Or *He*; also in verses [7](vref:Exod 39:7), [8](vref:Exod 39:8) and [22](vref:Exod 39:22)

# Exod 39:8
***The Breastpiece (39:8–21)***

# Exod 39:9
*ᵃ9* That is, about 9 inches (about 22 centimeters)

# Exod 39:11
*ᵃ11* Or *lapis lazuli*

# Exod 39:13
*ᵃ13* The precise identification of some of these precious stones is uncertain.

# Exod 39:22
***Other Priestly Garments (39:22–31)***

# Exod 39:23
*ᵃ23* The meaning of the Hebrew for this word is uncertain.

# Exod 39:32
***Moses Inspects the Tabernacle (39:32–43)***

# Exod 39:34
*ᵃ34* That is, dugongs

# Exod 40:1
***Setting Up the Tabernacle (40:1–33)***

# Exod 40:34
***The Glory of the Lᴏʀᴅ (40:34–38)***

# Lev 1:1
***The Burnt Offering (1:1–17)***

# Lev 1:3
*ᵃ3* Or *he*

# Lev 1:16
*ᵃ16* Or *crop and the feathers*; the meaning of the Hebrew for this word is uncertain.

# Lev 2:1
***The Grain Offering (2:1–16)***

# Lev 2:4
*ᵃ4* Or *and*

# Lev 3:1
***The Fellowship Offering (3:1–17)***\
\
*ᵃ1* Traditionally *peace offering*; also in verses [3](vref:Lev 3:3), [6](vref:Lev 3:6) and [9](vref:Lev 3:9)

# Lev 4:1
***The Sin Offering (4:1–5:13)***

# Lev 4:10
*ᵃ10* The Hebrew word can include both male and female.\
*ᵇ10* Traditionally *peace offering*; also in verses [26](vref:Lev 4:26), [31](vref:Lev 4:31) and [35](vref:Lev 4:35)

# Lev 5:11
*ᵃ11* That is, probably about 2 quarts (about 2 liters)

# Lev 5:14
***The Guilt Offering (5:14–6:7)***

# Lev 5:15
*ᵃ15* That is, about 2/5 ounce (about 11.5 grams)

# Lev 5:19
*ᵃ19* Or *has made full expiation for his*

# Lev 6:8
***The Burnt Offering (6:8–13)***

# Lev 6:12
*ᵃ12* Traditionally *peace offerings*

# Lev 6:14
***The Grain Offering (6:14–23)***

# Lev 6:18
*ᵃ18* Or *Whoever touches them must be holy*; similarly in verse [27](vref:Lev 6:27)

# Lev 6:20
*ᵃ20* Or *each*\
*ᵇ20* That is, probably about 2 quarts (about 2 liters)

# Lev 6:21
*ᵃ21* The meaning of the Hebrew for this word is uncertain.

# Lev 6:24
***The Sin Offering (6:24–30)***

# Lev 7:1
***The Guilt Offering (7:1–10)***

# Lev 7:11
***The Fellowship Offering (7:11–21)***\
\
*ᵃ11* Traditionally *peace offering*; also in verses [13–37](vref:Lev 7:13-37)

# Lev 7:22
***Eating Fat and Blood Forbidden (7:22–27)***

# Lev 7:25
*ᵃ25* Or *fire is*

# Lev 7:28
***The Priests’ Share (7:28–38)***

# Lev 8:1
***The Ordination of Aaron and His Sons (8:1–36)***

# Lev 8:31
*ᵃ31* Or *I was commanded:*

# Lev 9:1
***The Priests Begin Their Ministry (9:1–24)***

# Lev 9:4
*ᵃ4* The Hebrew word can include both male and female; also in verses [18](vref:Lev 9:18) and [19](vref:Lev 9:19).\
*ᵇ4* Traditionally *peace offering*; also in verses [18](vref:Lev 9:18) and [22](vref:Lev 9:22)

# Lev 10:1
***The Death of Nadab and Abihu (10:1–20)***

# Lev 10:6
*ᵃ6* Or *Do not uncover your heads*

# Lev 10:14
*ᵃ14* Traditionally *peace offerings*

# Lev 11:1
***Clean and Unclean Food (11:1–47)***

# Lev 11:5
*ᵃ5* That is, the hyrax or rock badger

# Lev 11:19
*ᵃ19* The precise identification of some of the birds, insects and animals in this chapter is uncertain.

# Lev 12:1
***Purification After Childbirth (12:1–8)***

# Lev 13:1
***Regulations About Infectious Skin Diseases (13:1–46)***

# Lev 13:2
*ᵃ2* Traditionally *leprosy*; the Hebrew word was used for various diseases affecting the skin—not necessarily leprosy; also elsewhere in this chapter.\
*ᵇ2* Or *descendants*

# Lev 13:3
*ᵃ3* Or *be lower than the rest of the skin*; also elsewhere in this chapter

# Lev 13:45
*ᵃ45* Or *clothes, uncover his head*

# Lev 13:47
***Regulations About Mildew (13:47–59)***

# Lev 14:1
***Cleansing From Infectious Skin Diseases (14:1–32)***

# Lev 14:3
*ᵃ3* Traditionally *leprosy*; the Hebrew word was used for various diseases affecting the skin—not necessarily leprosy; also elsewhere in this chapter.

# Lev 14:10
*ᵃ10* That is, probably about 6 quarts (about 6.5 liters)\
*ᵇ10* That is, probably about 2/3 pint (about 0.3 liter); also in verses [12](vref:Lev 14:12), [15](vref:Lev 14:15), [21](vref:Lev 14:21) and [24](vref:Lev 14:24)

# Lev 14:21
*ᵃ21* That is, probably about 2 quarts (about 2 liters)

# Lev 14:31
*ᵃ31* Septuagint and Syriac; Hebrew *³¹such as the person can afford, one*

# Lev 14:33
***Cleansing From Mildew (14:33–57)***

# Lev 15:1
***Discharges Causing Uncleanness (15:1–33)***

# Lev 15:31
*ᵃ31* Or *my tabernacle*

# Lev 16:1
***The Day of Atonement (16:1–34)***

# Lev 16:8
*ᵃ8* That is, the goat of removal; Hebrew *azazel*; also in verses [10](vref:Lev 16:10) and [26](vref:Lev 16:26)

# Lev 16:29
*ᵃ29* Or *must fast*; also in verse [31](vref:Lev 16:31)

# Lev 17:1
***Eating Blood Forbidden (17:1–16)***

# Lev 17:3
*ᵃ3* The Hebrew word can include both male and female.

# Lev 17:5
*ᵃ5* Traditionally *peace offerings*

# Lev 17:7
*ᵃ7* Or *demons*

# Lev 18:1
***Unlawful Sexual Relations (18:1–30)***

# Lev 18:21
*ᵃ21* Or *to be passed through ⌞​the fire​⌟*

# Lev 19:1
***Various Laws (19:1–37)***

# Lev 19:5
*ᵃ5* Traditionally *peace offering*

# Lev 19:23
*ᵃ23* Hebrew *uncircumcised*

# Lev 19:36
*ᵃ36* An ephah was a dry measure.\
*ᵇ36* A hin was a liquid measure.

# Lev 20:1
***Punishments for Sin (20:1–27)***

# Lev 20:2
*ᵃ2* Or *sacrifices*; also in verses [3](vref:Lev 20:3) and [4](vref:Lev 20:4)

# Lev 20:8
*ᵃ8* Or *who sanctifies you*; or *who sets you apart as holy*

# Lev 20:26
*ᵃ26* Or *be my holy ones*

# Lev 21:1
***Rules for Priests (21:1–22:16)***

# Lev 21:4
*ᵃ4* Or *unclean as a leader among his people*

# Lev 21:8
*ᵃ8* Or *who sanctify you*; or *who set you apart as holy*

# Lev 21:10
*ᵃ10* Or *not uncover his head*

# Lev 21:15
*ᵃ15* Or *who sanctifies him*; or *who sets him apart as holy*

# Lev 21:23
*ᵃ23* Or *who sanctifies them*; or *who sets them apart as holy*

# Lev 22:4
*ᵃ4* Traditionally *leprosy*; the Hebrew word was used for various diseases affecting the skin—not necessarily leprosy.

# Lev 22:9
*ᵃ9* Or *who sanctifies them*; or *who sets them apart as holy*; also in verse [16](vref:Lev 22:16)

# Lev 22:17
***Unacceptable Sacrifices (22:17–23:2)***

# Lev 22:21
*ᵃ21* Traditionally *peace offering*

# Lev 22:23
*ᵃ23* The Hebrew word can include both male and female.

# Lev 22:32
*ᵃ32* Or *made*\
*ᵇ32* Or *who sanctifies you*; or *who sets you apart as holy*

# Lev 23:3
***The Sabbath (23:3)***

# Lev 23:4
***The Passover and Unleavened Bread (23:4–8)***

# Lev 23:9
***Firstfruits (23:9–14)***

# Lev 23:13
*ᵃ13* That is, probably about 4 quarts (about 4.5 liters); also in verse [17](vref:Lev 23:17)\
*ᵇ13* That is, probably about 1 quart (about 1 liter)

# Lev 23:15
***Feast of Weeks (23:15–22)***

# Lev 23:19
*ᵃ19* Traditionally *peace offering*

# Lev 23:23
***Feast of Trumpets (23:23–25)***

# Lev 23:26
***Day of Atonement (23:26–32)***

# Lev 23:27
*ᵃ27* Or *and fast*; also in verses [29](vref:Lev 23:29) and [32](vref:Lev 23:32)

# Lev 23:33
***Feast of Tabernacles (23:33–44)***

# Lev 23:38
*ᵃ38* Or *These feasts are in addition to the Lᴏʀᴅ’s Sabbaths, and these offerings are*

# Lev 24:1
***Oil and Bread Set Before the Lᴏʀᴅ (24:1–9)***

# Lev 24:5
*ᵃ5* That is, probably about 4 quarts (about 4.5 liters)

# Lev 24:10
***A Blasphemer Stoned (24:10–23)***

# Lev 25:1
***The Sabbath Year (25:1–7)***

# Lev 25:8
***The Year of Jubilee (25:8–55)***

# Lev 25:36
*ᵃ36* Or *take excessive interest*; similarly in verse [37](vref:Lev 25:37)

# Lev 26:1
***Reward for Obedience (26:1–13)***

# Lev 26:11
*ᵃ11* Or *my tabernacle*

# Lev 26:14
***Punishment for Disobedience (26:14–46)***

# Lev 27:1
***Redeeming What Is the Lᴏʀᴅ’s (27:1–34)***

# Lev 27:3
*ᵃ3* That is, about 1 1/4 pounds (about 0.6 kilogram); also in verse [16](vref:Lev 27:16)\
*ᵇ3* That is, about 2/5 ounce (about 11.5 grams); also in verse [25](vref:Lev 27:25)

# Lev 27:4
*ᵃ4* That is, about 12 ounces (about 0.3 kilogram)

# Lev 27:5
*ᵃ5* That is, about 8 ounces (about 0.2 kilogram)\
*ᵇ5* That is, about 4 ounces (about 110 grams); also in verse [7](vref:Lev 27:7)

# Lev 27:6
*ᵃ6* That is, about 2 ounces (about 55 grams)\
*ᵇ6* That is, about 1 1/4 ounces (about 35 grams)

# Lev 27:7
*ᵃ7* That is, about 6 ounces (about 170 grams)

# Lev 27:16
*ᵃ16* That is, probably about 6 bushels (about 220 liters)

# Lev 27:21
*ᵃ21* Or *priest*

# Lev 27:26
*ᵃ26* The Hebrew word can include both male and female.

# Lev 27:28
*ᵃ28* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ.

# Lev 27:29
*ᵃ29* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Num 1:1
***The Census (1:1–54)***

# Num 2:1
***The Arrangement of the Tribal Camps (2:1–34)***

# Num 2:14
*ᵃ14* Many manuscripts of the Masoretic Text, Samaritan Pentateuch and Vulgate (see also [Num. 1:14](vref:Num 1:14)); most manuscripts of the Masoretic Text *Reuel*

# Num 3:1
***The Levites (3:1–51)***

# Num 3:9
*ᵃ9* Most manuscripts of the Masoretic Text; some manuscripts of the Masoretic Text, Samaritan Pentateuch and Septuagint (see also [Num. 8:16](vref:Num 8:16)) *to me*

# Num 3:28
*ᵃ28* Hebrew; some Septuagint manuscripts *8,300*

# Num 3:47
*ᵃ47* That is, about 2 ounces (about 55 grams)

# Num 3:50
*ᵃ50* That is, about 35 pounds (about 15.5 kilograms)

# Num 4:1
***The Kohathites (4:1–20)***

# Num 4:6
*ᵃ6* That is, dugongs; also in verses [8](vref:Num 4:8), [10](vref:Num 4:10), [11](vref:Num 4:11), [12](vref:Num 4:12), [14](vref:Num 4:14) and [25](vref:Num 4:25)

# Num 4:21
***The Gershonites (4:21–28)***

# Num 4:29
***The Merarites (4:29–33)***

# Num 4:34
***The Numbering of the Levite Clans (4:34–49)***

# Num 5:1
***The Purity of the Camp (5:1–4)***

# Num 5:2
*ᵃ2* Traditionally *leprosy*; the Hebrew word was used for various diseases affecting the skin—not necessarily leprosy.

# Num 5:5
***Restitution for Wrongs (5:5–10)***

# Num 5:6
*ᵃ6* Or *woman commits any wrong common to mankind*

# Num 5:11
***The Test for an Unfaithful Wife (5:11–31)***

# Num 5:15
*ᵃ15* That is, probably about 2 quarts (about 2 liters)

# Num 5:21
*ᵃ21* Or *causes you to have a miscarrying womb and barrenness*

# Num 5:22
*ᵃ22* Or *body and cause you to be barren and have a miscarrying womb*

# Num 5:27
*ᵃ27* Or *suffering; she will have barrenness and a miscarrying womb*

# Num 6:1
***The Nazirite (6:1–21)***

# Num 6:14
*ᵃ14* Traditionally *peace offering*; also in verses [17](vref:Num 6:17) and [18](vref:Num 6:18)

# Num 6:22
***The Priestly Blessing (6:22–27)***

# Num 7:1
***Offerings at the Dedication of the Tabernacle (7:1–89)***

# Num 7:13
*ᵃ13* That is, about 3 1/4 pounds (about 1.5 kilograms); also elsewhere in this chapter\
*ᵇ13* That is, about 1 3/4 pounds (about 0.8 kilogram); also elsewhere in this chapter

# Num 7:14
*ᵃ14* That is, about 4 ounces (about 110 grams); also elsewhere in this chapter

# Num 7:17
*ᵃ17* Traditionally *peace offering*; also elsewhere in this chapter

# Num 7:85
*ᵃ85* That is, about 60 pounds (about 28 kilograms)

# Num 7:86
*ᵃ86* That is, about 3 pounds (about 1.4 kilograms)

# Num 8:1
***Setting Up the Lamps (8:1–4)***

# Num 8:5
***The Setting Apart of the Levites (8:5–26)***

# Num 9:1
***The Passover (9:1–14)***

# Num 9:15
***The Cloud Above the Tabernacle (9:15–23)***

# Num 10:1
***The Silver Trumpets (10:1–10)***

# Num 10:10
*ᵃ10* Traditionally *peace offerings*

# Num 10:11
***The Israelites Leave Sinai (10:11–36)***

# Num 11:1
***Fire From the Lᴏʀᴅ (11:1–3)***

# Num 11:3
*ᵃ3* *Taberah* means *burning*.

# Num 11:4
***Quail From the Lᴏʀᴅ (11:4–35)***

# Num 11:25
*ᵃ25* Or *prophesied and continued to do so*

# Num 11:31
*ᵃ31* Or *They flew*\
*ᵇ31* Hebrew *two cubits* (about 1 meter)

# Num 11:32
*ᵃ32* That is, probably about 60 bushels (about 2.2 kiloliters)

# Num 11:34
*ᵃ34* *Kibroth Hattaavah* means *graves of craving*.

# Num 12:1
***Miriam and Aaron Oppose Moses (12:1–16)***

# Num 12:10
*ᵃ10* The Hebrew word was used for various diseases affecting the skin—not necessarily leprosy.

# Num 13:1
***Exploring Canaan (13:1–25)***

# Num 13:21
*ᵃ21* Or *toward the entrance to*

# Num 13:23
*ᵃ23* *Eshcol* means *cluster*; also in verse [24](vref:Num 13:24).

# Num 13:26
***Report on the Exploration (13:26–33)***

# Num 14:1
***The People Rebel (14:1–45)***

# Num 14:25
*ᵃ25* Hebrew *Yam Suph*; that is, Sea of Reeds

# Num 15:1
***Supplementary Offerings (15:1–21)***

# Num 15:4
*ᵃ4* That is, probably about 2 quarts (about 2 liters)\
*ᵇ4* That is, probably about 1 quart (about 1 liter); also in verse [5](vref:Num 15:5)

# Num 15:6
*ᵃ6* That is, probably about 4 quarts (about 4.5 liters)\
*ᵇ6* That is, probably about 1 1/4 quarts (about 1.2 liters); also in verse [7](vref:Num 15:7)

# Num 15:8
*ᵃ8* Traditionally *peace offering*

# Num 15:9
*ᵃ9* That is, probably about 6 quarts (about 6.5 liters)\
*ᵇ9* That is, probably about 2 quarts (about 2 liters); also in verse [10](vref:Num 15:10)

# Num 15:22
***Offerings for Unintentional Sins (15:22–31)***

# Num 15:32
***The Sabbath-Breaker Put to Death (15:32–36)***

# Num 15:37
***Tassels on Garments (15:37–41)***

# Num 16:1
***Korah, Dathan and Abiram (16:1–50)***\
\
*ᵃ1* Or *Peleth—took ⌞​men​⌟*

# Num 16:14
*ᵃ14* Or *you make slaves of*; or *you deceive*

# Num 16:30
*ᵃ30* Hebrew *Sheol*; also in verse [33](vref:Num 16:33)

# Num 17:1
***The Budding of Aaron’s Staff (17:1–13)***

# Num 18:1
***Duties of Priests and Levites (18:1–7)***

# Num 18:8
***Offerings for Priests and Levites (18:8–32)***

# Num 18:14
*ᵃ14* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ.

# Num 18:16
*ᵃ16* That is, about 2 ounces (about 55 grams)

# Num 19:1
***The Water of Cleansing (19:1–22)***

# Num 20:1
***Water From the Rock (20:1–13)***

# Num 20:13
*ᵃ13* *Meribah* means *quarreling*.

# Num 20:14
***Edom Denies Israel Passage (20:14–21)***

# Num 20:22
***The Death of Aaron (20:22–29)***

# Num 21:1
***Arad Destroyed (21:1–3)***

# Num 21:2
*ᵃ2* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verse [3](vref:Num 21:3).

# Num 21:3
*ᵃ3* *Hormah* means *destruction*.

# Num 21:4
***The Bronze Snake (21:4–9)***\
\
*ᵃ4* Hebrew *Yam Suph*; that is, Sea of Reeds

# Num 21:10
***The Journey to Moab (21:10–20)***

# Num 21:14
*ᵃ14* The meaning of the Hebrew for this phrase is uncertain.

# Num 21:15
*ᵃ14,15* Or *“I have been given from Suphah and the ravines / of the Arnon ¹⁵to*

# Num 21:21
***Defeat of Sihon and Og (21:21–35)***

# Num 22:1
***Balak Summons Balaam (22:1–20)***\
\
*ᵃ1* Hebrew *Jordan of Jericho*; possibly an ancient name for the Jordan River

# Num 22:5
*ᵃ5* That is, the Euphrates

# Num 22:21
***Balaam’s Donkey (22:21–41)***

# Num 22:32
*ᵃ32* The meaning of the Hebrew for this clause is uncertain.

# Num 23:1
***Balaam’s First Oracle (23:1–12)***

# Num 23:13
***Balaam’s Second Oracle (23:13–26)***

# Num 23:21
*ᵃ21* Or *He has not looked on Jacob’s offenses / or on the wrongs found in Israel.*

# Num 23:27
***Balaam’s Third Oracle (23:27–24:14)***

# Num 24:4
*ᵃ4* Hebrew *Shaddai*; also in verse [16](vref:Num 24:16)

# Num 24:15
***Balaam’s Fourth Oracle (24:15–19)***

# Num 24:17
*ᵃ17* Samaritan Pentateuch (see also [Jer. 48:45](vref:Jer 48:45)); the meaning of the word in the Masoretic Text is uncertain.\
*ᵇ17* Or possibly *Moab, / batter*\
*ᶜ17* Or *all the noisy boasters*

# Num 24:20
***Balaam’s Final Oracles (24:20–25)***

# Num 24:23
*ᵃ23* Masoretic Text; with a different word division of the Hebrew *A people will gather from the north.*

# Num 25:1
***Moab Seduces Israel (25:1–18)***

# Num 26:1
***The Second Census (26:1–65)***

# Num 26:3
*ᵃ3* Hebrew *Jordan of Jericho*; possibly an ancient name for the Jordan River; also in verse [63](vref:Num 26:63)

# Num 26:17
*ᵃ17* Samaritan Pentateuch and Syriac (see also [Gen. 46:16](vref:Gen 46:16)); Masoretic Text *Arod*

# Num 26:23
*ᵃ23* Samaritan Pentateuch, Septuagint, Vulgate and Syriac (see also [1 Chron. 7:1](vref:1 Chr 7:1)); Masoretic Text *through Puvah, the Punite*

# Num 26:39
*ᵃ39* A few manuscripts of the Masoretic Text, Samaritan Pentateuch, Vulgate and Syriac (see also Septuagint); most manuscripts of the Masoretic Text *Shephupham*

# Num 26:40
*ᵃ40* Samaritan Pentateuch and Vulgate (see also Septuagint); Masoretic Text does not have *through Ard*.

# Num 26:59
*ᵃ59* Or *Jochebed, a daughter of Levi, who was born to Levi*

# Num 27:1
***Zelophehad’s Daughters (27:1–11)***

# Num 27:12
***Joshua to Succeed Moses (27:12–23)***

# Num 27:18
*ᵃ18* Or *Spirit*

# Num 28:1
***Daily Offerings (28:1–8)***

# Num 28:5
*ᵃ5* That is, probably about 2 quarts (about 2 liters); also in verses [13](vref:Num 28:13), [21](vref:Num 28:21) and [29](vref:Num 28:29)\
*ᵇ5* That is, probably about 1 quart (about 1 liter); also in verses [7](vref:Num 28:7) and [14](vref:Num 28:14)

# Num 28:9
***Sabbath Offerings (28:9–10)***\
\
*ᵃ9* That is, probably about 4 quarts (about 4.5 liters); also in verses [12](vref:Num 28:12), [20](vref:Num 28:20) and [28](vref:Num 28:28)

# Num 28:11
***Monthly Offerings (28:11–15)***

# Num 28:12
*ᵃ12* That is, probably about 6 quarts (about 6.5 liters); also in verses [20](vref:Num 28:20) and [28](vref:Num 28:28)

# Num 28:14
*ᵃ14* That is, probably about 2 quarts (about 2 liters)\
*ᵇ14* That is, probably about 1 1/4 quarts (about 1.2 liters)

# Num 28:16
***The Passover (28:16–25)***

# Num 28:26
***Feast of Weeks (28:26–31)***

# Num 29:1
***Feast of Trumpets (29:1–6)***

# Num 29:3
*ᵃ3* That is, probably about 6 quarts (about 6.5 liters); also in verses [9](vref:Num 29:9) and [14](vref:Num 29:14)\
*ᵇ3* That is, probably about 4 quarts (about 4.5 liters); also in verses [9](vref:Num 29:9) and [14](vref:Num 29:14)

# Num 29:4
*ᵃ4* That is, probably about 2 quarts (about 2 liters); also in verses [10](vref:Num 29:10) and [15](vref:Num 29:15)

# Num 29:7
***Day of Atonement (29:7–11)***\
\
*ᵃ7* Or *must fast*

# Num 29:12
***Feast of Tabernacles (29:12–40)***

# Num 29:39
*ᵃ39* Traditionally *peace offerings*

# Num 30:1
***Vows (30:1–16)***

# Num 31:1
***Vengeance on the Midianites (31:1–24)***

# Num 31:12
*ᵃ12* Hebrew *Jordan of Jericho*; possibly an ancient name for the Jordan River

# Num 31:25
***Dividing the Spoils (31:25–54)***

# Num 31:52
*ᵃ52* That is, about 420 pounds (about 190 kilograms)

# Num 32:1
***The Transjordan Tribes (32:1–42)***

# Num 32:41
*ᵃ41* Or *them the settlements of Jair*

# Num 33:1
***Stages in Israel’s Journey (33:1–56)***

# Num 33:8
*ᵃ8* Many manuscripts of the Masoretic Text, Samaritan Pentateuch and Vulgate; most manuscripts of the Masoretic Text *left from before Hahiroth*

# Num 33:10
*ᵃ10* Hebrew *Yam Suph*; that is, Sea of Reeds; also in verse [11](vref:Num 33:11)

# Num 33:45
*ᵃ45* That is, Iye Abarim

# Num 33:48
*ᵃ48* Hebrew *Jordan of Jericho*; possibly an ancient name for the Jordan River; also in verse [50](vref:Num 33:50)

# Num 34:1
***Boundaries of Canaan (34:1–29)***

# Num 34:3
*ᵃ3* That is, the Dead Sea; also in verse [12](vref:Num 34:12)

# Num 34:4
*ᵃ4* Hebrew *Akrabbim*

# Num 34:5
*ᵃ5* That is, the Mediterranean; also in verses [6](vref:Num 34:6) and [7](vref:Num 34:7)

# Num 34:8
*ᵃ8* Or *to the entrance to*

# Num 34:11
*ᵃ11* That is, Galilee

# Num 34:15
*ᵃ15* *Jordan of Jericho* was possibly an ancient name for the Jordan River.

# Num 35:1
***Towns for the Levites (35:1–5)***\
\
*ᵃ1* Hebrew *Jordan of Jericho*; possibly an ancient name for the Jordan River

# Num 35:4
*ᵃ4* Hebrew *a thousand cubits* (about 450 meters)

# Num 35:5
*ᵃ5* Hebrew *two thousand cubits* (about 900 meters)

# Num 35:6
***Cities of Refuge (35:6–34)***

# Num 36:1
***Inheritance of Zelophehad’s Daughters (36:1–13)***

# Num 36:13
*ᵃ13* Hebrew *Jordan of Jericho*; possibly an ancient name for the Jordan River

# Deut 1:1
***The Command to Leave Horeb (1:1–8)***

# Deut 1:9
***The Appointment of Leaders (1:9–18)***

# Deut 1:19
***Spies Sent Out (1:19–25)***

# Deut 1:26
***Rebellion Against the Lᴏʀᴅ (1:26–46)***

# Deut 1:40
*ᵃ40* Hebrew *Yam Suph*; that is, Sea of Reeds

# Deut 2:1
***Wanderings in the Desert (2:1–23)***\
\
*ᵃ1* Hebrew *Yam Suph*; that is, Sea of Reeds

# Deut 2:23
*ᵃ23* That is, Crete

# Deut 2:24
***Defeat of Sihon King of Heshbon (2:24–37)***

# Deut 2:34
*ᵃ34* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Deut 3:1
***Defeat of Og King of Bashan (3:1–11)***

# Deut 3:6
*ᵃ6* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Deut 3:11
*ᵃ11* Or *sarcophagus*\
*ᵇ11* Hebrew *nine cubits long and four cubits wide* (about 4 meters long and 1.8 meters wide)

# Deut 3:12
***Division of the Land (3:12–20)***

# Deut 3:14
*ᵃ14* Or *called the settlements of Jair*

# Deut 3:17
*ᵃ17* That is, the Dead Sea

# Deut 3:21
***Moses Forbidden to Cross the Jordan (3:21–29)***

# Deut 4:1
***Obedience Commanded (4:1–14)***

# Deut 4:15
***Idolatry Forbidden (4:15–31)***

# Deut 4:32
***The Lᴏʀᴅ Is God (4:32–40)***

# Deut 4:33
*ᵃ33* Or *of a god*

# Deut 4:41
***Cities of Refuge (4:41–43)***

# Deut 4:44
***Introduction to the Law (4:44–49)***

# Deut 4:48
*ᵃ48* Hebrew; Syriac (see also [Deut. 3:9](vref:Deut 3:9)) *Sirion*

# Deut 4:49
*ᵃ49* That is, the Dead Sea

# Deut 5:1
***The Ten Commandments (5:1–33)***

# Deut 5:7
*ᵃ7* Or *besides*

# Deut 6:1
***Love the Lᴏʀᴅ Your God (6:1–25)***

# Deut 6:4
*ᵃ4* Or *The Lᴏʀᴅ our God is one Lᴏʀᴅ*; or *The Lᴏʀᴅ is our God, the Lᴏʀᴅ is one*; or *The Lᴏʀᴅ is our God, the Lᴏʀᴅ alone*

# Deut 7:1
***Driving Out the Nations (7:1–26)***

# Deut 7:2
*ᵃ2* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verse [26](vref:Deut 7:26).

# Deut 7:5
*ᵃ5* That is, symbols of the goddess Asherah; here and elsewhere in Deuteronomy

# Deut 8:1
***Do Not Forget the Lᴏʀᴅ (8:1–20)***

# Deut 9:1
***Not Because of Israel’s Righteousness (9:1–6)***

# Deut 9:7
***The Golden Calf (9:7–29)***

# Deut 9:15
*ᵃ15* Or *And I had the two tablets of the covenant with me, one in each hand*

# Deut 10:1
***Tablets Like the First Ones (10:1–11)***\
\
*ᵃ1* That is, an ark

# Deut 10:12
***Fear the Lᴏʀᴅ (10:12–22)***

# Deut 11:1
***Love and Obey the Lᴏʀᴅ (11:1–32)***

# Deut 11:4
*ᵃ4* Hebrew *Yam Suph*; that is, Sea of Reeds

# Deut 11:24
*ᵃ24* That is, the Mediterranean

# Deut 11:30
*ᵃ30* Or *Jordan, westward*

# Deut 12:1
***The One Place of Worship (12:1–32)***

# Deut 13:1
***Worshiping Other Gods (13:1–18)***

# Deut 13:15
*ᵃ15* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Deut 13:17
*ᵃ17* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Deut 14:1
***Clean and Unclean Food (14:1–21)***

# Deut 14:5
*ᵃ5* The precise identification of some of the birds and animals in this chapter is uncertain.

# Deut 14:7
*ᵃ7* That is, the hyrax or rock badger

# Deut 14:22
***Tithes (14:22–29)***

# Deut 15:1
***The Year for Canceling Debts (15:1–11)***

# Deut 15:12
***Freeing Servants (15:12–18)***

# Deut 15:19
***The Firstborn Animals (15:19–23)***

# Deut 16:1
***Passover (16:1–8)***

# Deut 16:6
*ᵃ6* Or *down, at the time of day*

# Deut 16:9
***Feast of Weeks (16:9–12)***

# Deut 16:13
***Feast of Tabernacles (16:13–17)***

# Deut 16:18
***Judges (16:18–20)***

# Deut 16:21
***Worshiping Other Gods (16:21–17:7)***\
\
*ᵃ21* Or *Do not plant any tree dedicated to Asherah*

# Deut 17:8
***Law Courts (17:8–13)***

# Deut 17:14
***The King (17:14–20)***

# Deut 18:1
***Offerings for Priests and Levites (18:1–8)***

# Deut 18:9
***Detestable Practices (18:9–13)***

# Deut 18:10
*ᵃ10* Or *who makes his son or daughter pass through*

# Deut 18:14
***The Prophet (18:14–22)***

# Deut 19:1
***Cities of Refuge (19:1–14)***

# Deut 19:15
***Witnesses (19:15–21)***

# Deut 20:1
***Going to War (20:1–20)***

# Deut 20:17
*ᵃ17* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Deut 20:19
*ᵃ19* Or *down to use in the siege, for the fruit trees are for the benefit of man.*

# Deut 21:1
***Atonement for an Unsolved Murder (21:1–9)***

# Deut 21:10
***Marrying a Captive Woman (21:10–14)***

# Deut 21:15
***The Right of the Firstborn (21:15–17)***

# Deut 21:18
***A Rebellious Son (21:18–21)***

# Deut 21:22
***Various Laws (21:22–22:12)***

# Deut 22:9
*ᵃ9* Or *be forfeited to the sanctuary*

# Deut 22:13
***Marriage Violations (22:13–30)***

# Deut 22:19
*ᵃ19* That is, about 2 1/2 pounds (about 1 kilogram)

# Deut 22:29
*ᵃ29* That is, about 1 1/4 pounds (about 0.6 kilogram)

# Deut 23:1
***Exclusion From the Assembly (23:1–8)***

# Deut 23:2
*ᵃ2* Or *one of illegitimate birth*

# Deut 23:4
*ᵃ4* That is, Northwest Mesopotamia

# Deut 23:9
***Uncleanness in the Camp (23:9–14)***

# Deut 23:15
***Miscellaneous Laws (23:15–25:19)***

# Deut 23:18
*ᵃ18* Hebrew *of a dog*

# Deut 24:8
*ᵃ8* The Hebrew word was used for various diseases affecting the skin—not necessarily leprosy.

# Deut 26:1
***Firstfruits and Tithes (26:1–15)***

# Deut 26:16
***Follow the Lᴏʀᴅ’s Commands (26:16–19)***

# Deut 27:1
***The Altar on Mount Ebal (27:1–8)***

# Deut 27:7
*ᵃ7* Traditionally *peace offerings*

# Deut 27:9
***Curses From Mount Ebal (27:9–26)***

# Deut 28:1
***Blessings for Obedience (28:1–14)***

# Deut 28:15
***Curses for Disobedience (28:15–68)***

# Deut 28:20
*ᵃ20* Hebrew *me*

# Deut 29:1
***Renewal of the Covenant (29:1–29)***

# Deut 29:19
*ᵃ19* Or *way, in order to add drunkenness to thirst.”*

# Deut 30:1
***Prosperity After Turning to the Lᴏʀᴅ (30:1–10)***

# Deut 30:3
*ᵃ3* Or *will bring you back from captivity*

# Deut 30:11
***The Offer of Life or Death (30:11–20)***

# Deut 31:1
***Joshua to Succeed Moses (31:1–8)***

# Deut 31:9
***The Reading of the Law (31:9–13)***

# Deut 31:14
***Israel’s Rebellion Predicted (31:14–29)***

# Deut 31:30
***The Song of Moses (31:30–32:47)***

# Deut 32:5
*ᵃ5* Or *Corrupt are they and not his children, / a generation warped and twisted to their shame*

# Deut 32:6
*ᵃ6* Or *Father, who bought you*

# Deut 32:8
*ᵃ8* Masoretic Text; Dead Sea Scrolls (see also Septuagint) *sons of God*

# Deut 32:15
*ᵃ15* *Jeshurun* means *the upright one*, that is, Israel.

# Deut 32:22
*ᵃ22* Hebrew *to Sheol*

# Deut 32:43
*ᵃ43* Or *Make his people rejoice, O nations*\
*ᵇ43* Masoretic Text; Dead Sea Scrolls (see also Septuagint) *people, / and let all the angels worship him /*

# Deut 32:44
*ᵃ44* Hebrew *Hoshea*, a variant of *Joshua*

# Deut 32:48
***Moses to Die on Mount Nebo (32:48–52)***

# Deut 33:1
***Moses Blesses the Tribes (33:1–29)***

# Deut 33:2
*ᵃ2* Or *from*\
*ᵇ2* The meaning of the Hebrew for this phrase is uncertain.

# Deut 33:5
*ᵃ5* *Jeshurun* means *the upright one*, that is, Israel; also in verse [26](vref:Deut 33:26).

# Deut 33:6
*ᵃ6* Or *but let*

# Deut 33:16
*ᵃ16* Or *of the one separated from*

# Deut 33:29
*ᵃ29* Or *will tread upon their bodies*

# Deut 34:1
***The Death of Moses (34:1–12)***

# Deut 34:2
*ᵃ2* That is, the Mediterranean

# Deut 34:6
*ᵃ6* Or *He was buried*

# Deut 34:9
*ᵃ9* Or *Spirit*

# Josh 1:1
***The Lᴏʀᴅ Commands Joshua (1:1–18)***

# Josh 1:4
*ᵃ4* That is, the Mediterranean

# Josh 2:1
***Rahab and the Spies (2:1–24)***\
\
*ᵃ1* Or possibly *an innkeeper*

# Josh 2:10
*ᵃ10* Hebrew *Yam Suph*; that is, Sea of Reeds\
*ᵇ10* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Josh 3:1
***Crossing the Jordan (3:1–4:24)***

# Josh 3:4
*ᵃ4* Hebrew *about two thousand cubits* (about 900 meters)

# Josh 3:16
*ᵃ16* That is, the Dead Sea

# Josh 4:9
*ᵃ9* Or *Joshua also set up twelve stones*

# Josh 4:23
*ᵃ23* Hebrew *Yam Suph*; that is, Sea of Reeds

# Josh 5:1
***Circumcision at Gilgal (5:1–12)***

# Josh 5:3
*ᵃ3* *Gibeath Haaraloth* means *hill of foreskins*.

# Josh 5:9
*ᵃ9* *Gilgal* sounds like the Hebrew for *roll*.

# Josh 5:12
*ᵃ12* Or *the day*

# Josh 5:13
***The Fall of Jericho (5:13–6:27)***

# Josh 5:14
*ᵃ14* Or *lord*

# Josh 6:17
*ᵃ17* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verses [18](vref:Josh 6:18) and [21](vref:Josh 6:21).\
*ᵇ17* Or possibly *innkeeper*; also in verses [22](vref:Josh 6:22) and [25](vref:Josh 6:25)

# Josh 7:1
***Achan’s Sin (7:1–26)***\
\
*ᵃ1* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verses [11](vref:Josh 7:11), [12](vref:Josh 7:12), [13](vref:Josh 7:13) and [15](vref:Josh 7:15).\
*ᵇ1* See Septuagint and [1 Chron. 2:6](vref:1 Chr 2:6); Hebrew *Zabdi*; also in verses [17](vref:Josh 7:17) and [18](vref:Josh 7:18).

# Josh 7:5
*ᵃ5* Or *as far as Shebarim*

# Josh 7:19
*ᵃ19* A solemn charge to tell the truth\
*ᵇ19* Or *and confess to him*

# Josh 7:21
*ᵃ21* Hebrew *Shinar*\
*ᵇ21* That is, about 5 pounds (about 2.3 kilograms)\
*ᶜ21* That is, about 1 1/4 pounds (about 0.6 kilogram)

# Josh 7:26
*ᵃ26* *Achor* means *trouble*.

# Josh 8:1
***Ai Destroyed (8:1–29)***

# Josh 8:26
*ᵃ26* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Josh 8:30
***The Covenant Renewed at Mount Ebal (8:30–35)***

# Josh 8:31
*ᵃ31* Traditionally *peace offerings*

# Josh 9:1
***The Gibeonite Deception (9:1–27)***\
\
*ᵃ1* That is, the Mediterranean

# Josh 9:4
*ᵃ4* Most Hebrew manuscripts; some Hebrew manuscripts, Vulgate and Syriac (see also Septuagint) *They prepared provisions and loaded their donkeys*

# Josh 10:1
***The Sun Stands Still (10:1–15)***\
\
*ᵃ1* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verses [28](vref:Josh 10:28), [35](vref:Josh 10:35), [37](vref:Josh 10:37), [39](vref:Josh 10:39) and [40](vref:Josh 10:40).

# Josh 10:13
*ᵃ13* Or *nation triumphed over*

# Josh 10:16
***Five Amorite Kings Killed (10:16–28)***

# Josh 10:29
***Southern Cities Conquered (10:29–43)***

# Josh 11:1
***Northern Kings Defeated (11:1–23)***

# Josh 11:2
*ᵃ2* Or *in the heights of Dor*

# Josh 11:11
*ᵃ11* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verses [12](vref:Josh 11:12), [20](vref:Josh 11:20) and [21](vref:Josh 11:21).

# Josh 12:1
***List of Defeated Kings (12:1–24)***

# Josh 12:3
*ᵃ3* That is, Galilee\
*ᵇ3* That is, the Dead Sea

# Josh 12:23
*ᵃ23* Or *in the heights of Dor*

# Josh 13:1
***Land Still to Be Taken (13:1–7)***

# Josh 13:5
*ᵃ5* That is, the area of Byblos\
*ᵇ5* Or *to the entrance to*

# Josh 13:8
***Division of the Land East of the Jordan (13:8–33)***\
\
*ᵃ8* Hebrew *With it* (that is, with the other half of Manasseh)

# Josh 13:27
*ᵃ27* That is, Galilee

# Josh 14:1
***Division of the Land West of the Jordan (14:1–5)***

# Josh 14:6
***Hebron Given to Caleb (14:6–15)***

# Josh 14:9
*ᵃ9* [Deut. 1:36](vref:Deut 1:36)

# Josh 15:1
***Allotment for Judah (15:1–63)***

# Josh 15:2
*ᵃ2* That is, the Dead Sea; also in verse [5](vref:Josh 15:5)

# Josh 15:3
*ᵃ3* Hebrew *Akrabbim*

# Josh 15:4
*ᵃ4* Hebrew *your*

# Josh 15:12
*ᵃ12* That is, the Mediterranean; also in verse [47](vref:Josh 15:47)

# Josh 15:18
*ᵃ18* Hebrew and some Septuagint manuscripts; other Septuagint manuscripts (see also note at [Judges 1:14](vref:Judg 1:14)) *Othniel, he urged her*

# Josh 15:36
*ᵃ36* Or *Gederah and Gederothaim*

# Josh 16:1
***Allotment for Ephraim and Manasseh (16:1–17:18)***\
\
*ᵃ1* *Jordan of Jericho* was possibly an ancient name for the Jordan River.

# Josh 16:2
*ᵃ2* Septuagint; Hebrew *Bethel to Luz*

# Josh 17:11
*ᵃ11* That is, Naphoth Dor

# Josh 18:1
***Division of the Rest of the Land (18:1–10)***

# Josh 18:11
***Allotment for Benjamin (18:11–28)***

# Josh 18:18
*ᵃ18* Septuagint; Hebrew *slope facing the Arabah*

# Josh 18:19
*ᵃ19* That is, the Dead Sea

# Josh 19:1
***Allotment for Simeon (19:1–9)***

# Josh 19:2
*ᵃ2* Or *Beersheba, Sheba*; [1 Chron. 4:28](vref:1 Chr 4:28) does not have *Sheba*.

# Josh 19:10
***Allotment for Zebulun (19:10–16)***

# Josh 19:17
***Allotment for Issachar (19:17–23)***

# Josh 19:24
***Allotment for Asher (19:24–31)***

# Josh 19:28
*ᵃ28* Some Hebrew manuscripts (see also [Joshua 21:30](vref:Josh 21:30)); most Hebrew manuscripts *Ebron*

# Josh 19:32
***Allotment for Naphtali (19:32–39)***

# Josh 19:34
*ᵃ34* Septuagint; Hebrew *west, and Judah, the Jordan,*

# Josh 19:40
***Allotment for Dan (19:40–48)***

# Josh 19:49
***Allotment for Joshua (19:49–51)***

# Josh 19:50
*ᵃ50* Also known as *Timnath Heres* (see [Judges 2:9](vref:Judg 2:9))

# Josh 20:1
***Cities of Refuge (20:1–9)***

# Josh 20:8
*ᵃ8* *Jordan of Jericho* was possibly an ancient name for the Jordan River.

# Josh 21:1
***Towns for the Levites (21:1–45)***

# Josh 22:1
***Eastern Tribes Return Home (22:1–34)***

# Josh 22:20
*ᵃ20* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Josh 22:23
*ᵃ23* Traditionally *peace offerings*; also in verse [27](vref:Josh 22:27)

# Josh 23:1
***Joshua’s Farewell to the Leaders (23:1–16)***

# Josh 23:4
*ᵃ4* That is, the Mediterranean

# Josh 24:1
***The Covenant Renewed at Shechem (24:1–27)***

# Josh 24:2
*ᵃ2* That is, the Euphrates; also in verses [3](vref:Josh 24:3), [14](vref:Josh 24:14) and [15](vref:Josh 24:15)

# Josh 24:6
*ᵃ6* Or *charioteers*\
*ᵇ6* Hebrew *Yam Suph*; that is, Sea of Reeds

# Josh 24:28
***Buried in the Promised Land (24:28–33)***

# Josh 24:30
*ᵃ30* Also known as *Timnath Heres* (see [Judges 2:9](vref:Judg 2:9))

# Josh 24:32
*ᵃ32* Hebrew *hundred kesitahs*; a kesitah was a unit of money of unknown weight and value.

# Judg 1:1
***Israel Fights the Remaining Canaanites (1:1–36)***

# Judg 1:14
*ᵃ14* Hebrew; Septuagint and Vulgate *Othniel, he urged her*

# Judg 1:16
*ᵃ16* That is, Jericho

# Judg 1:17
*ᵃ17* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.\
*ᵇ17* *Hormah* means *destruction*.

# Judg 1:18
*ᵃ18* Hebrew; Septuagint *Judah did not take*

# Judg 1:36
*ᵃ36* Hebrew *Akrabbim*

# Judg 2:1
***The Angel of the Lᴏʀᴅ at Bokim (2:1–5)***

# Judg 2:5
*ᵃ5* *Bokim* means *weepers*.

# Judg 2:6
***Disobedience and Defeat (2:6–3:6)***

# Judg 2:9
*ᵃ9* Also known as *Timnath Serah* (see [Joshua 19:50](vref:Josh 19:50) and [24:30](vref:Josh 24:30))

# Judg 2:16
*ᵃ16* Or *leaders*; similarly in verses [17–19](vref:Judg 2:17-19)

# Judg 3:3
*ᵃ3* Or *to the entrance to*

# Judg 3:7
***Othniel (3:7–11)***

# Judg 3:8
*ᵃ8* That is, Northwest Mesopotamia

# Judg 3:10
*ᵃ10* Or *leader*

# Judg 3:12
***Ehud (3:12–30)***

# Judg 3:13
*ᵃ13* That is, Jericho

# Judg 3:16
*ᵃ16* Hebrew *a cubit* (about 0.5 meter)

# Judg 3:19
*ᵃ19* Or *the stone quarries*; also in verse [26](vref:Judg 3:26)

# Judg 3:20
*ᵃ20* The meaning of the Hebrew for this phrase is uncertain.

# Judg 3:23
*ᵃ23* The meaning of the Hebrew for this word is uncertain.

# Judg 3:31
***Shamgar (3:31)***

# Judg 4:1
***Deborah (4:1–24)***

# Judg 4:4
*ᵃ4* Traditionally *judging*

# Judg 4:9
*ᵃ9* Or *But on the expedition you are undertaking*

# Judg 4:11
*ᵃ11* Or *father-in-law*

# Judg 5:1
***The Song of Deborah (5:1–31)***

# Judg 5:3
*ᵃ3* Or *of*\
*ᵇ3* Or */ with song I will praise*

# Judg 5:7
*ᵃ7* Or *Warriors*\
*ᵇ7* Or *you*

# Judg 5:11
*ᵃ11* Or *archers*; the meaning of the Hebrew for this word is uncertain.\
*ᵇ11* Or *villagers*

# Judg 5:16
*ᵃ16* Or *saddlebags*

# Judg 6:1
***Gideon (6:1–40)***

# Judg 6:15
*ᵃ15* Or *sir*

# Judg 6:19
*ᵃ19* That is, probably about 3/5 bushel (about 22 liters)

# Judg 6:25
*ᵃ25* Or *Take a full-grown, mature bull from your father’s herd*\
*ᵇ25* That is, a symbol of the goddess Asherah; here and elsewhere in Judges

# Judg 6:26
*ᵃ26* Or *build with layers of stone an*\
*ᵇ26* Or *full-grown*; also in verse [28](vref:Judg 6:28)

# Judg 6:32
*ᵃ32* *Jerub-Baal* means *let Baal contend*.

# Judg 7:1
***Gideon Defeats the Midianites (7:1–25)***

# Judg 8:1
***Zebah and Zalmunna (8:1–21)***

# Judg 8:8
*ᵃ8* Hebrew *Penuel*, a variant of *Peniel*; also in verses [9](vref:Judg 8:9) and [17](vref:Judg 8:17)

# Judg 8:22
***Gideon’s Ephod (8:22–27)***

# Judg 8:26
*ᵃ26* That is, about 43 pounds (about 19.5 kilograms)

# Judg 8:28
***Gideon’s Death (8:28–35)***

# Judg 9:1
***Abimelech (9:1–57)***

# Judg 9:4
*ᵃ4* That is, about 1 3/4 pounds (about 0.8 kilogram)

# Judg 9:29
*ᵃ29* Septuagint; Hebrew *him.” Then he said to Abimelech, “Call out your whole army!”*

# Judg 9:39
*ᵃ39* Or *Gaal went out in the sight of*

# Judg 10:1
***Tola (10:1–2)***

# Judg 10:2
*ᵃ2* Traditionally *judged*; also in verse [3](vref:Judg 10:3)

# Judg 10:3
***Jair (10:3–5)***

# Judg 10:4
*ᵃ4* Or *called the settlements of Jair*

# Judg 10:6
***Jephthah (10:6–11:40)***

# Judg 10:12
*ᵃ12* Hebrew; some Septuagint manuscripts *Midianites*

# Judg 11:16
*ᵃ16* Hebrew *Yam Suph*; that is, Sea of Reeds

# Judg 11:20
*ᵃ20* Or *however, would not make an agreement for Israel*

# Judg 11:27
*ᵃ27* Or *Ruler*

# Judg 12:1
***Jephthah and Ephraim (12:1–7)***

# Judg 12:7
*ᵃ7* Traditionally *judged*; also in verses [8–14](vref:Judg 12:8-14)

# Judg 12:8
***Ibzan, Elon and Abdon (12:8–15)***

# Judg 13:1
***The Birth of Samson (13:1–25)***

# Judg 13:18
*ᵃ18* Or *is wonderful*

# Judg 14:1
***Samson’s Marriage (14:1–20)***

# Judg 14:15
*ᵃ15* Some Septuagint manuscripts and Syriac; Hebrew *seventh*

# Judg 15:1
***Samson’s Vengeance on the Philistines (15:1–20)***

# Judg 15:16
*ᵃ16* Or *made a heap or two*; the Hebrew for *donkey* sounds like the Hebrew for *heap*.

# Judg 15:17
*ᵃ17* *Ramath Lehi* means *jawbone hill*.

# Judg 15:19
*ᵃ19* *En Hakkore* means *caller’s spring*.

# Judg 15:20
*ᵃ20* Traditionally *judged*

# Judg 16:1
***Samson and Delilah (16:1–22)***

# Judg 16:5
*ᵃ5* That is, about 28 pounds (about 13 kilograms)

# Judg 16:7
*ᵃ7* Or *bowstrings*; also in verses [8](vref:Judg 16:8) and [9](vref:Judg 16:9)

# Judg 16:14
*ᵃ13,14* Some Septuagint manuscripts; Hebrew *“⌞​I can​⌟ if you weave the seven braids of my head into the fabric ⌞​on the loom​⌟.” ¹⁴So she*

# Judg 16:19
*ᵃ19* Hebrew; some Septuagint manuscripts *and he began to weaken*

# Judg 16:23
***The Death of Samson (16:23–31)***

# Judg 16:31
*ᵃ31* Traditionally *judged*

# Judg 17:1
***Micah’s Idols (17:1–13)***

# Judg 17:2
*ᵃ2* That is, about 28 pounds (about 13 kilograms)

# Judg 17:4
*ᵃ4* That is, about 5 pounds (about 2.3 kilograms)

# Judg 17:8
*ᵃ8* Or *To carry on his profession*

# Judg 17:10
*ᵃ10* That is, about 4 ounces (about 110 grams)

# Judg 18:1
***Danites Settle in Laish (18:1–31)***

# Judg 18:7
*ᵃ7* The meaning of the Hebrew for this clause is uncertain.\
*ᵇ7* Hebrew; some Septuagint manuscripts *with the Arameans*

# Judg 18:12
*ᵃ12* *Mahaneh Dan* means *Dan’s camp*.

# Judg 18:30
*ᵃ30* An ancient Hebrew scribal tradition, some Septuagint manuscripts and Vulgate; Masoretic Text *Manasseh*

# Judg 19:1
***A Levite and His Concubine (19:1–30)***

# Judg 20:1
***Israelites Fight the Benjamites (20:1–48)***

# Judg 20:10
*ᵃ10* One Hebrew manuscript; most Hebrew manuscripts *Geba*, a variant of *Gibeah*

# Judg 20:18
*ᵃ18* Or *to the house of God*; also in verse [26](vref:Judg 20:26)

# Judg 20:26
*ᵃ26* Traditionally *peace offerings*

# Judg 20:33
*ᵃ33* Some Septuagint manuscripts and Vulgate; the meaning of the Hebrew for this word is uncertain.\
*ᵇ33* Hebrew *Geba*, a variant of *Gibeah*

# Judg 20:43
*ᵃ43* The meaning of the Hebrew for this word is uncertain.

# Judg 21:1
***Wives for the Benjamites (21:1–25)***

# Judg 21:2
*ᵃ2* Or *to the house of God*

# Judg 21:4
*ᵃ4* Traditionally *peace offerings*

# Ruth 1:1
***Naomi and Ruth (1:1–22)***\
\
*ᵃ1* Traditionally *judged*

# Ruth 1:20
*ᵃ20* *Naomi* means *pleasant*; also in verse [21](vref:Ruth 1:21).\
*ᵇ20* *Mara* means *bitter*.\
*ᶜ20* Hebrew *Shaddai*; also in verse [21](vref:Ruth 1:21)

# Ruth 1:21
*ᵃ21* Or *has testified against*

# Ruth 2:1
***Ruth Meets Boaz (2:1–23)***

# Ruth 2:17
*ᵃ17* That is, probably about 3/5 bushel (about 22 liters)

# Ruth 3:1
***Ruth and Boaz at the Threshing Floor (3:1–18)***\
\
*ᵃ1* Hebrew *find rest* (see [Ruth 1:9](vref:Ruth 1:9))

# Ruth 3:15
*ᵃ15* Most Hebrew manuscripts; many Hebrew manuscripts, Vulgate and Syriac *she*

# Ruth 4:1
***Boaz Marries Ruth (4:1–12)***

# Ruth 4:4
*ᵃ4* Many Hebrew manuscripts, Septuagint, Vulgate and Syriac; most Hebrew manuscripts *he*

# Ruth 4:5
*ᵃ5* Hebrew; Vulgate and Syriac *Naomi, you acquire Ruth the Moabitess,*

# Ruth 4:13
***The Genealogy of David (4:13–22)***

# Ruth 4:20
*ᵃ20* A few Hebrew manuscripts, some Septuagint manuscripts and Vulgate (see also verse [21](vref:Ruth 4:21) and Septuagint of [1 Chron. 2:11](vref:1 Chr 2:11)); most Hebrew manuscripts *Salma*

# 1 Sam 1:1
***The Birth of Samuel (1:1–20)***\
\
*ᵃ1* Or *from Ramathaim Zuphim*

# 1 Sam 1:9
*ᵃ9* That is, tabernacle

# 1 Sam 1:20
*ᵃ20* *Samuel* sounds like the Hebrew for *heard of God*.

# 1 Sam 1:21
***Hannah Dedicates Samuel (1:21–28)***

# 1 Sam 1:23
*ᵃ23* Masoretic Text; Dead Sea Scrolls, Septuagint and Syriac *your*

# 1 Sam 1:24
*ᵃ24* Dead Sea Scrolls, Septuagint and Syriac; Masoretic Text *with three bulls*\
*ᵇ24* That is, probably about 3/5 bushel (about 22 liters)

# 1 Sam 2:1
***Hannah’s Prayer (2:1–11)***\
\
*ᵃ1* *Horn* here symbolizes strength; also in verse [10](vref:1 Sam 2:10).

# 1 Sam 2:2
*ᵃ2* Or *no Holy One*

# 1 Sam 2:6
*ᵃ6* Hebrew *Sheol*

# 1 Sam 2:12
***Eli’s Wicked Sons (2:12–26)***

# 1 Sam 2:17
*ᵃ17* Or *men*

# 1 Sam 2:25
*ᵃ25* Or *the judges*

# 1 Sam 2:27
***Prophecy Against the House of Eli (2:27–36)***

# 1 Sam 2:29
*ᵃ29* The Hebrew is plural.

# 1 Sam 3:1
***The Lᴏʀᴅ Calls Samuel (3:1–4:1a)***

# 1 Sam 3:3
*ᵃ3* That is, tabernacle

# 1 Sam 3:13
*ᵃ13* Masoretic Text; an ancient Hebrew scribal tradition and Septuagint *sons blasphemed God*

# 1 Sam 4:1
***The Philistines Capture the Ark (4:1b–11)***

# 1 Sam 4:3
*ᵃ3* Or *he*

# 1 Sam 4:12
***Death of Eli (4:12–22)***

# 1 Sam 4:18
*ᵃ18* Traditionally *judged*

# 1 Sam 4:21
*ᵃ21* *Ichabod* means *no glory*.

# 1 Sam 5:1
***The Ark in Ashdod and Ekron (5:1–12)***

# 1 Sam 5:6
*ᵃ6* Hebrew; Septuagint and Vulgate *tumors. And rats appeared in their land, and death and destruction were throughout the city*

# 1 Sam 5:9
*ᵃ9* Or *with tumors in the groin* (see Septuagint)

# 1 Sam 5:11
*ᵃ11* Or *he*

# 1 Sam 6:1
***The Ark Returned to Israel (6:1–7:1)***

# 1 Sam 6:6
*ᵃ6* That is, God

# 1 Sam 6:18
*ᵃ18* A few Hebrew manuscripts (see also Septuagint); most Hebrew manuscripts *villages as far as Greater Abel, where*

# 1 Sam 6:19
*ᵃ19* A few Hebrew manuscripts; most Hebrew manuscripts and Septuagint *50,070*

# 1 Sam 7:2
***Samuel Subdues the Philistines at Mizpah (7:2–17)***

# 1 Sam 7:6
*ᵃ6* Traditionally *judge*

# 1 Sam 7:12
*ᵃ12* *Ebenezer* means *stone of help*.

# 1 Sam 8:1
***Israel Asks for a King (8:1–22)***

# 1 Sam 8:5
*ᵃ5* Traditionally *judge*; also in verses [6](vref:1 Sam 8:6) and [20](vref:1 Sam 8:20)

# 1 Sam 8:16
*ᵃ16* Septuagint; Hebrew *young men*

# 1 Sam 9:1
***Samuel Anoints Saul (9:1–10:8)***

# 1 Sam 9:8
*ᵃ8* That is, about 1/10 ounce (about 3 grams)

# 1 Sam 10:1
*ᵃ1* Hebrew; Septuagint and Vulgate *over his people Israel? You will reign over the Lᴏʀᴅ’s people and save them from the power of their enemies round about. And this will be a sign to you that the Lᴏʀᴅ has anointed you leader over his inheritance:*

# 1 Sam 10:8
*ᵃ8* Traditionally *peace offerings*

# 1 Sam 10:9
***Saul Made King (10:9–27)***

# 1 Sam 11:1
***Saul Rescues the City of Jabesh (11:1–11)***

# 1 Sam 11:12
***Saul Confirmed as King (11:12–15)***

# 1 Sam 11:15
*ᵃ15* Traditionally *peace offerings*

# 1 Sam 12:1
***Samuel’s Farewell Speech (12:1–25)***

# 1 Sam 12:11
*ᵃ11* Also called *Gideon*\
*ᵇ11* Some Septuagint manuscripts and Syriac; Hebrew *Bedan*\
*ᶜ11* Hebrew; some Septuagint manuscripts and Syriac *Samson*

# 1 Sam 13:1
***Samuel Rebukes Saul (13:1–15)***\
\
*ᵃ1* A few late manuscripts of the Septuagint; Hebrew does not have *thirty*.\
*ᵇ1* See the round number in [Acts 13:21](vref:Acts 13:21); Hebrew does not have *forty-*.

# 1 Sam 13:2
*ᵃ1,2* Or *and when he had reigned over Israel two years, ²he*

# 1 Sam 13:5
*ᵃ5* Some Septuagint manuscripts and Syriac; Hebrew *thirty thousand*

# 1 Sam 13:9
*ᵃ9* Traditionally *peace offerings*

# 1 Sam 13:15
*ᵃ15* Hebrew; Septuagint *Gilgal and went his way; the rest of the people went after Saul to meet the army, and they went out of Gilgal*

# 1 Sam 13:16
***Israel Without Weapons (13:16–22)***\
\
*ᵃ16* Two Hebrew manuscripts; most Hebrew manuscripts *Geba*, a variant of *Gibeah*

# 1 Sam 13:20
*ᵃ20* Septuagint; Hebrew *plowshares*

# 1 Sam 13:21
*ᵃ21* Hebrew *pim*; that is, about 1/4 ounce (about 8 grams)\
*ᵇ21* That is, about 1/8 ounce (about 4 grams)

# 1 Sam 13:23
***Jonathan Attacks the Philistines (13:23–14:14)***

# 1 Sam 14:14
*ᵃ14* Hebrew *half a yoke*; a “yoke” was the land plowed by a yoke of oxen in one day.

# 1 Sam 14:15
***Israel Routs the Philistines (14:15–23)***\
\
*ᵃ15* Or *a terrible panic*

# 1 Sam 14:18
*ᵃ18* Hebrew; Septuagint *“Bring the ephod.” (At that time he wore the ephod before the Israelites.)*

# 1 Sam 14:24
***Jonathan Eats Honey (14:24–48)***

# 1 Sam 14:25
*ᵃ25* Or *Now all the people of the land*

# 1 Sam 14:27
*ᵃ27* Or *his strength was renewed*

# 1 Sam 14:29
*ᵃ29* Or *my strength was renewed*

# 1 Sam 14:41
*ᵃ41* Hebrew; Septuagint *“Why have you not answered your servant today? If the fault is in me or my son Jonathan, respond with Urim, but if the men of Israel are at fault, respond with Thummim.”*

# 1 Sam 14:47
*ᵃ47* Masoretic Text; Dead Sea Scrolls and Septuagint *king*\
*ᵇ47* Hebrew; Septuagint *he was victorious*

# 1 Sam 14:49
***Saul’s Family (14:49–52)***

# 1 Sam 15:1
***The Lᴏʀᴅ Rejects Saul as King (15:1–35)***

# 1 Sam 15:3
*ᵃ3* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verses [8](vref:1 Sam 15:8), [9](vref:1 Sam 15:9), [15](vref:1 Sam 15:15), [18](vref:1 Sam 15:18), [20](vref:1 Sam 15:20) and [21](vref:1 Sam 15:21).

# 1 Sam 15:9
*ᵃ9* Or *the grown bulls*; the meaning of the Hebrew for this phrase is uncertain.

# 1 Sam 15:32
*ᵃ32* Or *him trembling, yet*

# 1 Sam 16:1
***Samuel Anoints David (16:1–13)***

# 1 Sam 16:11
*ᵃ11* Some Septuagint manuscripts; Hebrew *not gather around*

# 1 Sam 16:14
***David in Saul’s Service (16:14–23)***\
\
*ᵃ14* Or *injurious*; also in verses [15](vref:1 Sam 16:15), [16](vref:1 Sam 16:16) and [23](vref:1 Sam 16:23)

# 1 Sam 17:1
***David and Goliath (17:1–58)***

# 1 Sam 17:4
*ᵃ4* Hebrew *was six cubits and a span* (about 3 meters)

# 1 Sam 17:5
*ᵃ5* That is, about 125 pounds (about 57 kilograms)

# 1 Sam 17:7
*ᵃ7* That is, about 15 pounds (about 7 kilograms)

# 1 Sam 17:17
*ᵃ17* That is, probably about 3/5 bushel (about 22 liters)

# 1 Sam 17:18
*ᵃ18* Hebrew *thousand*\
*ᵇ18* Or *some token*; or *some pledge of spoils*

# 1 Sam 17:52
*ᵃ52* Some Septuagint manuscripts; Hebrew *a valley*

# 1 Sam 18:1
***Saul’s Jealousy of David (18:1–30)***

# 1 Sam 18:5
*ᵃ5* Or *wisely*

# 1 Sam 18:10
*ᵃ10* Or *injurious*

# 1 Sam 18:14
*ᵃ14* Or *he was very wise*

# 1 Sam 18:15
*ᵃ15* Or *wise*

# 1 Sam 18:19
*ᵃ19* Or *However,*

# 1 Sam 18:30
*ᵃ30* Or *David acted more wisely*

# 1 Sam 19:1
***Saul Tries to Kill David (19:1–24)***

# 1 Sam 19:9
*ᵃ9* Or *injurious*

# 1 Sam 19:13
*ᵃ13* Hebrew *teraphim*; also in verse [16](vref:1 Sam 19:16)

# 1 Sam 20:1
***David and Jonathan (20:1–42)***

# 1 Sam 20:25
*ᵃ25* Septuagint; Hebrew *wall. Jonathan arose*

# 1 Sam 21:1
***David at Nob (21:1–9)***

# 1 Sam 21:5
*ᵃ5* Or *from us in the past few days since*\
*ᵇ5* Or *bodies*

# 1 Sam 21:10
***David at Gath (21:10–15)***

# 1 Sam 22:1
***David at Adullam and Mizpah (22:1–5)***

# 1 Sam 22:6
***Saul Kills the Priests of Nob (22:6–23)***

# 1 Sam 23:1
***David Saves Keilah (23:1–6)***

# 1 Sam 23:7
***Saul Pursues David (23:7–29)***

# 1 Sam 23:23
*ᵃ23* Or *me at Nacon*

# 1 Sam 23:28
*ᵃ28* *Sela Hammahlekoth* means *rock of parting*.

# 1 Sam 24:1
***David Spares Saul’s Life (24:1–22)***

# 1 Sam 24:4
*ᵃ4* Or *“Today the Lᴏʀᴅ is saying*

# 1 Sam 25:1
***David, Nabal and Abigail (25:1–44)***\
\
*ᵃ1* Some Septuagint manuscripts; Hebrew *Paran*

# 1 Sam 25:18
*ᵃ18* That is, probably about a bushel (about 37 liters)

# 1 Sam 25:22
*ᵃ22* Some Septuagint manuscripts; Hebrew *with David’s enemies*

# 1 Sam 25:44
*ᵃ44* Hebrew *Palti*, a variant of *Paltiel*

# 1 Sam 26:1
***David Again Spares Saul’s Life (26:1–25)***

# 1 Sam 26:4
*ᵃ4* Or *had come to Nacon*

# 1 Sam 27:1
***David Among the Philistines (27:1–12)***

# 1 Sam 28:1
***Saul and the Witch of Endor (28:1–25)***

# 1 Sam 28:13
*ᵃ13* Or *see spirits*; or *see gods*

# 1 Sam 29:1
***Achish Sends David Back to Ziklag (29:1–11)***

# 1 Sam 30:1
***David Destroys the Amalekites (30:1–31)***

# 1 Sam 31:1
***Saul Takes His Life (31:1–13)***

# 2 Sam 1:1
***David Hears of Saul’s Death (1:1–16)***

# 2 Sam 1:17
***David’s Lament for Saul and Jonathan (1:17–27)***

# 2 Sam 2:1
***David Anointed King Over Judah (2:1–7)***

# 2 Sam 2:8
***War Between the Houses of David and Saul (2:8–3:5)***

# 2 Sam 2:9
*ᵃ9* Or *Asher*

# 2 Sam 2:16
*ᵃ16* *Helkath Hazzurim* means *field of daggers* or *field of hostilities*.

# 2 Sam 2:27
*ᵃ27* Or *spoken this morning, the men would not have taken up the pursuit of their brothers*; or *spoken, the men would have given up the pursuit of their brothers by morning*

# 2 Sam 2:29
*ᵃ29* Or *morning*; or *ravine*; the meaning of the Hebrew for this word is uncertain.

# 2 Sam 3:6
***Abner Goes Over to David (3:6–21)***

# 2 Sam 3:22
***Joab Murders Abner (3:22–39)***

# 2 Sam 3:29
*ᵃ29* The Hebrew word was used for various diseases affecting the skin—not necessarily leprosy.

# 2 Sam 4:1
***Ish-Bosheth Murdered (4:1–12)***

# 2 Sam 5:1
***David Becomes King Over Israel (5:1–5)***

# 2 Sam 5:6
***David Conquers Jerusalem (5:6–16)***

# 2 Sam 5:8
*ᵃ8* Or *use scaling hooks*\
*ᵇ8* Or *are hated by David*

# 2 Sam 5:9
*ᵃ9* Or *the Millo*

# 2 Sam 5:17
***David Defeats the Philistines (5:17–25)***

# 2 Sam 5:20
*ᵃ20* *Baal Perazim* means *the lord who breaks out*.

# 2 Sam 5:25
*ᵃ25* Septuagint (see also [1 Chron. 14:16](vref:1 Chr 14:16)); Hebrew *Geba*

# 2 Sam 6:1
***The Ark Brought to Jerusalem (6:1–23)***

# 2 Sam 6:2
*ᵃ2* That is, Kiriath Jearim; Hebrew *Baale Judah*, a variant of *Baalah of Judah*\
*ᵇ2* Hebrew; Septuagint and Vulgate do not have *the Name*.

# 2 Sam 6:4
*ᵃ3,4* Dead Sea Scrolls and some Septuagint manuscripts; Masoretic Text *cart ⁴and they brought it with the ark of God from the house of Abinadab, which was on the hill*

# 2 Sam 6:5
*ᵃ5* See Dead Sea Scrolls, Septuagint and [1 Chronicles 13:8](vref:1 Chr 13:8); Masoretic Text *celebrating before the Lᴏʀᴅ with all kinds of instruments made of pine*.

# 2 Sam 6:8
*ᵃ8* *Perez Uzzah* means *outbreak against Uzzah*.

# 2 Sam 6:17
*ᵃ17* Traditionally *peace offerings*; also in verse [18](vref:2 Sam 6:18)

# 2 Sam 7:1
***God’s Promise to David (7:1–17)***

# 2 Sam 7:11
*ᵃ11* Traditionally *judges*

# 2 Sam 7:16
*ᵃ16* Some Hebrew manuscripts and Septuagint; most Hebrew manuscripts *you*

# 2 Sam 7:18
***David’s Prayer (7:18–29)***

# 2 Sam 7:23
*ᵃ23* See Septuagint and [1 Chron. 17:21](vref:1 Chr 17:21); Hebrew *wonders for your land and before your people, whom you redeemed from Egypt, from the nations and their gods*.

# 2 Sam 8:1
***David’s Victories (8:1–14)***

# 2 Sam 8:4
*ᵃ4* Septuagint (see also Dead Sea Scrolls and [1 Chron. 18:4](vref:1 Chr 18:4)); Masoretic Text *captured seventeen hundred of his charioteers*

# 2 Sam 8:8
*ᵃ8* See some Septuagint manuscripts (see also [1 Chron. 18:8](vref:1 Chr 18:8)); Hebrew *Betah*.

# 2 Sam 8:9
*ᵃ9* Hebrew *Toi*, a variant of *Tou*; also in verse [10](vref:2 Sam 8:10)

# 2 Sam 8:10
*ᵃ10* A variant of *Hadoram*

# 2 Sam 8:12
*ᵃ12* Some Hebrew manuscripts, Septuagint and Syriac (see also [1 Chron. 18:11](vref:1 Chr 18:11)); most Hebrew manuscripts *Aram*

# 2 Sam 8:13
*ᵃ13* A few Hebrew manuscripts, Septuagint and Syriac (see also [1 Chron. 18:12](vref:1 Chr 18:12)); most Hebrew manuscripts *Aram* (that is, Arameans)

# 2 Sam 8:15
***David’s Officials (8:15–18)***

# 2 Sam 8:18
*ᵃ18* Or *were priests*

# 2 Sam 9:1
***David and Mephibosheth (9:1–13)***

# 2 Sam 9:11
*ᵃ11* Septuagint; Hebrew *my*

# 2 Sam 10:1
***David Defeats the Ammonites (10:1–19)***

# 2 Sam 10:16
*ᵃ16* That is, the Euphrates

# 2 Sam 10:18
*ᵃ18* Some Septuagint manuscripts (see also [1 Chron. 19:18](vref:1 Chr 19:18)); Hebrew *horsemen*

# 2 Sam 11:1
***David and Bathsheba (11:1–27)***

# 2 Sam 11:4
*ᵃ4* Or *with her. When she purified herself from her uncleanness,*

# 2 Sam 11:21
*ᵃ21* Also known as *Jerub-Baal* (that is, Gideon)

# 2 Sam 12:1
***Nathan Rebukes David (12:1–31)***

# 2 Sam 12:14
*ᵃ14* Masoretic Text; an ancient Hebrew scribal tradition *this you have shown utter contempt for the Lᴏʀᴅ*

# 2 Sam 12:25
*ᵃ25* *Jedidiah* means *loved by the Lᴏʀᴅ*.

# 2 Sam 12:30
*ᵃ30* Or *of Milcom* (that is, Molech)\
*ᵇ30* That is, about 75 pounds (about 34 kilograms)

# 2 Sam 12:31
*ᵃ31* The meaning of the Hebrew for this clause is uncertain.

# 2 Sam 13:1
***Amnon and Tamar (13:1–22)***

# 2 Sam 13:18
*ᵃ18* The meaning of the Hebrew for this phrase is uncertain.

# 2 Sam 13:19
*ᵃ19* The meaning of the Hebrew for this word is uncertain.

# 2 Sam 13:23
***Absalom Kills Amnon (13:23–39)***

# 2 Sam 13:34
*ᵃ34* Septuagint; Hebrew does not have this sentence.

# 2 Sam 13:39
*ᵃ39* Dead Sea Scrolls and some Septuagint manuscripts; Masoretic Text *But ⌞​the spirit of​⌟ David the king*

# 2 Sam 14:1
***Absalom Returns to Jerusalem (14:1–33)***

# 2 Sam 14:4
*ᵃ4* Many Hebrew manuscripts, Septuagint, Vulgate and Syriac; most Hebrew manuscripts *spoke*

# 2 Sam 14:26
*ᵃ26* That is, about 5 pounds (about 2.3 kilograms)

# 2 Sam 15:1
***Absalom’s Conspiracy (15:1–12)***

# 2 Sam 15:7
*ᵃ7* Some Septuagint manuscripts, Syriac and Josephus; Hebrew *forty*

# 2 Sam 15:8
*ᵃ8* Some Septuagint manuscripts; Hebrew does not have *in Hebron*.

# 2 Sam 15:13
***David Flees (15:13–37)***

# 2 Sam 15:24
*ᵃ24* Or *Abiathar went up*

# 2 Sam 16:1
***David and Ziba (16:1–4)***

# 2 Sam 16:5
***Shimei Curses David (16:5–14)***

# 2 Sam 16:15
***The Advice of Hushai and Ahithophel (16:15–17:29)***

# 2 Sam 17:1
*ᵃ1* Or *Let me*

# 2 Sam 17:2
*ᵃ2* Or *will*

# 2 Sam 17:9
*ᵃ9* Or *When some of the men fall at the first attack*

# 2 Sam 17:20
*ᵃ20* Or *“They passed by the sheep pen toward the water.”*

# 2 Sam 17:25
*ᵃ25* Hebrew *Ithra*, a variant of *Jether*\
*ᵇ25* Hebrew and some Septuagint manuscripts; other Septuagint manuscripts (see also [1 Chron. 2:17](vref:1 Chr 2:17)) *Ishmaelite* or *Jezreelite*\
*ᶜ25* Hebrew *Abigal*, a variant of *Abigail*

# 2 Sam 17:28
*ᵃ28* Most Septuagint manuscripts and Syriac; Hebrew *lentils, and roasted grain*

# 2 Sam 18:1
***Absalom’s Death (18:1–18)***

# 2 Sam 18:3
*ᵃ3* Two Hebrew manuscripts, some Septuagint manuscripts and Vulgate; most Hebrew manuscripts *care; for now there are ten thousand like us*

# 2 Sam 18:11
*ᵃ11* That is, about 4 ounces (about 115 grams)

# 2 Sam 18:12
*ᵃ12* That is, about 25 pounds (about 11 kilograms)\
*ᵇ12* A few Hebrew manuscripts, Septuagint, Vulgate and Syriac; most Hebrew manuscripts may be translated *Absalom, whoever you may be*.

# 2 Sam 18:13
*ᵃ13* Or *Otherwise, if I had acted treacherously toward him*

# 2 Sam 18:19
***David Mourns (18:19–19:8a)***

# 2 Sam 18:23
*ᵃ23* That is, the plain of the Jordan

# 2 Sam 19:8
***David Returns to Jerusalem (19:8b–43)***

# 2 Sam 20:1
***Sheba Rebels Against David (20:1–26)***

# 2 Sam 20:14
*ᵃ14* Or *Abel, even Beth Maacah*; also in verse [15](vref:2 Sam 20:15)

# 2 Sam 20:24
*ᵃ24* Some Septuagint manuscripts (see also [1 Kings 4:6](vref:1 Kgs 4:6) and [5:14](vref:1 Kgs 5:14)); Hebrew *Adoram*

# 2 Sam 21:1
***The Gibeonites Avenged (21:1–14)***

# 2 Sam 21:8
*ᵃ8* Two Hebrew manuscripts, some Septuagint manuscripts and Syriac (see also [1 Samuel 18:19](vref:1 Sam 18:19)); most Hebrew and Septuagint manuscripts *Michal*

# 2 Sam 21:15
***Wars Against the Philistines (21:15–22)***

# 2 Sam 21:16
*ᵃ16* That is, about 7 1/2 pounds (about 3.5 kilograms)

# 2 Sam 21:19
*ᵃ19* Or *son of Jair the weaver*\
*ᵇ19* Hebrew and Septuagint; [1 Chron. 20:5](vref:1 Chr 20:5) *son of Jair killed Lahmi the brother of Goliath*

# 2 Sam 22:1
***David’s Song of Praise (22:1–51)***

# 2 Sam 22:3
*ᵃ3* *Horn* here symbolizes strength.

# 2 Sam 22:6
*ᵃ6* Hebrew *Sheol*

# 2 Sam 22:8
*ᵃ8* Hebrew; Vulgate and Syriac (see also [Psalm 18:7](vref:Ps 18:7)) *mountains*

# 2 Sam 22:11
*ᵃ11* Many Hebrew manuscripts (see also [Psalm 18:10](vref:Ps 18:10)); most Hebrew manuscripts *appeared*

# 2 Sam 22:12
*ᵃ12* Septuagint and Vulgate (see also [Psalm 18:11](vref:Ps 18:11)); Hebrew *massed*

# 2 Sam 22:25
*ᵃ25* Hebrew; Septuagint and Vulgate (see also [Psalm 18:24](vref:Ps 18:24)) *to the cleanness of my hands*

# 2 Sam 22:30
*ᵃ30* Or *can run through a barricade*

# 2 Sam 22:33
*ᵃ33* Dead Sea Scrolls, some Septuagint manuscripts, Vulgate and Syriac (see also [Psalm 18:32](vref:Ps 18:32)); Masoretic Text *who is my strong refuge*

# 2 Sam 22:46
*ᵃ46* Some Septuagint manuscripts and Vulgate (see also [Psalm 18:45](vref:Ps 18:45)); Masoretic Text *they arm themselves.*

# 2 Sam 23:1
***The Last Words of David (23:1–7)***\
\
*ᵃ1* Or *Israel’s beloved singer*

# 2 Sam 23:8
***David’s Mighty Men (23:8–39)***\
\
*ᵃ8* Hebrew; some Septuagint manuscripts suggest *Ish-Bosheth*, that is, *Esh-Baal* (see also [1 Chron. 11:11](vref:1 Chr 11:11) *Jashobeam*).\
*ᵇ8* Probably a variant of *Hacmonite* (see [1 Chron. 11:11](vref:1 Chr 11:11))\
*ᶜ8* Some Septuagint manuscripts (see also [1 Chron. 11:11](vref:1 Chr 11:11)); Hebrew and other Septuagint manuscripts *Three; it was Adino the Eznite who killed eight hundred men*

# 2 Sam 23:9
*ᵃ9* See [1 Chron. 11:13](vref:1 Chr 11:13); Hebrew *gathered there.*

# 2 Sam 23:18
*ᵃ18* Most Hebrew manuscripts (see also [1 Chron. 11:20](vref:1 Chr 11:20)); two Hebrew manuscripts and Syriac *Thirty*

# 2 Sam 23:27
*ᵃ27* Hebrew; some Septuagint manuscripts (see also [1 Chron. 11:29](vref:1 Chr 11:29)) *Sibbecai*

# 2 Sam 23:29
*ᵃ29* Some Hebrew manuscripts and Vulgate (see also [1 Chron. 11:30](vref:1 Chr 11:30)); most Hebrew manuscripts *Heleb*

# 2 Sam 23:30
*ᵃ30* Hebrew; some Septuagint manuscripts (see also [1 Chron. 11:32](vref:1 Chr 11:32)) *Hurai*

# 2 Sam 23:33
*ᵃ33* Some Septuagint manuscripts (see also [1 Chron. 11:34](vref:1 Chr 11:34)); Hebrew does not have *son of*.\
*ᵇ33* Hebrew; some Septuagint manuscripts (see also [1 Chron. 11:35](vref:1 Chr 11:35)) *Sacar*

# 2 Sam 23:36
*ᵃ36* Some Septuagint manuscripts (see also [1 Chron. 11:38](vref:1 Chr 11:38)); Hebrew *Haggadi*

# 2 Sam 24:1
***David Counts the Fighting Men (24:1–17)***

# 2 Sam 24:2
*ᵃ2* Septuagint (see also verse [4](vref:2 Sam 24:4) and [1 Chron. 21:2](vref:1 Chr 21:2)); Hebrew *Joab the army commander*

# 2 Sam 24:13
*ᵃ13* Septuagint (see also [1 Chron. 21:12](vref:1 Chr 21:12)); Hebrew *seven*

# 2 Sam 24:18
***David Builds an Altar (24:18–25)***

# 2 Sam 24:24
*ᵃ24* That is, about 1 1/4 pounds (about 0.6 kilogram)

# 2 Sam 24:25
*ᵃ25* Traditionally *peace offerings*

# 1 Kgs 1:1
***Adonijah Sets Himself Up as King (1:1–27)***

# 1 Kgs 1:5
*ᵃ5* Or *charioteers*

# 1 Kgs 1:8
*ᵃ8* Or *and his friends*

# 1 Kgs 1:28
***David Makes Solomon King (1:28–53)***

# 1 Kgs 2:1
***David’s Charge to Solomon (2:1–12)***

# 1 Kgs 2:6
*ᵃ6* Hebrew *Sheol*; also in verse [9](vref:1 Kgs 2:9)

# 1 Kgs 2:13
***Solomon’s Throne Established (2:13–46)***

# 1 Kgs 2:34
*ᵃ34* Or *buried in his tomb*

# 1 Kgs 3:1
***Solomon Asks for Wisdom (3:1–15)***

# 1 Kgs 3:15
*ᵃ15* Traditionally *peace offerings*

# 1 Kgs 3:16
***A Wise Ruling (3:16–28)***

# 1 Kgs 4:1
***Solomon’s Officials and Governors (4:1–19)***

# 1 Kgs 4:11
*ᵃ11* Or *in the heights of Dor*

# 1 Kgs 4:20
***Solomon’s Daily Provisions (4:20–28)***

# 1 Kgs 4:21
*ᵃ21* That is, the Euphrates; also in verse [24](vref:1 Kgs 4:24)

# 1 Kgs 4:22
*ᵃ22* That is, probably about 185 bushels (about 6.6 kiloliters)\
*ᵇ22* That is, probably about 375 bushels (about 13.2 kiloliters)

# 1 Kgs 4:26
*ᵃ26* Some Septuagint manuscripts (see also [2 Chron. 9:25](vref:2 Chr 9:25)); Hebrew *forty*\
*ᵇ26* Or *charioteers*

# 1 Kgs 4:29
***Solomon’s Wisdom (4:29–34)***

# 1 Kgs 5:1
***Preparations for Building the Temple (5:1–18)***

# 1 Kgs 5:11
*ᵃ11* That is, probably about 125,000 bushels (about 4,400 kiloliters)\
*ᵇ11* Septuagint (see also [2 Chron. 2:10](vref:2 Chr 2:10)); Hebrew *twenty cors*\
*ᶜ11* That is, about 115,000 gallons (about 440 kiloliters)

# 1 Kgs 5:16
*ᵃ16* Hebrew; some Septuagint manuscripts (see also [2 Chron. 2:2](vref:2 Chr 2:2), [18](vref:2 Chr 2:18)) *thirty-six hundred*

# 1 Kgs 5:18
*ᵃ18* That is, Byblos

# 1 Kgs 6:1
***Solomon Builds the Temple (6:1–38)***\
\
*ᵃ1* Hebrew; Septuagint *four hundred and fortieth*

# 1 Kgs 6:2
*ᵃ2* That is, about 90 feet (about 27 meters) long and 30 feet (about 9 meters) wide and 45 feet (about 13.5 meters) high

# 1 Kgs 6:3
*ᵃ3* That is, about 30 feet (about 9 meters)\
*ᵇ3* That is, about 15 feet (about 4.5 meters)

# 1 Kgs 6:6
*ᵃ6* That is, about 7 1/2 feet (about 2.3 meters); also in verses [10](vref:1 Kgs 6:10) and [24](vref:1 Kgs 6:24)\
*ᵇ6* That is, about 9 feet (about 2.7 meters)\
*ᶜ6* That is, about 10 1/2 feet (about 3.1 meters)

# 1 Kgs 6:8
*ᵃ8* Septuagint; Hebrew *middle*

# 1 Kgs 6:16
*ᵃ16* That is, about 30 feet (about 9 meters)

# 1 Kgs 6:17
*ᵃ17* That is, about 60 feet (about 18 meters)

# 1 Kgs 6:20
*ᵃ20* That is, about 30 feet (about 9 meters) long, wide and high

# 1 Kgs 6:23
*ᵃ23* That is, about 15 feet (about 4.5 meters)

# 1 Kgs 7:1
***Solomon Builds His Palace (7:1–12)***

# 1 Kgs 7:2
*ᵃ2* That is, about 150 feet (about 46 meters) long, 75 feet (about 23 meters) wide and 45 feet (about 13.5 meters) high

# 1 Kgs 7:5
*ᵃ5* The meaning of the Hebrew for this verse is uncertain.

# 1 Kgs 7:6
*ᵃ6* That is, about 75 feet (about 23 meters) long and 45 feet (about 13.5 meters) wide

# 1 Kgs 7:7
*ᵃ7* Vulgate and Syriac; Hebrew *floor*

# 1 Kgs 7:10
*ᵃ10* That is, about 15 feet (about 4.5 meters)\
*ᵇ10* That is, about 12 feet (about 3.6 meters)

# 1 Kgs 7:13
***The Temple’s Furnishings (7:13–51)***\
\
*ᵃ13* Hebrew *Hiram*, a variant of *Huram*; also in verses [40](vref:1 Kgs 7:40) and [45](vref:1 Kgs 7:45)

# 1 Kgs 7:15
*ᵃ15* That is, about 27 feet (about 8.1 meters) high and 18 feet (about 5.4 meters) around

# 1 Kgs 7:16
*ᵃ16* That is, about 7 1/2 feet (about 2.3 meters); also in verse [23](vref:1 Kgs 7:23)

# 1 Kgs 7:18
*ᵃ18* Two Hebrew manuscripts and Septuagint; most Hebrew manuscripts *made the pillars, and there were two rows*\
*ᵇ18* Many Hebrew manuscripts and Syriac; most Hebrew manuscripts *pomegranates*

# 1 Kgs 7:19
*ᵃ19* That is, about 6 feet (about 1.8 meters); also in verse [38](vref:1 Kgs 7:38)

# 1 Kgs 7:21
*ᵃ21* *Jakin* probably means *he establishes*.\
*ᵇ21* *Boaz* probably means *in him is strength*.

# 1 Kgs 7:23
*ᵃ23* That is, about 15 feet (about 4.5 meters)\
*ᵇ23* That is, about 45 feet (about 13.5 meters)

# 1 Kgs 7:26
*ᵃ26* That is, about 3 inches (about 8 centimeters)\
*ᵇ26* That is, probably about 11,500 gallons (about 44 kiloliters); the Septuagint does not have this sentence.

# 1 Kgs 7:27
*ᵃ27* That is, about 6 feet (about 1.8 meters) long and wide and about 4 1/2 feet (about 1.3 meters) high

# 1 Kgs 7:31
*ᵃ31* That is, about 1 1/2 feet (about 0.5 meter)\
*ᵇ31* That is, about 2 1/4 feet (about 0.7 meter); also in verse [32](vref:1 Kgs 7:32)

# 1 Kgs 7:35
*ᵃ35* That is, about 3/4 foot (about 0.2 meter)

# 1 Kgs 7:38
*ᵃ38* That is, about 230 gallons (about 880 liters)

# 1 Kgs 8:1
***The Ark Brought to the Temple (8:1–21)***

# 1 Kgs 8:22
***Solomon’s Prayer of Dedication (8:22–61)***

# 1 Kgs 8:62
***The Dedication of the Temple (8:62–66)***

# 1 Kgs 8:63
*ᵃ63* Traditionally *peace offerings*; also in verse [64](vref:1 Kgs 8:64)

# 1 Kgs 8:65
*ᵃ65* Or *from the entrance to*

# 1 Kgs 9:1
***The Lᴏʀᴅ Appears to Solomon (9:1–9)***

# 1 Kgs 9:6
*ᵃ6* The Hebrew is plural.

# 1 Kgs 9:10
***Solomon’s Other Activities (9:10–28)***

# 1 Kgs 9:13
*ᵃ13* *Cabul* sounds like the Hebrew for *good-for-nothing*.

# 1 Kgs 9:14
*ᵃ14* That is, about 4 1/2 tons (about 4 metric tons)

# 1 Kgs 9:15
*ᵃ15* Or *the Millo*; also in verse [24](vref:1 Kgs 9:24)

# 1 Kgs 9:18
*ᵃ18* The Hebrew may also be read *Tamar*.

# 1 Kgs 9:19
*ᵃ19* Or *charioteers*

# 1 Kgs 9:21
*ᵃ21* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# 1 Kgs 9:25
*ᵃ25* Traditionally *peace offerings*

# 1 Kgs 9:26
*ᵃ26* Hebrew *Yam Suph*; that is, Sea of Reeds

# 1 Kgs 9:28
*ᵃ28* That is, about 16 tons (about 14.5 metric tons)

# 1 Kgs 10:1
***The Queen of Sheba Visits Solomon (10:1–13)***

# 1 Kgs 10:5
*ᵃ5* Or *the ascent by which he went up to*

# 1 Kgs 10:10
*ᵃ10* That is, about 4 1/2 tons (about 4 metric tons)

# 1 Kgs 10:11
*ᵃ11* Probably a variant of *algumwood*; also in verse [12](vref:1 Kgs 10:12)

# 1 Kgs 10:14
***Solomon’s Splendor (10:14–29)***\
\
*ᵃ14* That is, about 25 tons (about 23 metric tons)

# 1 Kgs 10:16
*ᵃ16* That is, about 7 1/2 pounds (about 3.5 kilograms)

# 1 Kgs 10:17
*ᵃ17* That is, about 3 3/4 pounds (about 1.7 kilograms)

# 1 Kgs 10:22
*ᵃ22* Hebrew *of ships of Tarshish*

# 1 Kgs 10:26
*ᵃ26* Or *charioteers*

# 1 Kgs 10:28
*ᵃ28* Or possibly *Muzur*, a region in Cilicia; also in verse [29](vref:1 Kgs 10:29)\
*ᵇ28* Probably *Cilicia*

# 1 Kgs 10:29
*ᵃ29* That is, about 15 pounds (about 7 kilograms)\
*ᵇ29* That is, about 3 3/4 pounds (about 1.7 kilograms)

# 1 Kgs 11:1
***Solomon’s Wives (11:1–13)***

# 1 Kgs 11:5
*ᵃ5* Hebrew *Milcom*; also in verse [33](vref:1 Kgs 11:33)

# 1 Kgs 11:14
***Solomon’s Adversaries (11:14–25)***

# 1 Kgs 11:24
*ᵃ24* Hebrew *destroyed them*

# 1 Kgs 11:26
***Jeroboam Rebels Against Solomon (11:26–40)***

# 1 Kgs 11:27
*ᵃ27* Or *the Millo*

# 1 Kgs 11:33
*ᵃ33* Hebrew; Septuagint, Vulgate and Syriac *because he has*

# 1 Kgs 11:41
***Solomon’s Death (11:41–43)***

# 1 Kgs 12:1
***Israel Rebels Against Rehoboam (12:1–24)***

# 1 Kgs 12:2
*ᵃ2* Or *he remained in*

# 1 Kgs 12:18
*ᵃ18* Some Septuagint manuscripts and Syriac (see also [1 Kings 4:6](vref:1 Kgs 4:6) and [5:14](vref:1 Kgs 5:14)); Hebrew *Adoram*

# 1 Kgs 12:25
***Golden Calves at Bethel and Dan (12:25–33)***\
\
*ᵃ25* Hebrew *Penuel*, a variant of *Peniel*

# 1 Kgs 13:1
***The Man of God From Judah (13:1–34)***

# 1 Kgs 14:1
***Ahijah’s Prophecy Against Jeroboam (14:1–20)***

# 1 Kgs 14:14
*ᵃ14* The meaning of the Hebrew for this sentence is uncertain.

# 1 Kgs 14:15
*ᵃ15* That is, the Euphrates\
*ᵇ15* That is, symbols of the goddess Asherah; here and elsewhere in 1 Kings

# 1 Kgs 14:21
***Rehoboam King of Judah (14:21–31)***

# 1 Kgs 14:31
*ᵃ31* Some Hebrew manuscripts and Septuagint (see also [2 Chron. 12:16](vref:2 Chr 12:16)); most Hebrew manuscripts *Abijam*

# 1 Kgs 15:1
***Abijah King of Judah (15:1–8)***\
\
*ᵃ1* Some Hebrew manuscripts and Septuagint (see also [2 Chron. 12:16](vref:2 Chr 12:16)); most Hebrew manuscripts *Abijam*; also in verses [7](vref:1 Kgs 15:7) and [8](vref:1 Kgs 15:8)

# 1 Kgs 15:2
*ᵃ2* A variant of *Absalom*; also in verse [10](vref:1 Kgs 15:10)

# 1 Kgs 15:6
*ᵃ6* Most Hebrew manuscripts; some Hebrew manuscripts and Syriac *Abijam* (that is, Abijah)

# 1 Kgs 15:9
***Asa King of Judah (15:9–24)***

# 1 Kgs 15:25
***Nadab King of Israel (15:25–32)***

# 1 Kgs 15:33
***Baasha King of Israel (15:33–16:7)***

# 1 Kgs 16:8
***Elah King of Israel (16:8–14)***

# 1 Kgs 16:15
***Zimri King of Israel (16:15–20)***

# 1 Kgs 16:21
***Omri King of Israel (16:21–28)***

# 1 Kgs 16:24
*ᵃ24* That is, about 150 pounds (about 70 kilograms)

# 1 Kgs 16:29
***Ahab Becomes King of Israel (16:29–34)***

# 1 Kgs 17:1
***Elijah Fed by Ravens (17:1–6)***\
\
*ᵃ1* Or *Tishbite, of the settlers*

# 1 Kgs 17:7
***The Widow at Zarephath (17:7–24)***

# 1 Kgs 18:1
***Elijah and Obadiah (18:1–15)***

# 1 Kgs 18:16
***Elijah on Mount Carmel (18:16–46)***

# 1 Kgs 18:32
*ᵃ32* That is, probably about 13 quarts (about 15 liters)

# 1 Kgs 19:1
***Elijah Flees to Horeb (19:1–9a)***

# 1 Kgs 19:3
*ᵃ3* Or *Elijah saw*

# 1 Kgs 19:9
***The Lᴏʀᴅ Appears to Elijah (19:9b–18)***

# 1 Kgs 19:19
***The Call of Elisha (19:19–21)***

# 1 Kgs 20:1
***Ben-Hadad Attacks Samaria (20:1–12)***

# 1 Kgs 20:12
*ᵃ12* Or *in Succoth*; also in verse [16](vref:1 Kgs 20:16)

# 1 Kgs 20:13
***Ahab Defeats Ben-Hadad (20:13–34)***

# 1 Kgs 20:35
***A Prophet Condemns Ahab (20:35–43)***

# 1 Kgs 20:39
*ᵃ39* That is, about 75 pounds (about 34 kilograms)

# 1 Kgs 20:42
*ᵃ42* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# 1 Kgs 21:1
***Naboth’s Vineyard (21:1–29)***

# 1 Kgs 21:23
*ᵃ23* Most Hebrew manuscripts; a few Hebrew manuscripts, Vulgate and Syriac (see also [2 Kings 9:26](vref:2 Kgs 9:26)) *the plot of ground at*

# 1 Kgs 22:1
***Micaiah Prophesies Against Ahab (22:1–28)***

# 1 Kgs 22:24
*ᵃ24* Or *Spirit of*

# 1 Kgs 22:29
***Ahab Killed at Ramoth Gilead (22:29–40)***

# 1 Kgs 22:38
*ᵃ38* Or *Samaria and cleaned the weapons*

# 1 Kgs 22:41
***Jehoshaphat King of Judah (22:41–50)***

# 1 Kgs 22:48
*ᵃ48* Hebrew *of ships of Tarshish*

# 1 Kgs 22:51
***Ahaziah King of Israel (22:51–53)***

# 2 Kgs 1:1
***The Lᴏʀᴅ’s Judgment on Ahaziah (1:1–18)***

# 2 Kgs 1:17
*ᵃ17* Hebrew *Jehoram*, a variant of *Joram*

# 2 Kgs 2:1
***Elijah Taken Up to Heaven (2:1–18)***

# 2 Kgs 2:19
***Healing of the Water (2:19–22)***

# 2 Kgs 2:23
***Elisha Is Jeered (2:23–25)***

# 2 Kgs 3:1
***Moab Revolts (3:1–27)***\
\
*ᵃ1* Hebrew *Jehoram*, a variant of *Joram*; also in verse [6](vref:2 Kgs 3:6)

# 2 Kgs 3:11
*ᵃ11* That is, he was Elijah’s personal servant.

# 2 Kgs 4:1
***The Widow’s Oil (4:1–7)***

# 2 Kgs 4:8
***The Shunammite’s Son Restored to Life (4:8–37)***

# 2 Kgs 4:38
***Death in the Pot (4:38–41)***

# 2 Kgs 4:42
***Feeding of a Hundred (4:42–44)***

# 2 Kgs 5:1
***Naaman Healed of Leprosy (5:1–27)***\
\
*ᵃ1* The Hebrew word was used for various diseases affecting the skin—not necessarily leprosy; also in verses [3](vref:2 Kgs 5:3), [6](vref:2 Kgs 5:6), [7](vref:2 Kgs 5:7), [11](vref:2 Kgs 5:11) and [27](vref:2 Kgs 5:27).

# 2 Kgs 5:5
*ᵃ5* That is, about 750 pounds (about 340 kilograms)\
*ᵇ5* That is, about 150 pounds (about 70 kilograms)

# 2 Kgs 5:22
*ᵃ22* That is, about 75 pounds (about 34 kilograms)

# 2 Kgs 6:1
***An Axhead Floats (6:1–7)***

# 2 Kgs 6:8
***Elisha Traps Blinded Arameans (6:8–23)***

# 2 Kgs 6:24
***Famine in Besieged Samaria (6:24–7:2)***

# 2 Kgs 6:25
*ᵃ25* That is, about 2 pounds (about 1 kilogram)\
*ᵇ25* That is, probably about 1/2 pint (about 0.3 liter)\
*ᶜ25* Or *of dove’s dung*\
*ᵈ25* That is, about 2 ounces (about 55 grams)

# 2 Kgs 7:1
*ᵃ1* That is, probably about 7 quarts (about 7.3 liters); also in verses [16](vref:2 Kgs 7:16) and [18](vref:2 Kgs 7:18)\
*ᵇ1* That is, about 2/5 ounce (about 11 grams); also in verses [16](vref:2 Kgs 7:16) and [18](vref:2 Kgs 7:18)\
*ᶜ1* That is, probably about 13 quarts (about 15 liters); also in verses [16](vref:2 Kgs 7:16) and [18](vref:2 Kgs 7:18)

# 2 Kgs 7:3
***The Siege Lifted (7:3–20)***\
\
*ᵃ3* The Hebrew word is used for various diseases affecting the skin—not necessarily leprosy; also in verse [8](vref:2 Kgs 7:8).

# 2 Kgs 8:1
***The Shunammite’s Land Restored (8:1–6)***

# 2 Kgs 8:7
***Hazael Murders Ben-Hadad (8:7–15)***

# 2 Kgs 8:10
*ᵃ10* The Hebrew may also be read *Go and say, ‘You will certainly not recover,’ for*.

# 2 Kgs 8:16
***Jehoram King of Judah (8:16–24)***

# 2 Kgs 8:21
*ᵃ21* Hebrew *Joram*, a variant of *Jehoram*; also in verses [23](vref:2 Kgs 8:23) and [24](vref:2 Kgs 8:24)

# 2 Kgs 8:25
***Ahaziah King of Judah (8:25–29)***

# 2 Kgs 8:29
*ᵃ29* Hebrew *Ramah*, a variant of *Ramoth*

# 2 Kgs 9:1
***Jehu Anointed King of Israel (9:1–13)***

# 2 Kgs 9:14
***Jehu Kills Joram and Ahaziah (9:14–29)***

# 2 Kgs 9:15
*ᵃ15* Hebrew *Jehoram*, a variant of *Joram*; also in verses [17](vref:2 Kgs 9:17) and [21–24](vref:2 Kgs 9:21-24)

# 2 Kgs 9:26
*ᵃ26* See [1 Kings 21:19](vref:1 Kgs 21:19).

# 2 Kgs 9:27
*ᵃ27* Or *fled by way of the garden house*

# 2 Kgs 9:30
***Jezebel Killed (9:30–37)***

# 2 Kgs 9:31
*ᵃ31* Or *“Did Zimri have peace, who murdered his master?”*

# 2 Kgs 9:36
*ᵃ36* See [1 Kings 21:23](vref:1 Kgs 21:23).

# 2 Kgs 10:1
***Ahab’s Family Killed (10:1–17)***\
\
*ᵃ1* Hebrew; some Septuagint manuscripts and Vulgate *of the city*

# 2 Kgs 10:18
***Ministers of Baal Killed (10:18–36)***

# 2 Kgs 11:1
***Athaliah and Joash (11:1–21)***

# 2 Kgs 11:2
*ᵃ2* Hebrew *Joram*, a variant of *Jehoram*

# 2 Kgs 11:8
*ᵃ8* Or *approaches the precincts*

# 2 Kgs 11:15
*ᵃ15* Or *out from the precincts*

# 2 Kgs 11:21
*ᵃ21* Hebrew *Jehoash*, a variant of *Joash*

# 2 Kgs 12:1
***Joash Repairs the Temple (12:1–21)***\
\
*ᵃ1* Hebrew *Jehoash*, a variant of *Joash*; also in verses [2](vref:2 Kgs 12:2), [4](vref:2 Kgs 12:4), [6](vref:2 Kgs 12:6), [7](vref:2 Kgs 12:7) and [18](vref:2 Kgs 12:18)

# 2 Kgs 13:1
***Jehoahaz King of Israel (13:1–9)***

# 2 Kgs 13:6
*ᵃ6* That is, a symbol of the goddess Asherah; here and elsewhere in 2 Kings

# 2 Kgs 13:9
*ᵃ9* Hebrew *Joash*, a variant of *Jehoash*; also in verses [12–14](vref:2 Kgs 13:12-14) and [25](vref:2 Kgs 13:25)

# 2 Kgs 13:10
***Jehoash King of Israel (13:10–25)***

# 2 Kgs 14:1
***Amaziah King of Judah (14:1–22)***\
\
*ᵃ1* Hebrew *Joash*, a variant of *Jehoash*; also in verses [13](vref:2 Kgs 14:13), [23](vref:2 Kgs 14:23) and [27](vref:2 Kgs 14:27)

# 2 Kgs 14:6
*ᵃ6* [Deut. 24:16](vref:Deut 24:16)

# 2 Kgs 14:13
*ᵃ13* Hebrew *four hundred cubits* (about 180 meters)

# 2 Kgs 14:21
*ᵃ21* Also called *Uzziah*

# 2 Kgs 14:23
***Jeroboam II King of Israel (14:23–29)***

# 2 Kgs 14:25
*ᵃ25* Or *from the entrance to*\
*ᵇ25* That is, the Dead Sea

# 2 Kgs 14:28
*ᵃ28* Or *Judah*

# 2 Kgs 15:1
***Azariah King of Judah (15:1–7)***

# 2 Kgs 15:5
*ᵃ5* The Hebrew word was used for various diseases affecting the skin—not necessarily leprosy.\
*ᵇ5* Or *in a house where he was relieved of responsibility*

# 2 Kgs 15:8
***Zechariah King of Israel (15:8–12)***

# 2 Kgs 15:10
*ᵃ10* Hebrew; some Septuagint manuscripts *in Ibleam*

# 2 Kgs 15:12
*ᵃ12* [2 Kings 10:30](vref:2 Kgs 10:30)

# 2 Kgs 15:13
***Shallum King of Israel (15:13–16)***

# 2 Kgs 15:17
***Menahem King of Israel (15:17–22)***

# 2 Kgs 15:19
*ᵃ19* Also called *Tiglath-Pileser*\
*ᵇ19* That is, about 37 tons (about 34 metric tons)

# 2 Kgs 15:20
*ᵃ20* That is, about 1 1/4 pounds (about 0.6 kilogram)

# 2 Kgs 15:23
***Pekahiah King of Israel (15:23–26)***

# 2 Kgs 15:27
***Pekah King of Israel (15:27–31)***

# 2 Kgs 15:32
***Jotham King of Judah (15:32–38)***

# 2 Kgs 16:1
***Ahaz King of Judah (16:1–20)***

# 2 Kgs 16:3
*ᵃ3* Or *even made his son pass through*

# 2 Kgs 16:12
*ᵃ12* Or *and went up*

# 2 Kgs 16:13
*ᵃ13* Traditionally *peace offerings*

# 2 Kgs 16:18
*ᵃ18* Or *the dais of his throne* (see Septuagint)

# 2 Kgs 17:1
***Hoshea Last King of Israel (17:1–6)***

# 2 Kgs 17:4
*ᵃ4* Or *to Sais, to the*; *So* is possibly an abbreviation for *Osorkon*.

# 2 Kgs 17:7
***Israel Exiled Because of Sin (17:7–23)***

# 2 Kgs 17:12
*ᵃ12* [Exodus 20:4,5](vref:Exod 20:4-5)

# 2 Kgs 17:17
*ᵃ17* Or *They made their sons and daughters pass through*

# 2 Kgs 17:24
***Samaria Resettled (17:24–41)***

# 2 Kgs 18:1
***Hezekiah King of Judah (18:1–16)***

# 2 Kgs 18:2
*ᵃ2* Hebrew *Abi*, a variant of *Abijah*

# 2 Kgs 18:4
*ᵃ4* Or *He called it*\
*ᵇ4* *Nehushtan* sounds like the Hebrew for *bronze* and *snake* and *unclean thing*.

# 2 Kgs 18:14
*ᵃ14* That is, about 11 tons (about 10 metric tons)\
*ᵇ14* That is, about 1 ton (about 1 metric ton)

# 2 Kgs 18:17
***Sennacherib Threatens Jerusalem (18:17–37)***

# 2 Kgs 18:24
*ᵃ24* Or *charioteers*

# 2 Kgs 19:1
***Jerusalem’s Deliverance Foretold (19:1–13)***

# 2 Kgs 19:9
*ᵃ9* That is, from the upper Nile region

# 2 Kgs 19:14
***Hezekiah’s Prayer (19:14–19)***

# 2 Kgs 19:20
***Isaiah Prophesies Sennacherib’s Fall (19:20–37)***

# 2 Kgs 20:1
***Hezekiah’s Illness (20:1–11)***

# 2 Kgs 20:12
***Envoys From Babylon (20:12–21)***

# 2 Kgs 21:1
***Manasseh King of Judah (21:1–18)***

# 2 Kgs 21:6
*ᵃ6* Or *He made his own son pass through*

# 2 Kgs 21:19
***Amon King of Judah (21:19–26)***

# 2 Kgs 22:1
***The Book of the Law Found (22:1–20)***

# 2 Kgs 22:17
*ᵃ17* Or *by everything they have done*

# 2 Kgs 23:1
***Josiah Renews the Covenant (23:1–30)***

# 2 Kgs 23:8
*ᵃ8* Or *high places*

# 2 Kgs 23:10
*ᵃ10* Or *to make his son or daughter pass through*

# 2 Kgs 23:13
*ᵃ13* Hebrew *Milcom*

# 2 Kgs 23:27
*ᵃ27* [1 Kings 8:29](vref:1 Kgs 8:29)

# 2 Kgs 23:31
***Jehoahaz King of Judah (23:31–35)***

# 2 Kgs 23:33
*ᵃ33* Hebrew; Septuagint (see also [2 Chron. 36:3](vref:2 Chr 36:3)) *Neco at Riblah in Hamath removed him*\
*ᵇ33* That is, about 3 3/4 tons (about 3.4 metric tons)\
*ᶜ33* That is, about 75 pounds (about 34 kilograms)

# 2 Kgs 23:36
***Jehoiakim King of Judah (23:36–24:7)***

# 2 Kgs 24:2
*ᵃ2* Or *Chaldean*

# 2 Kgs 24:8
***Jehoiachin King of Judah (24:8–17)***

# 2 Kgs 24:18
***Zedekiah King of Judah (24:18–20a)***

# 2 Kgs 24:20
***The Fall of Jerusalem (24:20b–25:26)***

# 2 Kgs 25:3
*ᵃ3* See [Jer. 52:6](vref:Jer 52:6).

# 2 Kgs 25:4
*ᵃ4* Or *Chaldeans*; also in verses [13](vref:2 Kgs 25:13), [25](vref:2 Kgs 25:25) and [26](vref:2 Kgs 25:26)\
*ᵇ4* Or *the Jordan Valley*

# 2 Kgs 25:5
*ᵃ5* Or *Chaldean*; also in verses [10](vref:2 Kgs 25:10) and [24](vref:2 Kgs 25:24)

# 2 Kgs 25:17
*ᵃ17* Hebrew *eighteen cubits* (about 8.1 meters)\
*ᵇ17* Hebrew *three cubits* (about 1.3 meters)

# 2 Kgs 25:27
***Jehoiachin Released (25:27–30)***\
\
*ᵃ27* Also called *Amel-Marduk*

# 1 Chr 1:1
***Historical Records From Adam to Abraham (1:1–27)***\
\
***To Noah’s Sons (1:1–4)***

# 1 Chr 1:4
*ᵃ4* Septuagint; Hebrew does not have *The sons of Noah:*

# 1 Chr 1:5
***The Japhethites (1:5–7)***\
\
*ᵃ5* *Sons* may mean *descendants* or *successors* or *nations*; also in verses [6–10](vref:1 Chr 1:6-10), [17](vref:1 Chr 1:17) and [20](vref:1 Chr 1:20).

# 1 Chr 1:6
*ᵃ6* Many Hebrew manuscripts and Vulgate (see also Septuagint and [Gen. 10:3](vref:Gen 10:3)); most Hebrew manuscripts *Diphath*

# 1 Chr 1:8
***The Hamites (1:8–16)***\
\
*ᵃ8* That is, Egypt; also in verse [11](vref:1 Chr 1:11)

# 1 Chr 1:10
*ᵃ10* *Father* may mean *ancestor* or *predecessor* or *founder*; also in verses [11](vref:1 Chr 1:11), [13](vref:1 Chr 1:13), [18](vref:1 Chr 1:18) and [20](vref:1 Chr 1:20).

# 1 Chr 1:13
*ᵃ13* Or *of the Sidonians, the foremost*

# 1 Chr 1:17
***The Semites (1:17–27)***\
\
*ᵃ17* One Hebrew manuscript and some Septuagint manuscripts (see also [Gen. 10:23](vref:Gen 10:23)); most Hebrew manuscripts do not have this line.

# 1 Chr 1:19
*ᵃ19* *Peleg* means *division*.

# 1 Chr 1:22
*ᵃ22* Some Hebrew manuscripts and Syriac (see also [Gen. 10:28](vref:Gen 10:28)); most Hebrew manuscripts *Ebal*

# 1 Chr 1:24
*ᵃ24* Hebrew; some Septuagint manuscripts *Arphaxad, Cainan* (see also note at [Gen. 11:10](vref:Gen 11:10))

# 1 Chr 1:28
***The Family of Abraham (1:28–34)***

# 1 Chr 1:29
***Descendants of Hagar (1:29–31)***

# 1 Chr 1:32
***Descendants of Keturah (1:32–33)***

# 1 Chr 1:34
***Descendants of Sarah (1:34)***

# 1 Chr 1:35
***Esau’s Sons (1:35–54)***

# 1 Chr 1:36
*ᵃ36* Many Hebrew manuscripts, some Septuagint manuscripts and Syriac (see also [Gen. 36:11](vref:Gen 36:11)); most Hebrew manuscripts *Zephi*\
*ᵇ36* Some Septuagint manuscripts (see also [Gen. 36:12](vref:Gen 36:12)); Hebrew *Gatam, Kenaz, Timna and Amalek*

# 1 Chr 1:38
***The People of Seir in Edom (1:38–42)***

# 1 Chr 1:40
*ᵃ40* Many Hebrew manuscripts and some Septuagint manuscripts (see also [Gen. 36:23](vref:Gen 36:23)); most Hebrew manuscripts *Alian*

# 1 Chr 1:41
*ᵃ41* Many Hebrew manuscripts and some Septuagint manuscripts (see also [Gen. 36:26](vref:Gen 36:26)); most Hebrew manuscripts *Hamran*

# 1 Chr 1:42
*ᵃ42* Many Hebrew and Septuagint manuscripts (see also [Gen. 36:27](vref:Gen 36:27)); most Hebrew manuscripts *Zaavan, Jaakan*\
*ᵇ42* Hebrew *Dishon*, a variant of *Dishan*

# 1 Chr 1:43
***The Rulers of Edom (1:43–54)***\
\
*ᵃ43* Or *before an Israelite king reigned over them*

# 1 Chr 1:48
*ᵃ48* Possibly the Euphrates

# 1 Chr 1:50
*ᵃ50* Many Hebrew manuscripts, some Septuagint manuscripts, Vulgate and Syriac (see also [Gen. 36:39](vref:Gen 36:39)); most Hebrew manuscripts *Pai*

# 1 Chr 2:1
***Israel’s Sons (2:1–2)***

# 1 Chr 2:3
***Judah (2:3–4:23)***\
\
***To Hezron’s Sons (2:3–9)***

# 1 Chr 2:6
*ᵃ6* Many Hebrew manuscripts, some Septuagint manuscripts and Syriac (see also [1 Kings 4:31](vref:1 Kgs 4:31)); most Hebrew manuscripts *Dara*

# 1 Chr 2:7
*ᵃ7* *Achar* means *trouble*; *Achar* is called *Achan* in Joshua.\
*ᵇ7* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# 1 Chr 2:9
*ᵃ9* Hebrew *Kelubai*, a variant of *Caleb*

# 1 Chr 2:10
***From Ram Son of Hezron (2:10–17)***

# 1 Chr 2:11
*ᵃ11* Septuagint (see also [Ruth 4:21](vref:Ruth 4:21)); Hebrew *Salma*

# 1 Chr 2:18
***Caleb Son of Hezron (2:18–24)***

# 1 Chr 2:23
*ᵃ23* Or *captured the settlements of Jair*

# 1 Chr 2:24
*ᵃ24* *Father* may mean *civic leader* or *military leader*; also in verses [42](vref:1 Chr 2:42), [45](vref:1 Chr 2:45), [49–52](vref:1 Chr 2:49-52) and possibly elsewhere.

# 1 Chr 2:25
***Jerahmeel Son of Hezron (2:25–41)***\
\
*ᵃ25* Or *Oren and Ozem, by*

# 1 Chr 2:42
***The Clans of Caleb (2:42–55)***\
\
*ᵃ42* The meaning of the Hebrew for this phrase is uncertain.

# 1 Chr 2:55
*ᵃ55* Or *of the Sopherites*\
*ᵇ55* Or *father of Beth Recab*

# 1 Chr 3:1
***The Sons of David (3:1–9)***

# 1 Chr 3:5
*ᵃ5* Hebrew *Shimea*, a variant of *Shammua*\
*ᵇ5* One Hebrew manuscript and Vulgate (see also Septuagint and [2 Samuel 11:3](vref:2 Sam 11:3)); most Hebrew manuscripts *Bathshua*

# 1 Chr 3:6
*ᵃ6* Two Hebrew manuscripts (see also [2 Samuel 5:15](vref:2 Sam 5:15) and [1 Chron. 14:5](vref:1 Chr 14:5)); most Hebrew manuscripts *Elishama*

# 1 Chr 3:10
***The Kings of Judah (3:10–16)***

# 1 Chr 3:11
*ᵃ11* Hebrew *Joram*, a variant of *Jehoram*

# 1 Chr 3:16
*ᵃ16* Hebrew *Jeconiah*, a variant of *Jehoiachin*; also in verse [17](vref:1 Chr 3:17)

# 1 Chr 3:17
***The Royal Line After the Exile (3:17–24)***

# 1 Chr 4:1
***Other Clans of Judah (4:1–23)***

# 1 Chr 4:3
*ᵃ3* Some Septuagint manuscripts (see also Vulgate); Hebrew *father*

# 1 Chr 4:4
*ᵃ4* *Father* may mean *civic leader* or *military leader*; also in verses [12](vref:1 Chr 4:12), [14](vref:1 Chr 4:14), [17](vref:1 Chr 4:17), [18](vref:1 Chr 4:18) and possibly elsewhere.

# 1 Chr 4:9
*ᵃ9* *Jabez* sounds like the Hebrew for *pain*.

# 1 Chr 4:12
*ᵃ12* Or *of the city of Nahash*

# 1 Chr 4:13
*ᵃ13* Some Septuagint manuscripts and Vulgate; Hebrew does not have *and Meonothai*.

# 1 Chr 4:14
*ᵃ14* *Ge Harashim* means *valley of craftsmen*.

# 1 Chr 4:24
***Simeon (4:24–43)***

# 1 Chr 4:33
*ᵃ33* Some Septuagint manuscripts (see also [Joshua 19:8](vref:Josh 19:8)); Hebrew *Baal*

# 1 Chr 4:41
*ᵃ41* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# 1 Chr 5:1
***Reuben (5:1–10)***

# 1 Chr 5:6
*ᵃ6* Hebrew *Tilgath-Pilneser*, a variant of *Tiglath-Pileser*; also in verse [26](vref:1 Chr 5:26)

# 1 Chr 5:11
***Gad (5:11–22)***

# 1 Chr 5:23
***The Half-Tribe of Manasseh (5:23–26)***

# 1 Chr 6:1
***Levi (6:1–81)***

# 1 Chr 6:16
*ᵃ16* Hebrew *Gershom*, a variant of *Gershon*; also in verses [17](vref:1 Chr 6:17), [20](vref:1 Chr 6:20), [43](vref:1 Chr 6:43), [62](vref:1 Chr 6:62) and [71](vref:1 Chr 6:71)

# 1 Chr 6:26
*ᵃ26* Some Hebrew manuscripts, Septuagint and Syriac; most Hebrew manuscripts *Ahimoth ²⁶and Elkanah. The sons of Elkanah:*

# 1 Chr 6:27
*ᵃ27* Some Septuagint manuscripts (see also [1 Samuel 1:19,20](vref:1 Sam 1:19-20) and [1 Chron. 6:33,34](vref:1 Chr 6:33-34)); Hebrew does not have *and Samuel his son*.

# 1 Chr 6:28
*ᵃ28* Some Septuagint manuscripts and Syriac (see also [1 Samuel 8:2](vref:1 Sam 8:2) and [1 Chron. 6:33](vref:1 Chr 6:33)); Hebrew does not have *Joel*.

# 1 Chr 6:31
***The Temple Musicians (6:31–81)***

# 1 Chr 6:40
*ᵃ40* Most Hebrew manuscripts; some Hebrew manuscripts, one Septuagint manuscript and Syriac *Maaseiah*

# 1 Chr 6:57
*ᵃ57* See [Joshua 21:13](vref:Josh 21:13); Hebrew *given the cities of refuge: Hebron, Libnah*.

# 1 Chr 6:59
*ᵃ59* Syriac (see also Septuagint and [Joshua 21:16](vref:Josh 21:16)); Hebrew does not have *Juttah*.

# 1 Chr 6:60
*ᵃ60* See [Joshua 21:17](vref:Josh 21:17); Hebrew does not have *Gibeon*.

# 1 Chr 6:67
*ᵃ67* See [Joshua 21:21](vref:Josh 21:21); Hebrew *given the cities of refuge: Shechem, Gezer*.

# 1 Chr 6:77
*ᵃ77* See Septuagint and [Joshua 21:34](vref:Josh 21:34); Hebrew does not have *Jokneam, Kartah*.

# 1 Chr 7:1
***Issachar (7:1–5)***

# 1 Chr 7:6
***Benjamin (7:6–12)***

# 1 Chr 7:13
***Naphtali (7:13)***\
\
*ᵃ13* Some Hebrew and Septuagint manuscripts (see also [Gen. 46:24](vref:Gen 46:24) and [Num. 26:49](vref:Num 26:49)); most Hebrew manuscripts *Shallum*

# 1 Chr 7:14
***Manasseh (7:14–19)***

# 1 Chr 7:20
***Ephraim (7:20–29)***

# 1 Chr 7:23
*ᵃ23* *Beriah* sounds like the Hebrew for *misfortune*.

# 1 Chr 7:25
*ᵃ25* Some Septuagint manuscripts; Hebrew does not have *his son*.

# 1 Chr 7:30
***Asher (7:30–40)***

# 1 Chr 7:34
*ᵃ34* Or *of his brother Shomer: Rohgah*

# 1 Chr 7:37
*ᵃ37* Possibly a variant of *Jether*

# 1 Chr 8:1
***The Genealogy of Saul the Benjamite (8:1–9:1a)***

# 1 Chr 8:3
*ᵃ3* Or *Gera the father of Ehud*

# 1 Chr 8:29
*ᵃ29* Some Septuagint manuscripts (see also [1 Chron. 9:35](vref:1 Chr 9:35)); Hebrew does not have *Jeiel*.\
*ᵇ29* *Father* may mean *civic leader* or *military leader*.

# 1 Chr 8:30
*ᵃ30* Some Septuagint manuscripts (see also [1 Chron. 9:36](vref:1 Chr 9:36)); Hebrew does not have *Ner*.

# 1 Chr 8:33
*ᵃ33* Also known as *Ish-Bosheth*

# 1 Chr 8:34
*ᵃ34* Also known as *Mephibosheth*

# 1 Chr 9:1
***The People in Jerusalem (9:1b–34)***

# 1 Chr 9:19
*ᵃ19* That is, the temple; also in verses [21](vref:1 Chr 9:21) and [23](vref:1 Chr 9:23)

# 1 Chr 9:35
***The Genealogy of Saul (9:35–44)***\
\
*ᵃ35* *Father* may mean *civic leader* or *military leader*.

# 1 Chr 9:39
*ᵃ39* Also known as *Ish-Bosheth*

# 1 Chr 9:40
*ᵃ40* Also known as *Mephibosheth*

# 1 Chr 9:41
*ᵃ41* Vulgate and Syriac (see also Septuagint and [1 Chron. 8:35](vref:1 Chr 8:35)); Hebrew does not have *and Ahaz*.

# 1 Chr 9:42
*ᵃ42* Some Hebrew manuscripts and Septuagint (see also [1 Chron. 8:36](vref:1 Chr 8:36)); most Hebrew manuscripts *Jarah, Jarah*

# 1 Chr 10:1
***Saul Takes His Life (10:1–14)***

# 1 Chr 11:1
***David Becomes King Over Israel (11:1–3)***

# 1 Chr 11:4
***David Conquers Jerusalem (11:4–9)***

# 1 Chr 11:8
*ᵃ8* Or *the Millo*

# 1 Chr 11:10
***David’s Mighty Men (11:10–47)***

# 1 Chr 11:11
*ᵃ11* Possibly a variant of *Jashob-Baal*\
*ᵇ11* Or *Thirty*; some Septuagint manuscripts *Three* (see also [2 Samuel 23:8](vref:2 Sam 23:8))

# 1 Chr 11:23
*ᵃ23* Hebrew *five cubits* (about 2.3 meters)

# 1 Chr 12:1
***Warriors Join David (12:1–22)***

# 1 Chr 12:22
*ᵃ22* Or *a great and mighty army*

# 1 Chr 12:23
***Others Join David at Hebron (12:23–40)***

# 1 Chr 13:1
***Bringing Back the Ark (13:1–14)***

# 1 Chr 13:3
*ᵃ3* Or *we neglected*\
*ᵇ3* Or *him*

# 1 Chr 13:5
*ᵃ5* Or *to the entrance to*

# 1 Chr 13:11
*ᵃ11* *Perez Uzzah* means *outbreak against Uzzah*.

# 1 Chr 14:1
***David’s House and Family (14:1–7)***

# 1 Chr 14:7
*ᵃ7* A variant of *Eliada*

# 1 Chr 14:8
***David Defeats the Philistines (14:8–17)***

# 1 Chr 14:11
*ᵃ11* *Baal Perazim* means *the lord who breaks out*.

# 1 Chr 15:1
***The Ark Brought to Jerusalem (15:1–16:6)***

# 1 Chr 15:7
*ᵃ7* Hebrew *Gershom*, a variant of *Gershon*

# 1 Chr 15:18
*ᵃ18* Three Hebrew manuscripts and most Septuagint manuscripts (see also verse [20](vref:1 Chr 15:20) and [1 Chron. 16:5](vref:1 Chr 16:5)); most Hebrew manuscripts *Zechariah son and* or *Zechariah, Ben and*\
*ᵇ18* Hebrew; Septuagint (see also verse [21](vref:1 Chr 15:21)) *Jeiel and Azaziah*

# 1 Chr 15:20
*ᵃ20* Probably a musical term

# 1 Chr 15:21
*ᵃ21* Probably a musical term

# 1 Chr 16:1
*ᵃ1* Traditionally *peace offerings*; also in verse [2](vref:1 Chr 16:2)

# 1 Chr 16:7
***David’s Psalm of Thanks (16:7–43)***

# 1 Chr 16:15
*ᵃ15* Some Septuagint manuscripts (see also [Psalm 105:8](vref:Ps 105:8)); Hebrew *Remember*

# 1 Chr 16:20
*ᵃ18–20* One Hebrew manuscript, Septuagint and Vulgate (see also [Psalm 105:12](vref:Ps 105:12)); most Hebrew manuscripts *inherit, / ¹⁹though you are but few in number, / few indeed, and strangers in it.” / ²⁰They*

# 1 Chr 16:29
*ᵃ29* Or *Lᴏʀᴅ with the splendor of*

# 1 Chr 17:1
***God’s Promise to David (17:1–15)***

# 1 Chr 17:6
*ᵃ6* Traditionally *judges*; also in verse [10](vref:1 Chr 17:10)

# 1 Chr 17:16
***David’s Prayer (17:16–27)***

# 1 Chr 18:1
***David’s Victories (18:1–13)***

# 1 Chr 18:8
*ᵃ8* Hebrew *Tibhath*, a variant of *Tebah*

# 1 Chr 18:14
***David’s Officials (18:14–17)***

# 1 Chr 18:16
*ᵃ16* Some Hebrew manuscripts, Vulgate and Syriac (see also [2 Samuel 8:17](vref:2 Sam 8:17)); most Hebrew manuscripts *Abimelech*

# 1 Chr 19:1
***The Battle Against the Ammonites (19:1–19)***

# 1 Chr 19:6
*ᵃ6* That is, about 37 tons (about 34 metric tons)\
*ᵇ6* That is, Northwest Mesopotamia

# 1 Chr 19:16
*ᵃ16* That is, the Euphrates

# 1 Chr 20:1
***The Capture of Rabbah (20:1–3)***

# 1 Chr 20:2
*ᵃ2* Or *of Milcom*, that is, Molech\
*ᵇ2* That is, about 75 pounds (about 34 kilograms)

# 1 Chr 20:4
***War With the Philistines (20:4–8)***

# 1 Chr 21:1
***David Numbers the Fighting Men (21:1–22:1)***

# 1 Chr 21:12
*ᵃ12* Hebrew; Septuagint and Vulgate (see also [2 Samuel 24:13](vref:2 Sam 24:13)) *of fleeing*

# 1 Chr 21:15
*ᵃ15* Hebrew *Ornan*, a variant of *Araunah*; also in verses [18–28](vref:1 Chr 21:18-28)

# 1 Chr 21:25
*ᵃ25* That is, about 15 pounds (about 7 kilograms)

# 1 Chr 21:26
*ᵃ26* Traditionally *peace offerings*

# 1 Chr 22:2
***Preparations for the Temple (22:2–19)***

# 1 Chr 22:9
*ᵃ9* *Solomon* sounds like and may be derived from the Hebrew for *peace*.

# 1 Chr 22:14
*ᵃ14* That is, about 3,750 tons (about 3,450 metric tons)\
*ᵇ14* That is, about 37,500 tons (about 34,500 metric tons)

# 1 Chr 23:1
***The Levites (23:1–6)***

# 1 Chr 23:7
***Gershonites (23:7–11)***

# 1 Chr 23:10
*ᵃ10* One Hebrew manuscript, Septuagint and Vulgate (see also verse [11](vref:1 Chr 23:11)); most Hebrew manuscripts *Zina*

# 1 Chr 23:12
***Kohathites (23:12–20)***

# 1 Chr 23:21
***Merarites (23:21–32)***

# 1 Chr 24:1
***The Division of Priests (24:1–19)***

# 1 Chr 24:20
***The Rest of the Levites (24:20–31)***

# 1 Chr 24:23
*ᵃ23* Two Hebrew manuscripts and some Septuagint manuscripts (see also [1 Chron. 23:19](vref:1 Chr 23:19)); most Hebrew manuscripts *The sons of Jeriah:*

# 1 Chr 25:1
***The Singers (25:1–31)***

# 1 Chr 25:3
*ᵃ3* One Hebrew manuscript and some Septuagint manuscripts (see also verse [17](vref:1 Chr 25:17)); most Hebrew manuscripts do not have *Shimei*.

# 1 Chr 25:5
*ᵃ5* Hebrew *exalt the horn*

# 1 Chr 25:9
*ᵃ9* See Septuagint; Hebrew does not have *his sons and relatives*.\
*ᵇ9* See the total in verse [7](vref:1 Chr 25:7); Hebrew does not have *twelve*.

# 1 Chr 25:11
*ᵃ11* A variant of *Zeri*

# 1 Chr 25:14
*ᵃ14* A variant of *Asarelah*

# 1 Chr 25:18
*ᵃ18* A variant of *Uzziel*

# 1 Chr 26:1
***The Gatekeepers (26:1–19)***

# 1 Chr 26:14
*ᵃ14* A variant of *Meshelemiah*

# 1 Chr 26:20
***The Treasurers and Other Officials (26:20–32)***\
\
*ᵃ20* Septuagint; Hebrew *As for the Levites, Ahijah was*

# 1 Chr 27:1
***Army Divisions (27:1–15)***

# 1 Chr 27:16
***Officers of the Tribes (27:16–24)***

# 1 Chr 27:24
*ᵃ24* Septuagint; Hebrew *number*

# 1 Chr 27:25
***The King’s Overseers (27:25–34)***

# 1 Chr 28:1
***David’s Plans for the Temple (28:1–21)***

# 1 Chr 29:1
***Gifts for Building the Temple (29:1–9)***

# 1 Chr 29:2
*ᵃ2* The meaning of the Hebrew for this word is uncertain.

# 1 Chr 29:4
*ᵃ4* That is, about 110 tons (about 100 metric tons)\
*ᵇ4* That is, about 260 tons (about 240 metric tons)

# 1 Chr 29:7
*ᵃ7* That is, about 190 tons (about 170 metric tons)\
*ᵇ7* That is, about 185 pounds (about 84 kilograms)\
*ᶜ7* That is, about 375 tons (about 345 metric tons)\
*ᵈ7* That is, about 675 tons (about 610 metric tons)\
*ᵉ7* That is, about 3,750 tons (about 3,450 metric tons)

# 1 Chr 29:10
***David’s Prayer (29:10–20)***

# 1 Chr 29:21
***Solomon Acknowledged as King (29:21–25)***

# 1 Chr 29:26
***The Death of David (29:26–30)***

# 2 Chr 1:1
***Solomon Asks for Wisdom (1:1–17)***

# 2 Chr 1:14
*ᵃ14* Or *charioteers*

# 2 Chr 1:16
*ᵃ16* Or possibly *Muzur*, a region in Cilicia; also in verse [17](vref:2 Chr 1:17)\
*ᵇ16* Probably Cilicia

# 2 Chr 1:17
*ᵃ17* That is, about 15 pounds (about 7 kilograms)\
*ᵇ17* That is, about 3 3/4 pounds (about 1.7 kilograms)

# 2 Chr 2:1
***Preparations for Building the Temple (2:1–18)***

# 2 Chr 2:3
*ᵃ3* Hebrew *Huram*, a variant of *Hiram*; also in verses [11](vref:2 Chr 2:11) and [12](vref:2 Chr 2:12)

# 2 Chr 2:8
*ᵃ8* Probably a variant of *almug*; possibly juniper

# 2 Chr 2:10
*ᵃ10* That is, probably about 125,000 bushels (about 4,400 kiloliters)\
*ᵇ10* That is, probably about 115,000 gallons (about 440 kiloliters)

# 2 Chr 3:1
***Solomon Builds the Temple (3:1–17)***\
\
*ᵃ1* Hebrew *Ornan*, a variant of *Araunah*

# 2 Chr 3:3
*ᵃ3* That is, about 90 feet (about 27 meters) long and 30 feet (about 9 meters) wide

# 2 Chr 3:4
*ᵃ4* That is, about 30 feet (about 9 meters); also in verses [8](vref:2 Chr 3:8), [11](vref:2 Chr 3:11) and [13](vref:2 Chr 3:13)\
*ᵇ4* Some Septuagint and Syriac manuscripts; Hebrew *and a hundred and twenty*

# 2 Chr 3:8
*ᵃ8* That is, about 23 tons (about 21 metric tons)

# 2 Chr 3:9
*ᵃ9* That is, about 1 1/4 pounds (about 0.6 kilogram)

# 2 Chr 3:11
*ᵃ11* That is, about 7 1/2 feet (about 2.3 meters); also in verse [15](vref:2 Chr 3:15)

# 2 Chr 3:13
*ᵃ13* Or *facing inward*

# 2 Chr 3:15
*ᵃ15* That is, about 52 feet (about 16 meters)

# 2 Chr 3:16
*ᵃ16* Or possibly *made chains in the inner sanctuary*; the meaning of the Hebrew for this phrase is uncertain.

# 2 Chr 3:17
*ᵃ17* *Jakin* probably means *he establishes*.\
*ᵇ17* *Boaz* probably means *in him is strength*.

# 2 Chr 4:1
***The Temple’s Furnishings (4:1–5:1)***\
\
*ᵃ1* That is, about 30 feet (about 9 meters) long and wide, and about 15 feet (about 4.5 meters) high

# 2 Chr 4:2
*ᵃ2* That is, about 7 1/2 feet (about 2.3 meters)\
*ᵇ2* That is, about 45 feet (about 13.5 meters)

# 2 Chr 4:3
*ᵃ3* That is, about 1 1/2 feet (about 0.5 meter)

# 2 Chr 4:5
*ᵃ5* That is, about 3 inches (about 8 centimeters)\
*ᵇ5* That is, about 17,500 gallons (about 66 kiloliters)

# 2 Chr 4:17
*ᵃ17* Hebrew *Zeredatha*, a variant of *Zarethan*

# 2 Chr 5:2
***The Ark Brought to the Temple (5:2–6:11)***

# 2 Chr 6:12
***Solomon’s Prayer of Dedication (6:12–42)***

# 2 Chr 6:13
*ᵃ13* That is, about 7 1/2 feet (about 2.3 meters)\
*ᵇ13* That is, about 4 1/2 feet (about 1.3 meters)

# 2 Chr 7:1
***The Dedication of the Temple (7:1–10)***

# 2 Chr 7:7
*ᵃ7* Traditionally *peace offerings*

# 2 Chr 7:8
*ᵃ8* Or *from the entrance to*

# 2 Chr 7:11
***The Lᴏʀᴅ Appears to Solomon (7:11–22)***

# 2 Chr 7:19
*ᵃ19* The Hebrew is plural.

# 2 Chr 8:1
***Solomon’s Other Activities (8:1–18)***

# 2 Chr 8:2
*ᵃ2* Hebrew *Huram*, a variant of *Hiram*; also in verse [18](vref:2 Chr 8:18)

# 2 Chr 8:6
*ᵃ6* Or *charioteers*

# 2 Chr 8:18
*ᵃ18* That is, about 17 tons (about 16 metric tons)

# 2 Chr 9:1
***The Queen of Sheba Visits Solomon (9:1–12)***

# 2 Chr 9:4
*ᵃ4* Or *the ascent by which he went up to*

# 2 Chr 9:9
*ᵃ9* That is, about 4 1/2 tons (about 4 metric tons)

# 2 Chr 9:10
*ᵃ10* Probably a variant of *almugwood*

# 2 Chr 9:13
***Solomon’s Splendor (9:13–28)***\
\
*ᵃ13* That is, about 25 tons (about 23 metric tons)

# 2 Chr 9:15
*ᵃ15* That is, about 7 1/2 pounds (about 3.5 kilograms)

# 2 Chr 9:16
*ᵃ16* That is, about 3 3/4 pounds (about 1.7 kilograms)

# 2 Chr 9:21
*ᵃ21* Hebrew *of ships that could go to Tarshish*\
*ᵇ21* Hebrew *Huram*, a variant of *Hiram*

# 2 Chr 9:25
*ᵃ25* Or *charioteers*

# 2 Chr 9:26
*ᵃ26* That is, the Euphrates

# 2 Chr 9:28
*ᵃ28* Or possibly *Muzur*, a region in Cilicia

# 2 Chr 9:29
***Solomon’s Death (9:29–31)***

# 2 Chr 10:1
***Israel Rebels Against Rehoboam (10:1–11:4)***

# 2 Chr 10:18
*ᵃ18* Hebrew *Hadoram*, a variant of *Adoniram*

# 2 Chr 11:5
***Rehoboam Fortifies Judah (11:5–17)***

# 2 Chr 11:18
***Rehoboam’s Family (11:18–23)***

# 2 Chr 12:1
***Shishak Attacks Jerusalem (12:1–16)***\
\
*ᵃ1* That is, Judah, as frequently in 2 Chronicles

# 2 Chr 12:3
*ᵃ3* That is, people from the upper Nile region

# 2 Chr 13:1
***Abijah King of Judah (13:1–14:1)***

# 2 Chr 13:2
*ᵃ2* Most Septuagint manuscripts and Syriac (see also [2 Chron. 11:20](vref:2 Chr 11:20) and [1 Kings 15:2](vref:1 Kgs 15:2)); Hebrew *Micaiah*\
*ᵇ2* Or *granddaughter*

# 2 Chr 14:2
***Asa King of Judah (14:2–15)***

# 2 Chr 14:3
*ᵃ3* That is, symbols of the goddess Asherah; here and elsewhere in 2 Chronicles

# 2 Chr 14:9
*ᵃ9* Hebrew *with an army of a thousand thousands* or *with an army of thousands upon thousands*

# 2 Chr 15:1
***Asa’s Reform (15:1–19)***

# 2 Chr 15:8
*ᵃ8* Vulgate and Syriac (see also Septuagint and verse [1](vref:2 Chr 15:1)); Hebrew does not have *Azariah son of*.

# 2 Chr 16:1
***Asa’s Last Years (16:1–14)***

# 2 Chr 16:4
*ᵃ4* Also known as *Abel Beth Maacah*

# 2 Chr 16:8
*ᵃ8* That is, people from the upper Nile region\
*ᵇ8* Or *charioteers*

# 2 Chr 17:1
***Jehoshaphat King of Judah (17:1–19)***

# 2 Chr 18:1
***Micaiah Prophesies Against Ahab (18:1–27)***

# 2 Chr 18:23
*ᵃ23* Or *Spirit of*

# 2 Chr 18:28
***Ahab Killed at Ramoth Gilead (18:28–19:3)***

# 2 Chr 19:2
*ᵃ2* Or *and make alliances with*

# 2 Chr 19:4
***Jehoshaphat Appoints Judges (19:4–11)***

# 2 Chr 20:1
***Jehoshaphat Defeats Moab and Ammon (20:1–30)***\
\
*ᵃ1* Some Septuagint manuscripts; Hebrew *Ammonites*

# 2 Chr 20:2
*ᵃ2* One Hebrew manuscript; most Hebrew manuscripts, Septuagint and Vulgate *Aram*\
*ᵇ2* That is, the Dead Sea

# 2 Chr 20:21
*ᵃ21* Or *him with the splendor of*

# 2 Chr 20:25
*ᵃ25* Some Hebrew manuscripts and Vulgate; most Hebrew manuscripts *corpses*

# 2 Chr 20:26
*ᵃ26* *Beracah* means *praise*.

# 2 Chr 20:31
***The End of Jehoshaphat’s Reign (20:31–21:3)***

# 2 Chr 20:36
*ᵃ36* Hebrew *of ships that could go to Tarshish*

# 2 Chr 20:37
*ᵃ37* Hebrew *sail for Tarshish*

# 2 Chr 21:2
*ᵃ2* That is, Judah, as frequently in 2 Chronicles

# 2 Chr 21:4
***Jehoram King of Judah (21:4–20)***

# 2 Chr 21:17
*ᵃ17* Hebrew *Jehoahaz*, a variant of *Ahaziah*

# 2 Chr 22:1
***Ahaziah King of Judah (22:1–9)***

# 2 Chr 22:2
*ᵃ2* Some Septuagint manuscripts and Syriac (see also [2 Kings 8:26](vref:2 Kgs 8:26)); Hebrew *forty-two*

# 2 Chr 22:5
*ᵃ5* Hebrew *Jehoram*, a variant of *Joram*; also in verses [6](vref:2 Chr 22:6) and [7](vref:2 Chr 22:7)

# 2 Chr 22:6
*ᵃ6* Hebrew *Ramah*, a variant of *Ramoth*\
*ᵇ6* Some Hebrew manuscripts, Septuagint, Vulgate and Syriac (see also [2 Kings 8:29](vref:2 Kgs 8:29)); most Hebrew manuscripts *Azariah*

# 2 Chr 22:10
***Athaliah and Joash (22:10–23:21)***

# 2 Chr 22:11
*ᵃ11* Hebrew *Jehoshabeath*, a variant of *Jehosheba*

# 2 Chr 23:6
*ᵃ6* Or *to observe the Lᴏʀᴅ’s command ⌞​not to enter​⌟*

# 2 Chr 23:14
*ᵃ14* Or *out from the precincts*

# 2 Chr 23:16
*ᵃ16* Or *covenant between ⌞​the Lᴏʀᴅ​⌟ and the people and the king that they* (see [2 Kings 11:17](vref:2 Kgs 11:17))

# 2 Chr 24:1
***Joash Repairs the Temple (24:1–16)***

# 2 Chr 24:17
***The Wickedness of Joash (24:17–27)***

# 2 Chr 24:23
*ᵃ23* Probably in the spring

# 2 Chr 24:26
*ᵃ26* A variant of *Jozabad*\
*ᵇ26* A variant of *Shomer*

# 2 Chr 25:1
***Amaziah King of Judah (25:1–28)***\
\
*ᵃ1* Hebrew *Jehoaddan*, a variant of *Jehoaddin*

# 2 Chr 25:4
*ᵃ4* [Deut. 24:16](vref:Deut 24:16)

# 2 Chr 25:6
*ᵃ6* That is, about 3 3/4 tons (about 3.4 metric tons); also in verse [9](vref:2 Chr 25:9)

# 2 Chr 25:17
*ᵃ17* Hebrew *Joash*, a variant of *Jehoash*; also in verses [18](vref:2 Chr 25:18), [21](vref:2 Chr 25:21), [23](vref:2 Chr 25:23) and [25](vref:2 Chr 25:25)

# 2 Chr 25:23
*ᵃ23* Hebrew *Jehoahaz*, a variant of *Ahaziah*\
*ᵇ23* Hebrew *four hundred cubits* (about 180 meters)

# 2 Chr 26:1
***Uzziah King of Judah (26:1–23)***\
\
*ᵃ1* Also called *Azariah*

# 2 Chr 26:5
*ᵃ5* Many Hebrew manuscripts, Septuagint and Syriac; other Hebrew manuscripts *vision*

# 2 Chr 26:19
*ᵃ19* The Hebrew word was used for various diseases affecting the skin—not necessarily leprosy; also in verses [20](vref:2 Chr 26:20), [21](vref:2 Chr 26:21) and [23](vref:2 Chr 26:23).

# 2 Chr 26:21
*ᵃ21* Or *in a house where he was relieved of responsibilities*

# 2 Chr 27:1
***Jotham King of Judah (27:1–9)***

# 2 Chr 27:5
*ᵃ5* That is, about 3 3/4 tons (about 3.4 metric tons)\
*ᵇ5* That is, probably about 62,000 bushels (about 2,200 kiloliters)

# 2 Chr 28:1
***Ahaz King of Judah (28:1–27)***

# 2 Chr 28:16
*ᵃ16* One Hebrew manuscript, Septuagint and Vulgate (see also [2 Kings 16:7](vref:2 Kgs 16:7)); most Hebrew manuscripts *kings*

# 2 Chr 28:19
*ᵃ19* That is, Judah, as frequently in 2 Chronicles

# 2 Chr 28:20
*ᵃ20* Hebrew *Tilgath-Pilneser*, a variant of *Tiglath-Pileser*

# 2 Chr 28:24
*ᵃ24* Or *and cut them up*

# 2 Chr 29:1
***Hezekiah Purifies the Temple (29:1–36)***

# 2 Chr 29:35
*ᵃ35* Traditionally *peace offerings*

# 2 Chr 30:1
***Hezekiah Celebrates the Passover (30:1–31:1)***

# 2 Chr 30:21
*ᵃ21* Or *priests praised the Lᴏʀᴅ every day with resounding instruments belonging to the Lᴏʀᴅ*

# 2 Chr 30:22
*ᵃ22* Traditionally *peace offerings*

# 2 Chr 31:2
***Contributions for Worship (31:2–21)***\
\
*ᵃ2* Traditionally *peace offerings*

# 2 Chr 32:1
***Sennacherib Threatens Jerusalem (32:1–23)***

# 2 Chr 32:4
*ᵃ4* Hebrew; Septuagint and Syriac *king*

# 2 Chr 32:5
*ᵃ5* Or *the Millo*

# 2 Chr 32:22
*ᵃ22* Hebrew; Septuagint and Vulgate *He gave them rest*

# 2 Chr 32:24
***Hezekiah’s Pride, Success and Death (32:24–33)***

# 2 Chr 33:1
***Manasseh King of Judah (33:1–20)***

# 2 Chr 33:6
*ᵃ6* Or *He made his sons pass through*

# 2 Chr 33:16
*ᵃ16* Traditionally *peace offerings*

# 2 Chr 33:18
*ᵃ18* That is, Judah, as frequently in 2 Chronicles

# 2 Chr 33:19
*ᵃ19* One Hebrew manuscript and Septuagint; most Hebrew manuscripts *of Hozai*

# 2 Chr 33:21
***Amon King of Judah (33:21–25)***

# 2 Chr 34:1
***Josiah’s Reforms (34:1–13)***

# 2 Chr 34:14
***The Book of the Law Found (34:14–33)***

# 2 Chr 34:20
*ᵃ20* Also called *Acbor son of Micaiah*

# 2 Chr 34:22
*ᵃ22* One Hebrew manuscript, Vulgate and Syriac; most Hebrew manuscripts do not have *had sent with him*.\
*ᵇ22* Also called *Tikvah*\
*ᶜ22* Also called *Harhas*

# 2 Chr 34:25
*ᵃ25* Or *by everything they have done*

# 2 Chr 35:1
***Josiah Celebrates the Passover (35:1–19)***

# 2 Chr 35:20
***The Death of Josiah (35:20–36:1)***

# 2 Chr 36:2
***Jehoahaz King of Judah (36:2–4)***\
\
*ᵃ2* Hebrew *Joahaz*, a variant of *Jehoahaz*; also in verse [4](vref:2 Chr 36:4)

# 2 Chr 36:3
*ᵃ3* That is, about 3 3/4 tons (about 3.4 metric tons)\
*ᵇ3* That is, about 75 pounds (about 34 kilograms)

# 2 Chr 36:5
***Jehoiakim King of Judah (36:5–8)***

# 2 Chr 36:7
*ᵃ7* Or *palace*

# 2 Chr 36:9
***Jehoiachin King of Judah (36:9–10)***\
\
*ᵃ9* One Hebrew manuscript, some Septuagint manuscripts and Syriac (see also [2 Kings 24:8](vref:2 Kgs 24:8)); most Hebrew manuscripts *eight*

# 2 Chr 36:10
*ᵃ10* Hebrew *brother*, that is, relative (see [2 Kings 24:17](vref:2 Kgs 24:17))

# 2 Chr 36:11
***Zedekiah King of Judah (36:11–14)***

# 2 Chr 36:15
***The Fall of Jerusalem (36:15–23)***

# 2 Chr 36:17
*ᵃ17* Or *Chaldeans*

# Ezra 1:1
***Cyrus Helps the Exiles to Return (1:1–11)***

# Ezra 1:7
*ᵃ7* Or *gods*

# Ezra 1:9
*ᵃ9* The meaning of the Hebrew for this word is uncertain.

# Ezra 2:1
***The List of the Exiles Who Returned (2:1–70)***

# Ezra 2:25
*ᵃ25* See Septuagint (see also [Neh. 7:29](vref:Neh 7:29)); Hebrew *Kiriath Arim*.

# Ezra 2:69
*ᵃ69* That is, about 1,100 pounds (about 500 kilograms)\
*ᵇ69* That is, about 3 tons (about 2.9 metric tons)

# Ezra 3:1
***Rebuilding the Altar (3:1–6)***

# Ezra 3:7
***Rebuilding the Temple (3:7–13)***

# Ezra 3:9
*ᵃ9* Hebrew *Yehudah*, probably a variant of *Hodaviah*

# Ezra 4:1
***Opposition to the Rebuilding (4:1–5)***

# Ezra 4:4
*ᵃ4* Or *and troubled them as they built*

# Ezra 4:6
***Later Opposition Under Xerxes and Artaxerxes (4:6–24)***\
\
*ᵃ6* Hebrew *Ahasuerus*, a variant of Xerxes’ Persian name

# Ezra 4:7
*ᵃ7* Or *written in Aramaic and translated*\
*ᵇ7* The text of [Ezra 4:8–6:18](vref:Ezra 4:8-6:18) is in Aramaic.

# Ezra 4:9
*ᵃ9* Or *officials, magistrates and governors over the men from*

# Ezra 4:10
*ᵃ10* Aramaic *Osnappar*, a variant of *Ashurbanipal*

# Ezra 5:1
***Tattenai’s Letter to Darius (5:1–17)***

# Ezra 5:4
*ᵃ4* See Septuagint; Aramaic *⁴We told them the names of the men constructing this building.*

# Ezra 5:14
*ᵃ14* Or *palace*

# Ezra 6:1
***The Decree of Darius (6:1–12)***

# Ezra 6:3
*ᵃ3* Aramaic *sixty cubits* (about 27 meters)

# Ezra 6:13
***Completion and Dedication of the Temple (6:13–18)***

# Ezra 6:19
***The Passover (6:19–22)***

# Ezra 7:1
***Ezra Comes to Jerusalem (7:1–10)***

# Ezra 7:11
***King Artaxerxes’ Letter to Ezra (7:11–28)***

# Ezra 7:12
*ᵃ12* The text of [Ezra 7:12–26](vref:Ezra 7:12-26) is in Aramaic.

# Ezra 7:22
*ᵃ22* That is, about 3 3/4 tons (about 3.4 metric tons)\
*ᵇ22* That is, probably about 600 bushels (about 22 kiloliters)\
*ᶜ22* That is, probably about 600 gallons (about 2.2 kiloliters)

# Ezra 8:1
***List of the Family Heads Returning With Ezra (8:1–14)***

# Ezra 8:5
*ᵃ5* Some Septuagint manuscripts (also [1 Esdras 8:32](vref:1 Esd 8:32)); Hebrew does not have *Zattu*.

# Ezra 8:10
*ᵃ10* Some Septuagint manuscripts (also [1 Esdras 8:36](vref:1 Esd 8:36)); Hebrew does not have *Bani*.

# Ezra 8:15
***The Return to Jerusalem (8:15–36)***

# Ezra 8:26
*ᵃ26* That is, about 25 tons (about 22 metric tons)\
*ᵇ26* That is, about 3 3/4 tons (about 3.4 metric tons)

# Ezra 8:27
*ᵃ27* That is, about 19 pounds (about 8.5 kilograms)

# Ezra 9:1
***Ezra’s Prayer About Intermarriage (9:1–15)***

# Ezra 10:1
***The People’s Confession of Sin (10:1–17)***

# Ezra 10:18
***Those Guilty of Intermarriage (10:18–44)***

# Ezra 10:38
*ᵃ37,38* See Septuagint (also [1 Esdras 9:34](vref:1 Esd 9:34)); Hebrew *Jaasu ³⁸and Bani and Binnui,*

# Ezra 10:44
*ᵃ44* Or *and they sent them away with their children*

# Neh 1:1
***Nehemiah’s Prayer (1:1–11)***

# Neh 2:1
***Artaxerxes Sends Nehemiah to Jerusalem (2:1–10)***

# Neh 2:11
***Nehemiah Inspects Jerusalem’s Walls (2:11–20)***

# Neh 2:13
*ᵃ13* Or *Serpent* or *Fig*

# Neh 3:1
***Builders of the Wall (3:1–32)***

# Neh 3:5
*ᵃ5* Or *their Lord* or *the governor*

# Neh 3:6
*ᵃ6* Or *Old*

# Neh 3:8
*ᵃ8* Or *They left out part of*

# Neh 3:13
*ᵃ13* Hebrew *a thousand cubits* (about 450 meters)

# Neh 3:15
*ᵃ15* Hebrew *Shelah*, a variant of *Shiloah*, that is, Siloam

# Neh 3:16
*ᵃ16* Hebrew; Septuagint, some Vulgate manuscripts and Syriac *tomb*

# Neh 3:18
*ᵃ18* Two Hebrew manuscripts and Syriac (see also Septuagint and verse [24](vref:Neh 3:24)); most Hebrew manuscripts *Bavvai*

# Neh 4:1
***Opposition to the Rebuilding (4:1–23)***

# Neh 4:5
*ᵃ5* Or *have provoked you to anger before*

# Neh 4:23
*ᵃ23* The meaning of the Hebrew for this clause is uncertain.

# Neh 5:1
***Nehemiah Helps the Poor (5:1–19)***

# Neh 5:15
*ᵃ15* That is, about 1 pound (about 0.5 kilogram)

# Neh 5:16
*ᵃ16* Most Hebrew manuscripts; some Hebrew manuscripts, Septuagint, Vulgate and Syriac *I*

# Neh 6:1
***Further Opposition to the Rebuilding (6:1–14)***

# Neh 6:2
*ᵃ2* Or *in Kephirim*

# Neh 6:6
*ᵃ6* Hebrew *Gashmu*, a variant of *Geshem*

# Neh 6:15
***The Completion of the Wall (6:15–7:3)***

# Neh 7:2
*ᵃ2* Or *Hanani, that is,*

# Neh 7:4
***The List of the Exiles Who Returned (7:4–73a)***

# Neh 7:68
*ᵃ68* Some Hebrew manuscripts (see also [Ezra 2:66](vref:Ezra 2:66)); most Hebrew manuscripts do not have this verse.

# Neh 7:70
*ᵃ70* That is, about 19 pounds (about 8.5 kilograms)

# Neh 7:71
*ᵃ71* That is, about 375 pounds (about 170 kilograms); also in verse [72](vref:Neh 7:72)\
*ᵇ71* That is, about 1 1/3 tons (about 1.2 metric tons)

# Neh 7:72
*ᵃ72* That is, about 1 1/4 tons (about 1.1 metric tons)

# Neh 7:73
***Ezra Reads the Law (7:73b–8:18)***

# Neh 8:8
*ᵃ8* Or *God, translating it*

# Neh 8:15
*ᵃ15* See [Lev. 23:37–40](vref:Lev 23:37-40).

# Neh 9:1
***The Israelites Confess Their Sins (9:1–37)***

# Neh 9:5
*ᵃ5* Or *God for ever and ever*

# Neh 9:9
*ᵃ9* Hebrew *Yam Suph*; that is, Sea of Reeds

# Neh 9:22
*ᵃ22* One Hebrew manuscript and Septuagint; most Hebrew manuscripts *Sihon, that is, the country of the*

# Neh 9:38
***The Agreement of the People (9:38–10:39)***

# Neh 10:32
*ᵃ32* That is, about 1/8 ounce (about 4 grams)

# Neh 11:1
***The New Residents of Jerusalem (11:1–36)***

# Neh 11:14
*ᵃ14* Most Septuagint manuscripts; Hebrew *their*

# Neh 12:1
***Priests and Levites (12:1–26)***

# Neh 12:4
*ᵃ4* Many Hebrew manuscripts and Vulgate (see also [Neh. 12:16](vref:Neh 12:16)); most Hebrew manuscripts *Ginnethoi*

# Neh 12:5
*ᵃ5* A variant of *Miniamin*

# Neh 12:14
*ᵃ14* Very many Hebrew manuscripts, some Septuagint manuscripts and Syriac (see also [Neh. 12:3](vref:Neh 12:3)); most Hebrew manuscripts *Shebaniah’s*

# Neh 12:15
*ᵃ15* Some Septuagint manuscripts (see also [Neh. 12:3](vref:Neh 12:3)); Hebrew *Meraioth’s*

# Neh 12:27
***Dedication of the Wall of Jerusalem (12:27–47)***

# Neh 12:31
*ᵃ31* Or *go alongside*\
*ᵇ31* Or *proceed alongside*

# Neh 12:38
*ᵃ38* Or *them alongside*

# Neh 12:39
*ᵃ39* Or *Old*

# Neh 13:1
***Nehemiah’s Final Reforms (13:1–31)***

# Esth 1:1
***Queen Vashti Deposed (1:1–22)***\
\
*ᵃ1* Hebrew *Ahasuerus*, a variant of Xerxes’ Persian name; here and throughout Esther\
*ᵇ1* That is, the upper Nile region

# Esth 2:1
***Esther Made Queen (2:1–18)***

# Esth 2:6
*ᵃ6* Hebrew *Jeconiah*, a variant of *Jehoiachin*

# Esth 2:19
***Mordecai Uncovers a Conspiracy (2:19–23)***

# Esth 2:21
*ᵃ21* Hebrew *Bigthan*, a variant of *Bigthana*

# Esth 2:23
*ᵃ23* Or *were hung* (or *impaled*) *on poles*; similarly elsewhere in Esther

# Esth 3:1
***Haman’s Plot to Destroy the Jews (3:1–15)***

# Esth 3:7
*ᵃ7* Septuagint; Hebrew does not have *And the lot fell on*.

# Esth 3:9
*ᵃ9* That is, about 375 tons (about 345 metric tons)

# Esth 4:1
***Mordecai Persuades Esther to Help (4:1–17)***

# Esth 5:1
***Esther’s Request to the King (5:1–14)***

# Esth 5:14
*ᵃ14* Hebrew *fifty cubits* (about 23 meters)

# Esth 6:1
***Mordecai Honored (6:1–14)***

# Esth 7:1
***Haman Hanged (7:1–10)***

# Esth 7:4
*ᵃ4* Or *quiet, but the compensation our adversary offers cannot be compared with the loss the king would suffer*

# Esth 7:9
*ᵃ9* Hebrew *fifty cubits* (about 23 meters)

# Esth 8:1
***The King’s Edict in Behalf of the Jews (8:1–17)***

# Esth 8:9
*ᵃ9* That is, the upper Nile region

# Esth 9:1
***Triumph of the Jews (9:1–17)***

# Esth 9:18
***Purim Celebrated (9:18–32)***

# Esth 9:25
*ᵃ25* Or *when Esther came before the king*

# Esth 10:1
***The Greatness of Mordecai (10:1–3)***

# Job 1:1
***Prologue (1:1–5)***

# Job 1:6
***Job’s First Test (1:6–22)***\
\
*ᵃ6* Hebrew *the sons of God*\
*ᵇ6* *Satan* means *accuser*.

# Job 1:21
*ᵃ21* Or *will return there*

# Job 2:1
***Job’s Second Test (2:1–10)***\
\
*ᵃ1* Hebrew *the sons of God*

# Job 2:10
*ᵃ10* The Hebrew word rendered *foolish* denotes moral deficiency.

# Job 2:11
***Job’s Three Friends (2:11–13)***

# Job 3:1
***Job Speaks (3:1–26)***

# Job 3:5
*ᵃ5* Or *and the shadow of death*

# Job 3:8
*ᵃ8* Or *the sea*

# Job 4:1
***Eliphaz (4:1–5:27)***

# Job 4:21
*ᵃ21* Some interpreters end the quotation after verse [17](vref:Job 4:17).

# Job 5:17
*ᵃ17* Hebrew *Shaddai*; here and throughout Job

# Job 6:1
***Job (6:1–7:21)***

# Job 6:6
*ᵃ6* The meaning of the Hebrew for this phrase is uncertain.

# Job 6:29
*ᵃ29* Or *my righteousness still stands*

# Job 7:9
*ᵃ9* Hebrew *Sheol*

# Job 7:20
*ᵃ20* A few manuscripts of the Masoretic Text, an ancient Hebrew scribal tradition and Septuagint; most manuscripts of the Masoretic Text *I have become a burden to myself.*

# Job 8:1
***Bildad (8:1–22)***

# Job 8:14
*ᵃ14* The meaning of the Hebrew for this word is uncertain.

# Job 8:19
*ᵃ19* Or *Surely all the joy it has / is that*

# Job 9:1
***Job (9:1–10:22)***

# Job 9:19
*ᵃ19* See Septuagint; Hebrew *me*.

# Job 9:30
*ᵃ30* Or *snow*

# Job 10:15
*ᵃ15* Or *and aware of*

# Job 10:21
*ᵃ21* Or *and the shadow of death*; also in verse [22](vref:Job 10:22)

# Job 11:1
***Zophar (11:1–20)***

# Job 11:8
*ᵃ8* Hebrew *than Sheol*

# Job 11:12
*ᵃ12* Or *wild donkey can be born tame*

# Job 12:1
***Job (12:1–14:22)***

# Job 12:6
*ᵃ6* Or *secure / in what God’s hand brings them*

# Job 12:18
*ᵃ18* Or *shackles of kings / and ties a belt*

# Job 13:15
*ᵃ15* Or *He will surely slay me; I have no hope— / yet I will*

# Job 14:3
*ᵃ3* Septuagint, Vulgate and Syriac; Hebrew *me*

# Job 14:13
*ᵃ13* Hebrew *Sheol*

# Job 14:14
*ᵃ14* Or *release*

# Job 15:1
***Eliphaz (15:1–35)***

# Job 15:23
*ᵃ23* Or *about, looking for food*

# Job 16:1
***Job (16:1–17:16)***

# Job 16:20
*ᵃ20* Or *My friends treat me with scorn*

# Job 17:13
*ᵃ13* Hebrew *Sheol*

# Job 17:16
*ᵃ16* Hebrew *Sheol*

# Job 18:1
***Bildad (18:1–21)***

# Job 18:15
*ᵃ15* Or *Nothing he had remains*

# Job 19:1
***Job (19:1–29)***

# Job 19:20
*ᵃ20* Or *only my gums*

# Job 19:24
*ᵃ24* Or *and*

# Job 19:25
*ᵃ25* Or *defender*\
*ᵇ25* Or *upon my grave*

# Job 19:26
*ᵃ26* Or *And after I awake, / though this ⌞​body​⌟ has been destroyed, / then*\
*ᵇ26* Or *apart from*

# Job 19:28
*ᵃ28* Many Hebrew manuscripts, Septuagint and Vulgate; most Hebrew manuscripts *me*

# Job 19:29
*ᵃ29* Or */ that you may come to know the Almighty*

# Job 20:1
***Zophar (20:1–29)***

# Job 20:4
*ᵃ4* Or *Adam*

# Job 20:28
*ᵃ28* Or *The possessions in his house will be carried off, / washed away*

# Job 21:1
***Job (21:1–34)***

# Job 21:13
*ᵃ13* Hebrew *Sheol*\
*ᵇ13* Or *in an instant*

# Job 21:20
*ᵃ17–20* Verses [17](vref:Job 21:17) and [18](vref:Job 21:18) may be taken as exclamations and [19](vref:Job 21:19) and [20](vref:Job 21:20) as declarations.

# Job 21:24
*ᵃ24* The meaning of the Hebrew for this word is uncertain.

# Job 21:30
*ᵃ30* Or *man is reserved for the day of calamity, / that he is brought forth to*

# Job 21:33
*ᵃ33* Or */ as a countless throng went*

# Job 22:1
***Eliphaz (22:1–30)***

# Job 23:1
***Job (23:1–24:25)***

# Job 23:2
*ᵃ2* Septuagint and Syriac; Hebrew */ the hand on me*\
*ᵇ2* Or *heavy on me in*

# Job 24:11
*ᵃ11* Or *olives between the millstones*; the meaning of the Hebrew for this word is uncertain.

# Job 24:17
*ᵃ17* Or *them, their morning is like the shadow of death*\
*ᵇ17* Or *of the shadow of death*

# Job 24:19
*ᵃ19* Hebrew *Sheol*

# Job 25:1
***Bildad (25:1–6)***

# Job 26:1
***Job (26:1–31:40)***

# Job 26:6
*ᵃ6* Hebrew *Sheol*\
*ᵇ6* Hebrew *Abaddon*

# Job 28:6
*ᵃ6* Or *lapis lazuli*; also in verse [16](vref:Job 28:16)

# Job 28:11
*ᵃ11* Septuagint, Aquila and Vulgate; Hebrew *He dams up*

# Job 28:22
*ᵃ22* Hebrew *Abaddon*

# Job 29:24
*ᵃ24* The meaning of the Hebrew for this clause is uncertain.

# Job 30:3
*ᵃ3* Or *gnawed*

# Job 30:4
*ᵃ4* Or *fuel*

# Job 30:12
*ᵃ12* The meaning of the Hebrew for this word is uncertain.

# Job 30:13
*ᵃ13* Or *me. / ‘No one can help him,’ ⌞​they say​⌟.*

# Job 30:18
*ᵃ18* Hebrew; Septuagint *⌞​God​⌟ grasps my clothing*

# Job 31:12
*ᵃ12* Hebrew *Abaddon*

# Job 31:33
*ᵃ33* Or *as Adam did*

# Job 32:1
***Elihu (32:1–37:24)***

# Job 32:3
*ᵃ3* Masoretic Text; an ancient Hebrew scribal tradition *Job, and so had condemned God*

# Job 32:8
*ᵃ8* Or *Spirit*; also in verse [18](vref:Job 32:18)

# Job 32:9
*ᵃ9* Or *many*; or *great*

# Job 33:13
*ᵃ13* Or *that he does not answer for any of his actions*

# Job 33:18
*ᵃ18* Or *preserve him from the grave*\
*ᵇ18* Or *from crossing the River*

# Job 33:22
*ᵃ22* Or *He draws near to the grave*\
*ᵇ22* Or *to the dead*

# Job 33:24
*ᵃ24* Or *grave*

# Job 33:28
*ᵃ28* Or *redeemed me from going down to the grave*

# Job 33:30
*ᵃ30* Or *turn him back from the grave*

# Job 34:14
*ᵃ14* Or *Spirit*

# Job 35:2
*ᵃ2* Or *My righteousness is more than God’s*

# Job 35:3
*ᵃ3* Or *you*

# Job 35:11
*ᵃ11* Or *teaches us by*\
*ᵇ11* Or *us wise by*

# Job 35:15
*ᵃ15* Symmachus, Theodotion and Vulgate; the meaning of the Hebrew for this word is uncertain.

# Job 36:12
*ᵃ12* Or *will cross the River*

# Job 36:20
*ᵃ20* The meaning of the Hebrew for verses [18–20](vref:Job 36:18-20) is uncertain.

# Job 36:27
*ᵃ27* Or *distill from the mist as rain*

# Job 36:31
*ᵃ31* Or *nourishes*

# Job 36:33
*ᵃ33* Or *announces his coming— / the One zealous against evil*

# Job 37:7
*ᵃ7* Or */ he fills all men with fear by his power*

# Job 37:13
*ᵃ13* Or *to favor them*

# Job 37:24
*ᵃ24* Or *for he does not have regard for any who think they are wise.*

# Job 38:1
***The Lᴏʀᴅ Speaks (38:1–41:34)***

# Job 38:7
*ᵃ7* Hebrew *the sons of God*

# Job 38:17
*ᵃ17* Or *gates of deep shadows*

# Job 38:31
*ᵃ31* Or *the twinkling*; or *the chains of the*

# Job 38:32
*ᵃ32* Or *the morning star in its season*\
*ᵇ32* Or *out Leo*

# Job 38:33
*ᵃ33* Or *his*; or *their*

# Job 38:36
*ᵃ36* The meaning of the Hebrew for this word is uncertain.

# Job 40:15
*ᵃ15* Possibly the hippopotamus or the elephant

# Job 40:17
*ᵃ17* Possibly trunk

# Job 40:24
*ᵃ24* Or *by a water hole*

# Job 41:1
*ᵃ1* Possibly the crocodile

# Job 41:15
*ᵃ15* Or *His pride is his*

# Job 42:1
***Job (42:1–6)***

# Job 42:7
***Epilogue (42:7–17)***

# Job 42:11
*ᵃ11* Hebrew *him a kesitah*; a kesitah was a unit of money of unknown weight and value.

# Ps 1:1
***BOOK I***\
\
***Psalms 1–41***\
\
***Psalm 1***

# Ps 2:1
***Psalm 2***\
\
*ᵃ1* Hebrew; Septuagint *rage*

# Ps 2:2
*ᵃ2* Or *anointed one*

# Ps 2:6
*ᵃ6* Or *king*

# Ps 2:7
*ᵃ7* Or *son*; also in verse [12](vref:Ps 2:12)\
*ᵇ7* Or *have begotten you*

# Ps 2:9
*ᵃ9* Or *will break them with a rod of iron*

# Ps 3:0
***Psalm 3***

# Ps 3:2
*ᵃ2* A word of uncertain meaning, occurring frequently in the Psalms; possibly a musical term

# Ps 3:3
*ᵃ3* Or *Lᴏʀᴅ, / my Glorious One, who lifts*

# Ps 4:0
***Psalm 4***

# Ps 4:2
*ᵃ2* Or *you dishonor my Glorious One*\
*ᵇ2* Or *seek lies*

# Ps 5:0
***Psalm 5***

# Ps 6:0
***Psalm 6***\
\
*ᵃ*Title: Probably a musical term

# Ps 6:5
*ᵃ5* Hebrew *Sheol*

# Ps 7:0
***Psalm 7***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 7:10
*ᵃ10* Or *sovereign*

# Ps 7:12
*ᵃ12* Or *If a man does not repent, / God*

# Ps 8:0
***Psalm 8***\
\
*ᵃ*Title: Probably a musical term

# Ps 8:2
*ᵃ2* Or *strength*

# Ps 8:5
*ᵃ5* Or *than God*

# Ps 9:0
***Psalm 9***\
\
*ᵃ*Psalms [9](vref:Ps 9) and [10](vref:Ps 10) may have been originally a single acrostic poem, the stanzas of which begin with the successive letters of the Hebrew alphabet. In the Septuagint they constitute one psalm.

# Ps 9:16
*ᵃ16* Or *Meditation*; possibly a musical notation

# Ps 9:17
*ᵃ17* Hebrew *Sheol*

# Ps 10:1
***Psalm 10***\
\
*ᵃ*Psalms [9](vref:Ps 9) and [10](vref:Ps 10) may have been originally a single acrostic poem, the stanzas of which begin with the successive letters of the Hebrew alphabet. In the Septuagint they constitute one psalm.

# Ps 11:0
***Psalm 11***

# Ps 11:3
*ᵃ3* Or *what is the Righteous One doing*

# Ps 11:5
*ᵃ5* Or *The Lᴏʀᴅ, the Righteous One, examines the wicked, /*

# Ps 12:0
***Psalm 12***\
\
*ᵃ*Title: Probably a musical term

# Ps 12:4
*ᵃ4* Or */ our lips are our plowshares*

# Ps 13:0
***Psalm 13***

# Ps 14:0
***Psalm 14***

# Ps 14:1
*ᵃ1* The Hebrew words rendered *fool* in Psalms denote one who is morally deficient.

# Ps 15:0
***Psalm 15***

# Ps 16:0
***Psalm 16***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 16:3
*ᵃ3* Or *As for the pagan priests who are in the land / and the nobles in whom all delight, I said:*

# Ps 16:10
*ᵃ10* Hebrew *Sheol*\
*ᵇ10* Or *your faithful one*

# Ps 16:11
*ᵃ11* Or *You will make*

# Ps 17:0
***Psalm 17***

# Ps 18:0
***Psalm 18***

# Ps 18:2
*ᵃ2* *Horn* here symbolizes strength.

# Ps 18:5
*ᵃ5* Hebrew *Sheol*

# Ps 18:13
*ᵃ13* Some Hebrew manuscripts and Septuagint (see also [2 Samuel 22:14](vref:2 Sam 22:14)); most Hebrew manuscripts *resounded, / amid hailstones and bolts of lightning*

# Ps 18:29
*ᵃ29* Or *can run through a barricade*

# Ps 19:0
***Psalm 19***

# Ps 19:3
*ᵃ3* Or *They have no speech, there are no words; / no sound is heard from them*

# Ps 19:4
*ᵃ4* Septuagint, Jerome and Syriac; Hebrew *line*

# Ps 20:0
***Psalm 20***

# Ps 20:9
*ᵃ9* Or *save! / O King, answer*

# Ps 21:0
***Psalm 21***

# Ps 22:0
***Psalm 22***

# Ps 22:3
*ᵃ3* Or *Yet you are holy, / enthroned on the praises of Israel*

# Ps 22:15
*ᵃ15* Or */ I am laid*

# Ps 22:16
*ᵃ16* Some Hebrew manuscripts, Septuagint and Syriac; most Hebrew manuscripts */ like the lion,*

# Ps 22:21
*ᵃ21* Or */ you have heard*

# Ps 22:25
*ᵃ25* Hebrew *him*

# Ps 23:0
***Psalm 23***

# Ps 23:4
*ᵃ4* Or *through the darkest valley*

# Ps 24:0
***Psalm 24***

# Ps 24:4
*ᵃ4* Or *swear falsely*

# Ps 24:6
*ᵃ6* Two Hebrew manuscripts and Syriac (see also Septuagint); most Hebrew manuscripts *face, Jacob*

# Ps 25:0
***Psalm 25***\
\
*ᵃ*This psalm is an acrostic poem, the verses of which begin with the successive letters of the Hebrew alphabet.

# Ps 26:0
***Psalm 26***

# Ps 27:0
***Psalm 27***

# Ps 27:2
*ᵃ2* Or *to slander me*

# Ps 27:8
*ᵃ8* Or *To you, O my heart, he has said, “Seek my*

# Ps 28:0
***Psalm 28***

# Ps 29:0
***Psalm 29***

# Ps 29:2
*ᵃ2* Or *Lᴏʀᴅ with the splendor of*

# Ps 29:6
*ᵃ6* That is, Mount Hermon

# Ps 29:9
*ᵃ9* Or *Lᴏʀᴅ makes the deer give birth*

# Ps 29:10
*ᵃ10* Or *sat*

# Ps 30:0
***Psalm 30***\
\
*ᵃ*Title: Or *palace*

# Ps 30:3
*ᵃ3* Hebrew *Sheol*

# Ps 30:7
*ᵃ7* Or *hill country*

# Ps 30:9
*ᵃ9* Or *there if I am silenced*

# Ps 31:0
***Psalm 31***

# Ps 31:10
*ᵃ10* Or *guilt*

# Ps 31:17
*ᵃ17* Hebrew *Sheol*

# Ps 32:0
***Psalm 32***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 33:1
***Psalm 33***

# Ps 33:7
*ᵃ7* Or *sea as into a heap*

# Ps 34:0
***Psalm 34***\
\
*ᵃ*This psalm is an acrostic poem, the verses of which begin with the successive letters of the Hebrew alphabet.

# Ps 35:0
***Psalm 35***

# Ps 35:3
*ᵃ3* Or *and block the way*

# Ps 35:16
*ᵃ16* Septuagint; Hebrew may mean *ungodly circle of mockers*.

# Ps 36:0
***Psalm 36***

# Ps 36:1
*ᵃ1* Or *heart: / Sin proceeds from the wicked.*

# Ps 36:7
*ᵃ7* Or *love, O God! / Men find*; or *love! / Both heavenly beings and men / find*

# Ps 37:0
***Psalm 37***\
\
*ᵃ*This psalm is an acrostic poem, the stanzas of which begin with the successive letters of the Hebrew alphabet.

# Ps 37:37
*ᵃ37* Or *there will be posterity*

# Ps 37:38
*ᵃ38* Or *posterity*

# Ps 38:0
***Psalm 38***

# Ps 39:0
***Psalm 39***

# Ps 40:0
***Psalm 40***

# Ps 40:4
*ᵃ4* Or *to falsehood*

# Ps 40:6
*ᵃ6* Hebrew; Septuagint *but a body you have prepared for me* (see also Symmachus and Theodotion)\
*ᵇ6* Or *opened*

# Ps 40:7
*ᵃ7* Or *come / with the scroll written for me*

# Ps 41:0
***Psalm 41***

# Ps 42:0
***BOOK II***\
\
***Psalms 42–72***\
\
***Psalm 42***\
\
*ᵃ*In many Hebrew manuscripts Psalms [42](vref:Ps 42) and [43](vref:Ps 43) constitute one psalm.\
*ᵇ*Title: Probably a literary or musical term

# Ps 42:6
*ᵃ5,6* A few Hebrew manuscripts, Septuagint and Syriac; most Hebrew manuscripts *praise him for his saving help. / ⁶O my God, my*

# Ps 43:1
***Psalm 43***\
\
*ᵃ*In many Hebrew manuscripts Psalms [42](vref:Ps 42) and [43](vref:Ps 43) constitute one psalm.

# Ps 44:0
***Psalm 44***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 44:4
*ᵃ4* Septuagint, Aquila and Syriac; Hebrew *King, O God; / command*

# Ps 45:0
***Psalm 45***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 45:12
*ᵃ12* Or *A Tyrian robe is among the gifts*

# Ps 46:0
***Psalm 46***\
\
*ᵃ*Title: Probably a musical term

# Ps 46:9
*ᵃ9* Or *chariots*

# Ps 47:0
***Psalm 47***

# Ps 47:7
*ᵃ7* Or *a maskil* (probably a literary or musical term)

# Ps 47:9
*ᵃ9* Or *shields*

# Ps 48:0
***Psalm 48***

# Ps 48:2
*ᵃ2* *Zaphon* can refer to a sacred mountain or the direction north.\
*ᵇ2* Or *earth, / Mount Zion, on the northern side / of the*

# Ps 49:0
***Psalm 49***

# Ps 49:11
*ᵃ11* Septuagint and Syriac; Hebrew *In their thoughts their houses will remain*\
*ᵇ11* Or */ for they have*

# Ps 49:12
*ᵃ12* Hebrew; Septuagint and Syriac read verse [12](vref:Ps 49:12) the same as verse [20](vref:Ps 49:20).

# Ps 49:14
*ᵃ14* Hebrew *Sheol*; also in verse [15](vref:Ps 49:15)

# Ps 49:15
*ᵃ15* Or *soul*

# Ps 50:0
***Psalm 50***

# Ps 50:21
*ᵃ21* Or *thought the ‘I ᴀᴍ’ was*

# Ps 50:23
*ᵃ23* Or *and to him who considers his way / I will show*

# Ps 51:0
***Psalm 51***

# Ps 51:6
*ᵃ6* The meaning of the Hebrew for this phrase is uncertain.\
*ᵇ6* Or *you desired . . . ; / you taught*

# Ps 51:17
*ᵃ17* Or *My sacrifice, O God, is*

# Ps 52:0
***Psalm 52***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 53:0
***Psalm 53***\
\
*ᵃ*Title: Probably a musical term\
*ᵇ*Title: Probably a literary or musical term

# Ps 54:0
***Psalm 54***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 55:0
***Psalm 55***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 55:15
*ᵃ15* Hebrew *Sheol*

# Ps 56:0
***Psalm 56***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 56:8
*ᵃ8* Or */ put my tears in your wineskin*

# Ps 56:13
*ᵃ13* Or *my soul*\
*ᵇ13* Or *the land of the living*

# Ps 57:0
***Psalm 57***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 58:0
***Psalm 58***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 58:9
*ᵃ9* The meaning of the Hebrew for this verse is uncertain.

# Ps 59:0
***Psalm 59***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 59:11
*ᵃ11* Or *sovereign*

# Ps 60:0
***Psalm 60***\
\
*ᵃ*Title: Probably a literary or musical term\
*ᵇ*Title: That is, Arameans of Northwest Mesopotamia\
*ᶜ*Title: That is, Arameans of central Syria

# Ps 61:0
***Psalm 61***

# Ps 62:0
***Psalm 62***

# Ps 62:7
*ᵃ7* Or */ God Most High is my salvation and my honor*

# Ps 63:0
***Psalm 63***

# Ps 64:0
***Psalm 64***

# Ps 64:5
*ᵃ5* Or *us*

# Ps 65:0
***Psalm 65***

# Ps 65:1
*ᵃ1* Or *befits*; the meaning of the Hebrew for this word is uncertain.

# Ps 65:3
*ᵃ3* Or *made atonement for*

# Ps 65:9
*ᵃ9* Or *for that is how you prepare the land*

# Ps 66:0
***Psalm 66***

# Ps 67:0
***Psalm 67***

# Ps 68:0
***Psalm 68***

# Ps 68:4
*ᵃ4* Or */ prepare the way for him who rides through the deserts*

# Ps 68:6
*ᵃ6* Or *the desolate in a homeland*

# Ps 68:13
*ᵃ13* Or *saddlebags*

# Ps 68:14
*ᵃ14* Hebrew *Shaddai*

# Ps 68:18
*ᵃ18* Or *gifts for men, / even*\
*ᵇ18* Or *they*

# Ps 68:28
*ᵃ28* Many Hebrew manuscripts, Septuagint and Syriac; most Hebrew manuscripts *Your God has summoned power for you*

# Ps 68:31
*ᵃ31* That is, the upper Nile region

# Ps 69:0
***Psalm 69***

# Ps 69:22
*ᵃ22* Or *snare / and their fellowship become*

# Ps 70:0
***Psalm 70***

# Ps 71:1
***Psalm 71***

# Ps 72:0
***Psalm 72***

# Ps 72:2
*ᵃ2* Or *May he*; similarly in verses [3–11](vref:Ps 72:3-11) and [17](vref:Ps 72:17)

# Ps 72:5
*ᵃ5* Septuagint; Hebrew *You will be feared*

# Ps 72:8
*ᵃ8* That is, the Euphrates\
*ᵇ8* Or *the end of the land*

# Ps 73:0
***BOOK III***\
\
***Psalms 73–89***\
\
***Psalm 73***

# Ps 73:4
*ᵃ4* With a different word division of the Hebrew; Masoretic Text *struggles at their death; / their bodies are healthy*

# Ps 73:7
*ᵃ7* Syriac (see also Septuagint); Hebrew *Their eyes bulge with fat*

# Ps 73:10
*ᵃ10* The meaning of the Hebrew for this verse is uncertain.

# Ps 74:0
***Psalm 74***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 75:0
***Psalm 75***

# Ps 76:0
***Psalm 76***

# Ps 76:10
*ᵃ10* Or *Surely the wrath of men brings you praise, / and with the remainder of wrath you arm yourself*

# Ps 77:0
***Psalm 77***

# Ps 78:0
***Psalm 78***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 79:0
***Psalm 79***

# Ps 80:0
***Psalm 80***

# Ps 80:11
*ᵃ11* Probably the Mediterranean\
*ᵇ11* That is, the Euphrates

# Ps 80:15
*ᵃ15* Or *branch*

# Ps 81:0
***Psalm 81***\
\
*ᵃ*Title: Probably a musical term

# Ps 81:5
*ᵃ5* Or */ and we heard a voice we had not known*

# Ps 82:0
***Psalm 82***

# Ps 82:2
*ᵃ2* The Hebrew is plural.

# Ps 83:0
***Psalm 83***

# Ps 83:7
*ᵃ7* That is, Byblos

# Ps 84:0
***Psalm 84***\
\
*ᵃ*Title: Probably a musical term

# Ps 84:6
*ᵃ6* Or *blessings*

# Ps 84:9
*ᵃ9* Or *sovereign*

# Ps 85:0
***Psalm 85***

# Ps 86:0
***Psalm 86***

# Ps 86:13
*ᵃ13* Hebrew *Sheol*

# Ps 86:16
*ᵃ16* Or *save your faithful son*

# Ps 87:0
***Psalm 87***

# Ps 87:4
*ᵃ4* A poetic name for Egypt\
*ᵇ4* That is, the upper Nile region\
*ᶜ4* Or *“O Rahab and Babylon, / Philistia, Tyre and Cush, / I will record concerning those who acknowledge me: / ‘This*

# Ps 88:0
***Psalm 88***\
\
*ᵃ*Title: Possibly a tune, “The Suffering of Affliction”\
*ᵇ*Title: Probably a literary or musical term

# Ps 88:3
*ᵃ3* Hebrew *Sheol*

# Ps 88:11
*ᵃ11* Hebrew *Abaddon*

# Ps 89:0
***Psalm 89***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 89:17
*ᵃ17* *Horn* here symbolizes strong one.

# Ps 89:18
*ᵃ18* Or *sovereign*

# Ps 89:24
*ᵃ24* *Horn* here symbolizes strength.

# Ps 89:48
*ᵃ48* Hebrew *Sheol*

# Ps 89:50
*ᵃ50* Or *your servants have*

# Ps 90:0
***BOOK IV***\
\
***Psalms 90–106***\
\
***Psalm 90***

# Ps 90:10
*ᵃ10* Or *yet the best of them*

# Ps 90:17
*ᵃ17* Or *beauty*

# Ps 91:1
***Psalm 91***\
\
*ᵃ1* Hebrew *Shaddai*

# Ps 91:2
*ᵃ2* Or *He says*

# Ps 92:0
***Psalm 92***

# Ps 92:10
*ᵃ10* *Horn* here symbolizes strength.

# Ps 93:1
***Psalm 93***

# Ps 94:1
***Psalm 94***

# Ps 95:1
***Psalm 95***

# Ps 95:8
*ᵃ8* *Meribah* means *quarreling*.\
*ᵇ8* *Massah* means *testing*.

# Ps 96:1
***Psalm 96***

# Ps 96:9
*ᵃ9* Or *Lᴏʀᴅ with the splendor of*

# Ps 97:1
***Psalm 97***

# Ps 98:0
***Psalm 98***

# Ps 99:1
***Psalm 99***

# Ps 99:8
*ᵃ8* Hebrew *them*\
*ᵇ8* Or */ an avenger of the wrongs done to them*

# Ps 100:0
***Psalm 100***

# Ps 100:3
*ᵃ3* Or *and not we ourselves*

# Ps 101:0
***Psalm 101***

# Ps 102:0
***Psalm 102***

# Ps 102:23
*ᵃ23* Or *By his power*

# Ps 103:0
***Psalm 103***

# Ps 104:1
***Psalm 104***

# Ps 104:4
*ᵃ4* Or *angels*

# Ps 104:18
*ᵃ18* That is, the hyrax or rock badger

# Ps 104:35
*ᵃ35* Hebrew *Hallelu Yah*; in the Septuagint this line stands at the beginning of [Psalm 105](vref:Ps 105).

# Ps 105:1
***Psalm 105***

# Ps 105:45
*ᵃ45* Hebrew *Hallelu Yah*

# Ps 106:1
***Psalm 106***\
\
*ᵃ1* Hebrew *Hallelu Yah*; also in verse [48](vref:Ps 106:48)

# Ps 106:7
*ᵃ7* Hebrew *Yam Suph*; that is, Sea of Reeds; also in verses [9](vref:Ps 106:9) and [22](vref:Ps 106:22)

# Ps 106:33
*ᵃ33* Or *against his spirit, / and rash words came from his lips*

# Ps 107:1
***BOOK V***\
\
***Psalms 107–150***\
\
***Psalm 107***

# Ps 107:3
*ᵃ3* Hebrew *north and the sea*

# Ps 108:0
***Psalm 108***

# Ps 109:0
***Psalm 109***

# Ps 109:6
*ᵃ6* Or *⌞​They say:​⌟ “Appoint* (with quotation marks at the end of verse [19](vref:Ps 109:19))\
*ᵇ6* Or *the Evil One*\
*ᶜ6* Or *let Satan*

# Ps 109:10
*ᵃ10* Septuagint; Hebrew *sought*

# Ps 109:17
*ᵃ17* Or *curse, / and it has*\
*ᵇ17* Or *blessing, / and it is*

# Ps 110:0
***Psalm 110***

# Ps 110:3
*ᵃ3* Or */ your young men will come to you like the dew*

# Ps 110:7
*ᵃ7* Or */ The One who grants succession will set him in authority*

# Ps 111:1
***Psalm 111***\
\
*ᵃ*This psalm is an acrostic poem, the lines of which begin with the successive letters of the Hebrew alphabet.\
*ᵇ1* Hebrew *Hallelu Yah*

# Ps 112:1
***Psalm 112***\
\
*ᵃ*This psalm is an acrostic poem, the lines of which begin with the successive letters of the Hebrew alphabet.\
*ᵇ1* Hebrew *Hallelu Yah*

# Ps 112:4
*ᵃ4* Or */ for ⌞​the Lᴏʀᴅ​⌟ is gracious and compassionate and righteous*

# Ps 112:9
*ᵃ9* *Horn* here symbolizes dignity.

# Ps 113:1
***Psalm 113***\
\
*ᵃ1* Hebrew *Hallelu Yah*; also in verse [9](vref:Ps 113:9)

# Ps 114:1
***Psalm 114***

# Ps 115:1
***Psalm 115***

# Ps 115:18
*ᵃ18* Hebrew *Hallelu Yah*

# Ps 116:1
***Psalm 116***

# Ps 116:3
*ᵃ3* Hebrew *Sheol*

# Ps 116:10
*ᵃ10* Or *believed even when*

# Ps 116:16
*ᵃ16* Or *servant, your faithful son*

# Ps 116:19
*ᵃ19* Hebrew *Hallelu Yah*

# Ps 117:1
***Psalm 117***

# Ps 117:2
*ᵃ2* Hebrew *Hallelu Yah*

# Ps 118:1
***Psalm 118***

# Ps 118:26
*ᵃ26* The Hebrew is plural.

# Ps 118:27
*ᵃ27* Or *Bind the festal sacrifice with ropes / and take it*

# Ps 119:1
***Psalm 119***\
\
**א Aleph (119:1–8)**\
\
*ᵃ*This psalm is an acrostic poem; the verses of each stanza begin with the same letter of the Hebrew alphabet.

# Ps 119:9
**ב Beth (119:9–16)**

# Ps 119:17
**ג Gimel (119:17–24)**

# Ps 119:25
**ד Daleth (119:25–32)**

# Ps 119:33
**ה He (119:33–40)**

# Ps 119:37
*ᵃ37* Two manuscripts of the Masoretic Text and Dead Sea Scrolls; most manuscripts of the Masoretic Text *life in your way*

# Ps 119:41
**ו Waw (119:41–48)**

# Ps 119:48
*ᵃ48* Or *for*

# Ps 119:49
**ז Zayin (119:49–56)**

# Ps 119:57
**ח Heth (119:57–64)**

# Ps 119:65
**ט Teth (119:65–72)**

# Ps 119:73
**י Yodh (119:73–80)**

# Ps 119:81
**כ Kaph (119:81–88)**

# Ps 119:89
**ל Lamedh (119:89–96)**

# Ps 119:97
**מ Mem (119:97–104)**

# Ps 119:105
**נ Nun (119:105–112)**

# Ps 119:113
**ס Samekh (119:113–120)**

# Ps 119:121
**ע Ayin (119:121–128)**

# Ps 119:129
**פ Pe (119:129–136)**

# Ps 119:137
**צ Tsadhe (119:137–144)**

# Ps 119:145
**ק Qoph (119:145–152)**

# Ps 119:153
**ר Resh (119:153–160)**

# Ps 119:161
**ש Sin and Shin (119:161–168)**

# Ps 119:169
**ת Taw (119:169–176)**

# Ps 120:0
***Psalm 120***

# Ps 121:0
***Psalm 121***

# Ps 122:0
***Psalm 122***

# Ps 123:0
***Psalm 123***

# Ps 124:0
***Psalm 124***

# Ps 125:0
***Psalm 125***

# Ps 126:0
***Psalm 126***

# Ps 126:1
*ᵃ1* Or *Lᴏʀᴅ restored the fortunes of*\
*ᵇ1* Or *men restored to health*

# Ps 126:4
*ᵃ4* Or *Bring back our captives*

# Ps 127:0
***Psalm 127***

# Ps 127:2
*ᵃ2* Or *eat— / for while they sleep he provides for*

# Ps 128:0
***Psalm 128***

# Ps 129:0
***Psalm 129***

# Ps 130:0
***Psalm 130***

# Ps 131:0
***Psalm 131***

# Ps 132:0
***Psalm 132***

# Ps 132:6
*ᵃ6* That is, Kiriath Jearim\
*ᵇ6* Or *heard of it in Ephrathah, / we found it in the fields of Jaar.* (And no quotes around verses [7–9](vref:Ps 132:7-9))

# Ps 132:17
*ᵃ17* *Horn* here symbolizes strong one, that is, king.

# Ps 133:0
***Psalm 133***

# Ps 134:0
***Psalm 134***

# Ps 135:1
***Psalm 135***\
\
*ᵃ1* Hebrew *Hallelu Yah*; also in verses [3](vref:Ps 135:3) and [21](vref:Ps 135:21)

# Ps 136:1
***Psalm 136***

# Ps 136:13
*ᵃ13* Hebrew *Yam Suph*; that is, Sea of Reeds; also in verse [15](vref:Ps 136:15)

# Ps 137:1
***Psalm 137***

# Ps 138:0
***Psalm 138***

# Ps 139:0
***Psalm 139***

# Ps 139:8
*ᵃ8* Hebrew *Sheol*

# Ps 139:17
*ᵃ17* Or *concerning*

# Ps 140:0
***Psalm 140***

# Ps 141:0
***Psalm 141***

# Ps 141:5
*ᵃ5* Or *Let the Righteous One*

# Ps 141:7
*ᵃ7* Hebrew *Sheol*

# Ps 142:0
***Psalm 142***\
\
*ᵃ*Title: Probably a literary or musical term

# Ps 143:0
***Psalm 143***

# Ps 144:0
***Psalm 144***

# Ps 144:2
*ᵃ2* Many manuscripts of the Masoretic Text, Dead Sea Scrolls, Aquila, Jerome and Syriac; most manuscripts of the Masoretic Text *subdues my people*

# Ps 144:14
*ᵃ14* Or *our chieftains will be firmly established*

# Ps 145:0
***Psalm 145***\
\
*ᵃ*This psalm is an acrostic poem, the verses of which (including verse [13](vref:Ps 145:13)b) begin with the successive letters of the Hebrew alphabet.

# Ps 145:5
*ᵃ5* Dead Sea Scrolls and Syriac (see also Septuagint); Masoretic Text *On the glorious splendor of your majesty / and on your wonderful works I will meditate*

# Ps 145:13
*ᵃ13* One manuscript of the Masoretic Text, Dead Sea Scrolls and Syriac (see also Septuagint); most manuscripts of the Masoretic Text do not have the last two lines of verse [13](vref:Ps 145:13).

# Ps 146:1
***Psalm 146***\
\
*ᵃ1* Hebrew *Hallelu Yah*; also in verse [10](vref:Ps 146:10)

# Ps 147:1
***Psalm 147***\
\
*ᵃ1* Hebrew *Hallelu Yah*; also in verse [20](vref:Ps 147:20)

# Ps 148:1
***Psalm 148***\
\
*ᵃ1* Hebrew *Hallelu Yah*; also in verse [14](vref:Ps 148:14)

# Ps 148:14
*ᵃ14* *Horn* here symbolizes strong one, that is, king.

# Ps 149:1
***Psalm 149***\
\
*ᵃ1* Hebrew *Hallelu Yah*; also in verse [9](vref:Ps 149:9)

# Ps 150:1
***Psalm 150***\
\
*ᵃ1* Hebrew *Hallelu Yah*; also in verse [6](vref:Ps 150:6)

# Prov 1:1
***Prologue: Purpose and Theme (1:1–7)***

# Prov 1:7
*ᵃ7* The Hebrew words rendered *fool* in Proverbs, and often elsewhere in the Old Testament, denote one who is morally deficient.

# Prov 1:8
***Exhortations to Embrace Wisdom (1:8–9:18)***\
\
***Warning Against Enticement (1:8–19)***

# Prov 1:12
*ᵃ12* Hebrew *Sheol*

# Prov 1:20
***Warning Against Rejecting Wisdom (1:20–33)***

# Prov 1:21
*ᵃ21* Hebrew; Septuagint */ on the tops of the walls*

# Prov 1:22
*ᵃ22* The Hebrew word rendered *simple* in Proverbs generally denotes one without moral direction and inclined to evil.

# Prov 2:1
***Moral Benefits of Wisdom (2:1–22)***

# Prov 2:17
*ᵃ17* Or *covenant of her God*

# Prov 3:1
***Further Benefits of Wisdom (3:1–35)***

# Prov 3:6
*ᵃ6* Or *will direct your paths*

# Prov 3:12
*ᵃ12* Hebrew; Septuagint */ and he punishes*

# Prov 4:1
***Wisdom Is Supreme (4:1–27)***

# Prov 4:7
*ᵃ7* Or *Whatever else you get*

# Prov 4:26
*ᵃ26* Or *Consider the*

# Prov 5:1
***Warning Against Adultery (5:1–23)***

# Prov 5:5
*ᵃ5* Hebrew *Sheol*

# Prov 6:1
***Warnings Against Folly (6:1–19)***

# Prov 6:11
*ᵃ11* Or *like a vagrant / and scarcity like a beggar*

# Prov 6:20
***Warning Against Adultery (6:20–35)***

# Prov 7:1
***Warning Against the Adulteress (7:1–27)***

# Prov 7:14
*ᵃ14* Traditionally *peace offerings*

# Prov 7:22
*ᵃ22* Syriac (see also Septuagint); Hebrew *fool*\
*ᵇ22* The meaning of the Hebrew for this line is uncertain.

# Prov 7:27
*ᵃ27* Hebrew *Sheol*

# Prov 8:1
***Wisdom’s Call (8:1–36)***

# Prov 8:16
*ᵃ16* Many Hebrew manuscripts and Septuagint; most Hebrew manuscripts *and nobles—all righteous rulers*

# Prov 8:22
*ᵃ22* Or *way*; or *dominion*\
*ᵇ22* Or *The Lᴏʀᴅ possessed me at the beginning of his work*; or *The Lᴏʀᴅ brought me forth at the beginning of his work*

# Prov 8:23
*ᵃ23* Or *fashioned*

# Prov 9:1
***Invitations of Wisdom and of Folly (9:1–18)***

# Prov 9:18
*ᵃ18* Hebrew *Sheol*

# Prov 10:1
***Proverbs of Solomon (10:1–22:16)***

# Prov 10:6
*ᵃ6* Or *but the mouth of the wicked conceals violence*; also in verse [11](vref:Prov 10:11)

# Prov 12:26
*ᵃ26* Or *man is a guide to his neighbor*

# Prov 12:27
*ᵃ27* The meaning of the Hebrew for this word is uncertain.

# Prov 13:15
*ᵃ15* Or *unfaithful does not endure*

# Prov 14:22
*ᵃ22* Or *show*

# Prov 14:33
*ᵃ33* Hebrew; Septuagint and Syriac */ but in the heart of fools she is not known*

# Prov 15:11
*ᵃ11* Hebrew *Sheol and Abaddon*

# Prov 15:24
*ᵃ24* Hebrew *Sheol*

# Prov 15:33
*ᵃ33* Or *Wisdom teaches the fear of the Lᴏʀᴅ*

# Prov 16:21
*ᵃ21* Or *words make a man persuasive*

# Prov 16:23
*ᵃ23* Or *mouth / and makes his lips persuasive*

# Prov 17:1
*ᵃ1* Hebrew *sacrifices*

# Prov 17:7
*ᵃ7* Or *Eloquent*

# Prov 19:7
*ᵃ7* The meaning of the Hebrew for this sentence is uncertain.

# Prov 19:22
*ᵃ22* Or *A man’s greed is his shame*

# Prov 20:27
*ᵃ27* Or *The spirit of man is the Lᴏʀᴅ’s lamp*

# Prov 21:6
*ᵃ6* Some Hebrew manuscripts, Septuagint and Vulgate; most Hebrew manuscripts *vapor for those who seek death*

# Prov 21:12
*ᵃ12* Or *The righteous man*

# Prov 21:21
*ᵃ21* Or *righteousness*

# Prov 21:28
*ᵃ28* Or */ but the words of an obedient man will live on*

# Prov 22:6
*ᵃ6* Or *Start*

# Prov 22:17
***Sayings of the Wise (22:17–24:22)***

# Prov 22:20
*ᵃ20* Or *not formerly written*; or *not written excellent*

# Prov 23:1
*ᵃ1* Or *who*

# Prov 23:7
*ᵃ7* Or *for as he thinks within himself, / so he is*; or *for as he puts on a feast, / so he is*

# Prov 23:14
*ᵃ14* Hebrew *Sheol*

# Prov 24:23
***Further Sayings of the Wise (24:23–34)***

# Prov 24:34
*ᵃ34* Or *like a vagrant / and scarcity like a beggar*

# Prov 25:1
***More Proverbs of Solomon (25:1–29:27)***

# Prov 25:4
*ᵃ4* Or *comes a vessel from*

# Prov 25:8
*ᵃ7,8* Or *nobleman / on whom you had set your eyes. / ⁸Do not go*

# Prov 26:23
*ᵃ23* With a different word division of the Hebrew; Masoretic Text *of silver dross*

# Prov 27:20
*ᵃ20* Hebrew *Sheol and Abaddon*

# Prov 28:3
*ᵃ3* Or *A poor man*

# Prov 29:21
*ᵃ21* The meaning of the Hebrew for this word is uncertain.

# Prov 30:1
***Sayings of Agur (30:1–33)***\
\
*ᵃ1* Or *Jakeh of Massa*\
*ᵇ1* Masoretic Text; with a different word division of the Hebrew *declared, “I am weary, O God; / I am weary, O God, and faint.*

# Prov 30:16
*ᵃ16* Hebrew *Sheol*

# Prov 30:26
*ᵃ26* That is, the hyrax or rock badger

# Prov 30:31
*ᵃ31* Or *king secure against revolt*

# Prov 31:1
***Sayings of King Lemuel (31:1–9)***\
\
*ᵃ1* Or *of Lemuel king of Massa, which*

# Prov 31:2
*ᵃ2* Or */ the answer to my prayers*

# Prov 31:10
***Epilogue: The Wife of Noble Character (31:10–31)***\
\
*ᵃ10* Verses [10–31](vref:Prov 31:10-31) are an acrostic, each verse beginning with a successive letter of the Hebrew alphabet.

# Eccl 1:1
***Everything Is Meaningless (1:1–11)***\
\
*ᵃ1* Or *leader of the assembly*; also in verses [2](vref:Eccl 1:2) and [12](vref:Eccl 1:12)

# Eccl 1:12
***Wisdom Is Meaningless (1:12–18)***

# Eccl 2:1
***Pleasures Are Meaningless (2:1–11)***

# Eccl 2:8
*ᵃ8* The meaning of the Hebrew for this phrase is uncertain.

# Eccl 2:12
***Wisdom and Folly Are Meaningless (2:12–16)***

# Eccl 2:17
***Toil Is Meaningless (2:17–26)***

# Eccl 3:1
***A Time for Everything (3:1–22)***

# Eccl 3:15
*ᵃ15* Or *God calls back the past*

# Eccl 3:19
*ᵃ19* Or *spirit*

# Eccl 3:21
*ᵃ21* Or *Who knows the spirit of man, which rises upward, or the spirit of the animal, which*

# Eccl 4:1
***Oppression, Toil, Friendlessness (4:1–12)***

# Eccl 4:13
***Advancement Is Meaningless (4:13–16)***

# Eccl 5:1
***Stand in Awe of God (5:1–7)***

# Eccl 5:8
***Riches Are Meaningless (5:8–6:12)***

# Eccl 7:1
***Wisdom (7:1–8:1)***

# Eccl 7:18
*ᵃ18* Or *will follow them both*

# Eccl 7:27
*ᵃ27* Or *leader of the assembly*

# Eccl 8:2
***Obey the King (8:2–17)***

# Eccl 8:8
*ᵃ8* Or *over his spirit to retain it*

# Eccl 8:9
*ᵃ9* Or *to their*

# Eccl 8:10
*ᵃ10* Some Hebrew manuscripts and Septuagint (Aquila); most Hebrew manuscripts *and are forgotten*

# Eccl 9:1
***A Common Destiny for All (9:1–12)***

# Eccl 9:2
*ᵃ2* Septuagint (Aquila), Vulgate and Syriac; Hebrew does not have *and the bad*.

# Eccl 9:4
*ᵃ4* Or *What then is to be chosen? With all who live, there is hope*

# Eccl 9:10
*ᵃ10* Hebrew *Sheol*

# Eccl 9:13
***Wisdom Better Than Folly (9:13–10:20)***

# Eccl 10:16
*ᵃ16* Or *king is a child*

# Eccl 11:1
***Bread Upon the Waters (11:1–6)***

# Eccl 11:5
*ᵃ5* Or *know how life* (or *the spirit*) */ enters the body being formed*

# Eccl 11:7
***Remember Your Creator While Young (11:7–12:8)***

# Eccl 12:8
*ᵃ8* Or *the leader of the assembly*; also in verses [9](vref:Eccl 12:9) and [10](vref:Eccl 12:10)

# Eccl 12:9
***The Conclusion of the Matter (12:9–14)***

# Song 1:2
***Belovedᵃ (1:2–4a)***\
\
*ᵃ*Primarily on the basis of the gender of the Hebrew pronouns used, male and female speakers are indicated in the margins by the captions *Lover* and *Beloved* respectively. The words of others are marked *Friends*. In some instances the divisions and their captions are debatable.

# Song 1:4
***Friends (1:4b)***\
\
***Beloved (1:4c–7)***\
\
*ᵃ4* The Hebrew is masculine singular.

# Song 1:5
*ᵃ5* Or *Salma*

# Song 1:8
***Friends (1:8)***

# Song 1:9
***Lover (1:9–11)***

# Song 1:12
***Beloved (1:12–14)***

# Song 1:15
***Lover (1:15)***

# Song 1:16
***Beloved (1:16)***

# Song 1:17
***Lover (1:17)***

# Song 2:1
***Belovedᵃ (2:1)***\
\
*ᵃ1* Or *Lover*\
*ᵇ1* Possibly a member of the crocus family

# Song 2:2
***Lover (2:2)***

# Song 2:3
***Beloved (2:3–13)***

# Song 2:14
***Lover (2:14–15)***

# Song 2:16
***Beloved (2:16–3:11)***

# Song 2:17
*ᵃ17* Or *the hills of Bether*

# Song 3:10
*ᵃ10* Or *its inlaid interior a gift of love / from*

# Song 4:1
***Lover (4:1–15)***

# Song 4:4
*ᵃ4* The meaning of the Hebrew for this word is uncertain.

# Song 4:15
*ᵃ15* Or *I am* (spoken by the *Beloved*)

# Song 4:16
***Beloved (4:16)***

# Song 5:1
***Lover (5:1a)***\
\
***Friends (5:1b)***

# Song 5:2
***Beloved (5:2–8)***

# Song 5:6
*ᵃ6* Or *heart had gone out to him when he spoke*

# Song 5:9
***Friends (5:9)***

# Song 5:10
***Beloved (5:10–16)***

# Song 5:14
*ᵃ14* Or *lapis lazuli*

# Song 6:1
***Friends (6:1)***

# Song 6:2
***Beloved (6:2–3)***

# Song 6:4
***Lover (6:4–9)***

# Song 6:10
***Friends (6:10)***

# Song 6:11
***Lover (6:11–12)***

# Song 6:12
*ᵃ12* Or *among the chariots of Amminadab*; or *among the chariots of the people of the prince*

# Song 6:13
***Friends (6:13a)***\
\
***Lover (6:13b–7:9a)***

# Song 7:9
***Beloved (7:9b–8:4)***\
\
*ᵃ9* Septuagint, Aquila, Vulgate and Syriac; Hebrew *lips of sleepers*

# Song 7:11
*ᵃ11* Or *henna bushes*

# Song 8:5
***Friends (8:5a)***\
\
***Beloved (8:5b–7)***

# Song 8:6
*ᵃ6* Or *ardor*\
*ᵇ6* Hebrew *Sheol*\
*ᶜ6* Or */ like the very flame of the Lᴏʀᴅ*

# Song 8:7
*ᵃ7* Or *he*

# Song 8:8
***Friends (8:8–9)***

# Song 8:10
***Beloved (8:10–12)***

# Song 8:11
*ᵃ11* That is, about 25 pounds (about 11.5 kilograms); also in verse [12](vref:Song 8:12)

# Song 8:12
*ᵃ12* That is, about 5 pounds (about 2.3 kilograms)

# Song 8:13
***Lover (8:13)***

# Song 8:14
***Beloved (8:14)***

# Isa 1:2
***A Rebellious Nation (1:2–31)***

# Isa 1:17
*ᵃ17* Or */ rebuke the oppressor*

# Isa 2:1
***The Mountain of the Lᴏʀᴅ (2:1–5)***

# Isa 2:6
***The Day of the Lᴏʀᴅ (2:6–22)***

# Isa 2:9
*ᵃ9* Or *not raise them up*

# Isa 2:16
*ᵃ16* Hebrew *every ship of Tarshish*

# Isa 3:1
***Judgment on Jerusalem and Judah (3:1–4:1)***

# Isa 4:2
***The Branch of the Lᴏʀᴅ (4:2–6)***

# Isa 4:4
*ᵃ4* Or *the Spirit*

# Isa 5:1
***The Song of the Vineyard (5:1–7)***

# Isa 5:8
***Woes and Judgments (5:8–30)***

# Isa 5:10
*ᵃ10* Hebrew *ten-yoke*, that is, the land plowed by 10 yoke of oxen in one day\
*ᵇ10* That is, probably about 6 gallons (about 22 liters)\
*ᶜ10* That is, probably about 6 bushels (about 220 liters)\
*ᵈ10* That is, probably about 3/5 bushel (about 22 liters)

# Isa 5:14
*ᵃ14* Hebrew *Sheol*

# Isa 5:17
*ᵃ17* Septuagint; Hebrew */ strangers will eat*

# Isa 6:1
***Isaiah’s Commission (6:1–13)***

# Isa 6:10
*ᵃ9,10* Hebrew; Septuagint *‘You will be ever hearing, but never understanding; / you will be ever seeing, but never perceiving.’ / ¹⁰This people’s heart has become calloused; / they hardly hear with their ears, / and they have closed their eyes*

# Isa 7:1
***The Sign of Immanuel (7:1–25)***

# Isa 7:2
*ᵃ2* Or *has set up camp in*

# Isa 7:3
*ᵃ3* *Shear-Jashub* means *a remnant will return*.

# Isa 7:14
*ᵃ14* The Hebrew is plural.\
*ᵇ14* Masoretic Text; Dead Sea Scrolls *and he* or *and they*\
*ᶜ14* *Immanuel* means *God with us*.

# Isa 7:20
*ᵃ20* That is, the Euphrates

# Isa 7:23
*ᵃ23* That is, about 25 pounds (about 11.5 kilograms)

# Isa 8:1
***Assyria, the Lᴏʀᴅ’s Instrument (8:1–10)***\
\
*ᵃ1* *Maher-Shalal-Hash-Baz* means *quick to the plunder, swift to the spoil*; also in verse [3](vref:Isa 8:3).

# Isa 8:7
*ᵃ7* That is, the Euphrates

# Isa 8:8
*ᵃ8* *Immanuel* means *God with us*.

# Isa 8:9
*ᵃ9* Or *Do your worst*

# Isa 8:10
*ᵃ10* Hebrew *Immanuel*

# Isa 8:11
***Fear God (8:11–22)***

# Isa 8:12
*ᵃ12* Or *Do not call for a treaty / every time these people call for a treaty*

# Isa 9:1
***To Us a Child Is Born (9:1–7)***

# Isa 9:2
*ᵃ2* Or *land of darkness*

# Isa 9:6
*ᵃ6* Or *Wonderful, Counselor*

# Isa 9:8
***The Lᴏʀᴅ’s Anger Against Israel (9:8–10:4)***

# Isa 9:20
*ᵃ20* Or *arm*

# Isa 10:5
***God’s Judgment on Assyria (10:5–19)***

# Isa 10:13
*ᵃ13* Or */ I subdued the mighty,*

# Isa 10:20
***The Remnant of Israel (10:20–34)***

# Isa 10:21
*ᵃ21* Hebrew *shear-jashub*; also in verse [22](vref:Isa 10:22)

# Isa 10:27
*ᵃ27* Hebrew; Septuagint *broken / from your shoulders*

# Isa 11:1
***The Branch From Jesse (11:1–16)***

# Isa 11:6
*ᵃ6* Hebrew; Septuagint *lion will feed*

# Isa 11:11
*ᵃ11* Hebrew *from Pathros*\
*ᵇ11* That is, the upper Nile region\
*ᶜ11* Hebrew *Shinar*

# Isa 11:13
*ᵃ13* Or *hostility*

# Isa 11:15
*ᵃ15* Hebrew *the River*

# Isa 12:1
***Songs of Praise (12:1–6)***

# Isa 13:1
***A Prophecy Against Babylon (13:1–14:23)***

# Isa 13:6
*ᵃ6* Hebrew *Shaddai*

# Isa 13:19
*ᵃ19* Or *Chaldeans’*

# Isa 14:4
*ᵃ4* Dead Sea Scrolls, Septuagint and Syriac; the meaning of the word in the Masoretic Text is uncertain.

# Isa 14:9
*ᵃ9* Hebrew *Sheol*; also in verses [11](vref:Isa 14:11) and [15](vref:Isa 14:15)

# Isa 14:13
*ᵃ13* Or *the north*; Hebrew *Zaphon*

# Isa 14:24
***A Prophecy Against Assyria (14:24–27)***

# Isa 14:28
***A Prophecy Against the Philistines (14:28–32)***

# Isa 15:1
***A Prophecy Against Moab (15:1–16:14)***

# Isa 15:9
*ᵃ9* Masoretic Text; Dead Sea Scrolls, some Septuagint manuscripts and Vulgate *Dibon*\
*ᵇ9* Masoretic Text; Dead Sea Scrolls, some Septuagint manuscripts and Vulgate *Dibon*

# Isa 16:5
*ᵃ5* Hebrew *tent*

# Isa 16:7
*ᵃ7* Or *“raisin cakes,”* a wordplay

# Isa 17:1
***An Oracle Against Damascus (17:1–14)***

# Isa 17:8
*ᵃ8* That is, symbols of the goddess Asherah

# Isa 18:1
***A Prophecy Against Cush (18:1–7)***\
\
*ᵃ1* Or *of locusts*\
*ᵇ1* That is, the upper Nile region

# Isa 19:1
***A Prophecy About Egypt (19:1–25)***

# Isa 19:13
*ᵃ13* Hebrew *Noph*

# Isa 19:18
*ᵃ18* Most manuscripts of the Masoretic Text; some manuscripts of the Masoretic Text, Dead Sea Scrolls and Vulgate *City of the Sun* (that is, Heliopolis)

# Isa 20:1
***A Prophecy Against Egypt and Cush (20:1–6)***

# Isa 20:3
*ᵃ3* That is, the upper Nile region; also in verse [5](vref:Isa 20:5)

# Isa 21:1
***A Prophecy Against Babylon (21:1–10)***

# Isa 21:8
*ᵃ8* Dead Sea Scrolls and Syriac; Masoretic Text *A lion*

# Isa 21:11
***A Prophecy Against Edom (21:11–12)***\
\
*ᵃ11* *Dumah* means *silence* or *stillness*, a wordplay on *Edom*.

# Isa 21:13
***A Prophecy Against Arabia (21:13–17)***

# Isa 22:1
***A Prophecy About Jerusalem (22:1–25)***

# Isa 22:23
*ᵃ23* Or *throne*

# Isa 23:1
***A Prophecy About Tyre (23:1–18)***\
\
*ᵃ1* Hebrew *Kittim*

# Isa 23:3
*ᵃ2,3* Masoretic Text; one Dead Sea Scroll *Sidon, / who cross over the sea; / your envoys ³are on the great waters. / The grain of the Shihor, / the harvest of the Nile,*

# Isa 23:10
*ᵃ10* Dead Sea Scrolls and some Septuagint manuscripts; Masoretic Text *Go through*

# Isa 23:11
*ᵃ11* Hebrew *Canaan*

# Isa 23:12
*ᵃ12* Hebrew *Kittim*

# Isa 23:13
*ᵃ13* Or *Chaldeans*

# Isa 24:1
***The Lᴏʀᴅ’s Devastation of the Earth (24:1–23)***

# Isa 24:22
*ᵃ22* Or *released*

# Isa 25:1
***Praise to the Lᴏʀᴅ (25:1–12)***

# Isa 25:11
*ᵃ11* The meaning of the Hebrew for this word is uncertain.

# Isa 26:1
***A Song of Praise (26:1–21)***

# Isa 26:8
*ᵃ8* Or *judgments*

# Isa 26:16
*ᵃ16* The meaning of the Hebrew for this clause is uncertain.

# Isa 27:1
***Deliverance of Israel (27:1–13)***

# Isa 27:8
*ᵃ8* See Septuagint; the meaning of the Hebrew for this word is uncertain.

# Isa 27:9
*ᵃ9* That is, symbols of the goddess Asherah

# Isa 27:12
*ᵃ12* Hebrew *River*

# Isa 28:1
***Woe to Ephraim (28:1–29)***

# Isa 28:10
*ᵃ10* Hebrew */ sav lasav sav lasav / kav lakav kav lakav* (possibly meaningless sounds; perhaps a mimicking of the prophet’s words); also in verse [13](vref:Isa 28:13)

# Isa 28:15
*ᵃ15* Hebrew *Sheol*; also in verse [18](vref:Isa 28:18)\
*ᵇ15* Or *false gods*

# Isa 28:25
*ᵃ25* The meaning of the Hebrew for this word is uncertain.

# Isa 29:1
***Woe to David’s City (29:1–24)***

# Isa 29:2
*ᵃ2* The Hebrew for *altar hearth* sounds like the Hebrew for *Ariel*.

# Isa 29:13
*ᵃ13* Hebrew; Septuagint *They worship me in vain; / their teachings are but rules taught by men*

# Isa 30:1
***Woe to the Obstinate Nation (30:1–33)***

# Isa 31:1
***Woe to Those Who Rely on Egypt (31:1–9)***

# Isa 32:1
***The Kingdom of Righteousness (32:1–8)***

# Isa 32:9
***The Women of Jerusalem (32:9–20)***

# Isa 33:1
***Distress and Help (33:1–24)***

# Isa 33:6
*ᵃ6* Or *is a treasure from him*

# Isa 33:8
*ᵃ8* Dead Sea Scrolls; Masoretic Text */ the cities*

# Isa 33:9
*ᵃ9* Or *dries up*

# Isa 34:1
***Judgment Against the Nations (34:1–17)***

# Isa 34:2
*ᵃ2* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verse [5](vref:Isa 34:5).

# Isa 34:11
*ᵃ11* The precise identification of these birds is uncertain.

# Isa 35:1
***Joy of the Redeemed (35:1–10)***

# Isa 35:8
*ᵃ8* Or */ the simple will not stray from it*

# Isa 36:1
***Sennacherib Threatens Jerusalem (36:1–22)***

# Isa 37:1
***Jerusalem’s Deliverance Foretold (37:1–13)***

# Isa 37:9
*ᵃ9* That is, from the upper Nile region

# Isa 37:14
***Hezekiah’s Prayer (37:14–20)***

# Isa 37:20
*ᵃ20* Dead Sea Scrolls (see also [2 Kings 19:19](vref:2 Kgs 19:19)); Masoretic Text *alone are the Lᴏʀᴅ*

# Isa 37:21
***Sennacherib’s Fall (37:21–38)***

# Isa 37:25
*ᵃ25* Dead Sea Scrolls (see also [2 Kings 19:24](vref:2 Kgs 19:24)); Masoretic Text does not have *in foreign lands*.

# Isa 37:27
*ᵃ27* Some manuscripts of the Masoretic Text, Dead Sea Scrolls and some Septuagint manuscripts (see also [2 Kings 19:26](vref:2 Kgs 19:26)); most manuscripts of the Masoretic Text *roof / and terraced fields*

# Isa 38:1
***Hezekiah’s Illness (38:1–22)***

# Isa 38:10
*ᵃ10* Hebrew *Sheol*

# Isa 38:11
*ᵃ11* A few Hebrew manuscripts; most Hebrew manuscripts *in the place of cessation*

# Isa 38:18
*ᵃ18* Hebrew *Sheol*

# Isa 39:1
***Envoys From Babylon (39:1–8)***

# Isa 40:1
***Comfort for God’s People (40:1–31)***

# Isa 40:3
*ᵃ3* Or *A voice of one calling in the desert: / “Prepare the way for the Lᴏʀᴅ*\
*ᵇ3* Hebrew; Septuagint *make straight the paths of our God*

# Isa 40:9
*ᵃ9* Or *O Zion, bringer of good tidings, / go up on a high mountain. / O Jerusalem, bringer of good tidings*

# Isa 40:13
*ᵃ13* Or *Spirit*; or *spirit*

# Isa 41:1
***The Helper of Israel (41:1–29)***

# Isa 41:2
*ᵃ2* Or */ whom victory meets at every step*

# Isa 42:1
***The Servant of the Lᴏʀᴅ (42:1–9)***

# Isa 42:10
***Song of Praise to the Lᴏʀᴅ (42:10–17)***

# Isa 42:18
***Israel Blind and Deaf (42:18–25)***

# Isa 43:1
***Israel’s Only Savior (43:1–13)***

# Isa 43:3
*ᵃ3* That is, the upper Nile region

# Isa 43:14
***God’s Mercy and Israel’s Unfaithfulness (43:14–28)***\
\
*ᵃ14* Or *Chaldeans*

# Isa 43:28
*ᵃ28* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Isa 44:1
***Israel the Chosen (44:1–5)***

# Isa 44:6
***The Lᴏʀᴅ, Not Idols (44:6–23)***

# Isa 44:24
***Jerusalem to Be Inhabited (44:24–45:25)***

# Isa 45:2
*ᵃ2* Dead Sea Scrolls and Septuagint; the meaning of the word in the Masoretic Text is uncertain.

# Isa 45:13
*ᵃ13* Hebrew *him*

# Isa 45:14
*ᵃ14* That is, the upper Nile region

# Isa 46:1
***Gods of Babylon (46:1–13)***\
\
*ᵃ1* Or *are but beasts and cattle*

# Isa 47:1
***The Fall of Babylon (47:1–15)***\
\
*ᵃ1* Or *Chaldeans*; also in verse [5](vref:Isa 47:5)

# Isa 48:1
***Stubborn Israel (48:1–11)***

# Isa 48:12
***Israel Freed (48:12–22)***

# Isa 48:14
*ᵃ14* Or *Chaldeans*; also in verse [20](vref:Isa 48:20)

# Isa 49:1
***The Servant of the Lᴏʀᴅ (49:1–7)***

# Isa 49:8
***Restoration of Israel (49:8–26)***

# Isa 49:12
*ᵃ12* Dead Sea Scrolls; Masoretic Text *Sinim*

# Isa 49:24
*ᵃ24* Dead Sea Scrolls, Vulgate and Syriac (see also Septuagint and verse [25](vref:Isa 49:25)); Masoretic Text *righteous*

# Isa 50:1
***Israel’s Sin and the Servant’s Obedience (50:1–11)***

# Isa 51:1
***Everlasting Salvation for Zion (51:1–16)***

# Isa 51:17
***The Cup of the Lᴏʀᴅ’s Wrath (51:17–52:12)***

# Isa 51:19
*ᵃ19* Dead Sea Scrolls, Septuagint, Vulgate and Syriac; Masoretic Text */ how can I*

# Isa 52:5
*ᵃ5* Dead Sea Scrolls and Vulgate; Masoretic Text *wail*

# Isa 52:13
***The Suffering and Glory of the Servant (52:13–53:12)***\
\
*ᵃ13* Or *will prosper*

# Isa 52:14
*ᵃ14* Hebrew *you*

# Isa 52:15
*ᵃ15* Hebrew; Septuagint *so will many nations marvel at him*

# Isa 53:8
*ᵃ8* Or *From arrest*\
*ᵇ8* Or *away. / Yet who of his generation considered / that he was cut off from the land of the living / for the transgression of my people, / to whom the blow was due?*

# Isa 53:10
*ᵃ10* Hebrew *though you make*

# Isa 53:11
*ᵃ11* Dead Sea Scrolls (see also Septuagint); Masoretic Text does not have *the light ⌞​of life​⌟*.\
*ᵇ11* Or (with Masoretic Text) *¹¹He will see the result of the suffering of his soul / and be satisfied*\
*ᶜ11* Or *by knowledge of him*

# Isa 53:12
*ᵃ12* Or *many*\
*ᵇ12* Or *numerous*

# Isa 54:1
***The Future Glory of Zion (54:1–17)***

# Isa 54:11
*ᵃ11* The meaning of the Hebrew for this word is uncertain.\
*ᵇ11* Or *lapis lazuli*

# Isa 55:1
***Invitation to the Thirsty (55:1–13)***

# Isa 56:1
***Salvation for Others (56:1–8)***

# Isa 56:9
***God’s Accusation Against the Wicked (56:9–57:13)***

# Isa 57:9
*ᵃ9* Or *to the king*\
*ᵇ9* Or *idols*\
*ᶜ9* Hebrew *Sheol*

# Isa 57:14
***Comfort for the Contrite (57:14–21)***

# Isa 58:1
***True Fasting (58:1–14)***

# Isa 58:8
*ᵃ8* Or *your righteous One*

# Isa 59:1
***Sin, Confession and Redemption (59:1–21)***

# Isa 59:19
*ᵃ19* Or *When the enemy comes in like a flood, / the Spirit of the Lᴏʀᴅ will put him to flight*

# Isa 60:1
***The Glory of Zion (60:1–22)***

# Isa 60:9
*ᵃ9* Or *the trading ships*

# Isa 61:1
***The Year of the Lᴏʀᴅ’s Favor (61:1–11)***\
\
*ᵃ1* Hebrew; Septuagint *the blind*

# Isa 62:1
***Zion’s New Name (62:1–12)***

# Isa 62:4
*ᵃ4* *Hephzibah* means *my delight is in her*.\
*ᵇ4* *Beulah* means *married*.

# Isa 62:5
*ᵃ5* Or *Builder*

# Isa 63:1
***God’s Day of Vengeance and Redemption (63:1–6)***

# Isa 63:7
***Praise and Prayer (63:7–64:12)***

# Isa 63:11
*ᵃ11* Or *But may he recall*

# Isa 63:19
*ᵃ19* Or *We are like those you have never ruled, / like those never called by your name*

# Isa 65:1
***Judgment and Salvation (65:1–16)***

# Isa 65:17
***New Heavens and a New Earth (65:17–25)***

# Isa 65:20
*ᵃ20* Or */ the sinner who reaches*

# Isa 66:1
***Judgment and Hope (66:1–24)***

# Isa 66:17
*ᵃ17* Or *gardens behind one of your temples, and*

# Isa 66:18
*ᵃ18* The meaning of the Hebrew for this clause is uncertain.

# Isa 66:19
*ᵃ19* Some Septuagint manuscripts *Put* (Libyans); Hebrew *Pul*

# Jer 1:4
***The Call of Jeremiah (1:4–19)***

# Jer 1:5
*ᵃ5* Or *chose*

# Jer 1:12
*ᵃ12* The Hebrew for *watching* sounds like the Hebrew for *almond tree*.

# Jer 2:1
***Israel Forsakes God (2:1–3:5)***

# Jer 2:6
*ᵃ6* Or *and the shadow of death*

# Jer 2:10
*ᵃ10* That is, Cyprus and western coastlands\
*ᵇ10* The home of Bedouin tribes in the Syro-Arabian desert

# Jer 2:11
*ᵃ11* Masoretic Text; an ancient Hebrew scribal tradition *my*

# Jer 2:16
*ᵃ16* Hebrew *Noph*\
*ᵇ16* Or *have cracked your skull*

# Jer 2:18
*ᵃ18* That is, a branch of the Nile\
*ᵇ18* That is, the Euphrates

# Jer 3:2
*ᵃ2* Or *an Arab*

# Jer 3:6
***Unfaithful Israel (3:6–4:4)***

# Jer 4:5
***Disaster From the North (4:5–31)***

# Jer 4:12
*ᵃ12* Or *comes at my command*

# Jer 5:1
***Not One Is Upright (5:1–31)***

# Jer 6:1
***Jerusalem Under Siege (6:1–30)***

# Jer 6:10
*ᵃ10* Hebrew *uncircumcised*

# Jer 7:1
***False Religion Worthless (7:1–29)***

# Jer 7:9
*ᵃ9* Or *and swear by false gods*

# Jer 7:30
***The Valley of Slaughter (7:30–8:3)***

# Jer 8:4
***Sin and Punishment (8:4–9:26)***

# Jer 8:13
*ᵃ13* The meaning of the Hebrew for this sentence is uncertain.

# Jer 8:18
*ᵃ18* The meaning of the Hebrew for this word is uncertain.

# Jer 9:3
*ᵃ3* Or *lies; / they are not valiant for truth*

# Jer 9:4
*ᵃ4* Or *a deceiving Jacob*

# Jer 9:6
*ᵃ6* That is, Jeremiah (the Hebrew is singular)

# Jer 9:26
*ᵃ26* Or *desert and who clip the hair by their foreheads*

# Jer 10:1
***God and Idols (10:1–16)***

# Jer 10:11
*ᵃ11* The text of this verse is in Aramaic.

# Jer 10:17
***Coming Destruction (10:17–22)***

# Jer 10:23
***Jeremiah’s Prayer (10:23–25)***

# Jer 11:1
***The Covenant Is Broken (11:1–17)***

# Jer 11:15
*ᵃ15* Or *Could consecrated meat avert your punishment? / Then you would rejoice*

# Jer 11:18
***Plot Against Jeremiah (11:18–23)***

# Jer 12:1
***Jeremiah’s Complaint (12:1–4)***

# Jer 12:4
*ᵃ4* Or *land mourn*

# Jer 12:5
***God’s Answer (12:5–17)***\
\
*ᵃ5* Or *If you put your trust in a land of safety*\
*ᵇ5* Or *the flooding of*

# Jer 13:1
***A Linen Belt (13:1–11)***

# Jer 13:4
*ᵃ4* Or possibly *the Euphrates*; also in verses [5–7](vref:Jer 13:5-7)

# Jer 13:12
***Wineskins (13:12–14)***

# Jer 13:15
***Threat of Captivity (13:15–27)***

# Jer 13:23
*ᵃ23* Hebrew *Cushite* (probably a person from the upper Nile region)

# Jer 14:1
***Drought, Famine, Sword (14:1–15:21)***

# Jer 14:14
*ᵃ14* Or *visions, worthless divinations*

# Jer 15:14
*ᵃ14* Some Hebrew manuscripts, Septuagint and Syriac (see also [Jer. 17:4](vref:Jer 17:4)); most Hebrew manuscripts *I will cause your enemies to bring you / into*

# Jer 16:1
***Day of Disaster (16:1–17:18)***

# Jer 17:2
*ᵃ2* That is, symbols of the goddess Asherah

# Jer 17:3
*ᵃ2,3* Or *hills / ³and the mountains of the land. / Your*

# Jer 17:19
***Keeping the Sabbath Holy (17:19–27)***

# Jer 18:1
***At the Potter’s House (18:1–19:15)***

# Jer 18:14
*ᵃ14* The meaning of the Hebrew for this sentence is uncertain.

# Jer 19:7
*ᵃ7* The Hebrew for *ruin* sounds like the Hebrew for *jar* (see verses [1](vref:Jer 19:1) and [10](vref:Jer 19:10)).

# Jer 20:1
***Jeremiah and Pashhur (20:1–6)***

# Jer 20:3
*ᵃ3* *Magor-Missabib* means *terror on every side*.

# Jer 20:7
***Jeremiah’s Complaint (20:7–18)***\
\
*ᵃ7* Or *persuaded*

# Jer 21:1
***God Rejects Zedekiah’s Request (21:1–14)***

# Jer 21:2
*ᵃ2* Hebrew *Nebuchadrezzar*, of which *Nebuchadnezzar* is a variant; here and often in Jeremiah and Ezekiel

# Jer 21:4
*ᵃ4* Or *Chaldeans*; also in verse [9](vref:Jer 21:9)

# Jer 22:1
***Judgment Against Evil Kings (22:1–30)***

# Jer 22:11
*ᵃ11* Also called *Jehoahaz*

# Jer 22:23
*ᵃ23* That is, the palace in Jerusalem (see [1 Kings 7:2](vref:1 Kgs 7:2))

# Jer 22:24
*ᵃ24* Hebrew *Coniah*, a variant of *Jehoiachin*; also in verse [28](vref:Jer 22:28)

# Jer 22:25
*ᵃ25* Or *Chaldeans*

# Jer 23:1
***The Righteous Branch (23:1–8)***

# Jer 23:5
*ᵃ5* Or *up from David’s line*

# Jer 23:9
***Lying Prophets (23:9–32)***

# Jer 23:10
*ᵃ10* Or *because of these things*\
*ᵇ10* Or *land mourns*

# Jer 23:33
***False Oracles and False Prophets (23:33–40)***\
\
*ᵃ33* Or *burden* (see Septuagint and Vulgate)\
*ᵇ33* Hebrew; Septuagint and Vulgate *‘You are the burden*. (The Hebrew for *oracle* and *burden* is the same.)

# Jer 24:1
***Two Baskets of Figs (24:1–10)***\
\
*ᵃ1* Hebrew *Jeconiah*, a variant of *Jehoiachin*

# Jer 24:5
*ᵃ5* Or *Chaldeans*

# Jer 25:1
***Seventy Years of Captivity (25:1–14)***

# Jer 25:9
*ᵃ9* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Jer 25:12
*ᵃ12* Or *Chaldeans*

# Jer 25:15
***The Cup of God’s Wrath (25:15–38)***

# Jer 25:23
*ᵃ23* Or *who clip the hair by their foreheads*

# Jer 25:26
*ᵃ26* *Sheshach* is a cryptogram for Babylon.

# Jer 25:38
*ᵃ38* Some Hebrew manuscripts and Septuagint (see also [Jer. 46:16](vref:Jer 46:16) and [50:16](vref:Jer 50:16)); most Hebrew manuscripts *anger*

# Jer 26:1
***Jeremiah Threatened With Death (26:1–24)***

# Jer 26:18
*ᵃ18* [Micah 3:12](vref:Mic 3:12)

# Jer 27:1
***Judah to Serve Nebuchadnezzar (27:1–22)***\
\
*ᵃ1* A few Hebrew manuscripts and Syriac (see also [Jer. 27:3](vref:Jer 27:3), [12](vref:Jer 27:12) and [28:1](vref:Jer 28:1)); most Hebrew manuscripts *Jehoiakim* (Most Septuagint manuscripts do not have this verse.)

# Jer 27:20
*ᵃ20* Hebrew *Jeconiah*, a variant of *Jehoiachin*

# Jer 28:1
***The False Prophet Hananiah (28:1–17)***

# Jer 28:4
*ᵃ4* Hebrew *Jeconiah*, a variant of *Jehoiachin*

# Jer 29:1
***A Letter to the Exiles (29:1–23)***

# Jer 29:2
*ᵃ2* Hebrew *Jeconiah*, a variant of *Jehoiachin*

# Jer 29:14
*ᵃ14* Or *will restore your fortunes*

# Jer 29:24
***Message to Shemaiah (29:24–32)***

# Jer 30:1
***Restoration of Israel (30:1–31:40)***

# Jer 30:3
*ᵃ3* Or *will restore the fortunes of my people Israel and Judah*

# Jer 31:3
*ᵃ3* Or *Lᴏʀᴅ has appeared to us from afar*

# Jer 31:22
*ᵃ22* Or *will go about ⌞​seeking​⌟*; or *will protect*

# Jer 31:23
*ᵃ23* Or *I restore their fortunes*

# Jer 31:32
*ᵃ32* Hebrew; Septuagint and Syriac */ and I turned away from*\
*ᵇ32* Or *was their master*

# Jer 32:1
***Jeremiah Buys a Field (32:1–44)***

# Jer 32:4
*ᵃ4* Or *Chaldeans*; also in verses [5](vref:Jer 32:5), [24](vref:Jer 32:24), [25](vref:Jer 32:25), [28](vref:Jer 32:28), [29](vref:Jer 32:29) and [43](vref:Jer 32:43)

# Jer 32:9
*ᵃ9* That is, about 7 ounces (about 200 grams)

# Jer 32:35
*ᵃ35* Or *to make their sons and daughters pass through ⌞​the fire​⌟*

# Jer 32:44
*ᵃ44* Or *will bring them back from captivity*

# Jer 33:1
***Promise of Restoration (33:1–26)***

# Jer 33:5
*ᵃ5* Or *Chaldeans*

# Jer 33:7
*ᵃ7* Or *will restore the fortunes of Judah and Israel*

# Jer 33:16
*ᵃ16* Or *he*

# Jer 33:24
*ᵃ24* Or *families*

# Jer 33:26
*ᵃ26* Or *will bring them back from captivity*

# Jer 34:1
***Warning to Zedekiah (34:1–7)***

# Jer 34:8
***Freedom for Slaves (34:8–22)***

# Jer 34:14
*ᵃ14* [Deut. 15:12](vref:Deut 15:12)

# Jer 35:1
***The Recabites (35:1–19)***

# Jer 35:11
*ᵃ11* Or *Chaldean*

# Jer 36:1
***Jehoiakim Burns Jeremiah’s Scroll (36:1–32)***

# Jer 37:1
***Jeremiah in Prison (37:1–21)***\
\
*ᵃ1* Hebrew *Coniah*, a variant of *Jehoiachin*

# Jer 37:5
*ᵃ5* Or *Chaldeans*; also in verses [8](vref:Jer 37:8), [9](vref:Jer 37:9), [13](vref:Jer 37:13) and [14](vref:Jer 37:14)

# Jer 37:10
*ᵃ10* Or *Chaldean*; also in verse [11](vref:Jer 37:11)

# Jer 38:1
***Jeremiah Thrown Into a Cistern (38:1–13)***\
\
*ᵃ1* Hebrew *Jucal*, a variant of *Jehucal*

# Jer 38:2
*ᵃ2* Or *Chaldeans*; also in verses [18](vref:Jer 38:18), [19](vref:Jer 38:19) and [23](vref:Jer 38:23)

# Jer 38:7
*ᵃ7* Probably from the upper Nile region\
*ᵇ7* Or *a eunuch*

# Jer 38:14
***Zedekiah Questions Jeremiah Again (38:14–28a)***

# Jer 38:23
*ᵃ23* Or *and you will cause this city to*

# Jer 38:28
***The Fall of Jerusalem (38:28b–39:18)***

# Jer 39:3
*ᵃ3* Or *Nergal-Sharezer, Samgar-Nebo, Sarsekim*

# Jer 39:4
*ᵃ4* Or *the Jordan Valley*

# Jer 39:5
*ᵃ5* Or *Chaldean*

# Jer 39:8
*ᵃ8* Or *Chaldeans*

# Jer 40:1
***Jeremiah Freed (40:1–6)***

# Jer 40:5
*ᵃ5* Or *Jeremiah answered*

# Jer 40:7
***Gedaliah Assassinated (40:7–41:15)***

# Jer 40:8
*ᵃ8* Hebrew *Jezaniah*, a variant of *Jaazaniah*

# Jer 40:9
*ᵃ9* Or *Chaldeans*; also in verse [10](vref:Jer 40:10)

# Jer 41:3
*ᵃ3* Or *Chaldean*

# Jer 41:16
***Flight to Egypt (41:16–43:13)***

# Jer 41:18
*ᵃ18* Or *Chaldeans*

# Jer 42:1
*ᵃ1* Hebrew; Septuagint (see also [43:2](vref:Jer 43:2)) *Azariah*

# Jer 42:20
*ᵃ20* Or *you erred in your hearts*

# Jer 43:3
*ᵃ3* Or *Chaldeans*

# Jer 43:12
*ᵃ12* Or *I*

# Jer 43:13
*ᵃ13* Or *in Heliopolis*

# Jer 44:1
***Disaster Because of Idolatry (44:1–30)***\
\
*ᵃ1* Hebrew *Noph*\
*ᵇ1* Hebrew *in Pathros*

# Jer 44:15
*ᵃ15* Hebrew *in Egypt and Pathros*

# Jer 45:1
***A Message to Baruch (45:1–5)***

# Jer 46:1
***A Message About Egypt (46:1–28)***

# Jer 46:9
*ᵃ9* That is, the upper Nile region

# Jer 46:14
*ᵃ14* Hebrew *Noph*; also in verse [19](vref:Jer 46:19)

# Jer 46:25
*ᵃ25* Hebrew *No*

# Jer 47:1
***A Message About the Philistines (47:1–7)***

# Jer 47:4
*ᵃ4* That is, Crete

# Jer 48:1
***A Message About Moab (48:1–47)***\
\
*ᵃ1* Or */ Misgab*

# Jer 48:2
*ᵃ2* The Hebrew for *Heshbon* sounds like the Hebrew for *plot*.\
*ᵇ2* The name of the Moabite town Madmen sounds like the Hebrew for *be silenced*.

# Jer 48:4
*ᵃ4* Hebrew; Septuagint */ proclaim it to Zoar*

# Jer 48:6
*ᵃ6* Or *like Aroer*

# Jer 48:9
*ᵃ9* Or *Give wings to Moab, / for she will fly away*

# Jer 48:25
*ᵃ25* *Horn* here symbolizes strength.

# Jer 48:41
*ᵃ41* Or *The cities*

# Jer 49:1
***A Message About Ammon (49:1–6)***\
\
*ᵃ1* Or *their king*; Hebrew *malcam*; also in verse [3](vref:Jer 49:3)

# Jer 49:7
***A Message About Edom (49:7–22)***

# Jer 49:21
*ᵃ21* Hebrew *Yam Suph*; that is, Sea of Reeds

# Jer 49:23
***A Message About Damascus (49:23–27)***\
\
*ᵃ23* Hebrew *on* or *by*

# Jer 49:28
***A Message About Kedar and Hazor (49:28–33)***

# Jer 49:32
*ᵃ32* Or *who clip the hair by their foreheads*

# Jer 49:34
***A Message About Elam (49:34–39)***

# Jer 50:1
***A Message About Babylon (50:1–51:64)***\
\
*ᵃ1* Or *Chaldeans*; also in verses [8](vref:Jer 50:8), [25](vref:Jer 50:25), [35](vref:Jer 50:35) and [45](vref:Jer 50:45)

# Jer 50:10
*ᵃ10* Or *Chaldea*

# Jer 50:21
*ᵃ21* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them; also in verse [26](vref:Jer 50:26).

# Jer 50:38
*ᵃ38* Or *A sword against*

# Jer 51:1
*ᵃ1* *Leb Kamai* is a cryptogram for Chaldea, that is, Babylonia.

# Jer 51:3
*ᵃ3* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ, often by totally destroying them.

# Jer 51:4
*ᵃ4* Or *Chaldea*

# Jer 51:5
*ᵃ5* Or */ and the land ⌞​of the Babylonians​⌟*

# Jer 51:24
*ᵃ24* Or *Chaldea*; also in verse [35](vref:Jer 51:35)

# Jer 51:35
*ᵃ35* Or *done to us and to our children*

# Jer 51:41
*ᵃ41* *Sheshach* is a cryptogram for Babylon.

# Jer 51:54
*ᵃ54* Or *Chaldeans*

# Jer 52:1
***The Fall of Jerusalem (52:1–30)***

# Jer 52:7
*ᵃ7* Or *Chaldeans*; also in verse [17](vref:Jer 52:17)\
*ᵇ7* Or *the Jordan Valley*

# Jer 52:8
*ᵃ8* Or *Chaldean*; also in verse [14](vref:Jer 52:14)

# Jer 52:15
*ᵃ15* Or *populace*

# Jer 52:21
*ᵃ21* That is, about 27 feet (about 8.1 meters) high and 18 feet (about 5.4 meters) in circumference

# Jer 52:22
*ᵃ22* That is, about 7 1/2 feet (about 2.3 meters)

# Jer 52:31
***Jehoiachin Released (52:31–34)***\
\
*ᵃ31* Also called *Amel-Marduk*

# Lam 1:1
*ᵃ*This chapter is an acrostic poem, the verses of which begin with the successive letters of the Hebrew alphabet.

# Lam 1:14
*ᵃ14* Most Hebrew manuscripts; Septuagint *He kept watch over my sins*

# Lam 1:15
*ᵃ15* Or *has set a time for me / when he will*

# Lam 2:1
*ᵃ*This chapter is an acrostic poem, the verses of which begin with the successive letters of the Hebrew alphabet.\
*ᵇ1* Or *How the Lord in his anger / has treated the Daughter of Zion with contempt*

# Lam 2:3
*ᵃ3* Or */ all the strength*; or *every king*; *horn* here symbolizes strength.

# Lam 2:17
*ᵃ17* *Horn* here symbolizes strength.

# Lam 3:1
*ᵃ*This chapter is an acrostic poem; the verses of each stanza begin with the successive letters of the Hebrew alphabet, and the verses within each stanza begin with the same letter.

# Lam 4:1
*ᵃ*This chapter is an acrostic poem, the verses of which begin with the successive letters of the Hebrew alphabet.

# Lam 4:7
*ᵃ7* Or *lapis lazuli*

# Ezek 1:1
***The Living Creatures and the Glory of the Lᴏʀᴅ (1:1–28)***\
\
*ᵃ1* Or *⌞​my​⌟*

# Ezek 1:3
*ᵃ3* Or *Ezekiel son of Buzi the priest*\
*ᵇ3* Or *Chaldeans*

# Ezek 1:17
*ᵃ17* Or *aside*

# Ezek 1:24
*ᵃ24* Hebrew *Shaddai*

# Ezek 1:26
*ᵃ26* Or *lapis lazuli*

# Ezek 2:1
***Ezekiel’s Call (2:1–3:15)***

# Ezek 3:16
***Warning to Israel (3:16–27)***

# Ezek 3:18
*ᵃ18* Or *in*; also in verses [19](vref:Ezek 3:19) and [20](vref:Ezek 3:20)

# Ezek 4:1
***Siege of Jerusalem Symbolized (4:1–5:17)***

# Ezek 4:4
*ᵃ4* Or *your side*

# Ezek 4:10
*ᵃ10* That is, about 8 ounces (about 0.2 kilogram)

# Ezek 4:11
*ᵃ11* That is, about 2/3 quart (about 0.6 liter)

# Ezek 4:17
*ᵃ17* Or *away in*

# Ezek 5:7
*ᵃ7* Most Hebrew manuscripts; some Hebrew manuscripts and Syriac *You have*

# Ezek 6:1
***A Prophecy Against the Mountains of Israel (6:1–14)***

# Ezek 6:14
*ᵃ14* Most Hebrew manuscripts; a few Hebrew manuscripts *Riblah*

# Ezek 7:1
***The End Has Come (7:1–27)***

# Ezek 7:5
*ᵃ5* Most Hebrew manuscripts; some Hebrew manuscripts and Syriac *Disaster after*

# Ezek 7:11
*ᵃ11* Or *The violent one has become*

# Ezek 8:1
***Idolatry in the Temple (8:1–18)***

# Ezek 8:2
*ᵃ2* Or *saw a fiery figure*

# Ezek 9:1
***Idolaters Killed (9:1–11)***

# Ezek 10:1
***The Glory Departs From the Temple (10:1–22)***\
\
*ᵃ1* Or *lapis lazuli*

# Ezek 10:5
*ᵃ5* Hebrew *El-Shaddai*

# Ezek 10:11
*ᵃ11* Or *aside*

# Ezek 11:1
***Judgment on Israel’s Leaders (11:1–15)***

# Ezek 11:3
*ᵃ3* Or *This is not the time to build houses.*

# Ezek 11:15
*ᵃ15* Or *are in exile with you* (see Septuagint and Syriac)\
*ᵇ15* Or *those to whom the people of Jerusalem have said, ‘Stay*

# Ezek 11:16
***Promised Return of Israel (11:16–25)***

# Ezek 11:24
*ᵃ24* Or *Chaldea*

# Ezek 12:1
***The Exile Symbolized (12:1–28)***

# Ezek 13:1
***False Prophets Condemned (13:1–23)***

# Ezek 13:3
*ᵃ3* Or *wicked*

# Ezek 13:14
*ᵃ14* Or *the city*

# Ezek 14:1
***Idolaters Condemned (14:1–11)***

# Ezek 14:12
***Judgment Inescapable (14:12–23)***

# Ezek 14:14
*ᵃ14* Or *Danel*; the Hebrew spelling may suggest a person other than the prophet Daniel; also in verse [20](vref:Ezek 14:20).

# Ezek 15:1
***Jerusalem, A Useless Vine (15:1–8)***

# Ezek 16:1
***An Allegory of Unfaithful Jerusalem (16:1–63)***

# Ezek 16:6
*ᵃ6* A few Hebrew manuscripts, Septuagint and Syriac; most Hebrew manuscripts *“Live!” And as you lay there in your blood I said to you, “Live!”*

# Ezek 16:7
*ᵃ7* Or *became mature*

# Ezek 16:9
*ᵃ9* Or *I had bathed*

# Ezek 16:15
*ᵃ15* Most Hebrew manuscripts; one Hebrew manuscript (see some Septuagint manuscripts) *by. Such a thing should not happen*

# Ezek 16:21
*ᵃ21* Or *and made them pass through ⌞​the fire​⌟*

# Ezek 16:29
*ᵃ29* Or *Chaldea*

# Ezek 16:36
*ᵃ36* Or *lust*

# Ezek 16:57
*ᵃ57* Many Hebrew manuscripts and Syriac; most Hebrew manuscripts, Septuagint and Vulgate *Aram*

# Ezek 17:1
***Two Eagles and a Vine (17:1–24)***

# Ezek 18:1
***The Soul Who Sins Will Die (18:1–32)***

# Ezek 18:8
*ᵃ8* Or *take interest*; similarly in verses [13](vref:Ezek 18:13) and [17](vref:Ezek 18:17)

# Ezek 18:10
*ᵃ10* Or *things to a brother*

# Ezek 18:17
*ᵃ17* Septuagint (see also verse [8](vref:Ezek 18:8)); Hebrew *from the poor*

# Ezek 19:1
***A Lament for Israel’s Princes (19:1–14)***

# Ezek 19:7
*ᵃ7* Targum (see Septuagint); Hebrew *He knew*

# Ezek 19:10
*ᵃ10* Two Hebrew manuscripts; most Hebrew manuscripts *your blood*

# Ezek 19:14
*ᵃ14* Or *from under its*

# Ezek 20:1
***Rebellious Israel (20:1–29)***

# Ezek 20:26
*ᵃ26* Or *—making every firstborn pass through ⌞​the fire​⌟*

# Ezek 20:29
*ᵃ29* *Bamah* means *high place*.

# Ezek 20:30
***Judgment and Restoration (20:30–44)***

# Ezek 20:31
*ᵃ31* Or *—making your sons pass through*

# Ezek 20:40
*ᵃ40* Or *and the gifts of your firstfruits*

# Ezek 20:45
***Prophecy Against the South (20:45–49)***

# Ezek 21:1
***Babylon, God’s Sword of Judgment (21:1–32)***

# Ezek 21:15
*ᵃ15* Septuagint; the meaning of the Hebrew for this word is uncertain.

# Ezek 22:1
***Jerusalem’s Sins (22:1–31)***

# Ezek 22:12
*ᵃ12* Or *usury and interest*

# Ezek 22:16
*ᵃ16* Or *When I have allotted you your inheritance*

# Ezek 22:24
*ᵃ24* Septuagint; Hebrew *has not been cleansed or rained on*

# Ezek 22:25
*ᵃ25* Septuagint; Hebrew *prophets*

# Ezek 23:1
***Two Adulterous Sisters (23:1–49)***

# Ezek 23:14
*ᵃ14* Or *Babylonians*

# Ezek 23:15
*ᵃ15* Or *Babylonia*; also in verse [16](vref:Ezek 23:16)

# Ezek 23:21
*ᵃ21* Syriac (see also verse [3](vref:Ezek 23:3)); Hebrew *caressed because of your young breasts*

# Ezek 23:24
*ᵃ24* The meaning of the Hebrew for this word is uncertain.

# Ezek 23:37
*ᵃ37* Or *even made the children they bore to me pass through ⌞​the fire​⌟*

# Ezek 23:42
*ᵃ42* Or *drunkards*

# Ezek 24:1
***The Cooking Pot (24:1–14)***

# Ezek 24:15
***Ezekiel’s Wife Dies (24:15–27)***

# Ezek 24:23
*ᵃ23* Or *away in*

# Ezek 25:1
***A Prophecy Against Ammon (25:1–7)***

# Ezek 25:8
***A Prophecy Against Moab (25:8–11)***

# Ezek 25:12
***A Prophecy Against Edom (25:12–14)***

# Ezek 25:15
***A Prophecy Against Philistia (25:15–17)***

# Ezek 26:1
***A Prophecy Against Tyre (26:1–21)***

# Ezek 26:7
*ᵃ7* Hebrew *Nebuchadrezzar*, of which *Nebuchadnezzar* is a variant; here and often in Ezekiel and Jeremiah

# Ezek 26:20
*ᵃ20* Septuagint; Hebrew *return, and I will give glory*

# Ezek 27:1
***A Lament for Tyre (27:1–36)***

# Ezek 27:5
*ᵃ5* That is, Hermon

# Ezek 27:6
*ᵃ6* Targum; the Masoretic Text has a different division of the consonants.\
*ᵇ6* Hebrew *Kittim*

# Ezek 27:9
*ᵃ9* That is, Byblos

# Ezek 27:15
*ᵃ15* Septuagint; Hebrew *Dedan*

# Ezek 27:16
*ᵃ16* Most Hebrew manuscripts; some Hebrew manuscripts and Syriac *Edom*

# Ezek 27:17
*ᵃ17* The meaning of the Hebrew for this word is uncertain.

# Ezek 28:1
***A Prophecy Against the King of Tyre (28:1–19)***

# Ezek 28:3
*ᵃ3* Or *Danel*; the Hebrew spelling may suggest a person other than the prophet Daniel.

# Ezek 28:13
*ᵃ13* Or *lapis lazuli*\
*ᵇ13* The precise identification of some of these precious stones is uncertain.\
*ᶜ13* The meaning of the Hebrew for this phrase is uncertain.

# Ezek 28:20
***A Prophecy Against Sidon (28:20–26)***

# Ezek 29:1
***A Prophecy Against Egypt (29:1–21)***

# Ezek 29:7
*ᵃ7* Syriac (see also Septuagint and Vulgate); Hebrew *and you caused their backs to stand*

# Ezek 29:10
*ᵃ10* That is, the upper Nile region

# Ezek 29:14
*ᵃ14* Hebrew *to Pathros*

# Ezek 29:21
*ᵃ21* *Horn* here symbolizes strength.

# Ezek 30:1
***A Lament for Egypt (30:1–26)***

# Ezek 30:4
*ᵃ4* That is, the upper Nile region; also in verses [5](vref:Ezek 30:5) and [9](vref:Ezek 30:9)

# Ezek 30:5
*ᵃ5* Hebrew *Cub*

# Ezek 30:13
*ᵃ13* Hebrew *Noph*; also in verse [16](vref:Ezek 30:16)

# Ezek 30:14
*ᵃ14* Hebrew *waste Pathros*\
*ᵇ14* Hebrew *No*; also in verses [15](vref:Ezek 30:15) and [16](vref:Ezek 30:16)

# Ezek 30:15
*ᵃ15* Hebrew *Sin*; also in verse [16](vref:Ezek 30:16)

# Ezek 30:17
*ᵃ17* Hebrew *Awen* (or *On*)\
*ᵇ17* Hebrew *Pi Beseth*

# Ezek 31:1
***A Cedar in Lebanon (31:1–18)***

# Ezek 31:15
*ᵃ15* Hebrew *Sheol*; also in verses [16](vref:Ezek 31:16) and [17](vref:Ezek 31:17)

# Ezek 32:1
***A Lament for Pharaoh (32:1–32)***

# Ezek 32:9
*ᵃ9* Hebrew; Septuagint *bring you into captivity among the nations, / to*

# Ezek 32:21
*ᵃ21* Hebrew *Sheol*; also in verse [27](vref:Ezek 32:27)

# Ezek 33:1
***Ezekiel a Watchman (33:1–20)***

# Ezek 33:8
*ᵃ8* Or *in*; also in verse [9](vref:Ezek 33:9)

# Ezek 33:10
*ᵃ10* Or *away in*

# Ezek 33:21
***Jerusalem’s Fall Explained (33:21–33)***

# Ezek 34:1
***Shepherds and Sheep (34:1–31)***

# Ezek 34:26
*ᵃ26* Or *I will make them and the places surrounding my hill a blessing*

# Ezek 35:1
***A Prophecy Against Edom (35:1–15)***

# Ezek 36:1
***A Prophecy to the Mountains of Israel (36:1–38)***

# Ezek 37:1
***The Valley of Dry Bones (37:1–14)***

# Ezek 37:5
*ᵃ5* The Hebrew for this word can also mean *wind* or *spirit* (see verses [6–14](vref:Ezek 37:6-14)).

# Ezek 37:15
***One Nation Under One King (37:15–28)***

# Ezek 37:23
*ᵃ23* Many Hebrew manuscripts (see also Septuagint); most Hebrew manuscripts *all their dwelling places where they sinned*

# Ezek 38:1
***A Prophecy Against Gog (38:1–39:29)***

# Ezek 38:2
*ᵃ2* Or *the prince of Rosh,*

# Ezek 38:3
*ᵃ3* Or *Gog, prince of Rosh,*

# Ezek 38:5
*ᵃ5* That is, the upper Nile region

# Ezek 38:13
*ᵃ13* Or *her strong lions*

# Ezek 39:1
*ᵃ1* Or *Gog, prince of Rosh,*

# Ezek 39:11
*ᵃ11* Or *of*\
*ᵇ11* That is, the Dead Sea\
*ᶜ11* *Hamon Gog* means *hordes of Gog*.

# Ezek 39:16
*ᵃ16* *Hamonah* means *horde*.

# Ezek 39:25
*ᵃ25* Or *now restore the fortunes of Jacob*

# Ezek 40:1
***The New Temple Area (40:1–4)***

# Ezek 40:5
***The East Gate to the Outer Court (40:5–16)***\
\
*ᵃ5* The common cubit was about 1 1/2 feet (about 0.5 meter).\
*ᵇ5* That is, about 3 inches (about 8 centimeters)

# Ezek 40:6
*ᵃ6* Septuagint; Hebrew *deep, the first threshold, one rod deep*

# Ezek 40:9
*ᵃ8,9* Many Hebrew manuscripts, Septuagint, Vulgate and Syriac; most Hebrew manuscripts *gateway facing the temple; it was one rod deep. ⁹Then he measured the portico of the gateway; it*

# Ezek 40:14
*ᵃ14* Septuagint; Hebrew *projecting wall*\
*ᵇ14* The meaning of the Hebrew for this verse is uncertain.

# Ezek 40:17
***The Outer Court (40:17–19)***

# Ezek 40:20
***The North Gate (40:20–23)***

# Ezek 40:24
***The South Gate (40:24–27)***

# Ezek 40:28
***Gates to the Inner Court (40:28–37)***

# Ezek 40:37
*ᵃ37* Septuagint (see also verses [31](vref:Ezek 40:31) and [34](vref:Ezek 40:34)); Hebrew *jambs*

# Ezek 40:38
***The Rooms for Preparing Sacrifices (40:38–43)***

# Ezek 40:44
***Rooms for the Priests (40:44–47)***\
\
*ᵃ44* Septuagint; Hebrew *were rooms for singers, which were*\
*ᵇ44* Septuagint; Hebrew *east*

# Ezek 40:48
***The Temple (40:48–41:26)***\
\
*ᵃ48* Septuagint; Hebrew *entrance was*

# Ezek 40:49
*ᵃ49* Septuagint; Hebrew *eleven*\
*ᵇ49* Hebrew; Septuagint *Ten steps led up to it*

# Ezek 41:1
*ᵃ1* The common cubit was about 1 1/2 feet (about 0.5 meter).\
*ᵇ1* One Hebrew manuscript and Septuagint; most Hebrew manuscripts *side, the width of the tent*

# Ezek 41:22
*ᵃ22* Septuagint; Hebrew *long*\
*ᵇ22* Septuagint; Hebrew *length*

# Ezek 42:1
***Rooms for the Priests (42:1–20)***

# Ezek 42:2
*ᵃ2* The common cubit was about 1 1/2 feet (about 0.5 meter).

# Ezek 42:4
*ᵃ4* Septuagint and Syriac; Hebrew *and one cubit*

# Ezek 42:10
*ᵃ10* Septuagint; Hebrew *Eastward*

# Ezek 42:16
*ᵃ16* See Septuagint of verse [17](vref:Ezek 42:17); Hebrew *rods*; also in verses [18](vref:Ezek 42:18) and [19](vref:Ezek 42:19).

# Ezek 42:17
*ᵃ17* Septuagint; Hebrew *rods*

# Ezek 43:1
***The Glory Returns to the Temple (43:1–12)***

# Ezek 43:3
*ᵃ3* Some Hebrew manuscripts and Vulgate; most Hebrew manuscripts *I*

# Ezek 43:7
*ᵃ7* Or *their spiritual adultery*; also in verse [9](vref:Ezek 43:9)\
*ᵇ7* Or *the corpses*; also in verse [9](vref:Ezek 43:9)

# Ezek 43:11
*ᵃ11* Some Hebrew manuscripts and Septuagint; most Hebrew manuscripts *regulations and its whole design*

# Ezek 43:13
***The Altar (43:13–27)***\
\
*ᵃ13* The common cubit was about 1 1/2 feet (about 0.5 meter).\
*ᵇ13* That is, about 3 inches (about 8 centimeters)\
*ᶜ13* That is, about 9 inches (about 22 centimeters)

# Ezek 43:27
*ᵃ27* Traditionally *peace offerings*

# Ezek 44:1
***The Prince, the Levites, the Priests (44:1–31)***

# Ezek 44:29
*ᵃ29* The Hebrew term refers to the irrevocable giving over of things or persons to the Lᴏʀᴅ.

# Ezek 45:1
***Division of the Land (45:1–12)***\
\
*ᵃ1* Septuagint (see also verses [3](vref:Ezek 45:3) and [5](vref:Ezek 45:5) and [48:9](vref:Ezek 48:9)); Hebrew *10,000*

# Ezek 45:3
*ᵃ3* That is, about 7 miles (about 12 kilometers)\
*ᵇ3* That is, about 3 miles (about 5 kilometers)

# Ezek 45:5
*ᵃ5* Septuagint; Hebrew *temple; they will have as their possession 20 rooms*

# Ezek 45:10
*ᵃ10* An ephah was a dry measure.\
*ᵇ10* A bath was a liquid measure.

# Ezek 45:11
*ᵃ11* A homer was a dry measure.

# Ezek 45:12
*ᵃ12* A shekel weighed about 2/5 ounce (about 11.5 grams).\
*ᵇ12* That is, 60 shekels; the common mina was 50 shekels.

# Ezek 45:13
***Offerings and Holy Days (45:13–46:24)***

# Ezek 45:15
*ᵃ15* Traditionally *peace offerings*; also in verse [17](vref:Ezek 45:17)

# Ezek 45:24
*ᵃ24* That is, probably about 4 quarts (about 4 liters)

# Ezek 46:2
*ᵃ2* Traditionally *peace offerings*; also in verse [12](vref:Ezek 46:12)

# Ezek 46:5
*ᵃ5* That is, probably about 3/5 bushel (about 22 liters)\
*ᵇ5* That is, probably about 4 quarts (about 4 liters)

# Ezek 46:22
*ᵃ22* The meaning of the Hebrew for this word is uncertain.

# Ezek 47:1
***The River From The Temple (47:1–12)***

# Ezek 47:3
*ᵃ3* That is, about 1,500 feet (about 450 meters)

# Ezek 47:8
*ᵃ8* Or *the Jordan Valley*\
*ᵇ8* That is, the Dead Sea

# Ezek 47:10
*ᵃ10* That is, the Mediterranean; also in verses [15](vref:Ezek 47:15), [19](vref:Ezek 47:19) and [20](vref:Ezek 47:20)

# Ezek 47:13
***The Boundaries of the Land (47:13–23)***

# Ezek 47:15
*ᵃ15* Or *past the entrance to*

# Ezek 47:16
*ᵃ15,16* See Septuagint and [Ezekiel 48:1](vref:Ezek 48:1); Hebrew *road to go into Zedad, ¹⁶Hamath, Berothah*

# Ezek 47:17
*ᵃ17* Hebrew *Enon*, a variant of *Enan*

# Ezek 47:18
*ᵃ18* Septuagint and Syriac; Hebrew *Israel. You will measure to the eastern sea*

# Ezek 47:20
*ᵃ20* Or *opposite the entrance to*

# Ezek 48:1
***The Division of the Land (48:1–29)***\
\
*ᵃ1* Or *to the entrance to*

# Ezek 48:8
*ᵃ8* That is, about 7 miles (about 12 kilometers)

# Ezek 48:9
*ᵃ9* That is, about 3 miles (about 5 kilometers)

# Ezek 48:28
*ᵃ28* That is, the Mediterranean

# Ezek 48:30
***The Gates of the City (48:30–35)***

# Dan 1:1
***Daniel’s Training in Babylon (1:1–21)***

# Dan 1:2
*ᵃ2* Hebrew *Shinar*

# Dan 1:4
*ᵃ4* Or *Chaldeans*

# Dan 1:10
*ᵃ10* The Hebrew for *your* and *you* in this verse is plural.

# Dan 2:1
***Nebuchadnezzar’s Dream (2:1–23)***

# Dan 2:2
*ᵃ2* Or *Chaldeans*; also in verses [4](vref:Dan 2:4), [5](vref:Dan 2:5) and [10](vref:Dan 2:10)

# Dan 2:3
*ᵃ3* Or *was*

# Dan 2:4
*ᵃ4* The text from here through chapter 7 is in Aramaic.

# Dan 2:24
***Daniel Interprets the Dream (2:24–49)***

# Dan 3:1
***The Image of Gold and the Fiery Furnace (3:1–30)***\
\
*ᵃ1* Aramaic *sixty cubits high and six cubits wide* (about 27 meters high and 2.7 meters wide)

# Dan 3:8
*ᵃ8* Or *Chaldeans*

# Dan 4:1
***Nebuchadnezzar’s Dream of a Tree (4:1–18)***

# Dan 4:7
*ᵃ7* Or *Chaldeans*

# Dan 4:13
*ᵃ13* Or *watchman*; also in verses [17](vref:Dan 4:17) and [23](vref:Dan 4:23)

# Dan 4:16
*ᵃ16* Or *years*; also in verses [23](vref:Dan 4:23), [25](vref:Dan 4:25) and [32](vref:Dan 4:32)

# Dan 4:19
***Daniel Interprets the Dream (4:19–27)***

# Dan 4:28
***The Dream Is Fulfilled (4:28–37)***

# Dan 5:1
***The Writing on the Wall (5:1–31)***

# Dan 5:2
*ᵃ2* Or *ancestor*; or *predecessor*; also in verses [11](vref:Dan 5:11), [13](vref:Dan 5:13) and [18](vref:Dan 5:18)

# Dan 5:7
*ᵃ7* Or *Chaldeans*; also in verse [11](vref:Dan 5:11)

# Dan 5:10
*ᵃ10* Or *queen mother*

# Dan 5:22
*ᵃ22* Or *descendant*; or *successor*

# Dan 5:25
*ᵃ25* Aramaic *UPARSIN* (that is, *AND PARSIN*)

# Dan 5:26
*ᵃ26* *Mene* can mean *numbered* or *mina* (a unit of money).

# Dan 5:27
*ᵃ27* *Tekel* can mean *weighed* or *shekel*.

# Dan 5:28
*ᵃ28* *Peres* (the singular of *Parsin*) can mean *divided* or *Persia* or *a half mina* or *a half shekel*.

# Dan 5:30
*ᵃ30* Or *Chaldeans*

# Dan 6:1
***Daniel in the Den of Lions (6:1–28)***

# Dan 6:28
*ᵃ28* Or *Darius, that is, the reign of Cyrus*

# Dan 7:1
***Daniel’s Dream of Four Beasts (7:1–14)***

# Dan 7:15
***The Interpretation of the Dream (7:15–28)***

# Dan 7:25
*ᵃ25* Or *for a year, two years and half a year*

# Dan 8:1
***Daniel’s Vision of a Ram and a Goat (8:1–14)***

# Dan 8:12
*ᵃ12* Or *rebellion, the armies*

# Dan 8:15
***The Interpretation of the Vision (8:15–27)***

# Dan 8:19
*ᵃ19* Or *because the end will be at the appointed time*

# Dan 9:1
***Daniel’s Prayer (9:1–19)***\
\
*ᵃ1* Hebrew *Ahasuerus*\
*ᵇ1* Or *Chaldean*

# Dan 9:20
***The Seventy “Sevens” (9:20–27)***

# Dan 9:24
*ᵃ24* Or *‘weeks’*; also in verses [25](vref:Dan 9:25) and [26](vref:Dan 9:26)\
*ᵇ24* Or *restrain*\
*ᶜ24* Or *Most Holy Place*; or *most holy One*

# Dan 9:25
*ᵃ25* Or *word*\
*ᵇ25* Or *an anointed one*; also in verse [26](vref:Dan 9:26)

# Dan 9:26
*ᵃ26* Or *off and will have no one*; or *off, but not for himself*

# Dan 9:27
*ᵃ27* Or *‘week’*\
*ᵇ27* Or *it*\
*ᶜ27* Or *And one who causes desolation will come upon the pinnacle of the abominable ⌞​temple​⌟, until the end that is decreed is poured out on the desolated ⌞​city​⌟*

# Dan 10:1
***Daniel’s Vision of a Man (10:1–11:1)***\
\
*ᵃ1* Or *true and burdensome*

# Dan 10:16
*ᵃ16* Most manuscripts of the Masoretic Text; one manuscript of the Masoretic Text, Dead Sea Scrolls and Septuagint *Then something that looked like a man’s hand*

# Dan 11:2
***The Kings of the South and the North (11:2–35)***

# Dan 11:6
*ᵃ6* Or *offspring*\
*ᵇ6* Or *child* (see Vulgate and Syriac)

# Dan 11:17
*ᵃ17* Or *but she*

# Dan 11:30
*ᵃ30* Hebrew *of Kittim*

# Dan 11:36
***The King Who Exalts Himself (11:36–45)***

# Dan 11:39
*ᵃ39* Or *land for a reward*

# Dan 11:45
*ᵃ45* Or *the sea and*

# Dan 12:1
***The End Times (12:1–13)***

# Dan 12:3
*ᵃ3* Or *who impart wisdom*

# Dan 12:7
*ᵃ7* Or *a year, two years and half a year*

# Hos 1:1
*ᵃ1* Hebrew *Joash*, a variant of *Jehoash*

# Hos 1:2
***Hosea’s Wife and Children (1:2–2:1)***

# Hos 1:6
*ᵃ6* *Lo-Ruhamah* means *not loved*.

# Hos 1:9
*ᵃ9* *Lo-Ammi* means *not my people*.

# Hos 2:2
***Israel Punished and Restored (2:2–23)***

# Hos 2:15
*ᵃ15* *Achor* means *trouble*.\
*ᵇ15* Or *respond*

# Hos 2:16
*ᵃ16* Hebrew *baal*

# Hos 2:19
*ᵃ19* Or *with*; also in verse [20](vref:Hos 2:20)\
*ᵇ19* Or *with*

# Hos 2:22
*ᵃ22* *Jezreel* means *God plants*.

# Hos 2:23
*ᵃ23* Hebrew *Lo-Ruhamah*\
*ᵇ23* Hebrew *Lo-Ammi*

# Hos 3:1
***Hosea’s Reconciliation With His Wife (3:1–5)***

# Hos 3:2
*ᵃ2* That is, about 6 ounces (about 170 grams)\
*ᵇ2* That is, probably about 10 bushels (about 330 liters)

# Hos 3:3
*ᵃ3* Or *wait for*

# Hos 4:1
***The Charge Against Israel (4:1–19)***

# Hos 4:2
*ᵃ2* That is, to pronounce a curse upon

# Hos 4:3
*ᵃ3* Or *dries up*

# Hos 4:7
*ᵃ7* Syriac and an ancient Hebrew scribal tradition; Masoretic Text *I will exchange*\
*ᵇ7* Masoretic Text; an ancient Hebrew scribal tradition *my*

# Hos 4:15
*ᵃ15* *Beth Aven* means *house of wickedness* (a name for Bethel, which means *house of God*).

# Hos 5:1
***Judgment Against Israel (5:1–15)***

# Hos 5:8
*ᵃ8* *Beth Aven* means *house of wickedness* (a name for Bethel, which means *house of God*).

# Hos 5:11
*ᵃ11* The meaning of the Hebrew for this word is uncertain.

# Hos 6:1
***Israel Unrepentant (6:1–7:16)***

# Hos 6:7
*ᵃ7* Or *As at Adam*; or *Like men*

# Hos 7:14
*ᵃ14* Most Hebrew manuscripts; some Hebrew manuscripts and Septuagint *They slash themselves*

# Hos 8:1
***Israel to Reap the Whirlwind (8:1–14)***

# Hos 9:1
***Punishment for Israel (9:1–10:15)***

# Hos 9:3
*ᵃ3* That is, ceremonially unclean

# Hos 9:8
*ᵃ8* Or *The prophet is the watchman over Ephraim, / the people of my God*

# Hos 10:5
*ᵃ5* *Beth Aven* means *house of wickedness* (a name for Bethel, which means *house of God*).

# Hos 10:6
*ᵃ6* Or *its counsel*

# Hos 10:8
*ᵃ8* Hebrew *aven*, a reference to Beth Aven (a derogatory name for Bethel)

# Hos 10:9
*ᵃ9* Or *there a stand was taken*

# Hos 11:1
***God’s Love for Israel (11:1–11)***

# Hos 11:2
*ᵃ2* Some Septuagint manuscripts; Hebrew *they*\
*ᵇ2* Septuagint; Hebrew *them*

# Hos 11:9
*ᵃ9* Or *come against any city*

# Hos 11:12
***Israel’s Sin (11:12–12:14)***

# Hos 12:2
*ᵃ2* *Jacob* means *he grasps the heel* (figuratively, *he deceives*).

# Hos 12:9
*ᵃ9* Or *God / ever since you were in*

# Hos 12:12
*ᵃ12* That is, Northwest Mesopotamia

# Hos 13:1
***The Lᴏʀᴅ’s Anger Against Israel (13:1–16)***

# Hos 13:2
*ᵃ2* Or *“Men who sacrifice / kiss*

# Hos 13:4
*ᵃ4* Or *God / ever since you were in*

# Hos 13:14
*ᵃ14* Hebrew *Sheol*

# Hos 14:1
***Repentance to Bring Blessing (14:1–9)***

# Hos 14:2
*ᵃ2* Or *offer our lips as sacrifices of bulls*

# Hos 14:8
*ᵃ8* Or *What more has Ephraim*

# Joel 1:2
***An Invasion of Locusts (1:2–12)***

# Joel 1:4
*ᵃ4* The precise meaning of the four Hebrew words used here for locusts is uncertain.

# Joel 1:8
*ᵃ8* Or *young woman*\
*ᵇ8* Or *betrothed*

# Joel 1:10
*ᵃ10* Or *ground mourns*

# Joel 1:13
***A Call to Repentance (1:13–20)***

# Joel 1:15
*ᵃ15* Hebrew *Shaddai*

# Joel 1:17
*ᵃ17* The meaning of the Hebrew for this word is uncertain.

# Joel 2:1
***An Army of Locusts (2:1–11)***

# Joel 2:12
***Rend Your Heart (2:12–17)***

# Joel 2:18
***The Lᴏʀᴅ’s Answer (2:18–27)***

# Joel 2:19
*ᵃ18,19* Or *Lᴏʀᴅ was jealous . . . / and took pity . . . / ¹⁹The Lᴏʀᴅ replied*

# Joel 2:20
*ᵃ20* That is, the Dead Sea\
*ᵇ20* That is, the Mediterranean\
*ᶜ20* Or *rise. / Surely it has done great things.”*

# Joel 2:23
*ᵃ23* Or */ the teacher for righteousness:*

# Joel 2:25
*ᵃ25* The precise meaning of the four Hebrew words used here for locusts is uncertain.

# Joel 2:28
***The Day of the Lᴏʀᴅ (2:28–32)***

# Joel 3:1
***The Nations Judged (3:1–16)***

# Joel 3:2
*ᵃ2* *Jehoshaphat* means *the Lᴏʀᴅ judges*; also in verse [12](vref:Joel 3:12).

# Joel 3:17
***Blessings for God’s People (3:17–21)***

# Joel 3:18
*ᵃ18* Or *Valley of Shittim*

# Amos 1:1
*ᵃ1* Hebrew *Joash*, a variant of *Jehoash*

# Amos 1:2
*ᵃ2* Or *shepherds mourn*

# Amos 1:3
***Judgment on Israel’s Neighbors (1:3–2:5)***

# Amos 1:5
*ᵃ5* Or *the inhabitants of*\
*ᵇ5* *Aven* means *wickedness*.

# Amos 1:8
*ᵃ8* Or *inhabitants*

# Amos 1:11
*ᵃ11* Or *sword / and destroyed his allies*

# Amos 1:15
*ᵃ15* Or */ Molech*; Hebrew *malcam*

# Amos 2:2
*ᵃ2* Or *of her cities*

# Amos 2:4
*ᵃ4* Or *by lies*\
*ᵇ4* Or *lies*

# Amos 2:6
***Judgment on Israel (2:6–16)***

# Amos 3:1
***Witnesses Summoned Against Israel (3:1–15)***

# Amos 3:12
*ᵃ12* The meaning of the Hebrew for this line is uncertain.

# Amos 4:1
***Israel Has Not Returned to God (4:1–13)***

# Amos 4:3
*ᵃ3* Masoretic Text; with a different word division of the Hebrew (see Septuagint) *out, O mountain of oppression*

# Amos 4:4
*ᵃ4* Or *tithes on the third day*

# Amos 4:6
*ᵃ6* Hebrew *you cleanness of teeth*

# Amos 4:11
*ᵃ11* Hebrew *God*

# Amos 5:1
***A Lament and Call to Repentance (5:1–17)***

# Amos 5:5
*ᵃ5* Or *grief*; or *wickedness*; Hebrew *aven*, a reference to Beth Aven (a derogatory name for Bethel)

# Amos 5:18
***The Day of the Lᴏʀᴅ (5:18–27)***

# Amos 5:22
*ᵃ22* Traditionally *peace offerings*

# Amos 5:26
*ᵃ26* Or *lifted up Sakkuth your king / and Kaiwan your idols, / your star-gods*; Septuagint *lifted up the shrine of Molech / and the star of your god Rephan, / their idols*

# Amos 6:1
***Woe to the Complacent (6:1–7)***

# Amos 6:8
***The Lᴏʀᴅ Abhors the Pride of Israel (6:8–14)***

# Amos 6:13
*ᵃ13* *Lo Debar* means *nothing*.\
*ᵇ13* *Karnaim* means *horns*; *horn* here symbolizes strength.

# Amos 6:14
*ᵃ14* Or *from the entrance to*

# Amos 7:1
***Locusts, Fire and a Plumb Line (7:1–9)***

# Amos 7:10
***Amos and Amaziah (7:10–17)***

# Amos 7:17
*ᵃ17* Hebrew *an unclean*

# Amos 8:1
***A Basket of Ripe Fruit (8:1–14)***

# Amos 8:3
*ᵃ3* Or *“the temple singers will wail*

# Amos 8:14
*ᵃ14* Or *by Ashima*; or *by the idol*\
*ᵇ14* Or *power*

# Amos 9:1
***Israel to Be Destroyed (9:1–10)***

# Amos 9:2
*ᵃ2* Hebrew *to Sheol*

# Amos 9:6
*ᵃ6* The meaning of the Hebrew for this phrase is uncertain.\
*ᵇ6* The meaning of the Hebrew for this word is uncertain.

# Amos 9:7
*ᵃ7* That is, people from the upper Nile region\
*ᵇ7* That is, Crete

# Amos 9:11
***Israel’s Restoration (9:11–15)***

# Amos 9:12
*ᵃ12* Hebrew; Septuagint *so that the remnant of men / and all the nations that bear my name may seek ⌞​the Lord​⌟*

# Amos 9:14
*ᵃ14* Or *will restore the fortunes of my*

# Obad 3
*ᵃ3* Or *of Sela*

# Obad 7
*ᵃ7* The meaning of the Hebrew for this clause is uncertain.

# Obad 21
*ᵃ21* Or *from*

# Jonah 1:1
***Jonah Flees From the Lᴏʀᴅ (1:1–17)***

# Jonah 2:1
***Jonah’s Prayer (2:1–10)***

# Jonah 2:2
*ᵃ2* Hebrew *Sheol*

# Jonah 2:5
*ᵃ5* Or *waters were at my throat*

# Jonah 3:1
***Jonah Goes to Nineveh (3:1–10)***

# Jonah 4:1
***Jonah’s Anger at the Lᴏʀᴅ’s Compassion (4:1–11)***

# Mic 1:3
***Judgment Against Samaria and Jerusalem (1:3–7)***

# Mic 1:8
***Weeping and Mourning (1:8–16)***

# Mic 1:9
*ᵃ9* Or *He*

# Mic 1:10
*ᵃ10* *Gath* sounds like the Hebrew for *tell*.\
*ᵇ10* Hebrew; Septuagint may suggest *not in Acco*. The Hebrew for *in Acco* sounds like the Hebrew for *weep*.\
*ᶜ10* *Beth Ophrah* means *house of dust*.

# Mic 1:11
*ᵃ11* *Shaphir* means *pleasant*.\
*ᵇ11* *Zaanan* sounds like the Hebrew for *come out*.

# Mic 1:12
*ᵃ12* *Maroth* sounds like the Hebrew for *bitter*.

# Mic 1:13
*ᵃ13* *Lachish* sounds like the Hebrew for *team*.

# Mic 1:14
*ᵃ14* *Aczib* means *deception*.

# Mic 1:15
*ᵃ15* *Mareshah* sounds like the Hebrew for *conqueror*.

# Mic 2:1
***Man’s Plans and God’s (2:1–5)***

# Mic 2:6
***False Prophets (2:6–11)***

# Mic 2:12
***Deliverance Promised (2:12–13)***

# Mic 3:1
***Leaders and Prophets Rebuked (3:1–12)***

# Mic 4:1
***The Mountain of the Lᴏʀᴅ (4:1–5)***

# Mic 4:6
***The Lᴏʀᴅ’s Plan (4:6–13)***

# Mic 4:8
*ᵃ8* Or *hill*

# Mic 5:1
***A Promised Ruler From Bethlehem (5:1–5a)***\
\
*ᵃ1* Or *Strengthen your walls, O walled city*

# Mic 5:2
*ᵃ2* Or *rulers*\
*ᵇ2* Hebrew *goings out*\
*ᶜ2* Or *from days of eternity*

# Mic 5:5
***Deliverance and Destruction (5:5b–15)***

# Mic 5:6
*ᵃ6* Or *crush*\
*ᵇ6* Or *Nimrod in its gates*

# Mic 5:14
*ᵃ14* That is, symbols of the goddess Asherah

# Mic 6:1
***The Lᴏʀᴅ’s Case Against Israel (6:1–8)***

# Mic 6:9
***Israel’s Guilt and Punishment (6:9–16)***\
\
*ᵃ9* The meaning of the Hebrew for this line is uncertain.

# Mic 6:10
*ᵃ10* An ephah was a dry measure.

# Mic 6:14
*ᵃ14* The meaning of the Hebrew for this word is uncertain.

# Mic 6:16
*ᵃ16* Septuagint; Hebrew *scorn due my people*

# Mic 7:1
***Israel’s Misery (7:1–7)***

# Mic 7:8
***Israel Will Rise (7:8–13)***

# Mic 7:14
***Prayer and Praise (7:14–20)***\
\
*ᵃ14* Or *in the middle of Carmel*

# Nah 1:2
***The Lᴏʀᴅ’s Anger Against Nineveh (1:2–15)***

# Nah 1:9
*ᵃ9* Or *What do you foes plot against the Lᴏʀᴅ? / He*

# Nah 1:10
*ᵃ10* The meaning of the Hebrew for this verse is uncertain.

# Nah 2:1
***Nineveh to Fall (2:1–13)***

# Nah 2:3
*ᵃ3* Hebrew; Septuagint and Syriac */ the horsemen rush to and fro*

# Nah 2:7
*ᵃ7* The meaning of the Hebrew for this word is uncertain.

# Nah 3:1
***Woe to Nineveh (3:1–19)***

# Nah 3:8
*ᵃ8* Hebrew *No Amon*

# Nah 3:9
*ᵃ9* That is, the upper Nile region

# Nah 3:18
*ᵃ18* Or *rulers*

# Hab 1:2
***Habakkuk’s Complaint (1:2–4)***

# Hab 1:5
***The Lᴏʀᴅ’s Answer (1:5–11)***

# Hab 1:6
*ᵃ6* Or *Chaldeans*

# Hab 1:9
*ᵃ9* The meaning of the Hebrew for this word is uncertain.

# Hab 1:12
***Habakkuk’s Second Complaint (1:12–2:1)***

# Hab 2:1
*ᵃ1* Or *and what to answer when I am rebuked*

# Hab 2:2
***The Lᴏʀᴅ’s Answer (2:2–20)***\
\
*ᵃ2* Or *so that whoever reads it*

# Hab 2:3
*ᵃ3* Or *Though he linger, wait for him; / he*

# Hab 2:4
*ᵃ4* Or *faithfulness*

# Hab 2:5
*ᵃ5* Hebrew *Sheol*

# Hab 2:7
*ᵃ7* Or *creditors*

# Hab 2:16
*ᵃ16* Masoretic Text; Dead Sea Scrolls, Aquila, Vulgate and Syriac (see also Septuagint) *and stagger*

# Hab 3:1
***Habakkuk’s Prayer (3:1–19)***\
\
*ᵃ1* Probably a literary or musical term

# Hab 3:3
*ᵃ3* A word of uncertain meaning; possibly a musical term; also in verses [9](vref:Hab 3:9) and [13](vref:Hab 3:13)

# Zeph 1:2
***Warning of Coming Destruction (1:2–3)***

# Zeph 1:3
*ᵃ3* The meaning of the Hebrew for this line is uncertain.

# Zeph 1:4
***Against Judah (1:4–13)***

# Zeph 1:5
*ᵃ5* Hebrew *Malcam*, that is, Milcom

# Zeph 1:9
*ᵃ9* See [1 Samuel 5:5](vref:1 Sam 5:5).

# Zeph 1:11
*ᵃ11* Or *the Mortar*\
*ᵇ11* Or *in*

# Zeph 1:14
***The Great Day of the Lᴏʀᴅ (1:14–2:3)***

# Zeph 2:4
***Against Philistia (2:4–7)***

# Zeph 2:6
*ᵃ6* The meaning of the Hebrew for this word is uncertain.

# Zeph 2:7
*ᵃ7* Or *will bring back their captives*

# Zeph 2:8
***Against Moab and Ammon (2:8–11)***

# Zeph 2:12
***Against Cush (2:12)***\
\
*ᵃ12* That is, people from the upper Nile region

# Zeph 2:13
***Against Assyria (2:13–15)***

# Zeph 3:1
***The Future of Jerusalem (3:1–20)***

# Zeph 3:8
*ᵃ8* Septuagint and Syriac; Hebrew *will rise up to plunder*

# Zeph 3:10
*ᵃ10* That is, the upper Nile region

# Zeph 3:18
*ᵃ18* Or *“I will gather you who mourn for the appointed feasts; / your reproach is a burden to you*

# Zeph 3:20
*ᵃ20* Or *I bring back your captives*

# Hag 1:1
***A Call to Build the House of the Lᴏʀᴅ (1:1–15)***\
\
*ᵃ1* A variant of *Jeshua*; here and elsewhere in Haggai

# Hag 2:1
***The Promised Glory of the New House (2:1–9)***

# Hag 2:10
***Blessings for a Defiled People (2:10–19)***

# Hag 2:15
*ᵃ15* Or *to the days past*

# Hag 2:20
***Zerubbabel the Lᴏʀᴅ’s Signet Ring (2:20–23)***

# Zech 1:1
***A Call to Return to the Lᴏʀᴅ (1:1–6)***

# Zech 1:7
***The Man Among the Myrtle Trees (1:7–17)***

# Zech 1:18
***Four Horns and Four Craftsmen (1:18–21)***

# Zech 2:1
***A Man With a Measuring Line (2:1–13)***

# Zech 2:9
*ᵃ8,9* Or *says after . . . eye: ⁹“I . . . plunder them.”*

# Zech 3:1
***Clean Garments for the High Priest (3:1–10)***\
\
*ᵃ1* A variant of *Jeshua*; here and elsewhere in Zechariah\
*ᵇ1* *Satan* means *accuser*.

# Zech 3:9
*ᵃ9* Or *facets*

# Zech 4:1
***The Gold Lampstand and the Two Olive Trees (4:1–14)***

# Zech 4:7
*ᵃ7* Or *Who*

# Zech 4:14
*ᵃ14* Or *two who bring oil and*

# Zech 5:1
***The Flying Scroll (5:1–4)***

# Zech 5:2
*ᵃ2* Hebrew *twenty cubits long and ten cubits wide* (about 9 meters long and 4.5 meters wide)

# Zech 5:5
***The Woman in a Basket (5:5–11)***

# Zech 5:6
*ᵃ6* Hebrew *an ephah*; also in verses [7–11](vref:Zech 5:7-11)\
*ᵇ6* Or *appearance*

# Zech 5:11
*ᵃ11* Hebrew *Shinar*

# Zech 6:1
***Four Chariots (6:1–8)***

# Zech 6:5
*ᵃ5* Or *winds*

# Zech 6:6
*ᵃ6* Or *horses after them*

# Zech 6:8
*ᵃ8* Or *spirit*

# Zech 6:9
***A Crown for Joshua (6:9–15)***

# Zech 6:14
*ᵃ14* Syriac; Hebrew *Helem*\
*ᵇ14* Or *and the gracious one, the*

# Zech 7:1
***Justice and Mercy, Not Fasting (7:1–14)***

# Zech 8:1
***The Lᴏʀᴅ Promises to Bless Jerusalem (8:1–23)***

# Zech 9:1
***Judgment on Israel’s Enemies (9:1–8)***\
\
*ᵃ1* Or *Damascus. / For the eye of the Lᴏʀᴅ is on all mankind, / as well as on the tribes of Israel,*

# Zech 9:9
***The Coming of Zion’s King (9:9–13)***\
\
*ᵃ9* Or *King*

# Zech 9:10
*ᵃ10* That is, the Euphrates\
*ᵇ10* Or *the end of the land*

# Zech 9:14
***The Lᴏʀᴅ Will Appear (9:14–17)***

# Zech 9:15
*ᵃ15* Or *bowl, / like*

# Zech 10:1
***The Lᴏʀᴅ Will Care for Judah (10:1–11:3)***

# Zech 10:5
*ᵃ4,5* Or *ruler, all of them together. / ⁵They*

# Zech 11:4
***Two Shepherds (11:4–17)***

# Zech 12:1
***Jerusalem’s Enemies to Be Destroyed (12:1–9)***

# Zech 12:10
***Mourning for the One They Pierced (12:10–14)***\
\
*ᵃ10* Or *the Spirit*\
*ᵇ10* Or *to*

# Zech 13:1
***Cleansing From Sin (13:1–6)***

# Zech 13:5
*ᵃ5* Or *farmer; a man sold me in my youth*

# Zech 13:6
*ᵃ6* Or *wounds between your hands*

# Zech 13:7
***The Shepherd Struck, the Sheep Scattered (13:7–9)***

# Zech 14:1
***The Lᴏʀᴅ Comes and Reigns (14:1–21)***

# Zech 14:5
*ᵃ5* Or *⁵My mountain valley will be blocked and will extend to Azel. It will be blocked as it was blocked because of the earthquake*

# Zech 14:8
*ᵃ8* That is, the Dead Sea\
*ᵇ8* That is, the Mediterranean

# Zech 14:18
*ᵃ18* Or *part, then the Lᴏʀᴅ*

# Zech 14:21
*ᵃ21* Or *merchant*

# Mal 1:1
*ᵃ1* *Malachi* means *my messenger*.

# Mal 1:2
***Jacob Loved, Esau Hated (1:2–5)***

# Mal 1:6
***Blemished Sacrifices (1:6–14)***

# Mal 2:1
***Admonition for the Priests (2:1–9)***

# Mal 2:3
*ᵃ3* Or *cut off* (see Septuagint)\
*ᵇ3* Or *will blight your grain*

# Mal 2:10
***Judah Unfaithful (2:10–16)***\
\
*ᵃ10* Or *father*

# Mal 2:12
*ᵃ12* Or *¹²May the Lᴏʀᴅ cut off from the tents of Jacob anyone who gives testimony in behalf of the man who does this*

# Mal 2:15
*ᵃ15* Or *¹⁵But the one ⌞​who is our father​⌟ did not do this, not as long as life remained in him. And what was he seeking? An offspring from God*

# Mal 2:16
*ᵃ16* Or *his wife*

# Mal 2:17
***The Day of Judgment (2:17–3:5)***

# Mal 3:6
***Robbing God (3:6–18)***

# Mal 3:17
*ᵃ17* Or *Almighty, “my treasured possession, in the day when I act*

# Mal 4:1
***The Day of the Lᴏʀᴅ (4:1–6)***

# Matt 1:1
***The Genealogy of Jesus (1:1–17)***

# Matt 1:11
*ᵃ11* That is, Jehoiachin; also in verse [12](vref:Matt 1:12)

# Matt 1:17
*ᵃ17* Or *Messiah*. “The Christ” (Greek) and “the Messiah” (Hebrew) both mean “the Anointed One.”

# Matt 1:18
***The Birth of Jesus Christ (1:18–25)***

# Matt 1:21
*ᵃ21* *Jesus* is the Greek form of *Joshua*, which means *the Lᴏʀᴅ saves*.

# Matt 1:23
*ᵃ23* [Isaiah 7:14](vref:Isa 7:14)

# Matt 2:1
***The Visit of the Magi (2:1–12)***\
\
*ᵃ1* Traditionally *Wise Men*

# Matt 2:2
*ᵃ2* Or *star when it rose*

# Matt 2:4
*ᵃ4* Or *Messiah*

# Matt 2:6
*ᵃ6* [Micah 5:2](vref:Mic 5:2)

# Matt 2:9
*ᵃ9* Or *seen when it rose*

# Matt 2:13
***The Escape to Egypt (2:13–18)***

# Matt 2:15
*ᵃ15* [Hosea 11:1](vref:Hos 11:1)

# Matt 2:18
*ᵃ18* [Jer. 31:15](vref:Jer 31:15)

# Matt 2:19
***The Return to Nazareth (2:19–23)***

# Matt 3:1
***John the Baptist Prepares the Way (3:1–12)***

# Matt 3:3
*ᵃ3* [Isaiah 40:3](vref:Isa 40:3)

# Matt 3:11
*ᵃ11* Or *in*

# Matt 3:13
***The Baptism of Jesus (3:13–17)***

# Matt 4:1
***The Temptation of Jesus (4:1–11)***

# Matt 4:4
*ᵃ4* [Deut. 8:3](vref:Deut 8:3)

# Matt 4:6
*ᵃ6* [Psalm 91:11,12](vref:Ps 91:11-12)

# Matt 4:7
*ᵃ7* [Deut. 6:16](vref:Deut 6:16)

# Matt 4:10
*ᵃ10* [Deut. 6:13](vref:Deut 6:13)

# Matt 4:12
***Jesus Begins to Preach (4:12–17)***

# Matt 4:16
*ᵃ16* [Isaiah 9:1,2](vref:Isa 9:1-2)

# Matt 4:18
***The Calling of the First Disciples (4:18–22)***

# Matt 4:23
***Jesus Heals the Sick (4:23–25)***

# Matt 4:25
*ᵃ25* That is, the Ten Cities

# Matt 5:1
***The Beatitudes (5:1–12)***

# Matt 5:13
***Salt and Light (5:13–16)***

# Matt 5:17
***The Fulfillment of the Law (5:17–20)***

# Matt 5:21
***Murder (5:21–26)***\
\
*ᵃ21* [Exodus 20:13](vref:Exod 20:13)

# Matt 5:22
*ᵃ22* Some manuscripts *brother without cause*\
*ᵇ22* An Aramaic term of contempt

# Matt 5:26
*ᵃ26* Greek *kodrantes*

# Matt 5:27
***Adultery (5:27–30)***\
\
*ᵃ27* [Exodus 20:14](vref:Exod 20:14)

# Matt 5:31
***Divorce (5:31–32)***\
\
*ᵃ31* [Deut. 24:1](vref:Deut 24:1)

# Matt 5:33
***Oaths (5:33–37)***

# Matt 5:38
***An Eye for an Eye (5:38–42)***\
\
*ᵃ38* [Exodus 21:24](vref:Exod 21:24); [Lev. 24:20](vref:Lev 24:20) and [Deut. 19:21](vref:Deut 19:21)

# Matt 5:43
***Love for Enemies (5:43–48)***\
\
*ᵃ43* [Lev. 19:18](vref:Lev 19:18)

# Matt 5:44
*ᵃ44* Some late manuscripts *enemies, bless those who curse you, do good to those who hate you*

# Matt 6:1
***Giving to the Needy (6:1–4)***

# Matt 6:5
***Prayer (6:5–15)***

# Matt 6:13
*ᵃ13* Or *from evil*; some late manuscripts *one, / for yours is the kingdom and the power and the glory forever. Amen.*

# Matt 6:16
***Fasting (6:16–18)***

# Matt 6:19
***Treasures in Heaven (6:19–24)***

# Matt 6:25
***Do Not Worry (6:25–34)***

# Matt 6:27
*ᵃ27* Or *single cubit to his height*

# Matt 7:1
***Judging Others (7:1–6)***

# Matt 7:7
***Ask, Seek, Knock (7:7–12)***

# Matt 7:13
***The Narrow and Wide Gates (7:13–14)***

# Matt 7:15
***A Tree and Its Fruit (7:15–23)***

# Matt 7:24
***The Wise and Foolish Builders (7:24–29)***

# Matt 8:1
***The Man With Leprosy (8:1–4)***

# Matt 8:2
*ᵃ2* The Greek word was used for various diseases affecting the skin—not necessarily leprosy.

# Matt 8:3
*ᵃ3* Greek *made clean*

# Matt 8:5
***The Faith of the Centurion (8:5–13)***

# Matt 8:14
***Jesus Heals Many (8:14–17)***

# Matt 8:17
*ᵃ17* [Isaiah 53:4](vref:Isa 53:4)

# Matt 8:18
***The Cost of Following Jesus (8:18–22)***

# Matt 8:23
***Jesus Calms the Storm (8:23–27)***

# Matt 8:28
***The Healing of Two Demon-possessed Men (8:28–34)***\
\
*ᵃ28* Some manuscripts *Gergesenes*; others *Gerasenes*

# Matt 9:1
***Jesus Heals a Paralytic (9:1–8)***

# Matt 9:9
***The Calling of Matthew (9:9–13)***

# Matt 9:13
*ᵃ13* [Hosea 6:6](vref:Hos 6:6)

# Matt 9:14
***Jesus Questioned About Fasting (9:14–17)***

# Matt 9:18
***A Dead Girl and a Sick Woman (9:18–26)***

# Matt 9:27
***Jesus Heals the Blind and Mute (9:27–34)***

# Matt 9:35
***The Workers Are Few (9:35–38)***

# Matt 10:1
***Jesus Sends Out the Twelve (10:1–42)***\
\
*ᵃ1* Greek *unclean*

# Matt 10:8
*ᵃ8* The Greek word was used for various diseases affecting the skin—not necessarily leprosy.

# Matt 10:25
*ᵃ25* Greek *Beezeboul* or *Beelzeboul*

# Matt 10:29
*ᵃ29* Greek *an assarion*

# Matt 10:36
*ᵃ36* [Micah 7:6](vref:Mic 7:6)

# Matt 11:1
***Jesus and John the Baptist (11:1–19)***\
\
*ᵃ1* Greek *in their towns*

# Matt 11:5
*ᵃ5* The Greek word was used for various diseases affecting the skin—not necessarily leprosy.

# Matt 11:10
*ᵃ10* [Mal. 3:1](vref:Mal 3:1)

# Matt 11:20
***Woe on Unrepentant Cities (11:20–24)***

# Matt 11:23
*ᵃ23* Greek *Hades*

# Matt 11:25
***Rest for the Weary (11:25–30)***

# Matt 12:1
***Lord of the Sabbath (12:1–14)***

# Matt 12:6
*ᵃ6* Or *something*; also in verses [41](vref:Matt 12:41) and [42](vref:Matt 12:42)

# Matt 12:7
*ᵃ7* [Hosea 6:6](vref:Hos 6:6)

# Matt 12:15
***God’s Chosen Servant (12:15–21)***

# Matt 12:21
*ᵃ21* [Isaiah 42:1–4](vref:Isa 42:1-4)

# Matt 12:22
***Jesus and Beelzebub (12:22–37)***

# Matt 12:24
*ᵃ24* Greek *Beezeboul* or *Beelzeboul*; also in verse [27](vref:Matt 12:27)

# Matt 12:38
***The Sign of Jonah (12:38–45)***

# Matt 12:41
*ᵃ41* Or *something*; also in verse [42](vref:Matt 12:42)

# Matt 12:43
*ᵃ43* Greek *unclean*

# Matt 12:46
***Jesus’ Mother and Brothers (12:46–50)***

# Matt 12:47
*ᵃ47* Some manuscripts do not have verse [47](vref:Matt 12:47).

# Matt 13:1
***The Parable of the Sower (13:1–23)***

# Matt 13:15
*ᵃ15* [Isaiah 6:9,10](vref:Isa 6:9-10)

# Matt 13:24
***The Parable of the Weeds (13:24–30)***

# Matt 13:31
***The Parable of the Mustard Seed and the Yeast (13:31–35)***

# Matt 13:33
*ᵃ33* Greek *three satas* (probably about 1/2 bushel or 22 liters)

# Matt 13:35
*ᵃ35* [Psalm 78:2](vref:Ps 78:2)

# Matt 13:36
***The Parable of the Weeds Explained (13:36–43)***

# Matt 13:44
***The Parable of the Hidden Treasure and the Pearl (13:44–46)***

# Matt 13:47
***The Parable of the Net (13:47–52)***

# Matt 13:53
***A Prophet Without Honor (13:53–58)***

# Matt 14:1
***John the Baptist Beheaded (14:1–12)***

# Matt 14:13
***Jesus Feeds the Five Thousand (14:13–21)***

# Matt 14:22
***Jesus Walks on the Water (14:22–36)***

# Matt 14:24
*ᵃ24* Greek *many stadia*

# Matt 15:1
***Clean and Unclean (15:1–20)***

# Matt 15:4
*ᵃ4* [Exodus 20:12](vref:Exod 20:12); [Deut. 5:16](vref:Deut 5:16)\
*ᵇ4* [Exodus 21:17](vref:Exod 21:17); [Lev. 20:9](vref:Lev 20:9)

# Matt 15:6
*ᵃ6* Some manuscripts *father or his mother*

# Matt 15:9
*ᵃ9* [Isaiah 29:13](vref:Isa 29:13)

# Matt 15:14
*ᵃ14* Some manuscripts *guides of the blind*

# Matt 15:21
***The Faith of the Canaanite Woman (15:21–28)***

# Matt 15:29
***Jesus Feeds the Four Thousand (15:29–39)***

# Matt 16:1
***The Demand for a Sign (16:1–4)***

# Matt 16:2
*ᵃ2* Some early manuscripts do not have the rest of verse [2](vref:Matt 16:2) and all of verse [3](vref:Matt 16:3).

# Matt 16:5
***The Yeast of the Pharisees and Sadducees (16:5–12)***

# Matt 16:13
***Peter’s Confession of Christ (16:13–20)***

# Matt 16:16
*ᵃ16* Or *Messiah*; also in verse [20](vref:Matt 16:20)

# Matt 16:18
*ᵃ18* *Peter* means *rock*.\
*ᵇ18* Or *hell*\
*ᶜ18* Or *not prove stronger than it*

# Matt 16:19
*ᵃ19* Or *have been*

# Matt 16:21
***Jesus Predicts His Death (16:21–28)***

# Matt 16:25
*ᵃ25* The Greek word means either *life* or *soul*; also in verse [26](vref:Matt 16:26).

# Matt 17:1
***The Transfiguration (17:1–13)***

# Matt 17:14
***The Healing of a Boy With a Demon (17:14–23)***

# Matt 17:20
*ᵃ20* Some manuscripts *you. ²¹But this kind does not go out except by prayer and fasting.*

# Matt 17:24
***The Temple Tax (17:24–27)***\
\
*ᵃ24* Greek *the two drachmas*

# Matt 18:1
***The Greatest in the Kingdom of Heaven (18:1–9)***

# Matt 18:10
***The Parable of the Lost Sheep (18:10–14)***\
\
*ᵃ10* Some manuscripts *heaven. ¹¹The Son of Man came to save what was lost.*

# Matt 18:15
***A Brother Who Sins Against You (18:15–20)***\
\
*ᵃ15* Some manuscripts do not have *against you*.

# Matt 18:16
*ᵃ16* [Deut. 19:15](vref:Deut 19:15)

# Matt 18:18
*ᵃ18* Or *have been*

# Matt 18:21
***The Parable of the Unmerciful Servant (18:21–35)***

# Matt 18:22
*ᵃ22* Or *seventy times seven*

# Matt 18:24
*ᵃ24* That is, millions of dollars

# Matt 18:28
*ᵃ28* That is, a few dollars

# Matt 19:1
***Divorce (19:1–12)***

# Matt 19:4
*ᵃ4* [Gen. 1:27](vref:Gen 1:27)

# Matt 19:5
*ᵃ5* [Gen. 2:24](vref:Gen 2:24)

# Matt 19:12
*ᵃ12* Or *have made themselves eunuchs*

# Matt 19:13
***The Little Children and Jesus (19:13–15)***

# Matt 19:16
***The Rich Young Man (19:16–30)***

# Matt 19:19
*ᵃ19* [Exodus 20:12–16](vref:Exod 20:12-16); [Deut. 5:16–20](vref:Deut 5:16-20)\
*ᵇ19* [Lev. 19:18](vref:Lev 19:18)

# Matt 19:29
*ᵃ29* Some manuscripts *mother or wife*

# Matt 20:1
***The Parable of the Workers in the Vineyard (20:1–16)***

# Matt 20:17
***Jesus Again Predicts His Death (20:17–19)***

# Matt 20:20
***A Mother’s Request (20:20–28)***

# Matt 20:29
***Two Blind Men Receive Sight (20:29–34)***

# Matt 21:1
***The Triumphal Entry (21:1–11)***

# Matt 21:5
*ᵃ5* [Zech. 9:9](vref:Zech 9:9)

# Matt 21:9
*ᵃ9* A Hebrew expression meaning “Save!” which became an exclamation of praise; also in verse [15](vref:Matt 21:15)\
*ᵇ9* [Psalm 118:26](vref:Ps 118:26)

# Matt 21:12
***Jesus at the Temple (21:12–17)***

# Matt 21:13
*ᵃ13* [Isaiah 56:7](vref:Isa 56:7)\
*ᵇ13* [Jer. 7:11](vref:Jer 7:11)

# Matt 21:16
*ᵃ16* [Psalm 8:2](vref:Ps 8:2)

# Matt 21:18
***The Fig Tree Withers (21:18–22)***

# Matt 21:23
***The Authority of Jesus Questioned (21:23–27)***

# Matt 21:28
***The Parable of the Two Sons (21:28–32)***

# Matt 21:33
***The Parable of the Tenants (21:33–46)***

# Matt 21:42
*ᵃ42* Or *cornerstone*\
*ᵇ42* [Psalm 118:22,23](vref:Ps 118:22-23)

# Matt 21:44
*ᵃ44* Some manuscripts do not have verse [44](vref:Matt 21:44).

# Matt 22:1
***The Parable of the Wedding Banquet (22:1–14)***

# Matt 22:15
***Paying Taxes to Caesar (22:15–22)***

# Matt 22:23
***Marriage at the Resurrection (22:23–33)***

# Matt 22:32
*ᵃ32* [Exodus 3:6](vref:Exod 3:6)

# Matt 22:34
***The Greatest Commandment (22:34–40)***

# Matt 22:37
*ᵃ37* [Deut. 6:5](vref:Deut 6:5)

# Matt 22:39
*ᵃ39* [Lev. 19:18](vref:Lev 19:18)

# Matt 22:41
***Whose Son Is the Christ? (22:41–46)***

# Matt 22:42
*ᵃ42* Or *Messiah*

# Matt 22:44
*ᵃ44* [Psalm 110:1](vref:Ps 110:1)

# Matt 23:1
***Seven Woes (23:1–39)***

# Matt 23:5
*ᵃ5* That is, boxes containing Scripture verses, worn on forehead and arm

# Matt 23:10
*ᵃ10* Or *Messiah*

# Matt 23:13
*ᵃ13* Some manuscripts *to. ¹⁴Woe to you, teachers of the law and Pharisees, you hypocrites! You devour widows’ houses and for a show make lengthy prayers. Therefore you will be punished more severely.*

# Matt 23:39
*ᵃ39* [Psalm 118:26](vref:Ps 118:26)

# Matt 24:1
***Signs of the End of the Age (24:1–35)***

# Matt 24:5
*ᵃ5* Or *Messiah*; also in verse [23](vref:Matt 24:23)

# Matt 24:15
*ᵃ15* [Daniel 9:27](vref:Dan 9:27); [11:31](vref:Dan 11:31); [12:11](vref:Dan 12:11)

# Matt 24:29
*ᵃ29* [Isaiah 13:10](vref:Isa 13:10); [34:4](vref:Isa 34:4)

# Matt 24:33
*ᵃ33* Or *he*

# Matt 24:34
*ᵃ34* Or *race*

# Matt 24:36
***The Day and Hour Unknown (24:36–51)***\
\
*ᵃ36* Some manuscripts do not have *nor the Son*.

# Matt 25:1
***The Parable of the Ten Virgins (25:1–13)***

# Matt 25:14
***The Parable of the Talents (25:14–30)***

# Matt 25:15
*ᵃ15* A talent was worth more than a thousand dollars.

# Matt 25:31
***The Sheep and the Goats (25:31–46)***

# Matt 26:1
***The Plot Against Jesus (26:1–5)***

# Matt 26:6
***Jesus Anointed at Bethany (26:6–13)***

# Matt 26:14
***Judas Agrees to Betray Jesus (26:14–16)***

# Matt 26:17
***The Lord’s Supper (26:17–30)***

# Matt 26:25
*ᵃ25* Or *“You yourself have said it”*

# Matt 26:28
*ᵃ28* Some manuscripts *the new*

# Matt 26:31
***Jesus Predicts Peter’s Denial (26:31–35)***\
\
*ᵃ31* [Zech. 13:7](vref:Zech 13:7)

# Matt 26:36
***Gethsemane (26:36–46)***

# Matt 26:47
***Jesus Arrested (26:47–56)***

# Matt 26:50
*ᵃ50* Or *“Friend, why have you come?”*

# Matt 26:57
***Before the Sanhedrin (26:57–68)***

# Matt 26:63
*ᵃ63* Or *Messiah*; also in verse [68](vref:Matt 26:68)

# Matt 26:69
***Peter Disowns Jesus (26:69–75)***

# Matt 27:1
***Judas Hangs Himself (27:1–10)***

# Matt 27:10
*ᵃ10* See [Zech. 11:12,13](vref:Zech 11:12-13); [Jer. 19:1–13](vref:Jer 19:1-13); [32:6–9](vref:Jer 32:6-9).

# Matt 27:11
***Jesus Before Pilate (27:11–26)***

# Matt 27:27
***The Soldiers Mock Jesus (27:27–31)***

# Matt 27:32
***The Crucifixion (27:32–44)***

# Matt 27:35
*ᵃ35* A few late manuscripts *lots that the word spoken by the prophet might be fulfilled: “They divided my garments among themselves and cast lots for my clothing.”* ([Psalm 22:18](vref:Ps 22:18))

# Matt 27:45
***The Death of Jesus (27:45–56)***

# Matt 27:46
*ᵃ46* Some manuscripts *Eli, Eli*\
*ᵇ46* [Psalm 22:1](vref:Ps 22:1)

# Matt 27:54
*ᵃ54* Or *a son*

# Matt 27:57
***The Burial of Jesus (27:57–61)***

# Matt 27:62
***The Guard at the Tomb (27:62–66)***

# Matt 28:1
***The Resurrection (28:1–10)***

# Matt 28:11
***The Guards’ Report (28:11–15)***

# Matt 28:16
***The Great Commission (28:16–20)***

# Matt 28:19
*ᵃ19* Or *into*; see [Acts 8:16](vref:Acts 8:16); [19:5](vref:Acts 19:5); [Rom. 6:3](vref:Rom 6:3); [1 Cor. 1:13](vref:1 Cor 1:13); [10:2](vref:1 Cor 10:2) and [Gal. 3:27](vref:Gal 3:27).

# Mark 1:1
***John the Baptist Prepares the Way (1:1–8)***\
\
*ᵃ1* Some manuscripts do not have *the Son of God*.

# Mark 1:2
*ᵃ2* [Mal. 3:1](vref:Mal 3:1)

# Mark 1:3
*ᵃ3* [Isaiah 40:3](vref:Isa 40:3)

# Mark 1:8
*ᵃ8* Or *in*

# Mark 1:9
***The Baptism and Temptation of Jesus (1:9–13)***

# Mark 1:14
***The Calling of the First Disciples (1:14–20)***

# Mark 1:21
***Jesus Drives Out an Evil Spirit (1:21–28)***

# Mark 1:23
*ᵃ23* Greek *unclean*; also in verses [26](vref:Mark 1:26) and [27](vref:Mark 1:27)

# Mark 1:29
***Jesus Heals Many (1:29–34)***

# Mark 1:35
***Jesus Prays in a Solitary Place (1:35–39)***

# Mark 1:40
***A Man With Leprosy (1:40–45)***\
\
*ᵃ40* The Greek word was used for various diseases affecting the skin—not necessarily leprosy.

# Mark 2:1
***Jesus Heals a Paralytic (2:1–12)***

# Mark 2:13
***The Calling of Levi (2:13–17)***

# Mark 2:18
***Jesus Questioned About Fasting (2:18–22)***

# Mark 2:23
***Lord of the Sabbath (2:23–3:6)***

# Mark 3:7
***Crowds Follow Jesus (3:7–12)***

# Mark 3:11
*ᵃ11* Greek *unclean*; also in verse [30](vref:Mark 3:30)

# Mark 3:13
***The Appointing of the Twelve Apostles (3:13–19)***

# Mark 3:14
*ᵃ14* Some manuscripts do not have *designating them apostles*.

# Mark 3:20
***Jesus and Beelzebub (3:20–30)***

# Mark 3:22
*ᵃ22* Greek *Beezeboul* or *Beelzeboul*

# Mark 3:31
***Jesus’ Mother and Brothers (3:31–35)***

# Mark 4:1
***The Parable of the Sower (4:1–20)***

# Mark 4:12
*ᵃ12* [Isaiah 6:9,10](vref:Isa 6:9-10)

# Mark 4:21
***A Lamp on a Stand (4:21–25)***

# Mark 4:26
***The Parable of the Growing Seed (4:26–29)***

# Mark 4:30
***The Parable of the Mustard Seed (4:30–34)***

# Mark 4:35
***Jesus Calms the Storm (4:35–41)***

# Mark 5:1
***The Healing of a Demon-possessed Man (5:1–20)***\
\
*ᵃ1* Some manuscripts *Gadarenes*; other manuscripts *Gergesenes*

# Mark 5:2
*ᵃ2* Greek *unclean*; also in verses [8](vref:Mark 5:8) and [13](vref:Mark 5:13)

# Mark 5:20
*ᵃ20* That is, the Ten Cities

# Mark 5:21
***A Dead Girl and a Sick Woman (5:21–43)***

# Mark 6:1
***A Prophet Without Honor (6:1–6a)***

# Mark 6:3
*ᵃ3* Greek *Joses*, a variant of *Joseph*

# Mark 6:6
***Jesus Sends Out the Twelve (6:6b–13)***

# Mark 6:7
*ᵃ7* Greek *unclean*

# Mark 6:14
***John the Baptist Beheaded (6:14–29)***\
\
*ᵃ14* Some early manuscripts *He was saying*

# Mark 6:20
*ᵃ20* Some early manuscripts *he did many things*

# Mark 6:30
***Jesus Feeds the Five Thousand (6:30–44)***

# Mark 6:37
*ᵃ37* Greek *take two hundred denarii*

# Mark 6:45
***Jesus Walks on the Water (6:45–56)***

# Mark 7:1
***Clean and Unclean (7:1–23)***

# Mark 7:4
*ᵃ4* Some early manuscripts *pitchers, kettles and dining couches*

# Mark 7:7
*ᵃ6,7* [Isaiah 29:13](vref:Isa 29:13)

# Mark 7:9
*ᵃ9* Some manuscripts *set up*

# Mark 7:10
*ᵃ10* [Exodus 20:12](vref:Exod 20:12); [Deut. 5:16](vref:Deut 5:16)\
*ᵇ10* [Exodus 21:17](vref:Exod 21:17); [Lev. 20:9](vref:Lev 20:9)

# Mark 7:15
*ᵃ15* Some early manuscripts *‘unclean.’ ¹⁶If anyone has ears to hear, let him hear.*

# Mark 7:24
***The Faith of a Syrophoenician Woman (7:24–30)***\
\
*ᵃ24* Many early manuscripts *Tyre and Sidon*

# Mark 7:25
*ᵃ25* Greek *unclean*

# Mark 7:31
***The Healing of a Deaf and Mute Man (7:31–37)***\
\
*ᵃ31* That is, the Ten Cities

# Mark 8:1
***Jesus Feeds the Four Thousand (8:1–13)***

# Mark 8:14
***The Yeast of the Pharisees and Herod (8:14–21)***

# Mark 8:22
***The Healing of a Blind Man at Bethsaida (8:22–26)***

# Mark 8:26
*ᵃ26* Some manuscripts *Don’t go and tell anyone in the village*

# Mark 8:27
***Peter’s Confession of Christ (8:27–30)***

# Mark 8:29
*ᵃ29* Or *Messiah*. “The Christ” (Greek) and “the Messiah” (Hebrew) both mean “the Anointed One.”

# Mark 8:31
***Jesus Predicts His Death (8:31–9:1)***

# Mark 8:35
*ᵃ35* The Greek word means either *life* or *soul*; also in verse [36](vref:Mark 8:36).

# Mark 9:2
***The Transfiguration (9:2–13)***

# Mark 9:14
***The Healing of a Boy With an Evil Spirit (9:14–32)***

# Mark 9:25
*ᵃ25* Greek *unclean*

# Mark 9:29
*ᵃ29* Some manuscripts *prayer and fasting*

# Mark 9:33
***Who Is the Greatest? (9:33–37)***

# Mark 9:38
***Whoever Is Not Against Us Is for Us (9:38–41)***

# Mark 9:42
***Causing to Sin (9:42–50)***

# Mark 9:43
*ᵃ43* Some manuscripts *out, ⁴⁴where / “ ‘their worm does not die, / and the fire is not quenched.’*

# Mark 9:45
*ᵃ45* Some manuscripts *hell, ⁴⁶where / “ ‘their worm does not die, / and the fire is not quenched.’*

# Mark 9:48
*ᵃ48* [Isaiah 66:24](vref:Isa 66:24)

# Mark 10:1
***Divorce (10:1–12)***

# Mark 10:6
*ᵃ6* [Gen. 1:27](vref:Gen 1:27)

# Mark 10:7
*ᵃ7* Some early manuscripts do not have *and be united to his wife*.

# Mark 10:8
*ᵃ8* [Gen. 2:24](vref:Gen 2:24)

# Mark 10:13
***The Little Children and Jesus (10:13–16)***

# Mark 10:17
***The Rich Young Man (10:17–31)***

# Mark 10:19
*ᵃ19* [Exodus 20:12–16](vref:Exod 20:12-16); [Deut. 5:16–20](vref:Deut 5:16-20)

# Mark 10:24
*ᵃ24* Some manuscripts *is for those who trust in riches*

# Mark 10:32
***Jesus Again Predicts His Death (10:32–34)***

# Mark 10:35
***The Request of James and John (10:35–45)***

# Mark 10:46
***Blind Bartimaeus Receives His Sight (10:46–52)***

# Mark 11:1
***The Triumphal Entry (11:1–11)***

# Mark 11:9
*ᵃ9* A Hebrew expression meaning “Save!” which became an exclamation of praise; also in verse [10](vref:Mark 11:10)\
*ᵇ9* [Psalm 118:25,26](vref:Ps 118:25-26)

# Mark 11:12
***Jesus Clears the Temple (11:12–19)***

# Mark 11:17
*ᵃ17* [Isaiah 56:7](vref:Isa 56:7)\
*ᵇ17* [Jer. 7:11](vref:Jer 7:11)

# Mark 11:19
*ᵃ19* Some early manuscripts *he*

# Mark 11:20
***The Withered Fig Tree (11:20–26)***

# Mark 11:22
*ᵃ22* Some early manuscripts *If you have*

# Mark 11:25
*ᵃ25* Some manuscripts *sins. ²⁶But if you do not forgive, neither will your Father who is in heaven forgive your sins.*

# Mark 11:27
***The Authority of Jesus Questioned (11:27–33)***

# Mark 12:1
***The Parable of the Tenants (12:1–12)***

# Mark 12:10
*ᵃ10* Or *cornerstone*

# Mark 12:11
*ᵃ11* [Psalm 118:22,23](vref:Ps 118:22-23)

# Mark 12:13
***Paying Taxes to Caesar (12:13–17)***

# Mark 12:18
***Marriage at the Resurrection (12:18–27)***

# Mark 12:23
*ᵃ23* Some manuscripts *resurrection, when men rise from the dead,*

# Mark 12:26
*ᵃ26* [Exodus 3:6](vref:Exod 3:6)

# Mark 12:28
***The Greatest Commandment (12:28–34)***

# Mark 12:29
*ᵃ29* Or *the Lord our God is one Lord*

# Mark 12:30
*ᵃ30* [Deut. 6:4,5](vref:Deut 6:4-5)

# Mark 12:31
*ᵃ31* [Lev. 19:18](vref:Lev 19:18)

# Mark 12:35
***Whose Son Is the Christ? (12:35–40)***\
\
*ᵃ35* Or *Messiah*

# Mark 12:36
*ᵃ36* [Psalm 110:1](vref:Ps 110:1)

# Mark 12:41
***The Widow’s Offering (12:41–44)***

# Mark 12:42
*ᵃ42* Greek *two lepta*\
*ᵇ42* Greek *kodrantes*

# Mark 13:1
***Signs of the End of the Age (13:1–31)***

# Mark 13:14
*ᵃ14* [Daniel 9:27](vref:Dan 9:27); [11:31](vref:Dan 11:31); [12:11](vref:Dan 12:11)\
*ᵇ14* Or *he*; also in verse [29](vref:Mark 13:29)

# Mark 13:21
*ᵃ21* Or *Messiah*

# Mark 13:25
*ᵃ25* [Isaiah 13:10](vref:Isa 13:10); [34:4](vref:Isa 34:4)

# Mark 13:30
*ᵃ30* Or *race*

# Mark 13:32
***The Day and Hour Unknown (13:32–37)***

# Mark 13:33
*ᵃ33* Some manuscripts *alert and pray*

# Mark 14:1
***Jesus Anointed at Bethany (14:1–11)***

# Mark 14:5
*ᵃ5* Greek *than three hundred denarii*

# Mark 14:12
***The Lord’s Supper (14:12–26)***

# Mark 14:24
*ᵃ24* Some manuscripts *the new*

# Mark 14:27
***Jesus Predicts Peter’s Denial (14:27–31)***\
\
*ᵃ27* [Zech. 13:7](vref:Zech 13:7)

# Mark 14:30
*ᵃ30* Some early manuscripts do not have *twice*.

# Mark 14:32
***Gethsemane (14:32–42)***

# Mark 14:36
*ᵃ36* Aramaic for *Father*

# Mark 14:43
***Jesus Arrested (14:43–52)***

# Mark 14:53
***Before the Sanhedrin (14:53–65)***

# Mark 14:61
*ᵃ61* Or *Messiah*

# Mark 14:66
***Peter Disowns Jesus (14:66–72)***

# Mark 14:68
*ᵃ68* Some early manuscripts *entryway and the rooster crowed*

# Mark 14:72
*ᵃ72* Some early manuscripts do not have *the second time*.\
*ᵇ72* Some early manuscripts do not have *twice*.

# Mark 15:1
***Jesus Before Pilate (15:1–15)***

# Mark 15:16
***The Soldiers Mock Jesus (15:16–20)***

# Mark 15:21
***The Crucifixion (15:21–32)***

# Mark 15:27
*ᵃ27* Some manuscripts *left, ²⁸and the scripture was fulfilled which says, “He was counted with the lawless ones”* ([Isaiah 53:12](vref:Isa 53:12))

# Mark 15:32
*ᵃ32* Or *Messiah*

# Mark 15:33
***The Death of Jesus (15:33–41)***

# Mark 15:34
*ᵃ34* [Psalm 22:1](vref:Ps 22:1)

# Mark 15:39
*ᵃ39* Some manuscripts do not have *heard his cry and*.\
*ᵇ39* Or *a son*

# Mark 15:42
***The Burial of Jesus (15:42–47)***

# Mark 16:1
***The Resurrection (16:1–20)***

# Mark 16:8
*ᵃ8* The most reliable early manuscripts and other ancient witnesses do not have [Mark 16:9–20](vref:Mark 16:9-20).

# Luke 1:1
***Introduction (1:1–4)***\
\
*ᵃ1* Or *been surely believed*

# Luke 1:5
***The Birth of John the Baptist Foretold (1:5–25)***

# Luke 1:15
*ᵃ15* Or *from his mother’s womb*

# Luke 1:26
***The Birth of Jesus Foretold (1:26–38)***

# Luke 1:35
*ᵃ35* Or *So the child to be born will be called holy,*

# Luke 1:39
***Mary Visits Elizabeth (1:39–45)***

# Luke 1:46
***Mary’s Song (1:46–56)***

# Luke 1:57
***The Birth of John the Baptist (1:57–66)***

# Luke 1:67
***Zechariah’s Song (1:67–80)***

# Luke 1:69
*ᵃ69* *Horn* here symbolizes strength.

# Luke 2:1
***The Birth of Jesus (2:1–7)***

# Luke 2:8
***The Shepherds and the Angels (2:8–20)***

# Luke 2:11
*ᵃ11* Or *Messiah*. “The Christ” (Greek) and “the Messiah” (Hebrew) both mean “the Anointed One”; also in verse [26](vref:Luke 2:26).

# Luke 2:21
***Jesus Presented in the Temple (2:21–40)***

# Luke 2:23
*ᵃ23* [Exodus 13:2](vref:Exod 13:2),[12](vref:Exod 13:12)

# Luke 2:24
*ᵃ24* [Lev. 12:8](vref:Lev 12:8)

# Luke 2:29
*ᵃ29* Or *promised, / now dismiss*

# Luke 2:37
*ᵃ37* Or *widow for eighty-four years*

# Luke 2:41
***The Boy Jesus at the Temple (2:41–52)***

# Luke 3:1
***John the Baptist Prepares the Way (3:1–20)***

# Luke 3:6
*ᵃ6* [Isaiah 40:3–5](vref:Isa 40:3-5)

# Luke 3:15
*ᵃ15* Or *Messiah*

# Luke 3:16
*ᵃ16* Or *in*

# Luke 3:21
***The Baptism and Genealogy of Jesus (3:21–38)***

# Luke 3:32
*ᵃ32* Some early manuscripts *Sala*

# Luke 3:33
*ᵃ33* Some manuscripts *Amminadab, the son of Admin, the son of Arni*; other manuscripts vary widely.

# Luke 4:1
***The Temptation of Jesus (4:1–13)***

# Luke 4:4
*ᵃ4* [Deut. 8:3](vref:Deut 8:3)

# Luke 4:8
*ᵃ8* [Deut. 6:13](vref:Deut 6:13)

# Luke 4:11
*ᵃ11* [Psalm 91:11,12](vref:Ps 91:11-12)

# Luke 4:12
*ᵃ12* [Deut. 6:16](vref:Deut 6:16)

# Luke 4:14
***Jesus Rejected at Nazareth (4:14–30)***

# Luke 4:19
*ᵃ19* [Isaiah 61:1,2](vref:Isa 61:1-2)

# Luke 4:27
*ᵃ27* The Greek word was used for various diseases affecting the skin—not necessarily leprosy.

# Luke 4:31
***Jesus Drives Out an Evil Spirit (4:31–37)***

# Luke 4:33
*ᵃ33* Greek *unclean*; also in verse [36](vref:Luke 4:36)

# Luke 4:38
***Jesus Heals Many (4:38–44)***

# Luke 4:41
*ᵃ41* Or *Messiah*

# Luke 4:44
*ᵃ44* Or *the land of the Jews*; some manuscripts *Galilee*

# Luke 5:1
***The Calling of the First Disciples (5:1–11)***\
\
*ᵃ1* That is, Sea of Galilee

# Luke 5:4
*ᵃ4* The Greek verb is plural.

# Luke 5:12
***The Man With Leprosy (5:12–16)***\
\
*ᵃ12* The Greek word was used for various diseases affecting the skin—not necessarily leprosy.

# Luke 5:17
***Jesus Heals a Paralytic (5:17–26)***

# Luke 5:27
***The Calling of Levi (5:27–32)***

# Luke 5:33
***Jesus Questioned About Fasting (5:33–39)***

# Luke 6:1
***Lord of the Sabbath (6:1–11)***

# Luke 6:12
***The Twelve Apostles (6:12–16)***

# Luke 6:17
***Blessings and Woes (6:17–26)***

# Luke 6:18
*ᵃ18* Greek *unclean*

# Luke 6:27
***Love for Enemies (6:27–36)***

# Luke 6:37
***Judging Others (6:37–42)***

# Luke 6:43
***A Tree and Its Fruit (6:43–45)***

# Luke 6:46
***The Wise and Foolish Builders (6:46–49)***

# Luke 7:1
***The Faith of the Centurion (7:1–10)***

# Luke 7:11
***Jesus Raises a Widow’s Son (7:11–17)***

# Luke 7:17
*ᵃ17* Or *the land of the Jews*

# Luke 7:18
***Jesus and John the Baptist (7:18–35)***

# Luke 7:22
*ᵃ22* The Greek word was used for various diseases affecting the skin—not necessarily leprosy.

# Luke 7:27
*ᵃ27* [Mal. 3:1](vref:Mal 3:1)

# Luke 7:36
***Jesus Anointed by a Sinful Woman (7:36–50)***

# Luke 7:41
*ᵃ41* A denarius was a coin worth about a day’s wages.

# Luke 8:1
***The Parable of the Sower (8:1–15)***

# Luke 8:10
*ᵃ10* [Isaiah 6:9](vref:Isa 6:9)

# Luke 8:16
***A Lamp on a Stand (8:16–18)***

# Luke 8:19
***Jesus’ Mother and Brothers (8:19–21)***

# Luke 8:22
***Jesus Calms the Storm (8:22–25)***

# Luke 8:26
***The Healing of a Demon-possessed Man (8:26–39)***\
\
*ᵃ26* Some manuscripts *Gadarenes*; other manuscripts *Gergesenes*; also in verse [37](vref:Luke 8:37)

# Luke 8:29
*ᵃ29* Greek *unclean*

# Luke 8:40
***A Dead Girl and a Sick Woman (8:40–56)***

# Luke 8:43
*ᵃ43* Many manuscripts *years, and she had spent all she had on doctors*

# Luke 9:1
***Jesus Sends Out the Twelve (9:1–9)***

# Luke 9:10
***Jesus Feeds the Five Thousand (9:10–17)***

# Luke 9:18
***Peter’s Confession of Christ (9:18–27)***

# Luke 9:20
*ᵃ20* Or *Messiah*

# Luke 9:28
***The Transfiguration (9:28–36)***

# Luke 9:37
***The Healing of a Boy With an Evil Spirit (9:37–45)***

# Luke 9:42
*ᵃ42* Greek *unclean*

# Luke 9:46
***Who Will Be the Greatest? (9:46–50)***

# Luke 9:51
***Samaritan Opposition (9:51–56)***

# Luke 9:54
*ᵃ54* Some manuscripts *them, even as Elijah did*

# Luke 9:56
*ᵃ55,56* Some manuscripts *them. And he said, “You do not know what kind of spirit you are of, for the Son of Man did not come to destroy men’s lives, but to save them.” ⁵⁶And*

# Luke 9:57
***The Cost of Following Jesus (9:57–62)***

# Luke 10:1
***Jesus Sends Out the Seventy-two (10:1–24)***\
\
*ᵃ1* Some manuscripts *seventy*; also in verse [17](vref:Luke 10:17)

# Luke 10:15
*ᵃ15* Greek *Hades*

# Luke 10:25
***The Parable of the Good Samaritan (10:25–37)***

# Luke 10:27
*ᵃ27* [Deut. 6:5](vref:Deut 6:5)\
*ᵇ27* [Lev. 19:18](vref:Lev 19:18)

# Luke 10:35
*ᵃ35* Greek *two denarii*

# Luke 10:38
***At the Home of Martha and Mary (10:38–42)***

# Luke 10:42
*ᵃ42* Some manuscripts *but few things are needed—or only one*

# Luke 11:1
***Jesus’ Teaching on Prayer (11:1–13)***

# Luke 11:2
*ᵃ2* Some manuscripts *Our Father in heaven*\
*ᵇ2* Some manuscripts *come. May your will be done on earth as it is in heaven.*

# Luke 11:4
*ᵃ4* Greek *everyone who is indebted to us*\
*ᵇ4* Some manuscripts *temptation but deliver us from the evil one*

# Luke 11:8
*ᵃ8* Or *persistence*

# Luke 11:11
*ᵃ11* Some manuscripts *for bread, will give him a stone; or if he asks for*

# Luke 11:14
***Jesus and Beelzebub (11:14–28)***

# Luke 11:15
*ᵃ15* Greek *Beezeboul* or *Beelzeboul*; also in verses [18](vref:Luke 11:18) and [19](vref:Luke 11:19)

# Luke 11:24
*ᵃ24* Greek *unclean*

# Luke 11:29
***The Sign of Jonah (11:29–32)***

# Luke 11:31
*ᵃ31* Or *something*; also in verse [32](vref:Luke 11:32)

# Luke 11:33
***The Lamp of the Body (11:33–36)***

# Luke 11:37
***Six Woes (11:37–54)***

# Luke 11:41
*ᵃ41* Or *what you have*

# Luke 12:1
***Warnings and Encouragements (12:1–12)***

# Luke 12:6
*ᵃ6* Greek *two assaria*

# Luke 12:13
***The Parable of the Rich Fool (12:13–21)***

# Luke 12:22
***Do Not Worry (12:22–34)***

# Luke 12:25
*ᵃ25* Or *single cubit to his height*

# Luke 12:35
***Watchfulness (12:35–48)***

# Luke 12:49
***Not Peace but Division (12:49–53)***

# Luke 12:54
***Interpreting the Times (12:54–59)***

# Luke 12:59
*ᵃ59* Greek *lepton*

# Luke 13:1
***Repent or Perish (13:1–9)***

# Luke 13:10
***A Crippled Woman Healed on the Sabbath (13:10–17)***

# Luke 13:18
***The Parables of the Mustard Seed and the Yeast (13:18–21)***

# Luke 13:21
*ᵃ21* Greek *three satas* (probably about 1/2 bushel or 22 liters)

# Luke 13:22
***The Narrow Door (13:22–30)***

# Luke 13:31
***Jesus’ Sorrow for Jerusalem (13:31–35)***

# Luke 13:35
*ᵃ35* [Psalm 118:26](vref:Ps 118:26)

# Luke 14:1
***Jesus at a Pharisee’s House (14:1–14)***

# Luke 14:5
*ᵃ5* Some manuscripts *donkey*

# Luke 14:15
***The Parable of the Great Banquet (14:15–24)***

# Luke 14:25
***The Cost of Being a Disciple (14:25–35)***

# Luke 15:1
***The Parable of the Lost Sheep (15:1–7)***

# Luke 15:8
***The Parable of the Lost Coin (15:8–10)***\
\
*ᵃ8* Greek *ten drachmas*, each worth about a day’s wages

# Luke 15:11
***The Parable of the Lost Son (15:11–32)***

# Luke 15:21
*ᵃ21* Some early manuscripts *son. Make me like one of your hired men.*

# Luke 16:1
***The Parable of the Shrewd Manager (16:1–15)***

# Luke 16:6
*ᵃ6* Greek *one hundred batous* (probably about 3 kiloliters)

# Luke 16:7
*ᵃ7* Greek *one hundred korous* (probably about 35 kiloliters)

# Luke 16:16
***Additional Teachings (16:16–18)***

# Luke 16:19
***The Rich Man and Lazarus (16:19–31)***

# Luke 16:23
*ᵃ23* Greek *Hades*

# Luke 17:1
***Sin, Faith, Duty (17:1–10)***

# Luke 17:11
***Ten Healed of Leprosy (17:11–19)***

# Luke 17:12
*ᵃ12* The Greek word was used for various diseases affecting the skin—not necessarily leprosy.

# Luke 17:20
***The Coming of the Kingdom of God (17:20–37)***

# Luke 17:21
*ᵃ21* Or *among*

# Luke 17:24
*ᵃ24* Some manuscripts do not have *in his day*.

# Luke 17:35
*ᵃ35* Some manuscripts *left. ³⁶Two men will be in the field; one will be taken and the other left.*

# Luke 18:1
***The Parable of the Persistent Widow (18:1–8)***

# Luke 18:9
***The Parable of the Pharisee and the Tax Collector (18:9–14)***

# Luke 18:11
*ᵃ11* Or *to*

# Luke 18:15
***The Little Children and Jesus (18:15–17)***

# Luke 18:18
***The Rich Ruler (18:18–30)***

# Luke 18:20
*ᵃ20* [Exodus 20:12–16](vref:Exod 20:12-16); [Deut. 5:16–20](vref:Deut 5:16-20)

# Luke 18:31
***Jesus Again Predicts His Death (18:31–34)***

# Luke 18:35
***A Blind Beggar Receives His Sight (18:35–43)***

# Luke 19:1
***Zacchaeus the Tax Collector (19:1–10)***

# Luke 19:11
***The Parable of the Ten Minas (19:11–27)***

# Luke 19:13
*ᵃ13* A mina was about three months’ wages.

# Luke 19:28
***The Triumphal Entry (19:28–44)***

# Luke 19:38
*ᵃ38* [Psalm 118:26](vref:Ps 118:26)

# Luke 19:45
***Jesus at the Temple (19:45–48)***

# Luke 19:46
*ᵃ46* [Isaiah 56:7](vref:Isa 56:7)\
*ᵇ46* [Jer. 7:11](vref:Jer 7:11)

# Luke 20:1
***The Authority of Jesus Questioned (20:1–8)***

# Luke 20:9
***The Parable of the Tenants (20:9–19)***

# Luke 20:17
*ᵃ17* Or *cornerstone*\
*ᵇ17* [Psalm 118:22](vref:Ps 118:22)

# Luke 20:20
***Paying Taxes to Caesar (20:20–26)***

# Luke 20:27
***The Resurrection and Marriage (20:27–40)***

# Luke 20:37
*ᵃ37* [Exodus 3:6](vref:Exod 3:6)

# Luke 20:41
***Whose Son Is the Christ? (20:41–47)***\
\
*ᵃ41* Or *Messiah*

# Luke 20:43
*ᵃ43* [Psalm 110:1](vref:Ps 110:1)

# Luke 21:1
***The Widow’s Offering (21:1–4)***

# Luke 21:2
*ᵃ2* Greek *two lepta*

# Luke 21:5
***Signs of the End of the Age (21:5–38)***

# Luke 21:32
*ᵃ32* Or *race*

# Luke 22:1
***Judas Agrees to Betray Jesus (22:1–6)***

# Luke 22:7
***The Last Supper (22:7–38)***

# Luke 22:31
*ᵃ31* The Greek is plural.

# Luke 22:37
*ᵃ37* [Isaiah 53:12](vref:Isa 53:12)

# Luke 22:39
***Jesus Prays on the Mount of Olives (22:39–46)***

# Luke 22:44
*ᵃ44* Some early manuscripts do not have verses [43](vref:Luke 22:43) and [44](vref:Luke 22:44).

# Luke 22:47
***Jesus Arrested (22:47–53)***

# Luke 22:54
***Peter Disowns Jesus (22:54–62)***

# Luke 22:63
***The Guards Mock Jesus (22:63–65)***

# Luke 22:66
***Jesus Before Pilate and Herod (22:66–23:25)***

# Luke 22:67
*ᵃ67* Or *Messiah*

# Luke 23:2
*ᵃ2* Or *Messiah*; also in verses [35](vref:Luke 23:35) and [39](vref:Luke 23:39)

# Luke 23:5
*ᵃ5* Or *over the land of the Jews*

# Luke 23:16
*ᵃ16* Some manuscripts *him.” ¹⁷Now he was obliged to release one man to them at the Feast.*

# Luke 23:26
***The Crucifixion (23:26–43)***

# Luke 23:30
*ᵃ30* [Hosea 10:8](vref:Hos 10:8)

# Luke 23:34
*ᵃ34* Some early manuscripts do not have this sentence.

# Luke 23:42
*ᵃ42* Some manuscripts *come with your kingly power*

# Luke 23:44
***Jesus’ Death (23:44–49)***

# Luke 23:50
***Jesus’ Burial (23:50–56)***

# Luke 24:1
***The Resurrection (24:1–12)***

# Luke 24:13
***On the Road to Emmaus (24:13–35)***\
\
*ᵃ13* Greek *sixty stadia* (about 11 kilometers)

# Luke 24:26
*ᵃ26* Or *Messiah*; also in verse [46](vref:Luke 24:46)

# Luke 24:36
***Jesus Appears to the Disciples (24:36–49)***

# Luke 24:50
***The Ascension (24:50–53)***

# John 1:1
***The Word Became Flesh (1:1–18)***

# John 1:5
*ᵃ5* Or *darkness, and the darkness has not overcome*

# John 1:9
*ᵃ9* Or *This was the true light that gives light to every man who comes into the world*

# John 1:13
*ᵃ13* Greek *of bloods*

# John 1:14
*ᵃ14* Or *the Only Begotten*

# John 1:18
*ᵃ18* Or *the Only Begotten*\
*ᵇ18* Some manuscripts *but the only* (or *only begotten*) *Son*

# John 1:19
***John the Baptist Denies Being the Christ (1:19–28)***

# John 1:20
*ᵃ20* Or *Messiah*. “The Christ” (Greek) and “the Messiah” (Hebrew) both mean “the Anointed One”; also in verse [25](vref:John 1:25).

# John 1:23
*ᵃ23* [Isaiah 40:3](vref:Isa 40:3)

# John 1:26
*ᵃ26* Or *in*; also in verses [31](vref:John 1:31) and [33](vref:John 1:33)

# John 1:29
***Jesus the Lamb of God (1:29–34)***

# John 1:35
***Jesus’ First Disciples (1:35–42)***

# John 1:42
*ᵃ42* Both *Cephas* (Aramaic) and *Peter* (Greek) mean *rock*.

# John 1:43
***Jesus Calls Philip and Nathanael (1:43–51)***

# John 1:50
*ᵃ50* Or *Do you believe . . . ?*

# John 1:51
*ᵃ51* The Greek is plural.

# John 2:1
***Jesus Changes Water to Wine (2:1–11)***

# John 2:6
*ᵃ6* Greek *two to three metretes* (probably about 75 to 115 liters)

# John 2:12
***Jesus Clears the Temple (2:12–25)***

# John 2:17
*ᵃ17* [Psalm 69:9](vref:Ps 69:9)

# John 2:23
*ᵃ23* Or *and believed in him*

# John 3:1
***Jesus Teaches Nicodemus (3:1–21)***

# John 3:3
*ᵃ3* Or *born from above*; also in verse [7](vref:John 3:7)

# John 3:6
*ᵃ6* Or *but spirit*

# John 3:7
*ᵃ7* The Greek is plural.

# John 3:13
*ᵃ13* Some manuscripts *Man, who is in heaven*

# John 3:15
*ᵃ15* Or *believes may have eternal life in him*

# John 3:16
*ᵃ16* Or *his only begotten Son*

# John 3:18
*ᵃ18* Or *God’s only begotten Son*

# John 3:21
*ᵃ21* Some interpreters end the quotation after verse [15](vref:John 3:15).

# John 3:22
***John the Baptist’s Testimony About Jesus (3:22–36)***

# John 3:25
*ᵃ25* Some manuscripts *and certain Jews*

# John 3:28
*ᵃ28* Or *Messiah*

# John 3:34
*ᵃ34* Greek *he*

# John 3:36
*ᵃ36* Some interpreters end the quotation after verse [30](vref:John 3:30).

# John 4:1
***Jesus Talks With a Samaritan Woman (4:1–26)***

# John 4:9
*ᵃ9* Or *do not use dishes Samaritans have used*

# John 4:27
***The Disciples Rejoin Jesus (4:27–38)***

# John 4:29
*ᵃ29* Or *Messiah*

# John 4:39
***Many Samaritans Believe (4:39–42)***

# John 4:43
***Jesus Heals the Official’s Son (4:43–54)***

# John 5:1
***The Healing at the Pool (5:1–15)***

# John 5:2
*ᵃ2* Some manuscripts *Bethzatha*; other manuscripts *Bethsaida*

# John 5:3
*ᵃ3* Some less important manuscripts *paralyzed—and they waited for the moving of the waters. ⁴From time to time an angel of the Lord would come down and stir up the waters. The first one into the pool after each such disturbance would be cured of whatever disease he had.*

# John 5:16
***Life Through the Son (5:16–30)***

# John 5:31
***Testimonies About Jesus (5:31–47)***

# John 5:39
*ᵃ39* Or *Study diligently* (the imperative)

# John 5:44
*ᵃ44* Some early manuscripts *the Only One*

# John 6:1
***Jesus Feeds the Five Thousand (6:1–15)***

# John 6:7
*ᵃ7* Greek *two hundred denarii*

# John 6:16
***Jesus Walks on the Water (6:16–24)***

# John 6:19
*ᵃ19* Greek *rowed twenty-five or thirty stadia* (about 5 or 6 kilometers)

# John 6:25
***Jesus the Bread of Life (6:25–59)***

# John 6:31
*ᵃ31* [Exodus 16:4](vref:Exod 16:4); [Neh. 9:15](vref:Neh 9:15); [Psalm 78:24,25](vref:Ps 78:24-25)

# John 6:45
*ᵃ45* [Isaiah 54:13](vref:Isa 54:13)

# John 6:60
***Many Disciples Desert Jesus (6:60–71)***

# John 6:63
*ᵃ63* Or *Spirit*

# John 7:1
***Jesus Goes to the Feast of Tabernacles (7:1–13)***

# John 7:8
*ᵃ8* Some early manuscripts do not have *yet*.

# John 7:14
***Jesus Teaches at the Feast (7:14–24)***

# John 7:25
***Is Jesus the Christ? (7:25–44)***

# John 7:26
*ᵃ26* Or *Messiah*; also in verses [27](vref:John 7:27), [31](vref:John 7:31), [41](vref:John 7:41) and [42](vref:John 7:42)

# John 7:38
*ᵃ37,38* Or */ If anyone is thirsty, let him come to me. / And let him drink, ³⁸who believes in me. / As*

# John 7:42
*ᵃ42* Greek *seed*

# John 7:45
***Unbelief of the Jewish Leaders (7:45–8:11)***

# John 7:52
*ᵃ52* Two early manuscripts *the Prophet*\
*ᵇ52* The earliest and most reliable manuscripts and other ancient witnesses do not have John 7:53–8:11.

# John 8:12
***The Validity of Jesus’ Testimony (8:12–30)***

# John 8:24
*ᵃ24* Or *I am he*; also in verse [28](vref:John 8:28)

# John 8:31
***The Children of Abraham (8:31–41)***

# John 8:33
*ᵃ33* Greek *seed*; also in verse [37](vref:John 8:37)

# John 8:38
*ᵃ38* Or *presence. Therefore do what you have heard from the Father.*

# John 8:39
*ᵃ39* Some early manuscripts *“If you are Abraham’s children,” said Jesus, “then*

# John 8:42
***The Children of the Devil (8:42–47)***

# John 8:48
***The Claims of Jesus About Himself (8:48–59)***

# John 9:1
***Jesus Heals a Man Born Blind (9:1–12)***

# John 9:13
***The Pharisees Investigate the Healing (9:13–34)***

# John 9:22
*ᵃ22* Or *Messiah*

# John 9:24
*ᵃ24* A solemn charge to tell the truth (see [Joshua 7:19](vref:Josh 7:19))

# John 9:35
***Spiritual Blindness (9:35–41)***

# John 10:1
***The Shepherd and His Flock (10:1–21)***

# John 10:9
*ᵃ9* Or *kept safe*

# John 10:22
***The Unbelief of the Jews (10:22–42)***\
\
*ᵃ22* That is, Hanukkah

# John 10:24
*ᵃ24* Or *Messiah*

# John 10:29
*ᵃ29* Many early manuscripts *What my Father has given me is greater than all*

# John 10:34
*ᵃ34* [Psalm 82:6](vref:Ps 82:6)

# John 11:1
***The Death of Lazarus (11:1–16)***

# John 11:17
***Jesus Comforts the Sisters (11:17–37)***

# John 11:18
*ᵃ18* Greek *fifteen stadia* (about 3 kilometers)

# John 11:27
*ᵃ27* Or *Messiah*

# John 11:38
***Jesus Raises Lazarus From the Dead (11:38–44)***

# John 11:45
***The Plot to Kill Jesus (11:45–57)***

# John 11:48
*ᵃ48* Or *temple*

# John 12:1
***Jesus Anointed at Bethany (12:1–11)***

# John 12:3
*ᵃ3* Greek *a litra* (probably about 0.5 liter)

# John 12:5
*ᵃ5* Greek *three hundred denarii*

# John 12:12
***The Triumphal Entry (12:12–19)***

# John 12:13
*ᵃ13* A Hebrew expression meaning “Save!” which became an exclamation of praise\
*ᵇ13* [Psalm 118:25,26](vref:Ps 118:25-26)

# John 12:15
*ᵃ15* [Zech. 9:9](vref:Zech 9:9)

# John 12:20
***Jesus Predicts His Death (12:20–36)***

# John 12:34
*ᵃ34* Or *Messiah*

# John 12:37
***The Jews Continue in Their Unbelief (12:37–50)***

# John 12:38
*ᵃ38* [Isaiah 53:1](vref:Isa 53:1)

# John 12:40
*ᵃ40* [Isaiah 6:10](vref:Isa 6:10)

# John 13:1
***Jesus Washes His Disciples’ Feet (13:1–17)***\
\
*ᵃ1* Or *he loved them to the last*

# John 13:18
***Jesus Predicts His Betrayal (13:18–30)***\
\
*ᵃ18* [Psalm 41:9](vref:Ps 41:9)

# John 13:31
***Jesus Predicts Peter’s Denial (13:31–38)***

# John 13:32
*ᵃ32* Many early manuscripts do not have *If God is glorified in him*.

# John 14:1
***Jesus Comforts His Disciples (14:1–4)***\
\
*ᵃ1* Or *You trust in God*

# John 14:5
***Jesus the Way to the Father (14:5–14)***

# John 14:7
*ᵃ7* Some early manuscripts *If you really have known me, you will know*

# John 14:15
***Jesus Promises the Holy Spirit (14:15–31)***

# John 14:17
*ᵃ17* Some early manuscripts *and is*

# John 15:1
***The Vine and the Branches (15:1–17)***

# John 15:2
*ᵃ2* The Greek for *prunes* also means *cleans*.

# John 15:18
***The World Hates the Disciples (15:18–16:4)***

# John 15:20
*ᵃ20* [John 13:16](vref:John 13:16)

# John 15:25
*ᵃ25* [Psalms 35:19](vref:Ps 35:19); [69:4](vref:Ps 69:4)

# John 16:5
***The Work of the Holy Spirit (16:5–16)***

# John 16:8
*ᵃ8* Or *will expose the guilt of the world*

# John 16:17
***The Disciples’ Grief Will Turn to Joy (16:17–33)***

# John 16:31
*ᵃ31* Or *“Do you now believe?”*

# John 17:1
***Jesus Prays for Himself (17:1–5)***

# John 17:6
***Jesus Prays for His Disciples (17:6–19)***\
\
*ᵃ6* Greek *your name*; also in verse [26](vref:John 17:26)

# John 17:17
*ᵃ17* Greek *hagiazo* (*set apart for sacred use* or *make holy*); also in verse [19](vref:John 17:19)

# John 17:20
***Jesus Prays for All Believers (17:20–26)***

# John 18:1
***Jesus Arrested (18:1–11)***

# John 18:9
*ᵃ9* [John 6:39](vref:John 6:39)

# John 18:12
***Jesus Taken to Annas (18:12–14)***

# John 18:15
***Peter’s First Denial (18:15–18)***

# John 18:19
***The High Priest Questions Jesus (18:19–24)***

# John 18:24
*ᵃ24* Or *(Now Annas had sent him, still bound, to Caiaphas the high priest.)*

# John 18:25
***Peter’s Second and Third Denials (18:25–27)***

# John 18:28
***Jesus Before Pilate (18:28–40)***

# John 19:1
***Jesus Sentenced to be Crucified (19:1–16a)***

# John 19:16
***The Crucifixion (19:16b–27)***

# John 19:24
*ᵃ24* [Psalm 22:18](vref:Ps 22:18)

# John 19:28
***The Death of Jesus (19:28–37)***

# John 19:36
*ᵃ36* [Exodus 12:46](vref:Exod 12:46); [Num. 9:12](vref:Num 9:12); [Psalm 34:20](vref:Ps 34:20)

# John 19:37
*ᵃ37* [Zech. 12:10](vref:Zech 12:10)

# John 19:38
***The Burial of Jesus (19:38–42)***

# John 19:39
*ᵃ39* Greek *a hundred litrai* (about 34 kilograms)

# John 20:1
***The Empty Tomb (20:1–9)***

# John 20:10
***Jesus Appears to Mary Magdalene (20:10–18)***

# John 20:19
***Jesus Appears to His Disciples (20:19–23)***

# John 20:24
***Jesus Appears to Thomas (20:24–31)***

# John 20:31
*ᵃ31* Some manuscripts *may continue to*

# John 21:1
***Jesus and the Miraculous Catch of Fish (21:1–14)***\
\
*ᵃ1* That is, Sea of Galilee

# John 21:8
*ᵃ8* Greek *about two hundred cubits* (about 90 meters)

# John 21:15
***Jesus Reinstates Peter (21:15–25)***

# Acts 1:1
***Jesus Taken Up Into Heaven (1:1–11)***

# Acts 1:5
*ᵃ5* Or *in*

# Acts 1:12
***Matthias Chosen to Replace Judas (1:12–26)***\
\
*ᵃ12* That is, about 3/4 of a mile (about 1,100 meters)

# Acts 1:15
*ᵃ15* Greek *brothers*

# Acts 1:20
*ᵃ20* [Psalm 69:25](vref:Ps 69:25)\
*ᵇ20* [Psalm 109:8](vref:Ps 109:8)

# Acts 2:1
***The Holy Spirit Comes at Pentecost (2:1–13)***

# Acts 2:4
*ᵃ4* Or *languages*; also in verse [11](vref:Acts 2:11)

# Acts 2:13
*ᵃ13* Or *sweet wine*

# Acts 2:14
***Peter Addresses the Crowd (2:14–41)***

# Acts 2:21
*ᵃ21* [Joel 2:28–32](vref:Joel 2:28-32)

# Acts 2:23
*ᵃ23* Or *of those not having the law* (that is, Gentiles)

# Acts 2:28
*ᵃ28* [Psalm 16:8–11](vref:Ps 16:8-11)

# Acts 2:31
*ᵃ31* Or *Messiah*. “The Christ” (Greek) and “the Messiah” (Hebrew) both mean “the Anointed One”; also in verse [36](vref:Acts 2:36).

# Acts 2:35
*ᵃ35* [Psalm 110:1](vref:Ps 110:1)

# Acts 2:42
***The Fellowship of the Believers (2:42–47)***

# Acts 3:1
***Peter Heals the Crippled Beggar (3:1–10)***

# Acts 3:11
***Peter Speaks to the Onlookers (3:11–26)***

# Acts 3:18
*ᵃ18* Or *Messiah*; also in verse [20](vref:Acts 3:20)

# Acts 3:23
*ᵃ23* [Deut. 18:15](vref:Deut 18:15),[18,19](vref:Deut 18:18-19)

# Acts 3:25
*ᵃ25* [Gen. 22:18](vref:Gen 22:18); [26:4](vref:Gen 26:4)

# Acts 4:1
***Peter and John Before the Sanhedrin (4:1–22)***

# Acts 4:11
*ᵃ11* Or *cornerstone*\
*ᵇ11* [Psalm 118:22](vref:Ps 118:22)

# Acts 4:23
***The Believers’ Prayer (4:23–31)***

# Acts 4:26
*ᵃ26* That is, Christ or Messiah\
*ᵇ26* [Psalm 2:1,2](vref:Ps 2:1-2)

# Acts 4:27
*ᵃ27* The Greek is plural.

# Acts 4:32
***The Believers Share Their Possessions (4:32–37)***

# Acts 5:1
***Ananias and Sapphira (5:1–11)***

# Acts 5:12
***The Apostles Heal Many (5:12–16)***

# Acts 5:16
*ᵃ16* Greek *unclean*

# Acts 5:17
***The Apostles Persecuted (5:17–42)***

# Acts 5:42
*ᵃ42* Or *Messiah*

# Acts 6:1
***The Choosing of the Seven (6:1–7)***

# Acts 6:8
***Stephen Seized (6:8–15)***

# Acts 7:1
***Stephen’s Speech to the Sanhedrin (7:1–53)***

# Acts 7:3
*ᵃ3* [Gen. 12:1](vref:Gen 12:1)

# Acts 7:7
*ᵃ7* [Gen. 15:13,14](vref:Gen 15:13-14)

# Acts 7:20
*ᵃ20* Or *was fair in the sight of God*

# Acts 7:28
*ᵃ28* [Exodus 2:14](vref:Exod 2:14)

# Acts 7:32
*ᵃ32* [Exodus 3:6](vref:Exod 3:6)

# Acts 7:34
*ᵃ34* [Exodus 3:5](vref:Exod 3:5),[7,8](vref:Exod 3:7-8),[10](vref:Exod 3:10)

# Acts 7:36
*ᵃ36* That is, Sea of Reeds

# Acts 7:37
*ᵃ37* [Deut. 18:15](vref:Deut 18:15)

# Acts 7:40
*ᵃ40* [Exodus 32:1](vref:Exod 32:1)

# Acts 7:43
*ᵃ43* [Amos 5:25–27](vref:Amos 5:25-27)

# Acts 7:46
*ᵃ46* Some early manuscripts *the house of Jacob*

# Acts 7:50
*ᵃ50* [Isaiah 66:1,2](vref:Isa 66:1-2)

# Acts 7:54
***The Stoning of Stephen (7:54–8:1a)***

# Acts 8:1
***The Church Persecuted and Scattered (8:1b–3)***

# Acts 8:4
***Philip in Samaria (8:4–8)***

# Acts 8:5
*ᵃ5* Or *Messiah*

# Acts 8:7
*ᵃ7* Greek *unclean*

# Acts 8:9
***Simon the Sorcerer (8:9–25)***

# Acts 8:16
*ᵃ16* Or *in*

# Acts 8:26
***Philip and the Ethiopian (8:26–40)***

# Acts 8:27
*ᵃ27* That is, from the upper Nile region

# Acts 8:33
*ᵃ33* [Isaiah 53:7,8](vref:Isa 53:7-8)

# Acts 8:36
*ᵃ36* Some late manuscripts *baptized?” ³⁷Philip said, “If you believe with all your heart, you may.” The eunuch answered, “I believe that Jesus Christ is the Son of God.”*

# Acts 9:1
***Saul’s Conversion (9:1–19a)***

# Acts 9:19
***Saul in Damascus and Jerusalem (9:19b–31)***

# Acts 9:22
*ᵃ22* Or *Messiah*

# Acts 9:32
***Aeneas and Dorcas (9:32–43)***

# Acts 9:36
*ᵃ36* Both *Tabitha* (Aramaic) and *Dorcas* (Greek) mean *gazelle*.

# Acts 10:1
***Cornelius Calls for Peter (10:1–8)***

# Acts 10:9
***Peter’s Vision (10:9–23a)***

# Acts 10:19
*ᵃ19* One early manuscript *two*; other manuscripts do not have the number.

# Acts 10:23
***Peter at Cornelius’ House (10:23b–48)***

# Acts 10:46
*ᵃ46* Or *other languages*

# Acts 11:1
***Peter Explains His Actions (11:1–18)***

# Acts 11:16
*ᵃ16* Or *in*

# Acts 11:19
***The Church in Antioch (11:19–30)***

# Acts 12:1
***Peter’s Miraculous Escape From Prison (12:1–19a)***

# Acts 12:19
***Herod’s Death (12:19b–25)***

# Acts 12:25
*ᵃ25* Some manuscripts *to*

# Acts 13:1
***Barnabas and Saul Sent Off (13:1–3)***

# Acts 13:4
***On Cyprus (13:4–12)***

# Acts 13:13
***In Pisidian Antioch (13:13–52)***

# Acts 13:18
*ᵃ18* Some manuscripts *and cared for them*

# Acts 13:33
*ᵃ33* Or *have begotten you*\
*ᵇ33* [Psalm 2:7](vref:Ps 2:7)

# Acts 13:34
*ᵃ34* [Isaiah 55:3](vref:Isa 55:3)

# Acts 13:35
*ᵃ35* [Psalm 16:10](vref:Ps 16:10)

# Acts 13:41
*ᵃ41* [Hab. 1:5](vref:Hab 1:5)

# Acts 13:47
*ᵃ47* The Greek is singular.\
*ᵇ47* [Isaiah 49:6](vref:Isa 49:6)

# Acts 14:1
***In Iconium (14:1–7)***

# Acts 14:8
***In Lystra and Derbe (14:8–20)***

# Acts 14:21
***The Return to Antioch in Syria (14:21–28)***

# Acts 14:23
*ᵃ23* Or *Barnabas ordained elders*; or *Barnabas had elders elected*

# Acts 15:1
***The Council at Jerusalem (15:1–21)***

# Acts 15:14
*ᵃ14* Greek *Simeon*, a variant of *Simon*; that is, Peter

# Acts 15:17
*ᵃ17* [Amos 9:11,12](vref:Amos 9:11-12)

# Acts 15:18
*ᵃ17,18* Some manuscripts *things’— / ¹⁸known to the Lord for ages is his work*

# Acts 15:22
***The Council’s Letter to Gentile Believers (15:22–35)***

# Acts 15:33
*ᵃ33* Some manuscripts *them, ³⁴but Silas decided to remain there*

# Acts 15:36
***Disagreement Between Paul and Barnabas (15:36–41)***

# Acts 16:1
***Timothy Joins Paul and Silas (16:1–5)***

# Acts 16:6
***Paul’s Vision of the Man of Macedonia (16:6–10)***

# Acts 16:11
***Lydia’s Conversion in Philippi (16:11–15)***

# Acts 16:16
***Paul and Silas in Prison (16:16–40)***

# Acts 17:1
***In Thessalonica (17:1–9)***

# Acts 17:3
*ᵃ3* Or *Messiah*

# Acts 17:5
*ᵃ5* Or *the assembly of the people*

# Acts 17:10
***In Berea (17:10–15)***

# Acts 17:16
***In Athens (17:16–34)***

# Acts 18:1
***In Corinth (18:1–17)***

# Acts 18:5
*ᵃ5* Or *Messiah*; also in verse [28](vref:Acts 18:28)

# Acts 18:18
***Priscilla, Aquila and Apollos (18:18–28)***

# Acts 18:25
*ᵃ25* Or *with fervor in the Spirit*

# Acts 19:1
***Paul in Ephesus (19:1–22)***

# Acts 19:2
*ᵃ2* Or *after*

# Acts 19:5
*ᵃ5* Or *in*

# Acts 19:6
*ᵃ6* Or *other languages*

# Acts 19:19
*ᵃ19* A drachma was a silver coin worth about a day’s wages.

# Acts 19:23
***The Riot in Ephesus (19:23–41)***

# Acts 20:1
***Through Macedonia and Greece (20:1–6)***

# Acts 20:7
***Eutychus Raised From the Dead at Troas (20:7–12)***

# Acts 20:13
***Paul’s Farewell to the Ephesian Elders (20:13–38)***

# Acts 20:28
*ᵃ28* Traditionally *bishops*\
*ᵇ28* Many manuscripts *of the Lord*

# Acts 21:1
***On to Jerusalem (21:1–16)***

# Acts 21:17
***Paul’s Arrival at Jerusalem (21:17–26)***

# Acts 21:27
***Paul Arrested (21:27–36)***

# Acts 21:37
***Paul Speaks to the Crowd (21:37–22:21)***

# Acts 21:40
*ᵃ40* Or possibly *Hebrew*; also in [22:2](vref:Acts 22:2)

# Acts 22:20
*ᵃ20* Or *witness*

# Acts 22:22
***Paul the Roman Citizen (22:22–29)***

# Acts 22:30
***Before the Sanhedrin (22:30–23:11)***

# Acts 23:5
*ᵃ5* [Exodus 22:28](vref:Exod 22:28)

# Acts 23:12
***The Plot to Kill Paul (23:12–22)***

# Acts 23:23
***Paul Transferred to Caesarea (23:23–35)***\
\
*ᵃ23* The meaning of the Greek for this word is uncertain.

# Acts 24:1
***The Trial Before Felix (24:1–27)***

# Acts 24:8
*ᵃ6–8* Some manuscripts *him and wanted to judge him according to our law. ⁷But the commander, Lysias, came and with the use of much force snatched him from our hands ⁸and ordered his accusers to come before you. By*

# Acts 25:1
***The Trial Before Festus (25:1–12)***

# Acts 25:13
***Festus Consults King Agrippa (25:13–22)***

# Acts 25:23
***Paul Before Agrippa (25:23–26:32)***

# Acts 26:14
*ᵃ14* Or *Hebrew*

# Acts 26:23
*ᵃ23* Or *Messiah*

# Acts 27:1
***Paul Sails for Rome (27:1–12)***

# Acts 27:9
*ᵃ9* That is, the Day of Atonement (Yom Kippur)

# Acts 27:13
***The Storm (27:13–26)***

# Acts 27:27
***The Shipwreck (27:27–44)***\
\
*ᵃ27* In ancient times the name referred to an area extending well south of Italy.

# Acts 27:28
*ᵃ28* Greek *twenty orguias* (about 37 meters)\
*ᵇ28* Greek *fifteen orguias* (about 27 meters)

# Acts 28:1
***Ashore on Malta (28:1–10)***

# Acts 28:11
***Arrival at Rome (28:11–16)***

# Acts 28:17
***Paul Preaches at Rome Under Guard (28:17–31)***

# Acts 28:27
*ᵃ27* [Isaiah 6:9,10](vref:Isa 6:9-10)

# Acts 28:28
*ᵃ28* Some manuscripts *listen!” ²⁹After he said this, the Jews left, arguing vigorously among themselves.*

# Rom 1:4
*ᵃ4* Or *who as to his spirit*\
*ᵇ4* Or *was appointed to be the Son of God with power*

# Rom 1:8
***Paul’s Longing to Visit Rome (1:8–17)***

# Rom 1:17
*ᵃ17* Or *is from faith to faith*\
*ᵇ17* [Hab. 2:4](vref:Hab 2:4)

# Rom 1:18
***God’s Wrath Against Mankind (1:18–32)***

# Rom 2:1
***God’s Righteous Judgment (2:1–16)***

# Rom 2:6
*ᵃ6* [Psalm 62:12](vref:Ps 62:12); [Prov. 24:12](vref:Prov 24:12)

# Rom 2:17
***The Jews and the Law (2:17–29)***

# Rom 2:24
*ᵃ24* [Isaiah 52:5](vref:Isa 52:5); [Ezek. 36:22](vref:Ezek 36:22)

# Rom 2:27
*ᵃ27* Or *who, by means of a*

# Rom 3:1
***God’s Faithfulness (3:1–8)***

# Rom 3:4
*ᵃ4* [Psalm 51:4](vref:Ps 51:4)

# Rom 3:9
***No One Is Righteous (3:9–20)***\
\
*ᵃ9* Or *worse*

# Rom 3:12
*ᵃ12* [Psalms 14:1–3](vref:Ps 14:1-3); [53:1–3](vref:Ps 53:1-3); [Eccles. 7:20](vref:Eccl 7:20)

# Rom 3:13
*ᵃ13* [Psalm 5:9](vref:Ps 5:9)\
*ᵇ13* [Psalm 140:3](vref:Ps 140:3)

# Rom 3:14
*ᵃ14* [Psalm 10:7](vref:Ps 10:7)

# Rom 3:17
*ᵃ17* [Isaiah 59:7,8](vref:Isa 59:7-8)

# Rom 3:18
*ᵃ18* [Psalm 36:1](vref:Ps 36:1)

# Rom 3:21
***Righteousness Through Faith (3:21–31)***

# Rom 3:25
*ᵃ25* Or *as the one who would turn aside his wrath, taking away sin*

# Rom 4:1
***Abraham Justified by Faith (4:1–25)***

# Rom 4:3
*ᵃ3* [Gen. 15:6](vref:Gen 15:6); also in verse [22](vref:Rom 4:22)

# Rom 4:8
*ᵃ8* [Psalm 32:1,2](vref:Ps 32:1-2)

# Rom 4:17
*ᵃ17* [Gen. 17:5](vref:Gen 17:5)

# Rom 4:18
*ᵃ18* [Gen. 15:5](vref:Gen 15:5)

# Rom 5:1
***Peace and Joy (5:1–11)***\
\
*ᵃ1* Or *let us*

# Rom 5:2
*ᵃ2* Or *let us*

# Rom 5:3
*ᵃ3* Or *let us*

# Rom 5:12
***Death Through Adam, Life Through Christ (5:12–21)***

# Rom 6:1
***Dead to Sin, Alive in Christ (6:1–14)***

# Rom 6:6
*ᵃ6* Or *be rendered powerless*

# Rom 6:15
***Slaves to Righteousness (6:15–23)***

# Rom 6:23
*ᵃ23* Or *through*

# Rom 7:1
***An Illustration From Marriage (7:1–6)***

# Rom 7:5
*ᵃ5* Or *the flesh*; also in verse [25](vref:Rom 7:25)

# Rom 7:7
***Struggling With Sin (7:7–25)***\
\
*ᵃ7* [Exodus 20:17](vref:Exod 20:17); [Deut. 5:21](vref:Deut 5:21)

# Rom 7:18
*ᵃ18* Or *my flesh*

# Rom 8:1
***Life Through the Spirit (8:1–17)***\
\
*ᵃ1* Some later manuscripts *Jesus, who do not live according to the sinful nature but according to the Spirit,*

# Rom 8:3
*ᵃ3* Or *the flesh*; also in verses [4](vref:Rom 8:4), [5](vref:Rom 8:5), [8](vref:Rom 8:8), [9](vref:Rom 8:9), [12](vref:Rom 8:12) and [13](vref:Rom 8:13)\
*ᵇ3* Or *man, for sin*\
*ᶜ3* Or *in the flesh*

# Rom 8:6
*ᵃ6* Or *mind set on the flesh*

# Rom 8:7
*ᵃ7* Or *the mind set on the flesh*

# Rom 8:15
*ᵃ15* Or *adoption*\
*ᵇ15* Aramaic for *Father*

# Rom 8:18
***Future Glory (8:18–27)***

# Rom 8:21
*ᵃ20,21* Or *subjected it in hope. ²¹For*

# Rom 8:28
***More Than Conquerors (8:28–39)***\
\
*ᵃ28* Some manuscripts *And we know that all things work together for good to those who love God*\
*ᵇ28* Or *works together with those who love him to bring about what is good—with those who*

# Rom 8:36
*ᵃ36* [Psalm 44:22](vref:Ps 44:22)

# Rom 8:38
*ᵃ38* Or *nor heavenly rulers*

# Rom 9:1
***God’s Sovereign Choice (9:1–29)***

# Rom 9:5
*ᵃ5* Or *Christ, who is over all. God be forever praised!* Or *Christ. God who is over all be forever praised!*

# Rom 9:7
*ᵃ7* [Gen. 21:12](vref:Gen 21:12)

# Rom 9:9
*ᵃ9* [Gen. 18:10](vref:Gen 18:10),[14](vref:Gen 18:14)

# Rom 9:12
*ᵃ12* [Gen. 25:23](vref:Gen 25:23)

# Rom 9:13
*ᵃ13* [Mal. 1:2,3](vref:Mal 1:2-3)

# Rom 9:15
*ᵃ15* [Exodus 33:19](vref:Exod 33:19)

# Rom 9:17
*ᵃ17* [Exodus 9:16](vref:Exod 9:16)

# Rom 9:20
*ᵃ20* [Isaiah 29:16](vref:Isa 29:16); [45:9](vref:Isa 45:9)

# Rom 9:25
*ᵃ25* [Hosea 2:23](vref:Hos 2:23)

# Rom 9:26
*ᵃ26* [Hosea 1:10](vref:Hos 1:10)

# Rom 9:28
*ᵃ28* [Isaiah 10:22,23](vref:Isa 10:22-23)

# Rom 9:29
*ᵃ29* [Isaiah 1:9](vref:Isa 1:9)

# Rom 9:30
***Israel’s Unbelief (9:30–10:21)***

# Rom 9:33
*ᵃ33* [Isaiah 8:14](vref:Isa 8:14); [28:16](vref:Isa 28:16)

# Rom 10:5
*ᵃ5* [Lev. 18:5](vref:Lev 18:5)

# Rom 10:6
*ᵃ6* [Deut. 30:12](vref:Deut 30:12)

# Rom 10:7
*ᵃ7* [Deut. 30:13](vref:Deut 30:13)

# Rom 10:8
*ᵃ8* [Deut. 30:14](vref:Deut 30:14)

# Rom 10:11
*ᵃ11* [Isaiah 28:16](vref:Isa 28:16)

# Rom 10:13
*ᵃ13* [Joel 2:32](vref:Joel 2:32)

# Rom 10:15
*ᵃ15* [Isaiah 52:7](vref:Isa 52:7)

# Rom 10:16
*ᵃ16* [Isaiah 53:1](vref:Isa 53:1)

# Rom 10:18
*ᵃ18* [Psalm 19:4](vref:Ps 19:4)

# Rom 10:19
*ᵃ19* [Deut. 32:21](vref:Deut 32:21)

# Rom 10:20
*ᵃ20* [Isaiah 65:1](vref:Isa 65:1)

# Rom 10:21
*ᵃ21* [Isaiah 65:2](vref:Isa 65:2)

# Rom 11:1
***The Remnant of Israel (11:1–24)***

# Rom 11:3
*ᵃ3* [1 Kings 19:10](vref:1 Kgs 19:10),[14](vref:1 Kgs 19:14)

# Rom 11:4
*ᵃ4* [1 Kings 19:18](vref:1 Kgs 19:18)

# Rom 11:6
*ᵃ6* Some manuscripts *by grace. But if by works, then it is no longer grace; if it were, work would no longer be work.*

# Rom 11:8
*ᵃ8* [Deut. 29:4](vref:Deut 29:4); [Isaiah 29:10](vref:Isa 29:10)

# Rom 11:10
*ᵃ10* [Psalm 69:22,23](vref:Ps 69:22-23)

# Rom 11:25
***All Israel Will Be Saved (11:25–32)***

# Rom 11:27
*ᵃ27* Or *will be*\
*ᵇ27* [Isaiah 59:20,21](vref:Isa 59:20-21); [27:9](vref:Isa 27:9); [Jer. 31:33,34](vref:Jer 31:33-34)

# Rom 11:31
*ᵃ31* Some manuscripts do not have *now*.

# Rom 11:33
***Doxology (11:33–36)***\
\
*ᵃ33* Or *riches and the wisdom and the*

# Rom 11:34
*ᵃ34* [Isaiah 40:13](vref:Isa 40:13)

# Rom 11:35
*ᵃ35* [Job 41:11](vref:Job 41:11)

# Rom 12:1
***Living Sacrifices (12:1–8)***\
\
*ᵃ1* Or *reasonable*

# Rom 12:6
*ᵃ6* Or *in agreement with the*

# Rom 12:9
***Love (12:9–21)***

# Rom 12:16
*ᵃ16* Or *willing to do menial work*

# Rom 12:19
*ᵃ19* [Deut. 32:35](vref:Deut 32:35)

# Rom 12:20
*ᵃ20* [Prov. 25:21,22](vref:Prov 25:21-22)

# Rom 13:1
***Submission to the Authorities (13:1–7)***

# Rom 13:8
***Love, for the Day Is Near (13:8–14)***

# Rom 13:9
*ᵃ9* [Exodus 20:13–15](vref:Exod 20:13-15),[17](vref:Exod 20:17); [Deut. 5:17–19](vref:Deut 5:17-19),[21](vref:Deut 5:21)\
*ᵇ9* [Lev. 19:18](vref:Lev 19:18)

# Rom 13:14
*ᵃ14* Or *the flesh*

# Rom 14:1
***The Weak and the Strong (14:1–15:13)***

# Rom 14:11
*ᵃ11* [Isaiah 45:23](vref:Isa 45:23)

# Rom 14:14
*ᵃ14* Or *that nothing*

# Rom 15:3
*ᵃ3* [Psalm 69:9](vref:Ps 69:9)

# Rom 15:8
*ᵃ8* Greek *circumcision*

# Rom 15:9
*ᵃ9* [2 Samuel 22:50](vref:2 Sam 22:50); [Psalm 18:49](vref:Ps 18:49)

# Rom 15:10
*ᵃ10* [Deut. 32:43](vref:Deut 32:43)

# Rom 15:11
*ᵃ11* [Psalm 117:1](vref:Ps 117:1)

# Rom 15:12
*ᵃ12* [Isaiah 11:10](vref:Isa 11:10)

# Rom 15:14
***Paul the Minister to the Gentiles (15:14–22)***

# Rom 15:21
*ᵃ21* [Isaiah 52:15](vref:Isa 52:15)

# Rom 15:23
***Paul’s Plan to Visit Rome (15:23–33)***

# Rom 16:1
***Personal Greetings (16:1–27)***\
\
*ᵃ1* Or *deaconess*

# Rom 16:3
*ᵃ3* Greek *Prisca*, a variant of *Priscilla*

# Rom 16:23
*ᵃ23* Some manuscripts *their greetings. ²⁴May the grace of our Lord Jesus Christ be with all of you. Amen.*

# 1 Cor 1:4
***Thanksgiving (1:4–9)***

# 1 Cor 1:10
***Divisions in the Church (1:10–17)***

# 1 Cor 1:12
*ᵃ12* That is, Peter

# 1 Cor 1:13
*ᵃ13* Or *in*; also in verse [15](vref:1 Cor 1:15)

# 1 Cor 1:18
***Christ the Wisdom and Power of God (1:18–2:5)***

# 1 Cor 1:19
*ᵃ19* [Isaiah 29:14](vref:Isa 29:14)

# 1 Cor 1:31
*ᵃ31* [Jer. 9:24](vref:Jer 9:24)

# 1 Cor 2:1
*ᵃ1* Some manuscripts *as I proclaimed to you God’s mystery*

# 1 Cor 2:6
***Wisdom From the Spirit (2:6–16)***

# 1 Cor 2:9
*ᵃ9* [Isaiah 64:4](vref:Isa 64:4)

# 1 Cor 2:13
*ᵃ13* Or *Spirit, interpreting spiritual truths to spiritual men*

# 1 Cor 2:16
*ᵃ16* [Isaiah 40:13](vref:Isa 40:13)

# 1 Cor 3:1
***On Divisions in the Church (3:1–23)***

# 1 Cor 3:19
*ᵃ19* [Job 5:13](vref:Job 5:13)

# 1 Cor 3:20
*ᵃ20* [Psalm 94:11](vref:Ps 94:11)

# 1 Cor 3:22
*ᵃ22* That is, Peter

# 1 Cor 4:1
***Apostles of Christ (4:1–21)***

# 1 Cor 5:1
***Expel the Immoral Brother! (5:1–13)***

# 1 Cor 5:5
*ᵃ5* Or *that his body*; or *that the flesh*

# 1 Cor 5:13
*ᵃ13* [Deut. 17:7](vref:Deut 17:7); [19:19](vref:Deut 19:19); [21:21](vref:Deut 21:21); [22:21](vref:Deut 22:21),[24](vref:Deut 22:24); [24:7](vref:Deut 24:7)

# 1 Cor 6:1
***Lawsuits Among Believers (6:1–11)***

# 1 Cor 6:4
*ᵃ4* Or *matters, do you appoint as judges men of little account in the church?*

# 1 Cor 6:12
***Sexual Immorality (6:12–20)***

# 1 Cor 6:16
*ᵃ16* [Gen. 2:24](vref:Gen 2:24)

# 1 Cor 7:1
***Marriage (7:1–40)***\
\
*ᵃ1* Or *“It is good for a man not to have sexual relations with a woman.”*

# 1 Cor 7:38
*ᵃ36–38* Or *³⁶If anyone thinks he is not treating his daughter properly, and if she is getting along in years, and he feels she ought to marry, he should do as he wants. He is not sinning. He should let her get married. ³⁷But the man who has settled the matter in his own mind, who is under no compulsion but has control over his own will, and who has made up his mind to keep the virgin unmarried—this man also does the right thing. ³⁸So then, he who gives his virgin in marriage does right, but he who does not give her in marriage does even better.*

# 1 Cor 8:1
***Food Sacrificed to Idols (8:1–13)***\
\
*ᵃ1* Or *“We all possess knowledge,” as you say*

# 1 Cor 9:1
***The Rights of an Apostle (9:1–27)***

# 1 Cor 9:5
*ᵃ5* That is, Peter

# 1 Cor 9:9
*ᵃ9* [Deut. 25:4](vref:Deut 25:4)

# 1 Cor 10:1
***Warnings From Israel’s History (10:1–13)***

# 1 Cor 10:6
*ᵃ6* Or *types*; also in verse [11](vref:1 Cor 10:11)

# 1 Cor 10:7
*ᵃ7* [Exodus 32:6](vref:Exod 32:6)

# 1 Cor 10:14
***Idol Feasts and the Lord’s Supper (10:14–22)***

# 1 Cor 10:23
***The Believer’s Freedom (10:23–11:1)***

# 1 Cor 10:26
*ᵃ26* [Psalm 24:1](vref:Ps 24:1)

# 1 Cor 10:28
*ᵃ28* Some manuscripts *conscience’ sake, for “the earth is the Lord’s and everything in it”*

# 1 Cor 11:2
***Propriety in Worship (11:2–16)***\
\
*ᵃ2* Or *traditions*

# 1 Cor 11:7
*ᵃ4–7* Or *⁴Every man who prays or prophesies with long hair dishonors his head. ⁵And every woman who prays or prophesies with no covering ⌞​of hair​⌟ on her head dishonors her head—she is just like one of the “shorn women.” ⁶If a woman has no covering, let her be for now with short hair, but since it is a disgrace for a woman to have her hair shorn or shaved, she should grow it again. ⁷A man ought not to have long hair*

# 1 Cor 11:17
***The Lord’s Supper (11:17–34)***

# 1 Cor 12:1
***Spiritual Gifts (12:1–31a)***

# 1 Cor 12:10
*ᵃ10* Or *languages*; also in verse [28](vref:1 Cor 12:28)

# 1 Cor 12:13
*ᵃ13* Or *with*; or *in*

# 1 Cor 12:30
*ᵃ30* Or *other languages*

# 1 Cor 12:31
***Love (12:31b–13:13)***\
\
*ᵃ31* Or *But you are eagerly desiring*

# 1 Cor 13:1
*ᵃ1* Or *languages*

# 1 Cor 13:3
*ᵃ3* Some early manuscripts *body that I may boast*

# 1 Cor 14:1
***Gifts of Prophecy and Tongues (14:1–25)***

# 1 Cor 14:2
*ᵃ2* Or *another language*; also in verses [4](vref:1 Cor 14:4), [13](vref:1 Cor 14:13), [14](vref:1 Cor 14:14), [19](vref:1 Cor 14:19), [26](vref:1 Cor 14:26) and [27](vref:1 Cor 14:27)\
*ᵇ2* Or *by the Spirit*

# 1 Cor 14:5
*ᵃ5* Or *other languages*; also in verses [6](vref:1 Cor 14:6), [18](vref:1 Cor 14:18), [22](vref:1 Cor 14:22), [23](vref:1 Cor 14:23) and [39](vref:1 Cor 14:39)

# 1 Cor 14:16
*ᵃ16* Or *among the inquirers*

# 1 Cor 14:21
*ᵃ21* [Isaiah 28:11,12](vref:Isa 28:11-12)

# 1 Cor 14:23
*ᵃ23* Or *some inquirers*

# 1 Cor 14:24
*ᵃ24* Or *or some inquirer*

# 1 Cor 14:26
***Orderly Worship (14:26–40)***

# 1 Cor 14:38
*ᵃ38* Some manuscripts *If he is ignorant of this, let him be ignorant*

# 1 Cor 15:1
***The Resurrection of Christ (15:1–11)***

# 1 Cor 15:3
*ᵃ3* Or *you at the first*

# 1 Cor 15:5
*ᵃ5* Greek *Cephas*

# 1 Cor 15:12
***The Resurrection of the Dead (15:12–34)***

# 1 Cor 15:27
*ᵃ27* [Psalm 8:6](vref:Ps 8:6)

# 1 Cor 15:32
*ᵃ32* [Isaiah 22:13](vref:Isa 22:13)

# 1 Cor 15:35
***The Resurrection Body (15:35–58)***

# 1 Cor 15:45
*ᵃ45* [Gen. 2:7](vref:Gen 2:7)

# 1 Cor 15:49
*ᵃ49* Some early manuscripts *so let us*

# 1 Cor 15:54
*ᵃ54* [Isaiah 25:8](vref:Isa 25:8)

# 1 Cor 15:55
*ᵃ55* [Hosea 13:14](vref:Hos 13:14)

# 1 Cor 16:1
***The Collection for God’s People (16:1–4)***

# 1 Cor 16:5
***Personal Requests (16:5–18)***

# 1 Cor 16:19
***Final Greetings (16:19–24)***\
\
*ᵃ19* Greek *Prisca*, a variant of *Priscilla*

# 1 Cor 16:22
*ᵃ22* In Aramaic the expression *Come, O Lord* is *Marana tha*.

# 1 Cor 16:24
*ᵃ24* Some manuscripts do not have *Amen*.

# 2 Cor 1:3
***The God of All Comfort (1:3–11)***

# 2 Cor 1:11
*ᵃ11* Many manuscripts *your*

# 2 Cor 1:12
***Paul’s Change of Plans (1:12–2:4)***

# 2 Cor 1:19
*ᵃ19* Greek *Silvanus*, a variant of *Silas*

# 2 Cor 2:5
***Forgiveness for the Sinner (2:5–11)***

# 2 Cor 2:12
***Ministers of the New Covenant (2:12–3:6)***

# 2 Cor 3:7
***The Glory of the New Covenant (3:7–18)***

# 2 Cor 3:18
*ᵃ18* Or *contemplate*

# 2 Cor 4:1
***Treasures in Jars of Clay (4:1–18)***

# 2 Cor 4:6
*ᵃ6* [Gen. 1:3](vref:Gen 1:3)

# 2 Cor 4:13
*ᵃ13* [Psalm 116:10](vref:Ps 116:10)

# 2 Cor 5:1
***Our Heavenly Dwelling (5:1–10)***

# 2 Cor 5:11
***The Ministry of Reconciliation (5:11–6:2)***

# 2 Cor 5:21
*ᵃ21* Or *be a sin offering*

# 2 Cor 6:2
*ᵃ2* [Isaiah 49:8](vref:Isa 49:8)

# 2 Cor 6:3
***Paul’s Hardships (6:3–13)***

# 2 Cor 6:14
***Do Not Be Yoked With Unbelievers (6:14–7:1)***

# 2 Cor 6:15
*ᵃ15* Greek *Beliar*, a variant of *Belial*

# 2 Cor 6:16
*ᵃ16* [Lev. 26:12](vref:Lev 26:12); [Jer. 32:38](vref:Jer 32:38); [Ezek. 37:27](vref:Ezek 37:27)

# 2 Cor 6:17
*ᵃ17* [Isaiah 52:11](vref:Isa 52:11); [Ezek. 20:34](vref:Ezek 20:34),[41](vref:Ezek 20:41)

# 2 Cor 6:18
*ᵃ18* [2 Samuel 7:14](vref:2 Sam 7:14); [7:8](vref:2 Sam 7:8)

# 2 Cor 7:2
***Paul’s Joy (7:2–16)***

# 2 Cor 8:1
***Generosity Encouraged (8:1–15)***

# 2 Cor 8:7
*ᵃ7* Some manuscripts *in our love for you*

# 2 Cor 8:15
*ᵃ15* [Exodus 16:18](vref:Exod 16:18)

# 2 Cor 8:16
***Titus Sent to Corinth (8:16–9:5)***

# 2 Cor 9:6
***Sowing Generously (9:6–15)***

# 2 Cor 9:9
*ᵃ9* [Psalm 112:9](vref:Ps 112:9)

# 2 Cor 10:1
***Paul’s Defense of His Ministry (10:1–18)***

# 2 Cor 10:7
*ᵃ7* Or *Look at the obvious facts*

# 2 Cor 10:15
*ᵃ13–15* Or *¹³We, however, will not boast about things that cannot be measured, but we will boast according to the standard of measurement that the God of measure has assigned us—a measurement that relates even to you. ¹⁴ . . . . ¹⁵Neither do we boast about things that cannot be measured in regard to the work done by others.*

# 2 Cor 10:17
*ᵃ17* [Jer. 9:24](vref:Jer 9:24)

# 2 Cor 11:1
***Paul and the False Apostles (11:1–15)***

# 2 Cor 11:16
***Paul Boasts About His Sufferings (11:16–33)***

# 2 Cor 12:1
***Paul’s Vision and His Thorn (12:1–10)***

# 2 Cor 12:11
***Paul’s Concern for the Corinthians (12:11–21)***

# 2 Cor 13:1
***Final Warnings (13:1–10)***\
\
*ᵃ1* [Deut. 19:15](vref:Deut 19:15)

# 2 Cor 13:11
***Final Greetings (13:11–14)***

# Gal 1:6
***No Other Gospel (1:6–10)***

# Gal 1:11
***Paul Called by God (1:11–24)***

# Gal 1:15
*ᵃ15* Or *from my mother’s womb*

# Gal 1:18
*ᵃ18* Greek *Cephas*

# Gal 2:1
***Paul Accepted by the Apostles (2:1–10)***

# Gal 2:7
*ᵃ7* Greek *uncircumcised*\
*ᵇ7* Greek *circumcised*; also in verses [8](vref:Gal 2:8) and [9](vref:Gal 2:9)

# Gal 2:9
*ᵃ9* Greek *Cephas*; also in verses [11](vref:Gal 2:11) and [14](vref:Gal 2:14)

# Gal 2:11
***Paul Opposes Peter (2:11–21)***

# Gal 2:21
*ᵃ21* Some interpreters end the quotation after verse [14](vref:Gal 2:14).

# Gal 3:1
***Faith or Observance of the Law (3:1–14)***

# Gal 3:6
*ᵃ6* [Gen. 15:6](vref:Gen 15:6)

# Gal 3:8
*ᵃ8* [Gen. 12:3](vref:Gen 12:3); [18:18](vref:Gen 18:18); [22:18](vref:Gen 22:18)

# Gal 3:10
*ᵃ10* [Deut. 27:26](vref:Deut 27:26)

# Gal 3:11
*ᵃ11* [Hab. 2:4](vref:Hab 2:4)

# Gal 3:12
*ᵃ12* [Lev. 18:5](vref:Lev 18:5)

# Gal 3:13
*ᵃ13* [Deut. 21:23](vref:Deut 21:23)

# Gal 3:15
***The Law and the Promise (3:15–25)***

# Gal 3:16
*ᵃ16* [Gen. 12:7](vref:Gen 12:7); [13:15](vref:Gen 13:15); [24:7](vref:Gen 24:7)

# Gal 3:24
*ᵃ24* Or *charge until Christ came*

# Gal 3:26
***Sons of God (3:26–4:7)***

# Gal 4:6
*ᵃ6* Aramaic for *Father*

# Gal 4:8
***Paul’s Concern for the Galatians (4:8–20)***

# Gal 4:21
***Hagar and Sarah (4:21–31)***

# Gal 4:27
*ᵃ27* [Isaiah 54:1](vref:Isa 54:1)

# Gal 4:30
*ᵃ30* [Gen. 21:10](vref:Gen 21:10)

# Gal 5:1
***Freedom in Christ (5:1–15)***

# Gal 5:13
*ᵃ13* Or *the flesh*; also in verses [16](vref:Gal 5:16), [17](vref:Gal 5:17), [19](vref:Gal 5:19) and [24](vref:Gal 5:24)

# Gal 5:14
*ᵃ14* [Lev. 19:18](vref:Lev 19:18)

# Gal 5:16
***Life by the Spirit (5:16–26)***

# Gal 6:1
***Doing Good to All (6:1–10)***

# Gal 6:8
*ᵃ8* Or *his flesh, from the flesh*

# Gal 6:11
***Not Circumcision but a New Creation (6:11–18)***

# Gal 6:14
*ᵃ14* Or *whom*

# Eph 1:1
*ᵃ1* Some early manuscripts do not have *in Ephesus*.\
*ᵇ1* Or *believers who are*

# Eph 1:3
***Spiritual Blessings in Christ (1:3–14)***

# Eph 1:5
*ᵃ4,5* Or *sight in love. ⁵He*

# Eph 1:9
*ᵃ8,9* Or *us. With all wisdom and understanding, ⁹he*

# Eph 1:11
*ᵃ11* Or *were made heirs*

# Eph 1:15
***Thanksgiving and Prayer (1:15–23)***

# Eph 1:17
*ᵃ17* Or *a spirit*

# Eph 2:1
***Made Alive in Christ (2:1–10)***

# Eph 2:3
*ᵃ3* Or *our flesh*

# Eph 2:11
***One in Christ (2:11–22)***

# Eph 3:1
***Paul the Preacher to the Gentiles (3:1–13)***

# Eph 3:14
***A Prayer for the Ephesians (3:14–21)***

# Eph 3:15
*ᵃ15* Or *whom all fatherhood*

# Eph 4:1
***Unity in the Body of Christ (4:1–16)***

# Eph 4:8
*ᵃ8* Or *God*\
*ᵇ8* [Psalm 68:18](vref:Ps 68:18)

# Eph 4:9
*ᵃ9* Or *the depths of the earth*

# Eph 4:17
***Living as Children of Light (4:17–5:21)***

# Eph 4:26
*ᵃ26* [Psalm 4:4](vref:Ps 4:4)

# Eph 5:5
*ᵃ5* Or *kingdom of the Christ and God*

# Eph 5:22
***Wives and Husbands (5:22–33)***

# Eph 5:26
*ᵃ26* Or *having cleansed*

# Eph 5:31
*ᵃ31* [Gen. 2:24](vref:Gen 2:24)

# Eph 6:1
***Children and Parents (6:1–4)***

# Eph 6:3
*ᵃ3* [Deut. 5:16](vref:Deut 5:16)

# Eph 6:5
***Slaves and Masters (6:5–9)***

# Eph 6:10
***The Armor of God (6:10–20)***

# Eph 6:21
***Final Greetings (6:21–24)***

# Phil 1:1
*ᵃ1* Traditionally *bishops*

# Phil 1:3
***Thanksgiving and Prayer (1:3–11)***

# Phil 1:12
***Paul’s Chains Advance the Gospel (1:12–30)***

# Phil 1:13
*ᵃ13* Or *whole palace*

# Phil 1:17
*ᵃ16,17* Some late manuscripts have verses [16](vref:Phil 1:16) and [17](vref:Phil 1:17) in reverse order.

# Phil 1:19
*ᵃ19* Or *salvation*

# Phil 2:1
***Imitating Christ’s Humility (2:1–11)***

# Phil 2:6
*ᵃ6* Or *in the form of*

# Phil 2:7
*ᵃ7* Or *the form*

# Phil 2:12
***Shining as Stars (2:12–18)***

# Phil 2:16
*ᵃ16* Or *hold on to*

# Phil 2:19
***Timothy and Epaphroditus (2:19–30)***

# Phil 3:1
***No Confidence in the Flesh (3:1–11)***

# Phil 3:12
***Pressing on Toward the Goal (3:12–4:1)***

# Phil 4:2
***Exhortations (4:2–9)***

# Phil 4:3
*ᵃ3* Or *loyal Syzygus*

# Phil 4:10
***Thanks for Their Gifts (4:10–20)***

# Phil 4:21
***Final Greetings (4:21–23)***

# Phil 4:23
*ᵃ23* Some manuscripts do not have *Amen*.

# Col 1:2
*ᵃ2* Or *believing*\
*ᵇ2* Some manuscripts *Father and the Lord Jesus Christ*

# Col 1:3
***Thanksgiving and Prayer (1:3–14)***

# Col 1:7
*ᵃ7* Some manuscripts *your*

# Col 1:12
*ᵃ12* Some manuscripts *us*

# Col 1:14
*ᵃ14* A few late manuscripts *redemption through his blood*

# Col 1:15
***The Supremacy of Christ (1:15–23)***

# Col 1:21
*ᵃ21* Or *minds, as shown by*

# Col 1:24
***Paul’s Labor for the Church (1:24–2:5)***

# Col 2:6
***Freedom From Human Regulations Through Life With Christ (2:6–23)***

# Col 2:11
*ᵃ11* Or *the flesh*

# Col 2:13
*ᵃ13* Or *your flesh*\
*ᵇ13* Some manuscripts *us*

# Col 2:15
*ᵃ15* Or *them in him*

# Col 3:1
***Rules for Holy Living (3:1–17)***

# Col 3:4
*ᵃ4* Some manuscripts *our*

# Col 3:6
*ᵃ6* Some early manuscripts *coming on those who are disobedient*

# Col 3:18
***Rules for Christian Households (3:18–4:1)***

# Col 4:2
***Further Instructions (4:2–6)***

# Col 4:7
***Final Greetings (4:7–18)***

# Col 4:8
*ᵃ8* Some manuscripts *that he may know about your*

# 1 Thess 1:1
*ᵃ1* Greek *Silvanus*, a variant of *Silas*\
*ᵇ1* Some early manuscripts *you from God our Father and the Lord Jesus Christ*

# 1 Thess 1:2
***Thanksgiving for the Thessalonians’ Faith (1:2–10)***

# 1 Thess 2:1
***Paul’s Ministry in Thessalonica (2:1–16)***

# 1 Thess 2:16
*ᵃ16* Or *them fully*

# 1 Thess 2:17
***Paul’s Longing to See the Thessalonians (2:17–3:5)***

# 1 Thess 3:2
*ᵃ2* Some manuscripts *brother and fellow worker*; other manuscripts *brother and God’s servant*

# 1 Thess 3:6
***Timothy’s Encouraging Report (3:6–13)***

# 1 Thess 4:1
***Living to Please God (4:1–12)***

# 1 Thess 4:4
*ᵃ4* Or *learn to live with his own wife*; or *learn to acquire a wife*

# 1 Thess 4:13
***The Coming of the Lord (4:13–5:11)***

# 1 Thess 5:12
***Final Instructions (5:12–24)***

# 2 Thess 1:1
*ᵃ1* Greek *Silvanus*, a variant of *Silas*

# 2 Thess 1:3
***Thanksgiving and Prayer (1:3–12)***

# 2 Thess 1:12
*ᵃ12* Or *God and Lord, Jesus Christ*

# 2 Thess 2:1
***The Man of Lawlessness (2:1–12)***

# 2 Thess 2:3
*ᵃ3* Some manuscripts *sin*

# 2 Thess 2:13
***Stand Firm (2:13–17)***\
\
*ᵃ13* Some manuscripts *because God chose you as his firstfruits*

# 2 Thess 2:15
*ᵃ15* Or *traditions*

# 2 Thess 3:1
***Request for Prayer (3:1–5)***

# 2 Thess 3:6
***Warning Against Idleness (3:6–15)***\
\
*ᵃ6* Or *tradition*

# 2 Thess 3:16
***Final Greetings (3:16–18)***

# 1 Tim 1:3
***Warning Against False Teachers of the Law (1:3–11)***

# 1 Tim 1:9
*ᵃ9* Or *that the law*

# 1 Tim 1:12
***The Lord’s Grace to Paul (1:12–20)***

# 1 Tim 2:1
***Instructions on Worship (2:1–15)***

# 1 Tim 2:15
*ᵃ15* Greek *she*\
*ᵇ15* Or *restored*

# 1 Tim 3:1
***Overseers and Deacons (3:1–16)***\
\
*ᵃ1* Traditionally *bishop*; also in verse [2](vref:1 Tim 3:2)

# 1 Tim 3:11
*ᵃ11* Or *way, deaconesses*

# 1 Tim 3:16
*ᵃ16* Some manuscripts *God*\
*ᵇ16* Or *in the flesh*

# 1 Tim 4:1
***Instructions to Timothy (4:1–16)***

# 1 Tim 5:1
***Advice About Widows, Elders and Slaves (5:1–6:2)***

# 1 Tim 5:9
*ᵃ9* Or *has had but one husband*

# 1 Tim 5:18
*ᵃ18* [Deut. 25:4](vref:Deut 25:4)\
*ᵇ18* [Luke 10:7](vref:Luke 10:7)

# 1 Tim 6:3
***Love of Money (6:3–10)***

# 1 Tim 6:11
***Paul’s Charge to Timothy (6:11–16)***

# 2 Tim 1:3
***Encouragement to Be Faithful (1:3–2:13)***

# 2 Tim 2:14
***A Workman Approved by God (2:14–26)***

# 2 Tim 2:19
*ᵃ19* [Num. 16:5](vref:Num 16:5) (see Septuagint)

# 2 Tim 3:1
***Godlessness in the Last Days (3:1–9)***

# 2 Tim 3:10
***Paul’s Charge to Timothy (3:10–4:8)***

# 2 Tim 4:9
***Personal Remarks (4:9–18)***

# 2 Tim 4:19
***Final Greetings (4:19–22)***\
\
*ᵃ19* Greek *Prisca*, a variant of *Priscilla*

# Titus 1:5
***Titus’ Task on Crete (1:5–16)***\
\
*ᵃ5* Or *ordain*

# Titus 1:7
*ᵃ7* Traditionally *bishop*

# Titus 2:1
***What Must Be Taught to Various Groups (2:1–15)***

# Titus 3:1
***Doing What Is Good (3:1–11)***

# Titus 3:12
***Final Remarks (3:12–15)***

# Phlm 4
***Thanksgiving and Prayer (4–7)***

# Phlm 8
***Paul’s Plea for Onesimus (8–22)***

# Phlm 10
*ᵃ10* *Onesimus* means *useful*.

# Heb 1:1
***The Son Superior to Angels (1:1–14)***

# Heb 1:5
*ᵃ5* Or *have begotten you*\
*ᵇ5* [Psalm 2:7](vref:Ps 2:7)\
*ᶜ5* [2 Samuel 7:14](vref:2 Sam 7:14); [1 Chron. 17:13](vref:1 Chr 17:13)

# Heb 1:6
*ᵃ6* [Deut. 32:43](vref:Deut 32:43) (see Dead Sea Scrolls and Septuagint)

# Heb 1:7
*ᵃ7* [Psalm 104:4](vref:Ps 104:4)

# Heb 1:9
*ᵃ9* [Psalm 45:6,7](vref:Ps 45:6-7)

# Heb 1:12
*ᵃ12* [Psalm 102:25–27](vref:Ps 102:25-27)

# Heb 1:13
*ᵃ13* [Psalm 110:1](vref:Ps 110:1)

# Heb 2:1
***Warning to Pay Attention (2:1–4)***

# Heb 2:5
***Jesus Made Like His Brothers (2:5–18)***

# Heb 2:7
*ᵃ7* Or *him for a little while*; also in verse [9](vref:Heb 2:9)

# Heb 2:8
*ᵃ8* [Psalm 8:4–6](vref:Ps 8:4-6)

# Heb 2:12
*ᵃ12* [Psalm 22:22](vref:Ps 22:22)

# Heb 2:13
*ᵃ13* [Isaiah 8:17](vref:Isa 8:17)\
*ᵇ13* [Isaiah 8:18](vref:Isa 8:18)

# Heb 2:17
*ᵃ17* Or *and that he might turn aside God’s wrath, taking away*

# Heb 3:1
***Jesus Greater Than Moses (3:1–6)***

# Heb 3:7
***Warning Against Unbelief (3:7–19)***

# Heb 3:11
*ᵃ11* [Psalm 95:7–11](vref:Ps 95:7-11)

# Heb 3:15
*ᵃ15* [Psalm 95:7,8](vref:Ps 95:7-8)

# Heb 3:18
*ᵃ18* Or *disbelieved*

# Heb 4:1
***A Sabbath-Rest for the People of God (4:1–13)***

# Heb 4:2
*ᵃ2* Many manuscripts *because they did not share in the faith of those who obeyed*

# Heb 4:3
*ᵃ3* [Psalm 95:11](vref:Ps 95:11); also in verse [5](vref:Heb 4:5)

# Heb 4:4
*ᵃ4* [Gen. 2:2](vref:Gen 2:2)

# Heb 4:7
*ᵃ7* [Psalm 95:7,8](vref:Ps 95:7-8)

# Heb 4:14
***Jesus the Great High Priest (4:14–5:10)***\
\
*ᵃ14* Or *gone into heaven*

# Heb 5:5
*ᵃ5* Or *have begotten you*\
*ᵇ5* [Psalm 2:7](vref:Ps 2:7)

# Heb 5:6
*ᵃ6* [Psalm 110:4](vref:Ps 110:4)

# Heb 5:11
***Warning Against Falling Away (5:11–6:12)***

# Heb 6:1
*ᵃ1* Or *from useless rituals*

# Heb 6:6
*ᵃ6* Or *repentance while*

# Heb 6:13
***The Certainty of God’s Promise (6:13–20)***

# Heb 6:14
*ᵃ14* [Gen. 22:17](vref:Gen 22:17)

# Heb 7:1
***Melchizedek the Priest (7:1–10)***

# Heb 7:11
***Jesus Like Melchizedek (7:11–28)***

# Heb 7:17
*ᵃ17* [Psalm 110:4](vref:Ps 110:4)

# Heb 7:21
*ᵃ21* [Psalm 110:4](vref:Ps 110:4)

# Heb 7:25
*ᵃ25* Or *forever*

# Heb 8:1
***The High Priest of a New Covenant (8:1–13)***

# Heb 8:5
*ᵃ5* [Exodus 25:40](vref:Exod 25:40)

# Heb 8:8
*ᵃ8* Some manuscripts may be translated *fault and said to the people*.

# Heb 8:12
*ᵃ12* [Jer. 31:31–34](vref:Jer 31:31-34)

# Heb 9:1
***Worship in the Earthly Tabernacle (9:1–10)***

# Heb 9:5
*ᵃ5* Traditionally *the mercy seat*

# Heb 9:11
***The Blood of Christ (9:11–28)***\
\
*ᵃ11* Some early manuscripts *are to come*

# Heb 9:14
*ᵃ14* Or *from useless rituals*

# Heb 9:16
*ᵃ16* Same Greek word as *covenant*; also in verse [17](vref:Heb 9:17)

# Heb 9:20
*ᵃ20* [Exodus 24:8](vref:Exod 24:8)

# Heb 10:1
***Christ’s Sacrifice Once for All (10:1–18)***

# Heb 10:7
*ᵃ7* [Psalm 40:6–8](vref:Ps 40:6-8) (see Septuagint)

# Heb 10:16
*ᵃ16* [Jer. 31:33](vref:Jer 31:33)

# Heb 10:17
*ᵃ17* [Jer. 31:34](vref:Jer 31:34)

# Heb 10:19
***A Call to Persevere (10:19–39)***

# Heb 10:30
*ᵃ30* [Deut. 32:35](vref:Deut 32:35)\
*ᵇ30* [Deut. 32:36](vref:Deut 32:36); [Psalm 135:14](vref:Ps 135:14)

# Heb 10:38
*ᵃ38* One early manuscript *But the righteous*\
*ᵇ38* [Hab. 2:3,4](vref:Hab 2:3-4)

# Heb 11:1
***By Faith (11:1–40)***

# Heb 11:11
*ᵃ11* Or *By faith even Sarah, who was past age, was enabled to bear children because she*

# Heb 11:18
*ᵃ18* Greek *seed*\
*ᵇ18* [Gen. 21:12](vref:Gen 21:12)

# Heb 11:29
*ᵃ29* That is, Sea of Reeds

# Heb 11:31
*ᵃ31* Or *unbelieving*

# Heb 11:37
*ᵃ37* Some early manuscripts *stoned; they were put to the test;*

# Heb 12:1
***God Disciplines His Sons (12:1–13)***

# Heb 12:6
*ᵃ6* [Prov. 3:11,12](vref:Prov 3:11-12)

# Heb 12:13
*ᵃ13* [Prov. 4:26](vref:Prov 4:26)

# Heb 12:14
***Warning Against Refusing God (12:14–29)***

# Heb 12:20
*ᵃ20* [Exodus 19:12,13](vref:Exod 19:12-13)

# Heb 12:21
*ᵃ21* [Deut. 9:19](vref:Deut 9:19)

# Heb 12:26
*ᵃ26* [Haggai 2:6](vref:Hag 2:6)

# Heb 12:29
*ᵃ29* [Deut. 4:24](vref:Deut 4:24)

# Heb 13:1
***Concluding Exhortations (13:1–21)***

# Heb 13:5
*ᵃ5* [Deut. 31:6](vref:Deut 31:6)

# Heb 13:6
*ᵃ6* [Psalm 118:6,7](vref:Ps 118:6-7)

# Jas 1:2
***Trials and Temptations (1:2–18)***

# Jas 1:19
***Listening and Doing (1:19–27)***

# Jas 2:1
***Favoritism Forbidden (2:1–13)***

# Jas 2:8
*ᵃ8* [Lev. 19:18](vref:Lev 19:18)

# Jas 2:11
*ᵃ11* [Exodus 20:14](vref:Exod 20:14); [Deut. 5:18](vref:Deut 5:18)\
*ᵇ11* [Exodus 20:13](vref:Exod 20:13); [Deut. 5:17](vref:Deut 5:17)

# Jas 2:14
***Faith and Deeds (2:14–26)***

# Jas 2:20
*ᵃ20* Some early manuscripts *dead*

# Jas 2:23
*ᵃ23* [Gen. 15:6](vref:Gen 15:6)

# Jas 3:1
***Taming the Tongue (3:1–12)***

# Jas 3:11
*ᵃ11* Greek *bitter* (see also verse [14](vref:Jas 3:14))

# Jas 3:13
***Two Kinds of Wisdom (3:13–18)***

# Jas 4:1
***Submit Yourselves to God (4:1–12)***

# Jas 4:5
*ᵃ5* Or *that God jealously longs for the spirit that he made to live in us*; or *that the Spirit he caused to live in us longs jealously*

# Jas 4:6
*ᵃ6* [Prov. 3:34](vref:Prov 3:34)

# Jas 4:13
***Boasting About Tomorrow (4:13–17)***

# Jas 5:1
***Warning to Rich Oppressors (5:1–6)***

# Jas 5:5
*ᵃ5* Or *yourselves as in a day of feasting*

# Jas 5:7
***Patience in Suffering (5:7–12)***

# Jas 5:13
***The Prayer of Faith (5:13–20)***

# 1 Pet 1:3
***Praise to God for a Living Hope (1:3–12)***

# 1 Pet 1:13
***Be Holy (1:13–2:3)***

# 1 Pet 1:16
*ᵃ16* [Lev. 11:44,45](vref:Lev 11:44-45); [19:2](vref:Lev 19:2); [20:7](vref:Lev 20:7)

# 1 Pet 1:22
*ᵃ22* Some early manuscripts *from a pure heart*

# 1 Pet 1:25
*ᵃ25* [Isaiah 40:6–8](vref:Isa 40:6-8)

# 1 Pet 2:4
***The Living Stone and a Chosen People (2:4–12)***

# 1 Pet 2:6
*ᵃ6* [Isaiah 28:16](vref:Isa 28:16)

# 1 Pet 2:7
*ᵃ7* Or *cornerstone*\
*ᵇ7* [Psalm 118:22](vref:Ps 118:22)

# 1 Pet 2:8
*ᵃ8* [Isaiah 8:14](vref:Isa 8:14)

# 1 Pet 2:13
***Submission to Rulers and Masters (2:13–25)***

# 1 Pet 2:22
*ᵃ22* [Isaiah 53:9](vref:Isa 53:9)

# 1 Pet 3:1
***Wives and Husbands (3:1–7)***

# 1 Pet 3:8
***Suffering for Doing Good (3:8–22)***

# 1 Pet 3:12
*ᵃ12* [Psalm 34:12–16](vref:Ps 34:12-16)

# 1 Pet 3:14
*ᵃ14* Or *not fear their threats*\
*ᵇ14* [Isaiah 8:12](vref:Isa 8:12)

# 1 Pet 3:19
*ᵃ18,19* Or *alive in the spirit, ¹⁹through which*

# 1 Pet 3:21
*ᵃ21* Or *response*

# 1 Pet 4:1
***Living for God (4:1–11)***

# 1 Pet 4:12
***Suffering for Being a Christian (4:12–19)***

# 1 Pet 4:18
*ᵃ18* [Prov. 11:31](vref:Prov 11:31)

# 1 Pet 5:1
***To Elders and Young Men (5:1–11)***

# 1 Pet 5:5
*ᵃ5* [Prov. 3:34](vref:Prov 3:34)

# 1 Pet 5:12
***Final Greetings (5:12–14)***\
\
*ᵃ12* Greek: *Silvanus*, a variant of *Silas*

# 2 Pet 1:3
***Making One’s Calling and Election Sure (1:3–11)***

# 2 Pet 1:12
***Prophecy of Scripture (1:12–21)***

# 2 Pet 1:17
*ᵃ17* [Matt. 17:5](vref:Matt 17:5); [Mark 9:7](vref:Mark 9:7); [Luke 9:35](vref:Luke 9:35)

# 2 Pet 2:1
***False Teachers and Their Destruction (2:1–22)***

# 2 Pet 2:4
*ᵃ4* Greek *Tartarus*\
*ᵇ4* Some manuscripts *into chains of darkness*

# 2 Pet 2:9
*ᵃ9* Or *unrighteous for punishment until the day of judgment*

# 2 Pet 2:10
*ᵃ10* Or *the flesh*

# 2 Pet 2:13
*ᵃ13* Some manuscripts *in their love feasts*

# 2 Pet 2:22
*ᵃ22* [Prov. 26:11](vref:Prov 26:11)

# 2 Pet 3:1
***The Day of the Lord (3:1–18)***

# 2 Pet 3:10
*ᵃ10* Some manuscripts *be burned up*

# 2 Pet 3:12
*ᵃ12* Or *as you wait eagerly for the day of God to come*

# 1 John 1:1
***The Word of Life (1:1–4)***

# 1 John 1:4
*ᵃ4* Some manuscripts *your*

# 1 John 1:5
***Walking in the Light (1:5–2:14)***

# 1 John 1:7
*ᵃ7* Or *every*

# 1 John 2:2
*ᵃ2* Or *He is the one who turns aside God’s wrath, taking away our sins, and not only ours but also*

# 1 John 2:5
*ᵃ5* Or *word, love for God*

# 1 John 2:10
*ᵃ10* Or *it*

# 1 John 2:15
***Do Not Love the World (2:15–17)***

# 1 John 2:18
***Warning Against Antichrists (2:18–27)***

# 1 John 2:20
*ᵃ20* Some manuscripts *and you know all things*

# 1 John 2:28
***Children of God (2:28–3:10)***

# 1 John 3:2
*ᵃ2* Or *when it is made known*

# 1 John 3:11
***Love One Another (3:11–24)***

# 1 John 4:1
***Test the Spirits (4:1–6)***

# 1 John 4:6
*ᵃ6* Or *spirit*

# 1 John 4:7
***God’s Love and Ours (4:7–21)***

# 1 John 4:9
*ᵃ9* Or *his only begotten Son*

# 1 John 4:10
*ᵃ10* Or *as the one who would turn aside his wrath, taking away*

# 1 John 5:1
***Faith in the Son of God (5:1–12)***

# 1 John 5:8
*ᵃ7,8* Late manuscripts of the Vulgate *testify in heaven: the Father, the Word and the Holy Spirit, and these three are one. ⁸And there are three that testify on earth: the* (not found in any Greek manuscript before the sixteenth century)

# 1 John 5:13
***Concluding Remarks (5:13–21)***

# Jude 1
*ᵃ1* Or *for*; or *in*

# Jude 3
***The Sin and Doom of Godless Men (3–16)***

# Jude 4
*ᵃ4* Or *men who were marked out for condemnation*

# Jude 5
*ᵃ5* Some early manuscripts *Jesus*

# Jude 17
***A Call to Persevere (17–23)***

# Jude 24
***Doxology (24–25)***

# Rev 1:1
***Prologue (1:1–3)***

# Rev 1:4
***Greetings and Doxology (1:4–8)***\
\
*ᵃ4* Or *the sevenfold Spirit*

# Rev 1:9
***One Like a Son of Man (1:9–20)***

# Rev 1:13
*ᵃ13* [Daniel 7:13](vref:Dan 7:13)

# Rev 1:20
*ᵃ20* Or *messengers*

# Rev 2:1
***To the Church in Ephesus (2:1–7)***\
\
*ᵃ1* Or *messenger*; also in verses [8](vref:Rev 2:8), [12](vref:Rev 2:12) and [18](vref:Rev 2:18)

# Rev 2:8
***To the Church in Smyrna (2:8–11)***

# Rev 2:12
***To the Church in Pergamum (2:12–17)***

# Rev 2:18
***To the Church in Thyatira (2:18–29)***

# Rev 2:27
*ᵃ27* [Psalm 2:9](vref:Ps 2:9)

# Rev 3:1
***To the Church in Sardis (3:1–6)***\
\
*ᵃ1* Or *messenger*; also in verses [7](vref:Rev 3:7) and [14](vref:Rev 3:14)\
*ᵇ1* Or *the sevenfold Spirit*

# Rev 3:7
***To the Church in Philadelphia (3:7–13)***

# Rev 3:14
***To the Church in Laodicea (3:14–22)***

# Rev 4:1
***The Throne in Heaven (4:1–11)***

# Rev 4:5
*ᵃ5* Or *the sevenfold Spirit*

# Rev 5:1
***The Scroll and the Lamb (5:1–14)***

# Rev 5:6
*ᵃ6* Or *the sevenfold Spirit*

# Rev 6:1
***The Seals (6:1–17)***

# Rev 6:6
*ᵃ6* Greek *a choinix* (probably about a liter)\
*ᵇ6* Greek *a denarius*

# Rev 7:1
***144,000 Sealed (7:1–8)***

# Rev 7:9
***The Great Multitude in White Robes (7:9–17)***

# Rev 8:1
***The Seventh Seal and the Golden Censer (8:1–5)***

# Rev 8:6
***The Trumpets (8:6–9:21)***

# Rev 8:11
*ᵃ11* That is, Bitterness

# Rev 9:11
*ᵃ11* *Abaddon* and *Apollyon* mean *Destroyer*.

# Rev 9:13
*ᵃ13* That is, projections

# Rev 10:1
***The Angel and the Little Scroll (10:1–11)***

# Rev 11:1
***The Two Witnesses (11:1–14)***

# Rev 11:15
***The Seventh Trumpet (11:15–19)***

# Rev 12:1
***The Woman and the Dragon (12:1–13:1a)***

# Rev 13:1
***The Beast out of the Sea (13:1b–10)***\
\
*ᵃ1* Some late manuscripts *And I*

# Rev 13:8
*ᵃ8* Or *written from the creation of the world in the book of life belonging to the Lamb that was slain*

# Rev 13:10
*ᵃ10* Some manuscripts *anyone kills*

# Rev 13:11
***The Beast out of the Earth (13:11–18)***

# Rev 14:1
***The Lamb and the 144,000 (14:1–5)***

# Rev 14:6
***The Three Angels (14:6–13)***

# Rev 14:14
***The Harvest of the Earth (14:14–20)***\
\
*ᵃ14* [Daniel 7:13](vref:Dan 7:13)

# Rev 14:20
*ᵃ20* That is, about 180 miles (about 300 kilometers)

# Rev 15:1
***Seven Angels With Seven Plagues (15:1–8)***

# Rev 16:1
***The Seven Bowls of God’s Wrath (16:1–21)***

# Rev 16:13
*ᵃ13* Greek *unclean*

# Rev 17:1
***The Woman on the Beast (17:1–18)***

# Rev 18:1
***The Fall of Babylon (18:1–24)***

# Rev 18:2
*ᵃ2* Greek *unclean*

# Rev 19:1
***Hallelujah! (19:1–10)***

# Rev 19:11
***The Rider on the White Horse (19:11–21)***

# Rev 19:15
*ᵃ15* [Psalm 2:9](vref:Ps 2:9)

# Rev 20:1
***The Thousand Years (20:1–6)***

# Rev 20:7
***Satan’s Doom (20:7–10)***

# Rev 20:11
***The Dead Are Judged (20:11–15)***

# Rev 21:1
***The New Jerusalem (21:1–27)***

# Rev 21:16
*ᵃ16* That is, about 1,400 miles (about 2,200 kilometers)

# Rev 21:17
*ᵃ17* That is, about 200 feet (about 65 meters)\
*ᵇ17* Or *high*

# Rev 21:20
*ᵃ20* The precise identification of some of these precious stones is uncertain.

# Rev 22:1
***The River of Life (22:1–6)***

# Rev 22:7
***Jesus Is Coming (22:7–21)***

# Rev 22:16
*ᵃ16* The Greek is plural.
