# Build

This folder contains the scripts used to run the build process.

## Build process

### Install Python

If setting up a new development environment, install the default Python 3 package for your system, if necessary. For example, on Fedora:

```sh
$ sudo dnf install python3
```

If setting up a new development environment, or the minor version of the default Python installation on your system has been updated after a system upgrade, install the Pipenv dependency manager into the default Python installation:

```sh
$ pip install --user pipenv
```

If setting up a new development environment, or the version of Python used in the project has changed, install Python version **3.11**. (_NOTE: It is not recommended to skip this step, even if the version of the default Python 3 installation on your system matches the Python version required by the project, because the default installation can be updated to a newer minor version at any time during a system upgrade, causing an existing local build environment to break._) For example, on Fedora:

```sh
$ sudo dnf install python3.11
```

### Install dependencies

If setting up a new development environment, or the project dependencies have changed since the last pull, install/update the project dependencies:

```sh
$ build/install_dependencies
```

### Build artifacts

To run all stages of the build process and generate the artifacts:

```sh
$ build/build
```

The resulting artifacts will be stored in _.build/artifacts_.

The _build_ folder contains other scripts that can be used to run individual stages of the build process if a full build is not required.

### Clean build output

To clean the build output:

```sh
$ build/clean
```

### Update Python dependencies

Until an automated solution (such as Dependabot) is implemented, the following manual process should be run from time to time to ensure Python dependencies are kept up to date.

1. List outdated package dependencies:
    ```sh
    $ pipenv update --outdated
    ```
1. For each dependency in _Pipfile_, update the version to the available version listed in the previous step.
1. Install updated dependences:
    ```sh
    $ pipenv update && pipenv install --dev
    ```
1. Clean the build output and run a full build to ensure no regressions have been introduced.
1. Commit and push the changes.

## Release process

Use the following process to publish a new release:

1. Increment the appropriate component of the `VERSION` variable defined in _build/common.sh_. Only one component should be changed (the most significant in the case of multiple changes in the release). This project uses semantic commit messages based on the [Conventional Commits](https://www.conventionalcommits.org/) specification.
    * A **major** version change occurs if any commit since the last release has a type with a `!` suffix or contains a `BREAKING CHANGE` footer (e.g., the new release is incompatible with a previous version of Accordance).
    * A **minor** version change occurs if any commit since the last release is of type `feat` (e.g., a book or chapter that was incomplete in the previous release).
    * A **patch** version change occurs if any other type of commit since the last release has been made (e.g., fixes to the text or notes [`fix`], documentation changes [`docs`], etc.).
1. Add the new release to `CHANGELOG.md`.
    * Convert the existing Unreleased section to a section for the new release.
        * Change the link name to the new release name (e.g., `1.3.2`).
        * Change the link target to show the differences between the new release and the previous release (e.g., `compare/1.3.1...1.3.2`).
        * Append the date of the new release in ISO-8601 format (e.g., ` - 2023-07-31` for 31 July 2023).
    * Create a new empty Unreleased section.
        * Change the link target to show the differences between the new release and HEAD (e.g., `compare/1.3.2...HEAD`).
1. Merge the above changes onto the `main` branch.
1. Tag the HEAD commit on the `main` branch:
    ```sh
    $ git tag <version>
    ```
    where _version_ is the value from step (1) (e.g., `1.3.2`).
1. Push the new tag upstream:
    ```sh
    $ git push --tags
    ```
1. Wait for the GitLab pipeline triggered by the new tag to finish.
1. Download the artifacts from the `deploy` job in the pipeline from the previous step. Verify that all modules can be successfully imported into Accordance.
1. From the project [Releases](https://gitlab.com/ssoloff/niv-1984/-/releases) page, click the **New release** button.
1. Select the tag you created in step (4).
1. Add the following release asset link:
    * URL: **`https://gitlab.com/ssoloff/niv-1984/-/jobs/<job-id>/artifacts/raw/.build/artifacts/niv-1984-<version>.zip`**, where _job-id_ is the identifier of the `deploy` job in the pipeline from step (6), and _version_ is the value from step (1).
    * Link title: **Distribution**.
1. Save the release by clicking the **Create release** button.
