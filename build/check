#!/usr/bin/env bash
#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

source build/common.sh

# Checks all shell scripts for lint.
#
# shellcheck disable=SC2317
function checkShellScriptsForLint() {
    local status="${STATUS_SUCCESS}"
    local shellScriptPaths
    shellScriptPaths=$(find . -maxdepth 3 -type f -exec awk 'FNR == 1 && /^#!.*bash/{print FILENAME}' {} +) || status="${STATUS_FAILURE}"
    echo "${shellScriptPaths}" | pipenv run xargs shellcheck --check-sourced --enable=all --severity=style || status="${STATUS_FAILURE}"
    endBuildStep "Check shell scripts for lint" "${status}"
}

# Checks all Python sources for lint.
#
# shellcheck disable=SC2317
function checkPythonSourcesForLint() {
    pipenv run pylint --recursive=y "${BUILD_SRC_FOLDER}"
    endBuildStep "Check Python sources for lint" $?
}

# Checks that the Bible text does not end with a newline.  Otherwise, Accordance will interpret a final blank line in
# the text as a final blank verse without raising a warning during import.
#
# shellcheck disable=SC2317
function checkTextDoesNotEndWithNewline() {
    local status="${STATUS_SUCCESS}"
    if [[ -s "${TEXT_SRC_FILE}" ]]; then
        local lastByte
        lastByte=$(tail -c 1 "${TEXT_SRC_FILE}") || status="${STATUS_FAILURE}"
        [[ -n "${lastByte}" ]] || status="${STATUS_FAILURE}"
    fi
    endBuildStep "Check text for illegal final newline" "${status}"
}

runBuildSteps \
    checkShellScriptsForLint \
    checkPythonSourcesForLint \
    checkTextDoesNotEndWithNewline
