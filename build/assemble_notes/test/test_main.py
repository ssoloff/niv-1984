#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# pylint: disable=missing-module-docstring

import os

from inspect import cleandoc
from tempfile import NamedTemporaryFile, TemporaryDirectory
from unittest import TestCase

from main import _main

class SmokeTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__main__should_assemble_notes(self) -> None:
        with NamedTemporaryFile('r+t') as input_file, TemporaryDirectory() as output_folder_name:
            # given: a Markdown file containing three verse notes
            input_file.write(cleandoc('''
            # Matt 1:1
            First verse note.

            # Matt 2:3
            Second verse note.

            # Matt 3:4
            Third verse note.
            '''))
            input_file.flush()
            input_file.seek(0)

            # when: running the application
            _main([input_file.name, output_folder_name])

            # then: it should assemble one RTF file per verse note
            self.assertCountEqual(
                ['Matt 1_1.rtf', 'Matt 2_3.rtf', 'Matt 3_4.rtf'],
                os.listdir(output_folder_name)
            )
