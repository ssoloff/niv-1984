#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# pylint: disable=missing-module-docstring

from inspect import cleandoc
from io import StringIO
from unittest import TestCase
from unittest.mock import Mock

from notes.model import BookId, ChapterId, MarkdownString, VerseId, VerseNote, VerseReference
from notes.persistence.markdown import (
    MarkdownDecodingError,
    read_markdown_verse_notes,
    _decode_book_id,
    _decode_chapter_id,
    _decode_verse_id,
    _decode_verse_note,
    _decode_verse_reference,
    _decode_verse_reference_link_target,
    _decode_verse_reference_links
)

class ReadMarkdownVerseNotesTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__when_input_stream_contains_zero_verse_notes__should_return_empty_iterable(self) -> None: # pylint: disable=line-too-long
        # given: an input stream containing zero verse notes
        input_stream = StringIO(cleandoc('''
        This is just some text.
        And some more text.
        '''))

        # when: reading the entire input stream
        verse_notes = list(read_markdown_verse_notes(lambda: input_stream))

        # then: it should return an empty iterable
        self.assertEqual(0, len(verse_notes))

    def test__when_input_stream_contains_one_verse_note__should_return_iterable_over_one_item(self) -> None: # pylint: disable=line-too-long
        # given: an input stream containing one verse note
        input_stream = StringIO(cleandoc('''
        # Matt 1:1
        First verse note.
        '''))

        # when: reading the entire input stream
        verse_notes = list(read_markdown_verse_notes(lambda: input_stream))

        # then: it should return an iterable with one item
        self.assertEqual(1, len(verse_notes))
        # and: the first item in the iterable should be the first verse note
        self.assertEqual(
            VerseNote(
                verse_reference=VerseReference(BookId.MATTHEW, ChapterId(1), VerseId(1)),
                text=MarkdownString('First verse note.')
            ),
            verse_notes[0]
        )

    def test__when_input_stream_contains_multiple_verse_notes__should_return_iterable_over_multiple_items(self) -> None: # pylint: disable=line-too-long
        # given: an input stream containing three verse notes
        input_stream = StringIO(cleandoc('''
        # Matt 1:1
        First verse note.

        # Matt 1:2
        Second verse note.

        # Matt 1:3
        Third verse note.
        '''))

        # when: reading the entire input stream
        verse_notes = list(read_markdown_verse_notes(lambda: input_stream))

        # then: it should return an iterable with three items
        self.assertEqual(3, len(verse_notes))
        # and: the first item in the iterable should be the first verse note
        self.assertEqual(
            VerseNote(
                verse_reference=VerseReference(BookId.MATTHEW, ChapterId(1), VerseId(1)),
                text=MarkdownString('First verse note.')
            ),
            verse_notes[0]
        )
        # and: the second item in the iterable should be the second verse note
        self.assertEqual(
            VerseNote(
                verse_reference=VerseReference(BookId.MATTHEW, ChapterId(1), VerseId(2)),
                text=MarkdownString('Second verse note.')
            ),
            verse_notes[1]
        )
        # and: the third item in the iterable should be the third verse note
        self.assertEqual(
            VerseNote(
                verse_reference=VerseReference(BookId.MATTHEW, ChapterId(1), VerseId(3)),
                text=MarkdownString('Third verse note.')
            ),
            verse_notes[2]
        )

    def test__when_input_stream_contains_invalid_verse_note__should_raise_exception(self) -> None:
        # given: an input stream with one verse note containing an invalid verse reference
        input_stream = StringIO(cleandoc('''
        # Matt 1:1
        First verse note.

        # Matt 0:2
        Second verse note.
        '''))

        # then: it should raise an exception indicating the verse note is invalid
        with self.assertRaisesRegex(MarkdownDecodingError, 'valid encoded verse reference'):
            # when: reading the entire input stream
            list(read_markdown_verse_notes(lambda: input_stream))

    def test__when_input_stream_contains_duplicate_verse_references__should_raise_exception(self) -> None: # pylint: disable=line-too-long
        # given: an input stream with two verse notes that have the same verse reference
        input_stream = StringIO(cleandoc('''
        # Matt 1:1
        First verse note.

        # Matt 1:1
        Second verse note.
        '''))

        # then: it should raise an exception indicating a duplicate verse reference was detected
        with self.assertRaisesRegex(MarkdownDecodingError, 'Duplicate verse reference'):
            # when: reading the entire input stream
            list(read_markdown_verse_notes(lambda: input_stream))

    @staticmethod
    def test__should_close_input_stream() -> None:
        # when: reading the entire input stream
        input_stream = StringIO()
        input_stream.close = Mock()
        list(read_markdown_verse_notes(lambda: input_stream))

        # then: it should close the input stream
        input_stream.close.assert_called_once()

class DecodeVerseNoteTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__should_return_verse_note(self) -> None:
        # when: decoding a verse note
        actual = _decode_verse_note(cleandoc('''
            # Matt 4:2
            This is a verse note.
            '''
        ))

        # then: it should return the verse note
        self.assertEqual(
            VerseNote(
                verse_reference=VerseReference(BookId.MATTHEW, ChapterId(4), VerseId(2)),
                text=MarkdownString('This is a verse note.')
            ),
            actual
        )

    def test__should_strip_leading_and_trailing_whitespace_in_text(self) -> None:
        # when: decoding a verse note with text that has leading and trailing whitespace
        actual = _decode_verse_note(
            '''# Matt 4:2


This is a verse note.


'''
        )

        # then: it should strip the leading and trailing whitespace in the text
        self.assertEqual(
            VerseNote(
                verse_reference=VerseReference(BookId.MATTHEW, ChapterId(4), VerseId(2)),
                text=MarkdownString('This is a verse note.')
            ),
            actual
        )

    def test__should_preserve_embedded_whitespace_in_text(self) -> None:
        # when: decoding a verse note with text that has embedded whitespace
        actual = _decode_verse_note(cleandoc('''
            # Matt 4:2
            line 1

                indented line 2


            line 3
            '''
        ))

        # then: it should preserve the embedded whitespace in the text
        self.assertEqual(
            VerseNote(
                verse_reference=VerseReference(BookId.MATTHEW, ChapterId(4), VerseId(2)),
                text=MarkdownString(cleandoc('''
                    line 1

                        indented line 2


                    line 3
                    '''
                ))
            ),
            actual
        )

    def test__should_decode_verse_reference_links(self) -> None:
        # when: decoding a verse note with text that has verse reference links
        actual = _decode_verse_note(cleandoc('''
            # Matt 4:2
            [Link 1](vref:Gen 1:2)
            [Link 2](vref:Exod 3:4)
            '''
        ))

        # then: it should decode the verse reference links
        self.assertEqual(
            VerseNote(
                verse_reference=VerseReference(BookId.MATTHEW, ChapterId(4), VerseId(2)),
                text=MarkdownString(cleandoc('''
                    [Link 1](vref:GENESIS 1:2)
                    [Link 2](vref:EXODUS 3:4)
                    '''
                ))
            ),
            actual
        )

class DecodeVerseReferenceTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__when_encoded_value_valid__should_return_verse_reference(self) -> None:
        params = [
            ('Matt 1:1', VerseReference(BookId.MATTHEW, ChapterId(1), VerseId(1))),
            ('Matt 28:20', VerseReference(BookId.MATTHEW, ChapterId(28), VerseId(20)))
        ]
        for encoded_value, expected_verse_reference in params:
            with self.subTest(encoded_value=encoded_value):
                # when: decoding a valid verse reference
                actual = _decode_verse_reference(encoded_value)

                # then: it should return the verse reference
                self.assertEqual(expected_verse_reference, actual)

    def test__when_encoded_value_has_missing_book_id__should_raise_exception(self) -> None:
        # then: it should raise an exception indicating the verse reference is invalid
        with self.assertRaisesRegex(MarkdownDecodingError, 'valid encoded verse reference'):
            # when: decoding a verse reference with a missing book identifier
            _decode_verse_reference(' 1:1')

    def test__when_encoded_value_has_invalid_book_id__should_raise_exception(self) -> None:
        # then: it should raise an exception indicating the book identifier is invalid
        with self.assertRaisesRegex(MarkdownDecodingError, 'valid encoded book identifier'):
            # when: decoding a verse reference with an invalid book identifier
            _decode_verse_reference('UNKNOWN 1:1')

    def test__when_encoded_value_has_invalid_chapter_id__should_raise_exception(self) -> None:
        params = [
            'Matt :1',
            'Matt -1:1',
            'Matt 0:1',
            'Matt one:1'
        ]
        for encoded_value in params:
            with self.subTest(encoded_value=encoded_value):
                # then: it should raise an exception indicating the verse reference is invalid
                with self.assertRaisesRegex(MarkdownDecodingError, 'valid encoded verse reference'):
                    # when: decoding a verse reference with an invalid chapter identifier
                    _decode_verse_reference(encoded_value)

    def test__when_encoded_value_has_invalid_verse_id__should_raise_exception(self) -> None:
        params = [
            'Matt 1:',
            'Matt 1:-1',
            'Matt 1:one'
        ]
        for encoded_value in params:
            with self.subTest(encoded_value=encoded_value):
                # then: it should raise an exception indicating the verse reference is invalid
                with self.assertRaisesRegex(MarkdownDecodingError, 'valid encoded verse reference'):
                    # when: decoding a verse reference with an invalid verse identifier
                    _decode_verse_reference(encoded_value)

    def test__when_encoded_value_has_single_chapter_book_with_chapter_id__should_use_specified_chapter_id(self) -> None: # pylint: disable=line-too-long
        params = [
            ('Obad 2:1', VerseReference(BookId.OBADIAH, ChapterId(2), VerseId(1))),
            ('Phlm 3:2', VerseReference(BookId.PHILEMON, ChapterId(3), VerseId(2))),
            ('2 John 4:3', VerseReference(BookId.SECOND_JOHN, ChapterId(4), VerseId(3))),
            ('3 John 5:4', VerseReference(BookId.THIRD_JOHN, ChapterId(5), VerseId(4))),
            ('Jude 6:5', VerseReference(BookId.JUDE, ChapterId(6), VerseId(5)))
        ]
        for encoded_value, expected_verse_reference in params:
            with self.subTest(encoded_value=encoded_value):
                # when: decoding a valid verse reference that has a single-chapter book
                # and: the chapter identifier is specified
                actual = _decode_verse_reference(encoded_value)

                # then: it should return the verse reference using the specified chapter identifier
                self.assertEqual(expected_verse_reference, actual)

    def test__when_encoded_value_has_single_chapter_book_without_chapter_id__should_use_default_chapter_id(self) -> None: # pylint: disable=line-too-long
        params = [
            ('Obad 2', VerseReference(BookId.OBADIAH, ChapterId(1), VerseId(2))),
            ('Phlm 3', VerseReference(BookId.PHILEMON, ChapterId(1), VerseId(3))),
            ('2 John 4', VerseReference(BookId.SECOND_JOHN, ChapterId(1), VerseId(4))),
            ('3 John 5', VerseReference(BookId.THIRD_JOHN, ChapterId(1), VerseId(5))),
            ('Jude 6', VerseReference(BookId.JUDE, ChapterId(1), VerseId(6)))
        ]
        for encoded_value, expected_verse_reference in params:
            with self.subTest(encoded_value=encoded_value):
                # when: decoding a valid verse reference that has a single-chapter book
                # and: the chapter identifier is unspecified
                actual = _decode_verse_reference(encoded_value)

                # then: it should return the verse reference using the default chapter identifier
                self.assertEqual(expected_verse_reference, actual)

    def test__when_encoded_value_has_multi_chapter_book_without_chapter_id__should_raise_exception(self) -> None: # pylint: disable=line-too-long
        # then: it should raise an exception indicating the verse reference is invalid
        with self.assertRaisesRegex(MarkdownDecodingError, 'valid encoded verse reference'):
            # when: decoding a verse reference that has a multi-chapter book
            # and: the chapter identifier is unspecified
            _decode_verse_reference('Matt 4')

class DecodeBookIdTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__when_value_valid__should_return_book_id(self) -> None:
        params = [
            ('Gen', BookId.GENESIS),
            ('Exod', BookId.EXODUS),
            ('Lev', BookId.LEVITICUS),
            ('Num', BookId.NUMBERS),
            ('Deut', BookId.DEUTERONOMY),
            ('Josh', BookId.JOSHUA),
            ('Judg', BookId.JUDGES),
            ('Ruth', BookId.RUTH),
            ('1 Sam', BookId.FIRST_SAMUEL),
            ('2 Sam', BookId.SECOND_SAMUEL),
            ('1 Kgs', BookId.FIRST_KINGS),
            ('2 Kgs', BookId.SECOND_KINGS),
            ('1 Chr', BookId.FIRST_CHRONICLES),
            ('2 Chr', BookId.SECOND_CHRONICLES),
            ('Ezra', BookId.EZRA),
            ('Neh', BookId.NEHEMIAH),
            ('Esth', BookId.ESTHER),
            ('Job', BookId.JOB),
            ('Ps', BookId.PSALMS),
            ('Prov', BookId.PROVERBS),
            ('Eccl', BookId.ECCLESIASTES),
            ('Song', BookId.SONG_OF_SONGS),
            ('Isa', BookId.ISAIAH),
            ('Jer', BookId.JEREMIAH),
            ('Lam', BookId.LAMENTATIONS),
            ('Ezek', BookId.EZEKIEL),
            ('Dan', BookId.DANIEL),
            ('Hos', BookId.HOSEA),
            ('Joel', BookId.JOEL),
            ('Amos', BookId.AMOS),
            ('Obad', BookId.OBADIAH),
            ('Jonah', BookId.JONAH),
            ('Mic', BookId.MICAH),
            ('Nah', BookId.NAHUM),
            ('Hab', BookId.HABAKKUK),
            ('Zeph', BookId.ZEPHANIAH),
            ('Hag', BookId.HAGGAI),
            ('Zech', BookId.ZECHARIAH),
            ('Mal', BookId.MALACHI),
            ('1 Esd', BookId.FIRST_ESDRAS),
            ('Matt', BookId.MATTHEW),
            ('Mark', BookId.MARK),
            ('Luke', BookId.LUKE),
            ('John', BookId.JOHN),
            ('Acts', BookId.ACTS),
            ('Rom', BookId.ROMANS),
            ('1 Cor', BookId.FIRST_CORINTHIANS),
            ('2 Cor', BookId.SECOND_CORINTHIANS),
            ('Gal', BookId.GALATIANS),
            ('Eph', BookId.EPHESIANS),
            ('Phil', BookId.PHILIPPIANS),
            ('Col', BookId.COLOSSIANS),
            ('1 Thess', BookId.FIRST_THESSALONIANS),
            ('2 Thess', BookId.SECOND_THESSALONIANS),
            ('1 Tim', BookId.FIRST_TIMOTHY),
            ('2 Tim', BookId.SECOND_TIMOTHY),
            ('Titus', BookId.TITUS),
            ('Phlm', BookId.PHILEMON),
            ('Heb', BookId.HEBREWS),
            ('Jas', BookId.JAMES),
            ('1 Pet', BookId.FIRST_PETER),
            ('2 Pet', BookId.SECOND_PETER),
            ('1 John', BookId.FIRST_JOHN),
            ('2 John', BookId.SECOND_JOHN),
            ('3 John', BookId.THIRD_JOHN),
            ('Jude', BookId.JUDE),
            ('Rev', BookId.REVELATION)
        ]
        for encoded_value, expected_book_id in params:
            with self.subTest(encoded_value=encoded_value):
                # when: decoding a valid book identifier
                actual = _decode_book_id(encoded_value)

                # then: it should return the book identifier
                self.assertEqual(expected_book_id, actual)

    def test__when_value_invalid__should_raise_exception(self) -> None:
        # then: it should raise an exception indicating the encoded value is invalid
        with self.assertRaisesRegex(MarkdownDecodingError, 'valid encoded book identifier'):
            # when: decoding an invalid book identifier
            _decode_book_id('UNKNOWN')

class DecodeChapterIdTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__should_return_chapter_id(self) -> None:
        params = [
            ('1', ChapterId(1)),
            ('9', ChapterId(9)),
            ('10', ChapterId(10)),
            ('99', ChapterId(99)),
            ('100', ChapterId(100)),
            ('1234567890', ChapterId(1234567890))
        ]
        for encoded_value, expected_chapter_id in params:
            with self.subTest(encoded_value=encoded_value):
                # when: decoding a chapter identifier
                actual = _decode_chapter_id(encoded_value)

                # then: it should return the chapter identifier
                self.assertEqual(expected_chapter_id, actual)

class DecodeVerseIdTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__should_return_verse_id(self) -> None:
        params = [
            ('0', VerseId(0)),
            ('1', VerseId(1)),
            ('9', VerseId(9)),
            ('10', VerseId(10)),
            ('99', VerseId(99)),
            ('100', VerseId(100)),
            ('1234567890', VerseId(1234567890))
        ]
        for encoded_value, expected_verse_id in params:
            with self.subTest(encoded_value=encoded_value):
                # when: decoding a verse identifier
                actual = _decode_verse_id(encoded_value)

                # then: it should return the verse identifier
                self.assertEqual(expected_verse_id, actual)

class DecodeVerseReferenceLinksTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__when_value_valid_and_contains_zero_links__should_return_value_unchanged(self) -> None:
        # when: decoding a value containing zero verse reference links
        actual = _decode_verse_reference_links('1 2')

        # then: it should return the value unchanged
        self.assertEqual('1 2', actual)

    def test__when_value_valid_and_contains_one_link__should_return_value_with_link_decoded(self) -> None: # pylint: disable=line-too-long
        # when: decoding a value containing one verse reference link
        actual = _decode_verse_reference_links('1 [Isaiah 40:6–8](vref:Isa 40:6-8) 2')

        # then: it should return the value with the link decoded
        self.assertEqual('1 [Isaiah 40:6–8](vref:ISAIAH 40:6-8) 2', actual)

    def test__when_value_valid_and_contains_two_links__should_return_value_with_links_decoded(self) -> None: # pylint: disable=line-too-long
        # when: decoding a value containing two verse reference links
        actual = _decode_verse_reference_links(
            '1 [Isaiah 40:6–8](vref:Isa 40:6-8) 2 [Job 2:1,2](vref:Job 2:1,2) 3'
        )

        # then: it should return the value with the links decoded
        self.assertEqual(
            '1 [Isaiah 40:6–8](vref:ISAIAH 40:6-8) 2 [Job 2:1,2](vref:JOB 2:1,2) 3',
            actual
        )

    def test__when_value_invalid__should_raise_exception(self) -> None:
        # then: it should raise an exception indicating the encoded value is invalid
        with self.assertRaisesRegex(
            MarkdownDecodingError,
            'valid encoded verse reference link target'
        ):
            # when: decoding a invalid verse reference link
            _decode_verse_reference_links('1 [Isaiah 40:6–8](vref: Isa 40:6-8) 2')

class DecodeVerseReferenceLinkTargetTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__when_value_invalid__should_raise_exception(self) -> None:
        # then: it should raise an exception indicating the encoded value is invalid
        with self.assertRaisesRegex(
            MarkdownDecodingError,
            'valid encoded verse reference link target'
        ):
            # when: decoding an invalid verse reference link target
            _decode_verse_reference_link_target(' Isa 40:6-8') # extra leading space

    def test__when_book_id_supported__should_decode_book_identifier(self) -> None:
        # when: decoding a verse reference link target containing a supported book identifier
        actual = _decode_verse_reference_link_target('Isa 40:6-8')

        # then: it should return the verse reference link target with the decoded book identifier
        self.assertEqual('ISAIAH 40:6-8', actual)

    def test__when_book_id_unsupported__should_raise_exception(self) -> None:
        # then: it should raise an exception indicating the book identifier is invalid
        with self.assertRaisesRegex(MarkdownDecodingError, 'valid encoded book identifier'):
            # when: decoding a verse reference link target containing an unsupported book identifier
            _decode_verse_reference_link_target('UNKNOWN 40:6-8')
