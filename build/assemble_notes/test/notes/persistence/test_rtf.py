#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# pylint: disable=missing-module-docstring

from io import StringIO
from pathlib import Path
from unittest import TestCase
from unittest.mock import Mock

from notes.model import BookId, ChapterId, MarkdownString, VerseId, VerseNote, VerseReference
from notes.persistence.rtf import (
    RtfEncodingError,
    write_rtf_verse_note,
    _encode_book_id,
    _encode_text,
    _encode_verse_reference_links,
    _get_output_file_name
)

class WriteRtfVerseNoteTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def setUp(self) -> None:
        self.verse_note = VerseNote(
            verse_reference=VerseReference(BookId.MATTHEW, ChapterId(4), VerseId(2)),
            text=MarkdownString('A verse **note**.')
        )
        self.output_folder_path = Path('/some-output-folder')

    def test__should_write_rtf_verse_note(self) -> None:
        # when: writing an RTF verse note
        output_stream = StringIO()
        output_stream.write = Mock()
        output_stream_factory = Mock(return_value=output_stream)
        write_rtf_verse_note(self.verse_note, self.output_folder_path, output_stream_factory)

        # then: it should create an output stream using the correct file name
        output_stream_factory.assert_called_with(self.output_folder_path.joinpath('Matt 4_2.rtf'))
        # and: it should write the RTF verse note
        self.assertRegex(
            output_stream.write.call_args[0][0],
            r'{\\pard .* A verse {\\b note}.\\par}'
        )

    def test__should_close_output_stream(self) -> None:
        # when: writing an RTF verse note
        output_stream = StringIO()
        output_stream.close = Mock()
        write_rtf_verse_note(self.verse_note, self.output_folder_path, lambda _: output_stream)

        # then: it should close the output stream
        output_stream.close.assert_called_once()

class GetOutputFileNameTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__should_return_file_name_with_encoded_verse_reference(self) -> None:
        # when: getting the output file name for a verse reference
        actual = _get_output_file_name(VerseReference(BookId.MATTHEW, ChapterId(4), VerseId(2)))

        # then: it should return the file name with the encoded verse reference
        self.assertEqual(Path('Matt 4_2.rtf'), actual)

class EncodeBookIdTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__when_book_id_supported__should_return_encoded_book_id(self) -> None:
        params = [
            (BookId.GENESIS, 'Gen'),
            (BookId.EXODUS, 'Ex'),
            (BookId.LEVITICUS, 'Lev'),
            (BookId.NUMBERS, 'Num'),
            (BookId.DEUTERONOMY, 'Deut'),
            (BookId.JOSHUA, 'Josh'),
            (BookId.JUDGES, 'Judg'),
            (BookId.RUTH, 'Ruth'),
            (BookId.FIRST_SAMUEL, '1Sam'),
            (BookId.SECOND_SAMUEL, '2Sam'),
            (BookId.FIRST_KINGS, '1Kings'),
            (BookId.SECOND_KINGS, '2Kings'),
            (BookId.FIRST_CHRONICLES, '1Chr'),
            (BookId.SECOND_CHRONICLES, '2Chr'),
            (BookId.EZRA, 'Ezra'),
            (BookId.NEHEMIAH, 'Neh'),
            (BookId.ESTHER, 'Esth'),
            (BookId.JOB, 'Job'),
            (BookId.PSALMS, 'Psa'),
            (BookId.PROVERBS, 'Prov'),
            (BookId.ECCLESIASTES, 'Eccl'),
            (BookId.SONG_OF_SONGS, 'Song'),
            (BookId.ISAIAH, 'Is'),
            (BookId.JEREMIAH, 'Jer'),
            (BookId.LAMENTATIONS, 'Lam'),
            (BookId.EZEKIEL, 'Ezek'),
            (BookId.DANIEL, 'Dan'),
            (BookId.HOSEA, 'Hos'),
            (BookId.JOEL, 'Joel'),
            (BookId.AMOS, 'Amos'),
            (BookId.OBADIAH, 'Obad'),
            (BookId.JONAH, 'Jonah'),
            (BookId.MICAH, 'Mic'),
            (BookId.NAHUM, 'Nah'),
            (BookId.HABAKKUK, 'Hab'),
            (BookId.ZEPHANIAH, 'Zeph'),
            (BookId.HAGGAI, 'Hag'),
            (BookId.ZECHARIAH, 'Zech'),
            (BookId.MALACHI, 'Mal'),
            (BookId.FIRST_ESDRAS, '1Esdr'),
            (BookId.MATTHEW, 'Matt'),
            (BookId.MARK, 'Mark'),
            (BookId.LUKE, 'Luke'),
            (BookId.JOHN, 'John'),
            (BookId.ACTS, 'Acts'),
            (BookId.ROMANS, 'Rom'),
            (BookId.FIRST_CORINTHIANS, '1Cor'),
            (BookId.SECOND_CORINTHIANS, '2Cor'),
            (BookId.GALATIANS, 'Gal'),
            (BookId.EPHESIANS, 'Eph'),
            (BookId.PHILIPPIANS, 'Phil'),
            (BookId.COLOSSIANS, 'Col'),
            (BookId.FIRST_THESSALONIANS, '1Th'),
            (BookId.SECOND_THESSALONIANS, '2Th'),
            (BookId.FIRST_TIMOTHY, '1Tim'),
            (BookId.SECOND_TIMOTHY, '2Tim'),
            (BookId.TITUS, 'Titus'),
            (BookId.PHILEMON, 'Philem'),
            (BookId.HEBREWS, 'Heb'),
            (BookId.JAMES, 'James'),
            (BookId.FIRST_PETER, '1Pet'),
            (BookId.SECOND_PETER, '2Pet'),
            (BookId.FIRST_JOHN, '1John'),
            (BookId.SECOND_JOHN, '2John'),
            (BookId.THIRD_JOHN, '3John'),
            (BookId.JUDE, 'Jude'),
            (BookId.REVELATION, 'Rev')
        ]
        for book_id, expected_file_name_component in params:
            with self.subTest(book_id=book_id):
                # when: encoding a supported book identifier
                actual = _encode_book_id(book_id)

                # then: it should return the encoded book identifier
                self.assertEqual(expected_file_name_component, actual)

    def test__when_book_id_unsupported__should_raise_exception(self) -> None:
        # then: it should raise an exception indicating the book identifier is unsupported
        with self.assertRaisesRegex(RtfEncodingError, 'supported book identifier'):
            # when: encoding an unsupported book identifier
            _encode_book_id(None)

class EncodeTextTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__should_return_rtf_document(self) -> None:
        # when: encoding verse note text
        actual = _encode_text(MarkdownString('This is a **note**.'))

        # then: it should return an RTF document
        self.assertRegex(actual, r'\A{\\rtf1\\ansi')
        self.assertRegex(actual, '}\n\\Z')
        # and: the document should contain the encoded verse note text
        self.assertRegex(actual, r'{\\pard .* This is a {\\b note}.\\par}')

    def test__should_encode_verse_reference_links(self) -> None:
        # when: encoding verse note text
        actual = _encode_text(MarkdownString(
            '[Link 1](vref:GENESIS 1:2) [Link 2](vref:EXODUS 3:4)'
        ))

        # then: it should encode the first verse reference link
        self.assertRegex(
            actual,
            r'{\\field{\\\*\\fldinst{HYPERLINK "accord://read/Gen_1:2"}}{\\fldrslt{\\ul\nLink 1\n}}}' # pylint: disable=line-too-long
        )
        # and: it should encode the second verse reference link
        self.assertRegex(
            actual,
            r'{\\field{\\\*\\fldinst{HYPERLINK "accord://read/Ex_3:4"}}{\\fldrslt{\\ul\nLink 2\n}}}'
        )

class EncodeVerseReferenceLinksTestCase(TestCase):
    # pylint: disable=missing-class-docstring, missing-function-docstring

    def test__when_book_id_supported__should_encode_verse_reference_link(self) -> None:
        # when: encoding a verse reference link containing a supported book identifier
        actual = _encode_verse_reference_links('[Isaiah 40:6–8](vref:ISAIAH 40:6-8)')

        # then: it should return the encoded verse reference link
        self.assertEqual('[Isaiah 40:6–8](accord://read/Is_40:6-8)', actual)

    def test__when_book_id_unsupported__should_raise_exception(self) -> None:
        # then: it should raise an exception indicating the book identifier is unsupported
        with self.assertRaisesRegex(RtfEncodingError, 'supported book identifier'):
            # when: encoding a verse reference link containing an unsupported book identifier
            _encode_verse_reference_links('[Isaiah 40:6–8](vref:UNKNOWN 40:6-8)')
