#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

'''
Assembles all Bible notes from a single Markdown file into a collection of RTF files importable by
Accordance.
'''

import argparse
import sys

from pathlib import Path
from typing import TextIO

from notes.persistence.markdown import read_markdown_verse_notes
from notes.persistence.rtf import write_rtf_verse_note

def _main(str_args: list[str]) -> None:
    '''
    Entry point for the application.

    Args:
        str_args: The command line arguments to the application.
    '''

    args = _parse_args(str_args)

    for verse_note in read_markdown_verse_notes(lambda: _open_input_file(args.input_file_path)):
        write_rtf_verse_note(verse_note, args.output_folder_path, _open_output_file)

def _parse_args(str_args: list[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input_file_path',
        type=Path,
        help='The path to the Markdown verse notes file to be used as input.'
    )
    parser.add_argument(
        'output_folder_path',
        type=Path,
        help=(
            'The path to the folder where all RTF verse note files will be output. '
            'This folder must exist prior to running the application.'
        )
    )
    return parser.parse_args(str_args)

def _open_input_file(input_file_path: Path) -> TextIO:
    return open(input_file_path, mode='rt', encoding='utf-8')

def _open_output_file(output_file_path: Path) -> TextIO:
    return open(output_file_path, mode='wt', encoding='utf-8')

if __name__ == '__main__':
    _main(sys.argv[1:])
