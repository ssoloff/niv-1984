#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

'''
The RTF persistence layer for verse note assembly.

This persistence layer encodes the domain as one RTF file per verse. Each file contains the note for
a single verse encoded as an RTF document. The name of the file identifies the verse reference. All
of the verse note files must reside in a single folder for proper import into Accordance.
'''

import re

from pathlib import Path
from typing import Callable, TextIO

import pypandoc

from notes.model import BookId, ChapterId, MarkdownString, VerseId, VerseNote, VerseReference

class RtfEncodingError(Exception):
    '''
    Raised when an error occurs while encoding a domain model value to the RTF persistence layer.
    '''

def write_rtf_verse_note(
    verse_note: VerseNote,
    output_folder_path: Path,
    output_stream_factory: Callable[[Path], TextIO]
) -> None:
    '''
    Writes the specified verse note as an RTF file in the specified folder.

    Args:
        verse_note: The verse note to write.
        output_folder_path: The path to the folder where the verse note file is to be written.
        output_stream_factory: A factory method that should create an empty file at the given path
            and return the corresponding output stream.

    Raises:
        RtfEncodingError: If an error occurs while encoding the verse note to the RTF persistence
            layer.
    '''

    output_file_path = output_folder_path.joinpath(
        _get_output_file_name(verse_note.verse_reference)
    )
    with output_stream_factory(output_file_path) as output_stream:
        output_stream.write(_encode_verse_note(verse_note))

def _get_output_file_name(verse_reference: VerseReference) -> Path:
    return Path(
        f'{_encode_book_id(verse_reference.book_id)}'
        ' '
        f'{_encode_chapter_id(verse_reference.chapter_id)}'
        '_'
        f'{_encode_verse_id(verse_reference.verse_id)}'
        '.rtf'
    )

_ENCODED_BOOK_IDS_BY_BOOK_ID = {
    # Accordance book abbreviations
    BookId.GENESIS: 'Gen',
    BookId.EXODUS: 'Ex',
    BookId.LEVITICUS: 'Lev',
    BookId.NUMBERS: 'Num',
    BookId.DEUTERONOMY: 'Deut',
    BookId.JOSHUA: 'Josh',
    BookId.JUDGES: 'Judg',
    BookId.RUTH: 'Ruth',
    BookId.FIRST_SAMUEL: '1Sam',
    BookId.SECOND_SAMUEL: '2Sam',
    BookId.FIRST_KINGS: '1Kings',
    BookId.SECOND_KINGS: '2Kings',
    BookId.FIRST_CHRONICLES: '1Chr',
    BookId.SECOND_CHRONICLES: '2Chr',
    BookId.EZRA: 'Ezra',
    BookId.NEHEMIAH: 'Neh',
    BookId.ESTHER: 'Esth',
    BookId.JOB: 'Job',
    BookId.PSALMS: 'Psa',
    BookId.PROVERBS: 'Prov',
    BookId.ECCLESIASTES: 'Eccl',
    BookId.SONG_OF_SONGS: 'Song',
    BookId.ISAIAH: 'Is',
    BookId.JEREMIAH: 'Jer',
    BookId.LAMENTATIONS: 'Lam',
    BookId.EZEKIEL: 'Ezek',
    BookId.DANIEL: 'Dan',
    BookId.HOSEA: 'Hos',
    BookId.JOEL: 'Joel',
    BookId.AMOS: 'Amos',
    BookId.OBADIAH: 'Obad',
    BookId.JONAH: 'Jonah',
    BookId.MICAH: 'Mic',
    BookId.NAHUM: 'Nah',
    BookId.HABAKKUK: 'Hab',
    BookId.ZEPHANIAH: 'Zeph',
    BookId.HAGGAI: 'Hag',
    BookId.ZECHARIAH: 'Zech',
    BookId.MALACHI: 'Mal',
    BookId.FIRST_ESDRAS: '1Esdr',
    BookId.MATTHEW: 'Matt',
    BookId.MARK: 'Mark',
    BookId.LUKE: 'Luke',
    BookId.JOHN: 'John',
    BookId.ACTS: 'Acts',
    BookId.ROMANS: 'Rom',
    BookId.FIRST_CORINTHIANS: '1Cor',
    BookId.SECOND_CORINTHIANS: '2Cor',
    BookId.GALATIANS: 'Gal',
    BookId.EPHESIANS: 'Eph',
    BookId.PHILIPPIANS: 'Phil',
    BookId.COLOSSIANS: 'Col',
    BookId.FIRST_THESSALONIANS: '1Th',
    BookId.SECOND_THESSALONIANS: '2Th',
    BookId.FIRST_TIMOTHY: '1Tim',
    BookId.SECOND_TIMOTHY: '2Tim',
    BookId.TITUS: 'Titus',
    BookId.PHILEMON: 'Philem',
    BookId.HEBREWS: 'Heb',
    BookId.JAMES: 'James',
    BookId.FIRST_PETER: '1Pet',
    BookId.SECOND_PETER: '2Pet',
    BookId.FIRST_JOHN: '1John',
    BookId.SECOND_JOHN: '2John',
    BookId.THIRD_JOHN: '3John',
    BookId.JUDE: 'Jude',
    BookId.REVELATION: 'Rev'
}

def _encode_book_id(book_id: BookId) -> str:
    encoded_book_id = _ENCODED_BOOK_IDS_BY_BOOK_ID.get(book_id)
    if encoded_book_id is None:
        raise RtfEncodingError(f'"{book_id}" is not a supported book identifier')
    return encoded_book_id

def _encode_chapter_id(chapter_id: ChapterId) -> str:
    return str(chapter_id.value)

def _encode_verse_id(verse_id: VerseId) -> str:
    return str(verse_id.value)

def _encode_verse_note(verse_note: VerseNote) -> str:
    return _encode_text(verse_note.text)

def _encode_text(text: MarkdownString) -> str:
    value = text.value
    value = _encode_verse_reference_links(value)
    return _convert_markdown_to_rtf(value)

_VERSE_REFERENCE_LINK_PATTERN = re.compile(
    r'(?<=\]\()vref:(?P<book_id>[A-Z_]+) (?P<ch_vv_ref>.+?)(?=\))'
)

def _encode_verse_reference_links(text: str) -> str:
    return _VERSE_REFERENCE_LINK_PATTERN.sub(
        lambda _: _encode_verse_reference_link(_.group('book_id'), _.group('ch_vv_ref')),
        text
    )

def _encode_verse_reference_link(book_id_name: str, ch_vv_ref: str) -> str:
    return f'accord://read/{_encode_book_id_for_name(book_id_name)}_{ch_vv_ref}'

def _encode_book_id_for_name(book_id_name: str) -> str:
    try:
        return _encode_book_id(BookId[book_id_name])
    except KeyError as ex:
        raise RtfEncodingError(f'"{book_id_name}" is not a supported book identifier') from ex

def _convert_markdown_to_rtf(value: str) -> str:
    return pypandoc.convert_text(value, to='rtf', format='markdown', extra_args=['--standalone'])
