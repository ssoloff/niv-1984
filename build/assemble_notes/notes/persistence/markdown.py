#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

'''
The Markdown persistence layer for verse note assembly.

This persistence layer encodes the domain within a single Markdown file that contains up to one note
per verse. Each note is identified by a level-1 heading ("#") that contains the verse reference. Any
Markdown text after the verse reference and before the next level-1 heading (or the end of the file)
is considered the note associated with the verse.

For example:

    # Matt. 4:2
    This is a note for Matthew 4:2.

    # Rom. 5:8
    This is a note for Romans 5:8.

    And this line will also be part of the note for Romans 5:8.

    # Rev. 10:1

    Finally, a note for Revelation 10:1 (the previous blank line is not included).
'''

import re

from collections.abc import Iterable
from typing import Callable, TextIO

from notes.model import BookId, ChapterId, MarkdownString, VerseId, VerseNote, VerseReference

class MarkdownDecodingError(Exception):
    '''
    Raised when an error occurs while decoding an encoded domain model value from the Markdown
    persistence layer.
    '''

def read_markdown_verse_notes(input_stream_factory: Callable[[], TextIO]) -> Iterable[VerseNote]:
    '''
    Reads one verse note at a time from the specified Markdown input stream until the end of the
    stream is reached. If the input stream contains no recognizable verse notes, an empty iterable
    will be returned.

    Args:
        input_stream_factory: A factory method that should return the Markdown input stream from
            which to read the verse notes.

    Raises:
        MarkdownDecodingError: If an invalid Markdown encoded verse note is encountered while
            reading the input stream.
    '''

    processed_verse_references = set()

    with input_stream_factory() as input_stream:
        for match in _VERSE_NOTE_PATTERN.finditer(input_stream.read()):
            verse_note = _decode_verse_note(match.group())
            if verse_note.verse_reference in processed_verse_references:
                raise MarkdownDecodingError(
                    f'Duplicate verse reference "{verse_note.verse_reference}" detected'
                )
            processed_verse_references.add(verse_note.verse_reference)
            yield verse_note

_VERSE_NOTE_REGEX = r'#\s+(?P<verse_reference>.+?)\n(?P<text>.+?)(?=#|\Z)'
_VERSE_NOTE_PATTERN = re.compile(_VERSE_NOTE_REGEX, re.DOTALL | re.MULTILINE)

def _decode_verse_note(encoded_value: str) -> VerseNote:
    match = _VERSE_NOTE_PATTERN.fullmatch(encoded_value)
    assert match is not None
    return VerseNote(
        verse_reference=_decode_verse_reference(match.group('verse_reference')),
        text=_decode_text(match.group('text'))
    )

_BOOK_ID_REGEX = r'(?:\d )?\w+'
_CHAPTER_ID_REGEX = r'[1-9]\d*'
_VERSE_ID_REGEX = r'\d+'
_VERSE_REFERENCE_REGEX = (
    f'(?P<book_id>{_BOOK_ID_REGEX})'
    ' '
    f'(?:(?P<chapter_id>{_CHAPTER_ID_REGEX}):)?'
    f'(?P<verse_id>{_VERSE_ID_REGEX})'
)
_VERSE_REFERENCE_PATTERN = re.compile(_VERSE_REFERENCE_REGEX)

_SINGLE_CHAPTER_BOOK_IDS = {
    BookId.OBADIAH,
    BookId.PHILEMON,
    BookId.SECOND_JOHN,
    BookId.THIRD_JOHN,
    BookId.JUDE
}

def _decode_verse_reference(encoded_value: str) -> VerseReference:
    match = _VERSE_REFERENCE_PATTERN.fullmatch(encoded_value)
    if match is None:
        raise MarkdownDecodingError(
            f'Expected valid encoded verse reference but was "{encoded_value}"'
        )

    book_id = _decode_book_id(match.group('book_id'))

    chapter_id_match = match.group('chapter_id')
    if chapter_id_match is not None:
        chapter_id = _decode_chapter_id(chapter_id_match)
    else:
        if book_id not in _SINGLE_CHAPTER_BOOK_IDS:
            raise MarkdownDecodingError(
                f'Expected valid encoded verse reference but was "{encoded_value}"'
            )
        chapter_id = ChapterId(1)

    verse_id = _decode_verse_id(match.group('verse_id'))

    return VerseReference(book_id=book_id, chapter_id=chapter_id, verse_id=verse_id)

_BOOK_IDS_BY_ENCODED_BOOK_ID = {
    # SBL book abbreviations
    'Gen': BookId.GENESIS,
    'Exod': BookId.EXODUS,
    'Lev': BookId.LEVITICUS,
    'Num': BookId.NUMBERS,
    'Deut': BookId.DEUTERONOMY,
    'Josh': BookId.JOSHUA,
    'Judg': BookId.JUDGES,
    'Ruth': BookId.RUTH,
    '1 Sam': BookId.FIRST_SAMUEL,
    '2 Sam': BookId.SECOND_SAMUEL,
    '1 Kgs': BookId.FIRST_KINGS,
    '2 Kgs': BookId.SECOND_KINGS,
    '1 Chr': BookId.FIRST_CHRONICLES,
    '2 Chr': BookId.SECOND_CHRONICLES,
    'Ezra': BookId.EZRA,
    'Neh': BookId.NEHEMIAH,
    'Esth': BookId.ESTHER,
    'Job': BookId.JOB,
    'Ps': BookId.PSALMS,
    'Prov': BookId.PROVERBS,
    'Eccl': BookId.ECCLESIASTES,
    'Song': BookId.SONG_OF_SONGS,
    'Isa': BookId.ISAIAH,
    'Jer': BookId.JEREMIAH,
    'Lam': BookId.LAMENTATIONS,
    'Ezek': BookId.EZEKIEL,
    'Dan': BookId.DANIEL,
    'Hos': BookId.HOSEA,
    'Joel': BookId.JOEL,
    'Amos': BookId.AMOS,
    'Obad': BookId.OBADIAH,
    'Jonah': BookId.JONAH,
    'Mic': BookId.MICAH,
    'Nah': BookId.NAHUM,
    'Hab': BookId.HABAKKUK,
    'Zeph': BookId.ZEPHANIAH,
    'Hag': BookId.HAGGAI,
    'Zech': BookId.ZECHARIAH,
    'Mal': BookId.MALACHI,
    '1 Esd': BookId.FIRST_ESDRAS,
    'Matt': BookId.MATTHEW,
    'Mark': BookId.MARK,
    'Luke': BookId.LUKE,
    'John': BookId.JOHN,
    'Acts': BookId.ACTS,
    'Rom': BookId.ROMANS,
    '1 Cor': BookId.FIRST_CORINTHIANS,
    '2 Cor': BookId.SECOND_CORINTHIANS,
    'Gal': BookId.GALATIANS,
    'Eph': BookId.EPHESIANS,
    'Phil': BookId.PHILIPPIANS,
    'Col': BookId.COLOSSIANS,
    '1 Thess': BookId.FIRST_THESSALONIANS,
    '2 Thess': BookId.SECOND_THESSALONIANS,
    '1 Tim': BookId.FIRST_TIMOTHY,
    '2 Tim': BookId.SECOND_TIMOTHY,
    'Titus': BookId.TITUS,
    'Phlm': BookId.PHILEMON,
    'Heb': BookId.HEBREWS,
    'Jas': BookId.JAMES,
    '1 Pet': BookId.FIRST_PETER,
    '2 Pet': BookId.SECOND_PETER,
    '1 John': BookId.FIRST_JOHN,
    '2 John': BookId.SECOND_JOHN,
    '3 John': BookId.THIRD_JOHN,
    'Jude': BookId.JUDE,
    'Rev': BookId.REVELATION
}

def _decode_book_id(encoded_value: str) -> BookId:
    book_id = _BOOK_IDS_BY_ENCODED_BOOK_ID.get(encoded_value)
    if book_id is None:
        raise MarkdownDecodingError(
            f'Expected valid encoded book identifier but was "{encoded_value}"'
        )
    return book_id

def _decode_chapter_id(encoded_value: str) -> ChapterId:
    assert re.fullmatch(_CHAPTER_ID_REGEX, encoded_value) is not None
    return ChapterId(int(encoded_value))

def _decode_verse_id(encoded_value: str) -> VerseId:
    assert re.fullmatch(_VERSE_ID_REGEX, encoded_value) is not None
    return VerseId(int(encoded_value))

def _decode_text(text: str) -> MarkdownString:
    text = text.strip()
    text = _decode_verse_reference_links(text)
    return MarkdownString(text)

_VERSE_REFERENCE_LINK_PATTERN = re.compile(
    r'(?<=\]\(vref:)(?P<verse_reference_link_target>.+?)(?=\))'
)

def _decode_verse_reference_links(text: str) -> str:
    return _VERSE_REFERENCE_LINK_PATTERN.sub(
        lambda _: _decode_verse_reference_link_target(_.group('verse_reference_link_target')),
        text
    )

_VERSE_REFERENCE_LINK_TARGET_PATTERN = re.compile(fr'(?P<book_id>{_BOOK_ID_REGEX})(?P<rest>.+)')

def _decode_verse_reference_link_target(encoded_value: str) -> str:
    match = _VERSE_REFERENCE_LINK_TARGET_PATTERN.fullmatch(encoded_value)
    if match is None:
        raise MarkdownDecodingError(
            f'Expected valid encoded verse reference link target but was "{encoded_value}"'
        )

    return _decode_book_id(match.group('book_id')).name + match.group('rest')
