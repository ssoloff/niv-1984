#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

'''The domain model for the application.'''

from dataclasses import dataclass
from enum import Enum, auto, unique

@unique
class BookId(Enum):
    '''Identifies a single book of the Bible.'''

    GENESIS = auto()
    EXODUS = auto()
    LEVITICUS = auto()
    NUMBERS = auto()
    DEUTERONOMY = auto()
    JOSHUA = auto()
    JUDGES = auto()
    RUTH = auto()
    FIRST_SAMUEL = auto()
    SECOND_SAMUEL = auto()
    FIRST_KINGS = auto()
    SECOND_KINGS = auto()
    FIRST_CHRONICLES = auto()
    SECOND_CHRONICLES = auto()
    EZRA = auto()
    NEHEMIAH = auto()
    ESTHER = auto()
    JOB = auto()
    PSALMS = auto()
    PROVERBS = auto()
    ECCLESIASTES = auto()
    SONG_OF_SONGS = auto()
    ISAIAH = auto()
    JEREMIAH = auto()
    LAMENTATIONS = auto()
    EZEKIEL = auto()
    DANIEL = auto()
    HOSEA = auto()
    JOEL = auto()
    AMOS = auto()
    OBADIAH = auto()
    JONAH = auto()
    MICAH = auto()
    NAHUM = auto()
    HABAKKUK = auto()
    ZEPHANIAH = auto()
    HAGGAI = auto()
    ZECHARIAH = auto()
    MALACHI = auto()
    FIRST_ESDRAS = auto()
    MATTHEW = auto()
    MARK = auto()
    LUKE = auto()
    JOHN = auto()
    ACTS = auto()
    ROMANS = auto()
    FIRST_CORINTHIANS = auto()
    SECOND_CORINTHIANS = auto()
    GALATIANS = auto()
    EPHESIANS = auto()
    PHILIPPIANS = auto()
    COLOSSIANS = auto()
    FIRST_THESSALONIANS = auto()
    SECOND_THESSALONIANS = auto()
    FIRST_TIMOTHY = auto()
    SECOND_TIMOTHY = auto()
    TITUS = auto()
    PHILEMON = auto()
    HEBREWS = auto()
    JAMES = auto()
    FIRST_PETER = auto()
    SECOND_PETER = auto()
    FIRST_JOHN = auto()
    SECOND_JOHN = auto()
    THIRD_JOHN = auto()
    JUDE = auto()
    REVELATION = auto()

@dataclass(frozen=True)
class ChapterId:
    '''Identifies a single chapter within a book.'''

    value: int

@dataclass(frozen=True)
class VerseId:
    '''Identifies a single verse within a chapter.'''

    value: int

@dataclass(frozen=True)
class VerseReference:
    '''An absolute reference to a single verse within a chapter within a book.'''

    book_id: BookId
    chapter_id: ChapterId
    verse_id: VerseId

@dataclass(frozen=True)
class MarkdownString:
    '''A string containing Markdown.'''

    value: str

@dataclass(frozen=True)
class VerseNote:
    '''A note for a single verse.'''

    text: MarkdownString
    verse_reference: VerseReference
