#!/usr/bin/env bash
#
# Copyright © 2022-2025 Steven Soloff
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# Current release version.
declare -rx VERSION="1.0.1"

# Paths to files and folders used during the build.  All paths are relative to the repository root unless otherwise
# noted.
REPO_ROOT_FOLDER=$(pwd)
declare -rx BUILD_OUTPUT_FOLDER="${REPO_ROOT_FOLDER}/.build"
declare -rx BUILD_ARTIFACTS_FOLDER="${BUILD_OUTPUT_FOLDER}/artifacts"
declare -rx DISTRO_ARTIFACT_FILE="${BUILD_ARTIFACTS_FOLDER}/niv-1984-${VERSION}.zip"
declare -rx BUILD_ASSEMBLY_FOLDER="${BUILD_OUTPUT_FOLDER}/out"
declare -rx FRONT_BACK_MATTER_ASSEMBLY_FILE="${BUILD_ASSEMBLY_FOLDER}/NIV (1984) Front-Back Matter.html"
declare -rx NOTES_ASSEMBLY_FOLDER="${BUILD_ASSEMBLY_FOLDER}/notes"
declare -rx README_ASSEMBLY_FILE="${BUILD_ASSEMBLY_FOLDER}/README.md"
declare -rx TEXT_ASSEMBLY_FILE="${BUILD_ASSEMBLY_FOLDER}/NIV (1984).txt"
declare -rx BUILD_SRC_FOLDER="${REPO_ROOT_FOLDER}/build"
declare -rx ASSEMBLE_NOTES_SRC_FOLDER="${BUILD_SRC_FOLDER}/assemble_notes"
declare -rx SRC_FOLDER="${REPO_ROOT_FOLDER}/src"
declare -rx FRONT_BACK_MATTER_SRC_FILE="${SRC_FOLDER}/front-back-matter.html"
declare -rx NOTES_SRC_FILE="${SRC_FOLDER}/notes.md"
declare -rx README_SRC_FILE="${SRC_FOLDER}/README.md"
declare -rx TEXT_SRC_FILE="${SRC_FOLDER}/text.txt"

# Status indicating successful execution.
declare -rx STATUS_SUCCESS=0

# Status indicating unsuccessful execution.
declare -rx STATUS_FAILURE=1

# The composite status of all build steps in the build script.
compositeStatus="${STATUS_SUCCESS}"

# Called by a build step to indicate it is complete.
#
# 1 - A brief description of the build step.
# 2 - Zero if the build step succeeded; or non-zero if the build step failed.
function endBuildStep() {
    local -r reset="\e[0m"
    local -r green="\e[32m"
    local -r red="\e[31m"
    local -r buildStepSuccessCondition="$1"
    local -r buildStepStatus="$2"

    if [[ "${buildStepStatus}" -eq "${STATUS_SUCCESS}" ]]; then
        echo -e "${green}✔${reset} ${buildStepSuccessCondition}"
    else
        compositeStatus="${STATUS_FAILURE}"
        echo -e "${red}✘${reset} ${buildStepSuccessCondition}"
    fi
}

# Runs the specified build steps sequentially.  All build steps will be run even if any prior script fails.
#
# @ - Each argument represents a build step that is executed sequentially without any arguments.
function runBuildSteps() {
    for buildStep in "$@"; do
        ${buildStep}
    done
    exit "${compositeStatus}"
}
