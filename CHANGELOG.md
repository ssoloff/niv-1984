# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/ssoloff/niv-1984/-/compare/1.0.1...HEAD)

No significant changes.

## [1.0.1](https://gitlab.com/ssoloff/niv-1984/-/compare/1.0.0...1.0.1) - 2025-01-08

### Fixed

* fix: Add missing supplied word markers (!19)
* fix: Add missing vocative "O" (!18)
* fix: Change "quietened" to "quieted" (!16)
* fix: Add missing comma in Isaiah 33:20 (!15)
* fix: Remove superfluous line breaks in Ezra 2:20, 35 (!14)
* fix: Change "to and fro" to "back and forth" (!13)
* fix: Remove superfluous line break in Job 7:10 (!12)
* fix: Change "stiff1088 necked" to "stiff-necked" in 2 Chronicles 36:13 (!11)
* fix: Change "worth while" to "worthwhile" (!10)
* fix: Change "caused havoc" to "raised havoc" in Acts 9:21 (!9)
* fix: Change "sheep-shearer" to "sheepshearer" (!8)
* fix: Change "to hand" to "on hand" in 1 Samuel 21:3-4 (!7)
* fix: Remove superfluous paragraph break in Joshua 12:24b (!5)
* fix: Change "warder" to "warden" (!3)
* fix: Remove superfluous line break in Proverbs 3:4 (!1)

## [1.0.0](https://gitlab.com/ssoloff/niv-1984/-/commits/1.0.0) - 2023-09-05

Initial release.
